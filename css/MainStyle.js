import React, {StyleSheet} from 'react-native'

export default StyleSheet.create({
  flex: {
    flex: 1,
  },
  rowFlex: {
    flexDirection: 'row'
  },
  columnFlex: {
    flexDirection: 'column'
  },
  fitScreen: {
    width: '100%',
    height: '100%'
  },
});
