// Selection Popup Types
export const Occupations = "occupations";
export const Vacancies = "Vacancies";
export const Province = "Province";
export const City = "City";
export const Gender = "Gender";
export const AreaSize = "AreaSize";
export const Specialization = "Specialization";
export const ContactUsSubjects = "ContactUsSubjects";
export const Salesman = "Salesman";

//Products Types
export const Pesticide = "pesticide";
export const Seeds = "seeds";
export const Fertilizer = "fertilizer";

//Product Details Types
export const TypeText = "text";
export const TypeImage = "image";
export const TypeFile = "file";

//Facebook Events
export const ProductPage = "Product_Page";
export const AboutUsPage = "AboutUs_Page";
export const LatestNewsPage = "Latest_News_Page";
export const NotificationsPage = "Notifications_Page";
export const CareersPage = "Careers_Page";
export const ContactUsPage = "ContactUs_Page";
export const WeatherPage = "Weather_Page";
export const FertilizationGuidePage = "Fertilization_Page";
export const ShareAction = "Share_Action";
export const ProfilePage = "Profile_Page";
export const MainCategory = "MainCategory";
export const MainCareer = "MainCareer";
export const Porduct = "Porduct";
export const PorductDetails = "Porduct_Details";
export const NotificationOpened = "Notification_Opened";
export const DeleteAccount = "Delete_Account";
export const SocialMedia = "Social_Media";
export const UpdateInterests = "UpdateInterests";

// Weather 
export const WeatherAppId = "299666f17923c2a7dbff16b9eaf41abe";
