export const BaseUrl = "http://chaccouragri.pixelperfect.tech/api/web/v1/"; 
///staging: chaccouragri.pixelperfect.tech ///production: chaccour-agri.info

//Meta Data
export const MetaData = "users/get-meta-data";

//User
export const Signup = "users/signup";
export const Verify = "users/validate-code";
export const Resend = "users/resend-code";
export const Login = "users/signin"
export const GetProfile = "users/get-profile"
export const EditProfile = "users/edit-profile"
export const FBLogin = "users/signin-via-facebook"
export const ForgetPasswordRequest = "users/forgot-password-request"
export const ForgetPassword = "users/forgot-password"
export const ChangePassword = "users/change-password"
export const DeleteUser = "users/delete-user"
export const SalesmanCallLog = "users/salesmanlog"
export const Share = "users/share-counter" 
export const SetNotificationToken = "users/set-notification-token" 
export const UnSetNotificationToken = "users/unset-notification-token" 
export const GetNotifications = "users/get-notifications" 


//Products
export const ProductsCategory = "products/get-categories";
export const ProductsCategoryDetails = "products/get-categories-products";
export const ProductDetails = "products/get-product";
export const GetAllFertilizerGuides = "products/get-all-fertilizer-guides"
export const FertilizDetails = "products/get-fertilizer-guide";

//Career
export const Career = "products/career";

//ContactUs
export const ContactUs = "contact-us/contact-us";

//Facebook Posts
export const PostFromCms = "users/posts"
export const ClientId = "428105137684472"
export const ClientSecret = "7f19830869f287d0c55237034c27fd36"
export const BaseUrlAccessTokenFb = "https://graph.facebook.com/oauth/access_token?";
export const BaseUrlGraphFb = "https://graph.facebook.com/402467856503477/posts?";

// Weather 

export const BaseUrlWeather = "https://api.openweathermap.org/data/2.5/forecast";
