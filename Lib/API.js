import axios from 'axios';
import RNFetchBlob from "react-native-fetch-blob";
import { LoginManager, AccessToken, GraphRequest, GraphRequestManager } from 'react-native-fbsdk';
import DeviceInfo from 'react-native-device-info';

import * as Constants from "../Config/constants"
import * as AppConstants from "../Config/AppConstants"


axios.defaults.baseURL = Constants.BaseUrl
axios.defaults.timeout = 30000 // 30 seconds

function getErrorMessageFromErrorCode(error) {
  if (error) {
    try {
      if (error.response.data) {
        const status = error.response.data.status
        const errorCode = error.response.data.code

        if (status == 400) {
          switch (errorCode) {
            case 1:
              return { errorMessage: "الرجاء التحقق من المعلومات المدخلة", shouldRetry: false, shouldVerify: false, shouldLogout: false }
              break;
            case 2:
              return { errorMessage: "رقم الجوال او كلمة المرور خاطئة. الرجاء التحقق منهما", shouldRetry: false, shouldVerify: false, shouldLogout: false }
              break;
            case 3:
              return { errorMessage: "رمز التفعيل غير صحيح. الرجاء التحقق من الرمز المرسل", shouldRetry: false, shouldVerify: false, shouldLogout: false }
              break;
            case 4:
              return { errorMessage: "كلمة المرور القديمة غير صحيحة. الرجاء التحقق منها", shouldRetry: false, shouldVerify: false, shouldLogout: false }
              break;
            case 6:
              return { errorMessage: "لا يوجد لديك صلاحية لاتمام هذه العملية", shouldRetry: false, shouldVerify: false, shouldLogout: true }
              break;
            case 7:
              return { errorMessage: "الرجاء تأكيد الحساب لاتمام هذه العملية", shouldRetry: false, shouldVerify: true, shouldLogout: false }
              break;
            case 8:
              return { errorMessage: "النوع المطلوب غير موجود", shouldRetry: false, shouldVerify: false, shouldLogout: false }
              break;
            case 9:
              return { errorMessage: "هذا الرقم مستخدم من قبل. الرجاء ادخال رقم آخر", shouldRetry: false, shouldVerify: false, shouldLogout: false }
              break;
            case 10:
              return { errorMessage: "هذا الرقم مستخدم من قبل. الرجاء ادخال رقم آخر", shouldRetry: false, shouldVerify: false, shouldLogout: false }
              break;
            case 11:
              return { errorMessage: "لا يمكنك إعادة ضبط كلمة المرور لحساب فيسبوك", shouldRetry: false, shouldVerify: false, shouldLogout: false }
              break;
            case 12:
              return { errorMessage: "لقد تجاوزت الحد المسموح لإرسال الرسائل النّصية. الرجاء إعادة المحاولة لاحقاً", shouldRetry: false, shouldVerify: false, shouldLogout: false }
              break;
            default:
              return { errorMessage: "حدث خطأ في الاتصال. الرجاء إعادة المحاولة", shouldRetry: true, shouldVerify: false, shouldLogout: false }
              break;
          }
        } else if (status == 401) {
          switch (errorCode) {
            case 5:
              return { errorMessage: "لا يوجد لديك صلاحية لاتمام هذه العملية", shouldRetry: false, shouldVerify: false, shouldLogout: true }
              break;
            case 1:
              return { errorMessage: "عذراً المنتج غير متواجد", shouldRetry: false, shouldVerify: false, shouldLogout: false }
              break;
            case 2:
              return { errorMessage: "لا يوجد هذا النوع من البذور", shouldRetry: false, shouldVerify: false, shouldLogout: false }
              break;
            case 3:
              return { errorMessage: "لا يوجد هذا النوع من السماد", shouldRetry: false, shouldVerify: false, shouldLogout: false }
              break;
            case 4:
              return { errorMessage: "لا يوجد هذا النوع من المبيدات", shouldRetry: false, shouldVerify: false, shouldLogout: false }
              break;
            case 6:
              return { errorMessage: "لا يوجد مستخدم لهذا الرقم", shouldRetry: false, shouldVerify: false, shouldLogout: true }
              break;
            case 7:
              return { errorMessage: "لا يوجد مستخدم لهذا الرقم", shouldRetry: false, shouldVerify: false, shouldLogout: false }
              break;
            case 8:
              //This is for Facebook if user not found to continue to register
              return -1
              break;
            default:
              return { errorMessage: "حدث خطأ في الاتصال. الرجاء إعادة المحاولة", shouldRetry: true, shouldVerify: false, shouldLogout: false }
              break;
          }
        } else {
          return { errorMessage: "حدث خطأ في الاتصال. الرجاء إعادة المحاولة", shouldRetry: true, shouldVerify: false, shouldLogout: false }
        }
      } else {
        return { errorMessage: "حدث خطأ في الاتصال. الرجاء إعادة المحاولة", shouldRetry: true, shouldVerify: false, shouldLogout: false }
      }
    } catch (e) {
      return { errorMessage: "حدث خطأ في الاتصال. الرجاء إعادة المحاولة", shouldRetry: true, shouldVerify: false, shouldLogout: false }
    }
  } else {
    return { errorMessage: "حدث خطأ في الاتصال. الرجاء إعادة المحاولة", shouldRetry: true, shouldVerify: false, shouldLogout: false }
  }
}

export function getMetaData(callback) {
  const instance = axios.create({
    baseURL: Constants.BaseUrl,
    timeout: 30000,
    headers: { 'Content-Type': "application/json" }
  });

  instance.post(Constants.MetaData, {})
    .then(function (response) {
      if (response.data) {
        if (response.data.data) {
          let data = response.data.data
          callback(true, data, null)

        } else {
          callback(false, null, getErrorMessageFromErrorCode(null))
        }
      }
    })
    .catch(function (error) {
      callback(false, null, getErrorMessageFromErrorCode(error))
    });
}

export function signup(photo, firstName, lastName, occupationId, mobile, provinceId, cityId, genderId, birthdate, password, fbData, areaSize, specialization, callback) {

  var params = {
    "first_name": firstName,
    "last_name": lastName,
    "province": provinceId,
    "city": cityId,
    "gender": genderId,
    "birthdate": birthdate,
    "occupation": occupationId,
    "phone_number": mobile,
    "password": password
  }
  if (photo) {
    params["photo"] = photo
  }
  if (fbData) {
    params["facebook_token"] = fbData.fbToken
    params["facebook_id"] = fbData.fbId
  }
  if (areaSize) {
    params["area_size"] = areaSize
  }
  if (specialization) {
    params["specialization"] = specialization
  }

  axios.post(Constants.Signup, params
  )
    .then(function (response) {
      if (response.data) {
        if (response.data.code == 0) {
          callback(true, null, null)
        } else {
          callback(false, null, getErrorMessageFromErrorCode(null))
        }
      }
    })
    .catch(function (error) {
      callback(false, null, getErrorMessageFromErrorCode(error))
    });
}

export function verifyAccount(code, number, callback) {
  const instance = axios.create({
    baseURL: Constants.BaseUrl,
    timeout: 30000,
    headers: { 'Content-Type': "application/json" }
  });

  instance.post(Constants.Verify, { code: code, phone: number })
    .then(function (response) {
      if (response.data) {
        if (response.data.code == 0) {
          callback(true, null, null)
        } else {
          callback(false, null, getErrorMessageFromErrorCode(null))
        }
      }
    })
    .catch(function (error) {
      callback(false, null, getErrorMessageFromErrorCode(error))
    });
}

export function resendCode(number, callback) {
  const instance = axios.create({
    baseURL: Constants.BaseUrl,
    timeout: 30000,
    headers: { 'Content-Type': "application/json" }
  });

  instance.post(Constants.Resend, { phone: number })
    .then(function (response) {
      if (response.data) {
        if (response.data.code == 0) {
          callback(true, null, null)
        } else {
          callback(false, null, getErrorMessageFromErrorCode(null))
        }
      }
    })
    .catch(function (error) {
      callback(false, null, getErrorMessageFromErrorCode(error))
    });
}

export function login(mobile, password, callback) {

  axios.post(Constants.Login, {
    phone_number: mobile,
    password: password
  })
    .then(function (response) {
      if (response.data) {
        if (response.data.data.user) {
          let user = response.data.data.user
          callback(true, user, null)
        } else {
          callback(false, null, getErrorMessageFromErrorCode(null))
        }
      }
    })
    .catch(function (error) {
      callback(false, null, getErrorMessageFromErrorCode(error))
    });
}

export function getProfile(token, callback) {

  const instance = axios.create({
    baseURL: Constants.BaseUrl,
    timeout: 30000,
    headers: { 'Content-Type': "application/json", 'token': token }
  });

  instance.post(Constants.GetProfile, {})
    .then(function (response) {
      if (response.data) {
        if (response.data.data.user) {
          let user = response.data.data.user
          callback(true, user, null)
        } else {
          callback(false, null, getErrorMessageFromErrorCode(null))
        }
      }
    })
    .catch(function (error) {
      callback(false, null, getErrorMessageFromErrorCode(error))
    });
}

export function editProfile(token, firstName, lastName, occupation, province, city, gender, birthdate, photo, interests, areaSize, specialization, callback) {

  const instance = axios.create({
    baseURL: Constants.BaseUrl,
    timeout: 30000,
    headers: { 'Content-Type': "application/json", 'token': token }
  });

  var params = {}

  if (firstName) {
    params["first_name"] = firstName
  }
  if (lastName) {
    params["last_name"] = lastName
  }
  if (occupation) {
    params["occupation"] = occupation
  }
  if (province) {
    params["province"] = province
  }
  if (city) {
    params["city"] = city
  }
  if (gender) {
    params["gender"] = gender
  }
  if (birthdate) {
    params["birthdate"] = birthdate
  }
  if (interests) {
    params["interests"] = interests
  }
  if (photo) {
    params["photo"] = photo
  }

  params["area_size"] = areaSize
  params["specialization"] = specialization
  params["app_version"] = DeviceInfo.getVersion()


  instance.post(Constants.EditProfile, params)
    .then(function (response) {
      if (response.data) {
        if (response.data.data.user) {
          let user = response.data.data.user
          callback(true, user, null)
        } else {
          callback(false, null, getErrorMessageFromErrorCode(null))
        }
      }
    })
    .catch(function (error) {
      callback(false, null, getErrorMessageFromErrorCode(error))
    });
}

export function loginViaFacebook(callback) {
  LoginManager.logOut();
  LoginManager.logInWithPermissions(['public_profile'])
    .then((result) => {
      if (result.isCancelled) {
        callback(true, null, 0)
      } else {
        AccessToken.getCurrentAccessToken()
          .then((tokenObj) => {
            return tokenObj
          })
          .then((tokenObj) => {
            let fbToken = tokenObj.accessToken
            let fbId = tokenObj.userID

            const responseInfoCallback = (error, userData) => {
              if (error) {
                callback(false, null, getErrorMessageFromErrorCode(null))
              } else {
                const instance = axios.create({
                  baseURL: Constants.BaseUrl,
                  timeout: 30000,
                  headers: { 'Content-Type': "application/json" }
                });

                instance.post(Constants.FBLogin, { facebook_token: fbToken, facebook_id: fbId })
                  .then(function (response) {
                    if (response.data) {
                      if (response.data.data.user) {
                        let user = response.data.data.user
                        callback(true, user, null)
                      } else {
                        let jsonData = { fbToken: fbToken, fbId: fbId, mobile: "" }
                        callback(false, jsonData, getErrorMessageFromErrorCode(null))
                      }
                    }
                  })
                  .catch(function (error) {
                    let jsonData = { fbToken: fbToken, fbId: fbId, mobile: "" }
                    jsonData["firstName"] = userData.first_name
                    jsonData["lastName"] = userData.last_name
                    if (userData.picture) {
                      if (userData.picture.data) {
                        if (userData.picture.data.url) {
                          jsonData["profilePic"] = { uri: userData.picture.data.url }
                          const fs = RNFetchBlob.fs;
                          let imagePath = null;
                          RNFetchBlob.config({
                            fileCache: false
                          })
                            .fetch("GET", userData.picture.data.url)
                            .then(resp => {
                              let base64Str = resp.base64()
                              jsonData["profilePic64"] = base64Str

                              if (error.response.data) {
                                if (error.response.data.data) {
                                  let mobile = error.response.data.data.phone_number
                                  jsonData.mobile = mobile
                                }
                              }
                              callback(false, jsonData, getErrorMessageFromErrorCode(error))
                            })
                        }
                      }
                    } else {
                      if (error.response.data) {
                        if (error.response.data.data) {
                          let mobile = error.response.data.data.phone_number
                          jsonData.mobile = mobile
                        }
                      }
                      callback(false, jsonData, getErrorMessageFromErrorCode(error))
                    }
                  });
              }
            }

            const infoRequest = new GraphRequest('/me', {
              accessToken: fbToken,
              parameters: {
                fields: {
                  string: 'first_name,last_name,picture.width(400)',
                }
              }
            }, responseInfoCallback);
            new GraphRequestManager().addRequest(infoRequest).start()
          })
      }
    })
    .catch((e) => {
      callback(false, null, getErrorMessageFromErrorCode(null))
    })
}

export function loginViaFacebookId(facebookId, callback) {

  const instance = axios.create({
    baseURL: Constants.BaseUrl,
    timeout: 30000,
    headers: { 'Content-Type': "application/json" }
  });

  instance.post(Constants.FBLogin, { facebook_id: facebookId })
    .then(function (response) {
      if (response.data) {
        if (response.data.data.user) {
          let user = response.data.data.user
          callback(true, user, null)
        } else {
          let jsonData = { fbId: facebookId, mobile: "" }
          callback(false, jsonData, getErrorMessageFromErrorCode(null))
        }
      }
    })
    .catch(function (error) {
      let jsonData = { fbId: facebookId, mobile: "" }
      if (error.response.data) {
        if (error.response.data.data) {
          let mobile = error.response.data.data.phone_number
          jsonData.mobile = mobile
        }
      }
      callback(false, jsonData, getErrorMessageFromErrorCode(error))
    });
}

export function getFacebookPostsCms(token, oldData, callback) {

  const instance = axios.create({
    baseURL: Constants.BaseUrl,
    timeout: 30000,
    headers: { 'Content-Type': "application/json", 'token': token }
  });

  var params = {}

  if (oldData) {
    params["offset"] = oldData.offset
  }

  instance.post(Constants.PostFromCms, params)
    .then(function (response) {
      if (response.data) {
        if (response.data.data) {
          let data = response.data.data
          callback(true, data, null)
        } else {
          callback(false, null, getErrorMessageFromErrorCode(null))
        }
      }
    })
    .catch(function (error) {
      callback(false, null, getErrorMessageFromErrorCode(error))
    });
}


export function getFacebookPosts(oldData, callback) {

  const instanceGet = axios.create({
    timeout: 10000,
    headers: { 'Content-Type': "application/json" }
  });

  if (oldData) {
    instanceGet.get(oldData.paging.next)
      .then(function (response) {
        if (response.data) {
          if (response.data.data) {
            callback(true, response.data, null)
          } else {
            callback(false, null, getErrorMessageFromErrorCode(null))
          }
        }
      })
      .catch(function (error) {
        callback(false, null, getErrorMessageFromErrorCode(null))
      });
  } else {
    instanceGet.get(`${Constants.BaseUrlAccessTokenFb}client_id=${Constants.ClientId}&client_secret=${Constants.ClientSecret}&grant_type=client_credentials`)
      .then(function (response) {
        if (response.data) {
          if (response.data.access_token) {
            let token = response.data.access_token
            // -ToDo: once we get approval from facebook we should remove this token
            token = "EAAGFWZC6ofZCgBAOcOZBwLLZBjDYIN8Rb2DAC400Y1B4kRuau7ekDBkcZAIT1YqZBo73GaZCxLXLQTEAJm0Xd3kCZBHB5uoYbaDgcnDzalMN15sRAYouF9szbsAoAZBPbHarZBLgZA4lxVK0EysJFbpPouUJz8BNbZBAUxvQKFIEPg5WfAZDZD"
            instanceGet.get(`${Constants.BaseUrlGraphFb}fields=id,created_time,message,link,attachments&access_token=${token}`)
              .then(function (response) {
                if (response.data) {
                  if (response.data.data) {
                    callback(true, response.data, null)
                  } else {
                    callback(false, null, getErrorMessageFromErrorCode(null))
                  }
                }
              })
              .catch(function (error) {
                callback(false, null, getErrorMessageFromErrorCode(null))
              });

          } else {
            callback(false, null, getErrorMessageFromErrorCode(null))
          }
        }
      })
      .catch(function (error) {
        callback(false, null, getErrorMessageFromErrorCode(null))
      });
  }
}

export function forgetPassword(number, callback) {

  const instance = axios.create({
    baseURL: Constants.BaseUrl,
    timeout: 30000,
    headers: { 'Content-Type': "application/json" }
  });

  instance.post(Constants.ForgetPasswordRequest, { phone: number })
    .then(function (response) {
      if (response.data) {
        if (response.data.code == 0) {
          callback(true, null, null)
        } else {
          callback(false, null, getErrorMessageFromErrorCode(null))
        }
      }
    })
    .catch(function (error) {
      callback(false, null, getErrorMessageFromErrorCode(error))
    });
}

export function forgetPasswordAction(newPassword, code, callback) {

  const instance = axios.create({
    baseURL: Constants.BaseUrl,
    timeout: 30000,
    headers: { 'Content-Type': "application/json" }
  });

  instance.post(Constants.ForgetPassword, { new_password: newPassword, code: code })
    .then(function (response) {
      if (response.data) {
        if (response.data.data.user) {
          let user = response.data.data.user
          callback(true, user, null)
        } else {
          callback(false, null, getErrorMessageFromErrorCode(null))
        }
      }
    })
    .catch(function (error) {
      callback(false, null, getErrorMessageFromErrorCode(error))
    });
}

export function getProductsForCategory(token, category, offset, callback) {

  const instance = axios.create({
    baseURL: Constants.BaseUrl,
    timeout: 30000,
    headers: { 'Content-Type': "application/json", 'token': token }
  });

  var params = { type: category }

  if (offset) {
    params["offset"] = offset
  }

  instance.post(Constants.ProductsCategory, params)
    .then(function (response) {
      if (response.data) {
        if (response.data.data) {
          let data = response.data.data
          callback(true, data, null)
        } else {
          callback(false, null, getErrorMessageFromErrorCode(null))
        }
      }
    })
    .catch(function (error) {
      callback(false, null, getErrorMessageFromErrorCode(error))
    });
}

export function getProductsForCategoryDetails(token, category, id, offset, callback) {

  const instance = axios.create({
    baseURL: Constants.BaseUrl,
    timeout: 30000,
    headers: { 'Content-Type': "application/json", 'token': token }
  });

  var params = { type: category, id: id }

  if (offset) {
    params["offset"] = offset
  }

  instance.post(Constants.ProductsCategoryDetails, params)
    .then(function (response) {
      if (response.data) {
        if (response.data.data) {
          let data = response.data.data
          callback(true, data, null)
        } else {
          callback(false, null, getErrorMessageFromErrorCode(null))
        }
      }
    })
    .catch(function (error) {
      callback(false, null, getErrorMessageFromErrorCode(error))
    });
}

export function getProductDetails(token, id, callback) {

  const instance = axios.create({
    baseURL: Constants.BaseUrl,
    timeout: 30000,
    headers: { 'Content-Type': "application/json", 'token': token }
  });

  instance.post(Constants.ProductDetails, { id: id })
    .then(function (response) {
      if (response.data) {
        if (response.data.data) {
          let data = response.data.data
          callback(true, data, null)
        } else {
          callback(false, null, getErrorMessageFromErrorCode(null))
        }
      }
    })
    .catch(function (error) {
      callback(false, null, getErrorMessageFromErrorCode(error))
    });
}

export function applayForJob(token, firstName, lastName, email, mobile, vacancy, experienceYears, salaryExpected, province, city, moreInfo, file, callback) {

  var fileName = "";
  try {
    fileName = decodeURIComponent(escape(file.fileName));
  } catch (e) {
    let tmpFileName = file.fileName
    let docIndex = tmpFileName.indexOf(".doc")
    let docxIndex = tmpFileName.indexOf(".docx")
    let pdfIndex = tmpFileName.indexOf(".pdf")
    let trimIndex = docIndex != -1 ? docIndex : docxIndex != -1 ? docxIndex : pdfIndex != -1 ? pdfIndex : -1
    if (trimIndex != -1) {
      fileName = "file" + tmpFileName.slice(trimIndex, trimIndex + (docxIndex != -1 ? 5 : 4))
    }
  }

  const formData = new FormData();
  formData.append('file', {
    uri: file.uri,
    type: file.type,
    name: fileName
  });
  formData.append('first_name', firstName);
  formData.append('last_name', lastName);
  formData.append('e_mail', email);
  formData.append('telephone_number', mobile);
  formData.append('years_of_experience', experienceYears);
  formData.append('expected_salary', salaryExpected);
  formData.append('province', province);
  formData.append('city', city);
  formData.append('vacancy_id', vacancy);
  // if ((moreInfo != null) && (moreInfo != "")) {
    formData.append('message', moreInfo);
  // } 

  const instance = axios.create({
    baseURL: Constants.BaseUrl,
    timeout: 120000,
    headers: { "content-type": "multipart/form-data; charset=UTF-8; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW", "token": token }
  });

  instance.post(Constants.Career, formData)
    .then(function (response) {
      if (response.data) {
        if (response.data.code == 0) {
          callback(true, null, null)
        } else {
          callback(false, null, getErrorMessageFromErrorCode(null))
        }
      }
    })
    .catch(function (error) {
      callback(false, null, getErrorMessageFromErrorCode(error))
    });
}

export function sendContactUs(token, firstName, lastName, email, mobile, subject, message, callback) {

  const instance = axios.create({
    baseURL: Constants.BaseUrl,
    timeout: 30000,
    headers: { 'Content-Type': "application/json", 'token': token }
  });

  let fullName = firstName + " " + lastName
  const params = {
    name: fullName,
    phone: mobile,
    email: email,
    subject: subject,
    message: message,
  }

  instance.post(Constants.ContactUs, params)
    .then(function (response) {
      if (response.data) {
        if (response.data.code == 0) {
          callback(true, null, null)
        } else {
          callback(false, null, getErrorMessageFromErrorCode(null))
        }
      }
    })
    .catch(function (error) {
      callback(false, null, getErrorMessageFromErrorCode(error))
    });
}

export function editPassword(token, oldPassword, newPassword, callback) {

  const instance = axios.create({
    baseURL: Constants.BaseUrl,
    timeout: 30000,
    headers: { 'Content-Type': "application/json", 'token': token }
  });

  instance.post(Constants.ChangePassword, { new_password: newPassword, old_password: oldPassword })
    .then(function (response) {
      if (response.data) {
        if (response.data.code == 0) {
          callback(true, null, null)
        } else {
          callback(false, null, getErrorMessageFromErrorCode(null))
        }
      }
    })
    .catch(function (error) {
      callback(false, null, getErrorMessageFromErrorCode(error))
    });
}

export function deleteAccount(token, callback) {

  const instance = axios.create({
    baseURL: Constants.BaseUrl,
    timeout: 30000,
    headers: { 'Content-Type': "application/json", 'token': token }
  });

  instance.post(Constants.DeleteUser, {})
    .then(function (response) {
      if (response.data) {
        if (response.data.code == 0) {
          callback(true, null, null)
        } else {
          callback(false, null, getErrorMessageFromErrorCode(null))
        }
      }
    })
    .catch(function (error) {
      callback(false, null, getErrorMessageFromErrorCode(error))
    });
}

export function salesmanCallLog(token, callback) {

  const instance = axios.create({
    baseURL: Constants.BaseUrl,
    timeout: 30000,
    headers: { 'Content-Type': "application/json", 'token': token }
  });

  instance.post(Constants.SalesmanCallLog, { salesman_id: salesmanId })
    .then(function (response) {
      if (response.data) {
        if (response.data.code == 0) {
          callback(true, null, null)
        } else {
          callback(false, null, getErrorMessageFromErrorCode(null))
        }
      }
    })
    .catch(function (error) {
      callback(false, null, getErrorMessageFromErrorCode(error))
    });
}

export function getWeather(location, callback) {

  const instance = axios.create({
    baseURL: Constants.BaseUrlWeather,
    timeout: 30000,
    headers: { 'Content-Type': "application/json" }
  });

  var params = {
    units: 'metric',
    lang: 'ar',
    lat: location.latitude,
    lon: location.longitude,
    appid: AppConstants.WeatherAppId,
  }
  
  instance.get('', { params })
    .then(function (response) {
      if (response.data) {
        if (response.data.list) {
          let data = response.data.list
          callback(true, data, null)
        } else {
          callback(false, null, getErrorMessageFromErrorCode(null))          
        }
      }
    })
    .catch(function (error) {
      callback(false, null, getErrorMessageFromErrorCode(error))
    });
}

export function share(token, callback) {

  const instance = axios.create({
    baseURL: Constants.BaseUrl,
    timeout: 30000,
    headers: { 'Content-Type': "application/json", 'token': token }
  });

  instance.post(Constants.Share)
    .then(function (response) {
      if (response.data) {
        if (response.data.code == 0) {
          callback(true, null, null)
        } else {
          callback(false, null, getErrorMessageFromErrorCode(null))
        }
      }
    })
    .catch(function (error) {
      callback(false, null, getErrorMessageFromErrorCode(error))
    });
}

export function getAllFertilizerGuides(token, oldData, callback) {

  const instance = axios.create({
    baseURL: Constants.BaseUrl,
    timeout: 30000,
    headers: { 'Content-Type': "application/json", 'token': token }
  });

  var params = {}

  if (oldData) {
    params["offset"] = oldData.offset
  }

  instance.post(Constants.GetAllFertilizerGuides, params)
    .then(function (response) {
      if (response.data) {
        if (response.data.data) {
          let data = response.data.data
          callback(true, data, null)
        } else {
          callback(false, null, getErrorMessageFromErrorCode(null))
        }
      }
    })
    .catch(function (error) {
      callback(false, null, getErrorMessageFromErrorCode(error))
    });
}

export function getFertilizDetailsItem(token, id, callback) {

  const instance = axios.create({
    baseURL: Constants.BaseUrl,
    timeout: 30000,
    headers: { 'Content-Type': "application/json", 'token': token }
  });

  instance.post(Constants.FertilizDetails, { id: id })
    .then(function (response) {
      if (response.data) {
        if (response.data.data) {
          let data = response.data.data
          callback(true, data, null)
        } else {
          callback(false, null, getErrorMessageFromErrorCode(null))
        }
      }
    })
    .catch(function (error) {
      callback(false, null, getErrorMessageFromErrorCode(error))
    });
}


export function registerPushNotification(token, deviceToken, callback) {

  const instance = axios.create({
    baseURL: Constants.BaseUrl,
    timeout: 30000,
    headers: { 'Content-Type': "application/json", 'token': token }
  });

  instance.post(Constants.SetNotificationToken, { notification_token: deviceToken })
    .then(function (response) {
      if (response.data) {
        if (response.data.code == 0) {          
          callback(true, null, null)
        } else {
          callback(false, null, getErrorMessageFromErrorCode(null))
        }
      }
    })
    .catch(function (error) {
      callback(false, null, getErrorMessageFromErrorCode(error))
    });
}

export function unRegisterPushNotification(token, callback) {

  const instance = axios.create({
    baseURL: Constants.BaseUrl,
    timeout: 30000,
    headers: { 'Content-Type': "application/json", 'token': token }
  });

  instance.post(Constants.UnSetNotificationToken)
    .then(function (response) {
      if (response.data) {
        if (response.data.code == 0) {
          callback(true, null, null)
        } else {
          callback(false, null, getErrorMessageFromErrorCode(null))
        }
      }
    })
    .catch(function (error) {      
      callback(false, null, getErrorMessageFromErrorCode(error))
    });
}
export function getNotifications(token, oldData, callback) {

  const instance = axios.create({
    baseURL: Constants.BaseUrl,
    timeout: 30000,
    headers: { 'Content-Type': "application/json", 'token': token }
  });

  let params = {}

  if (oldData) {
    params["offset"] = oldData.offset
  }

  instance.post(Constants.GetNotifications, params)
    .then(function (response) {
      if (response.data) {
        if (response.data.data) {
          let data = response.data.data
          callback(true, data, null)
        } else {
          callback(false, null, getErrorMessageFromErrorCode(null))
        }
      }
    })
    .catch(function (error) {
      callback(false, null, getErrorMessageFromErrorCode(error))
    });
}
