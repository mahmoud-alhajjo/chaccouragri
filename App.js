/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';

import { Provider } from 'react-redux'
import { createStore, applyMiddleware, combineReducers, compose} from 'redux'
import thunkMiddleware from 'redux-thunk'
import {createLogger} from 'redux-logger'
import reducer from './Reducers'
import { setupRNListener } from 'react-native-redux-listener';

import MainApp from './Components/Main Structure/MainApp'

// middleware that logs actions
const loggerMiddleware = createLogger({ predicate: (getState, action) => __DEV__  });

function configureStore(initialState) {
  const enhancer = compose(
    setupRNListener({
        monitorAppState: false,
        monitorNetInfo: false,
        monitorKeyboard: false,
        monitorDeepLinks: false,
        monitorBackButton: false,
    }),
    applyMiddleware(
      thunkMiddleware, // lets us dispatch() functions
      loggerMiddleware
    ),
  );

  return createStore(reducer, initialState, enhancer);
}

const store = configureStore({})

export default class App extends Component<Props> {
  render() {
    return (
      <Provider store={store}>
        <MainApp />
      </Provider>
    );
  }
}
