import AsyncStorage from '@react-native-community/async-storage';
import Geolocation from '@react-native-community/geolocation';
import {
  PermissionsAndroid
} from 'react-native';
import { NavigationActions, StackActions } from 'react-navigation';
import Moment from 'moment';
import { AppEventsLogger } from 'react-native-fbsdk';
import moment from 'moment';
import PushNotification from 'react-native-push-notification';

import * as types from './types'
import * as api from '../Lib/API';

export function logEvent(eventName, params) {
  return (dispatch) => {
    AsyncStorage.getItem('user').then((jsonUser) => {
      if (jsonUser) {
        let user = JSON.parse(jsonUser)
        let birthYear = Moment(user.birthdate, 'DD-MM-YYYY').locale('en')
        let birthYearString = birthYear.format('YYYY')
        let userParams = { mobile: user.phone_number, birthYear: birthYearString, gender: user.gender }
        if (params) {
          params["mobile"] = userParams.mobile
          params["birthYear"] = userParams.birthYear
          params["gender"] = userParams.gender
          AppEventsLogger.logEvent(eventName, (params));
        } else {
          AppEventsLogger.logEvent(eventName, (userParams));
        }
      }
    })
      .catch((e) => { })

  };
}

export function getUserFromLocal() {
  return (dispatch) => {
    AsyncStorage.getItem('user').then((jsonUser) => {
      if (jsonUser) {
        let user = JSON.parse(jsonUser)
        dispatch({ type: types.USER, data: user });
      }
    })
      .catch((e) => { })
  };
}

export function addNotificationToUser(payload, read, isNotificationScreen) {
  return (dispatch) => {
    var readFlag = read
    if (isNotificationScreen) {
      readFlag = isNotificationScreen
    }
    let notification = {
      id: payload.id,
      read: readFlag
    }
    AsyncStorage.getItem('user').then((jsonUser) => {
      if (jsonUser) {
        let user = JSON.parse(jsonUser)
        if (user.notifications) {
          var found = false
          for (var i = 0; i < user.notifications.length; i++) {
            if (user.notifications[i].id == notification.id) {
              found = true
              break
            }
          }
          if (!found) {
            user.notifications = [notification, ...user.notifications]
          }
        } else {
          user["notifications"] = [notification]
        }
        AsyncStorage.setItem('user', JSON.stringify(user));
        dispatch({ type: types.USER, data: user });
      } else {
        dispatch({ type: types.USER, data: null });
      }
    })
      .catch((e) => {
        dispatch({ type: types.USER, data: null });
      })
  };
}

export function setUserNotificationRead(notification) {
  return (dispatch) => {
    AsyncStorage.getItem('user').then((jsonUser) => {
      if (jsonUser) {
        let user = JSON.parse(jsonUser)
        if (user.notifications) {
          for (var i = 0; i < user.notifications.length; i++) {
            if (notification != null) {

            } else {
              user.notifications[i].read = true
            }
          }
        }
        AsyncStorage.setItem('user', JSON.stringify(user));
        dispatch({ type: types.USER, data: user });
      } else {
        dispatch({ type: types.USER, data: null });
      }
    })
      .catch((e) => {
        dispatch({ type: types.USER, data: null });
      })
  };
}

export function goToNotificationScreen(toggle) {
  return (dispatch) => {
    dispatch({ type: types.GO_TO_NOTIFICATION, data: toggle });
  };
}

export function logoutAndDeleteAllData(navigation) {
  return (dispatch) => {
    AsyncStorage.getItem('user').then((jsonUser) => {
      if (jsonUser) {
        PushNotification.cancelAllLocalNotifications()
        let user = JSON.parse(jsonUser)
        api.unRegisterPushNotification(user.token, function (success, data, error) {
          if (success) {
          } else if (error) {
          }
        });
        if (navigation) {
          dispatch({ type: types.LOGOUT, data: null });
          let actionToDispatch = StackActions.reset({
            index: 0,
            key: null,
            actions: [
              NavigationActions.navigate({ routeName: 'Splash' }),
            ]
          })
          navigation.dispatch(actionToDispatch)
        } else {
          dispatch({ type: types.LOGGEDOUT, data: true });
        }
      }
    })
      .catch((e) => {
      })
  };
}

export function toggleAboutUsView(open) {
  return (dispatch) => {
    dispatch({ type: types.TOGGLE_ABOUTUS_VIEW, data: open });
  };
}

export function toggleProfileView(open) {
  return (dispatch) => {
    dispatch({ type: types.TOGGLE_PROFILE_VIEW, data: open });
  };
}

export function navigateToHomeFromSplash(navigation) {
  return (dispatch) => {
    let actionToDispatch = StackActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({ routeName: 'MainStructure' }),
      ]
    })
    navigation.dispatch(actionToDispatch)
  };
}

export function navigateToHomeFromSplashForUser(navigation) {
  return (dispatch) => {
    AsyncStorage.getItem('user').then((jsonUser) => {
      let user = JSON.parse(jsonUser)
      var routeName = 'Interests'
      if (user.interests) {
        if (user.interests.length > 0) {
          routeName = "MainStructure"
        }
      }
      let actionToDispatch = StackActions.reset({
        index: 0,
        actions: [
          NavigationActions.navigate({ routeName: routeName }),
        ]
      })
      navigation.dispatch(actionToDispatch)
    }).catch((e) => { })
  };
}

export function navigateToOnBoardingFromSplash(navigation) {
  return (dispatch) => {
    let actionToDispatch = StackActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({ routeName: 'OnBoarding' }),
      ]
    })
    navigation.dispatch(actionToDispatch)
  };
}

export function backToOnBoardingFromVerification(navigation) {
  return (dispatch) => {
    dispatch({ type: types.REMOVE_NONE_ACTIVE_USER, data: null });
    navigation.dispatch(StackActions.popToTop());
  };
}

export function toggleVerificationView(toggle) {
  return (dispatch) => {
    dispatch({ type: types.TOGGLE_VERIFICATION_VIEW, data: toggle });
  };
}


export function getMetaData(successCB, errorCB) {
  return (dispatch) => {
    api.getMetaData(function (success, data, error) {
      if (success) {
        dispatch({ type: types.META_DATA_SUCCESS, data: data });
        AsyncStorage.getItem('user').then((user) => {
          if (user) {
            successCB(true)
          } else {
            successCB(false)
          }
        }).catch((e) => { })
      } else if (error) {
        errorCB(error)
      }
    });
  };
}

export function getOccupations(successCB) {
  return (dispatch) => {
    AsyncStorage.getItem('occupations').then((jsonOccupations) => {
      if (jsonOccupations) {
        let occupations = JSON.parse(jsonOccupations)
        dispatch({ type: types.OCCUPATIONS, data: occupations });
        successCB()
      } else {
        dispatch({ type: types.OCCUPATIONS, data: null });
      }
    }).catch((e) => { dispatch({ type: types.OCCUPATIONS, data: null }) })
  };
}

export function getProvince(successCB) {
  return (dispatch) => {
    AsyncStorage.getItem('province').then((jsonProvince) => {
      if (jsonProvince) {
        let province = JSON.parse(jsonProvince)
        dispatch({ type: types.PROVINCE, data: province });
        successCB()
      } else {
        dispatch({ type: types.PROVINCE, data: null });
      }
    }).catch((e) => { dispatch({ type: types.PROVINCE, data: null }) })
  };
}

export function getCities(provinceId, successCB) {
  return (dispatch) => {
    AsyncStorage.getItem('city').then((jsonCity) => {
      if (jsonCity) {
        let city = JSON.parse(jsonCity)
        let cities = city.filter((item) => {
          return item.province_id == provinceId
        });
        dispatch({ type: types.CITY, data: cities });
        successCB()
      } else {
        dispatch({ type: types.CITY, data: null });
      }
    }).catch((e) => { dispatch({ type: types.CITY, data: null }) })
  };
}

export function getGender(successCB) {
  return (dispatch) => {
    let gender = [{ id: "male", name: "ذكر" }, { id: "female", name: "انثى" }]
    dispatch({ type: types.GENDER, data: gender });
    successCB()
  };
}

export function getAreaSize(successCB) {
  return (dispatch) => {
    let sizes = [{ id: "<50", name: "أقل من 50 دنم" }, { id: "50-100", name: "100 - 50 دنم" }, { id: "100-150", name: "150 - 100 دنم" }, { id: ">200", name: "أكبر من 200 دنم" }]
    dispatch({ type: types.AREA_SIZE, data: sizes });
    successCB()
  };
}

export function getSpecialization(successCB) {
  return (dispatch) => {
    let specialization = [
      { id: "قسم الاقتصاد الزراعي", name: "قسم الاقتصاد الزراعي" },
      { id: "قسم الإنتاج الحيواني", name: "قسم الإنتاج الحيواني" },
      { id: "قسم العلوم الأساسية", name: "قسم العلوم الأساسية" },
      { id: "قسم علوم الأغذية", name: "قسم علوم الأغذية" },
      { id: "قسم علوم البستنة", name: "قسم علوم البستنة" },
      { id: "قسم علوم التربة", name: "قسم علوم التربة" },
      { id: "قسم المحاصيل الحقلية", name: "قسم المحاصيل الحقلية" },
      { id: "قسم الموارد الطبيعية المتجددة والبيئة", name: "قسم الموارد الطبيعية المتجددة والبيئة" },
      { id: "قسم الهندسة الريفية", name: "قسم الهندسة الريفية" },
      { id: "قسم وقاية النبات", name: "قسم وقاية النبات" }
    ]
    dispatch({ type: types.SPECIALIZATION, data: specialization });
    successCB()
  };
}

export function getContactUsSubjects(successCB) {
  return (dispatch) => {
    AsyncStorage.getItem('subjects').then((jsonSubjects) => {
      if (jsonSubjects) {
        let subjects = JSON.parse(jsonSubjects)
        dispatch({ type: types.CONTACT_US_SUBJECTS, data: subjects });
        successCB()
      } else {
        dispatch({ type: types.CONTACT_US_SUBJECTS, data: null });
      }
    }).catch((e) => { dispatch({ type: types.CONTACT_US_SUBJECTS, data: null }) })
  };
}

export function getSalesman() {
  return (dispatch) => {
    AsyncStorage.getItem('salesman').then((jsonSalesman) => {
      if (jsonSalesman) {
        var salesman = JSON.parse(jsonSalesman)
        AsyncStorage.getItem('user').then((jsonUser) => {
          if (jsonUser) {
            let user = JSON.parse(jsonUser)
            salesman = salesman.filter((item) => {
              return item.provinces.includes(user.province) && item.cities.includes(user.city)
            });
            dispatch({ type: types.SALESMAN, data: salesman });
          } else {
            dispatch({ type: types.SALESMAN, data: null });
          }
        })
      } else {
        dispatch({ type: types.SALESMAN, data: null });
      }
    }).catch((e) => { dispatch({ type: types.SALESMAN, data: null }) })
  };
}

export function getMainInterests() {
  return (dispatch) => {
    AsyncStorage.getItem('interest').then((jsonInterests) => {
      if (jsonInterests) {
        let interests = JSON.parse(jsonInterests)
        AsyncStorage.getItem('user').then((jsonUser) => {
          if (jsonUser) {
            let user = JSON.parse(jsonUser)
            let mainInterests = interests.filter((item) => {
              return ((item.interest_type == null) && item.ocupation_ids.includes(user.occupation))
            });
            for (var i = 0; i < mainInterests.length; i++) {
              var selected = false
              let subInterests = interests.filter((item) => {
                return item.interest_type == mainInterests[i].id
              }).map(({ id }) => (id));
              mainInterests[i]["hasChilds"] = subInterests.length > 0
              if (subInterests.length > 0) {
                for (var j = 0; j < user.interests.length; j++) {
                  selected = subInterests.includes(user.interests[j])
                  if (selected) {
                    break
                  }
                }
              } else {
                selected = user.interests.includes(mainInterests[i].id)
              }
              mainInterests[i]["selected"] = selected
            }
            dispatch({ type: types.MAIN_INTERESTS, data: mainInterests });
          } else {
            dispatch({ type: types.MAIN_INTERESTS, data: null });
          }
        })
      } else {
        dispatch({ type: types.MAIN_INTERESTS, data: null });
      }
    }).catch((e) => { dispatch({ type: types.MAIN_INTERESTS, data: null }) })
  };
}

export function getInterestsByType(mainInterestsIds, successCB) {
  return (dispatch) => {
    AsyncStorage.getItem('interest').then((jsonInterests) => {
      if (jsonInterests) {
        let interests = JSON.parse(jsonInterests)
        AsyncStorage.getItem('user').then((jsonUser) => {
          if (jsonUser) {
            let user = JSON.parse(jsonUser)
            let allInterests = []
            for (var i = 0; i < mainInterestsIds.length; i++) {
              let subInterests = interests.filter((item) => {
                return item.interest_type == mainInterestsIds[i]
              });
              allInterests = [...allInterests, ...subInterests]
            }
            for (var i = 0; i < allInterests.length; i++) {
              allInterests[i]["selected"] = user.interests.includes(allInterests[i].id)
            }
            dispatch({ type: types.SUB_INTERESTS, data: allInterests });
            successCB()
          } else {
            dispatch({ type: types.SUB_INTERESTS, data: null });
          }
        })
      } else {
        dispatch({ type: types.SUB_INTERESTS, data: null });
      }
    }).catch((e) => { dispatch({ type: types.SUB_INTERESTS, data: null }) })
  };
}

export function signup(photo, firstName, lastName, occupationId, mobile, provinceId, cityId, genderId, birthdate, password, fbData, areaSize, specialization, successCB, errorCB) {
  return (dispatch) => {
    dispatch({ type: types.LOADER, data: true });
    api.signup(photo, firstName, lastName, occupationId, mobile, provinceId, cityId, genderId, birthdate, password, fbData, areaSize, specialization, function (success, data, error) {
      if (success) {
        dispatch({ type: types.LOADER, data: false });
        dispatch({ type: types.SIGNUP_SUCESS, data: { mobile: mobile, password: password, fbData: fbData } });
        successCB();
      } else if (error) {
        dispatch({ type: types.LOADER, data: false });
        if (error.shouldVerify) {
          dispatch({ type: types.SIGNUP_SUCESS, data: { mobile: mobile, password: password, fbData: fbData } });
        }
        errorCB(error)
      }
    });
  };
}

export function getNoneActiveUser(successCB) {
  return (dispatch) => {
    AsyncStorage.getItem('nonActiveUser').then((jsonUser) => {
      if (jsonUser) {
        let user = JSON.parse(jsonUser)
        dispatch({ type: types.NONE_ACTIVE_USER, data: user });
      } else {
        dispatch({ type: types.NONE_ACTIVE_USER, data: null });
      }
      successCB();
    }).catch((e) => {
      dispatch({ type: types.NONE_ACTIVE_USER, data: null })
      successCB();
    })
  };
}

export function verifyAccount(code, number, successCB, errorCB) {
  return (dispatch) => {
    dispatch({ type: types.LOADER, data: true });
    api.verifyAccount(code, number, function (success, data, error) {
      if (success) {
        dispatch({ type: types.LOADER, data: false });
        dispatch({ type: types.REMOVE_NONE_ACTIVE_USER, data: null });
        successCB();
      } else if (error) {
        dispatch({ type: types.LOADER, data: false });
        errorCB(error)
      }
    });
  };
}

export function resendCode(number, successCB, errorCB) {
  return (dispatch) => {
    dispatch({ type: types.LOADER, data: true });
    api.resendCode(number, function (success, data, error) {
      if (success) {
        dispatch({ type: types.LOADER, data: false });
        successCB();
      } else if (error) {
        dispatch({ type: types.LOADER, data: false });
        errorCB(error)
      }
    });
  };
}

export function loginForNonActiveSplash(mobile, password, successCB, errorCB) {
  return (dispatch) => {
    api.login(mobile, password, function (success, data, error) {
      if (success) {
        dispatch({ type: types.LOGIN_SUCCESS, data: data });
        successCB();
      } else if (error) {
        if (error.shouldVerify) {
          dispatch({ type: types.SIGNUP_SUCESS, data: { mobile: mobile, password: password } });
        }
        errorCB(error)
      }
    });

  };
}

export function login(mobile, password, navigation, errorCB) {
  return (dispatch) => {
    dispatch({ type: types.LOADER, data: true });
    api.login(mobile, password, function (success, data, error) {
      if (success) {
        dispatch({ type: types.LOGIN_SUCCESS, data: data });
        dispatch({ type: types.LOADER, data: false });
        let user = data
        var routeName = 'Interests'
        if (user.interests) {
          if (user.interests.length > 0) {
            routeName = "MainStructure"
          }
        }
        let actionToDispatch = StackActions.reset({
          index: 0,
          actions: [
            NavigationActions.navigate({ routeName: routeName }),
          ]
        })
        dispatch({ type: types.USER, data: user });
        navigation.dispatch(actionToDispatch)
      } else if (error) {
        dispatch({ type: types.LOADER, data: false });
        if (error.shouldVerify) {
          dispatch({ type: types.SIGNUP_SUCESS, data: { mobile: mobile, password: password } });
        }
        errorCB(error)
      }
    });

  };
}

export function getProfile(successCB, errorCB) {
  return (dispatch) => {
    AsyncStorage.getItem('user').then((jsonUser) => {
      let user = JSON.parse(jsonUser)
      api.getProfile(user.token, function (success, data, error) {
        if (success) {
          dispatch({ type: types.LOGIN_SUCCESS, data: data });
          successCB();
        } else if (error) {
          errorCB(error)
        }
      });
    }).catch((e) => { })
  };
}

export function editProfile(firstName, lastName, occupation, province, city, gender, birthdate, photo, interests, areaSize, specialization, successCB, errorCB) {
  return (dispatch) => {
    dispatch({ type: types.LOADER, data: true });
    AsyncStorage.getItem('user').then((jsonUser) => {
      let user = JSON.parse(jsonUser)
      api.editProfile(user.token, firstName, lastName, occupation, province, city, gender, birthdate, photo, interests, areaSize, specialization, function (success, data, error) {
        if (success) {
          dispatch({ type: types.LOADER, data: false });
          dispatch({ type: types.EDIT_PROFILE, data: data });
          successCB();
        } else if (error) {
          dispatch({ type: types.LOADER, data: false });
          errorCB(error)
        }
      });
    }).catch((e) => { })
  };
}

export function loginViaFacebook(navigation, errorCB) {
  return (dispatch) => {
    dispatch({ type: types.LOADER, data: true });
    api.loginViaFacebook(function (success, data, error) {
      if (success) {
        dispatch({ type: types.LOADER, data: false });
        if (data !== null) {
          dispatch({ type: types.LOGIN_SUCCESS, data: data });
          let user = data
          var routeName = 'Interests'
          if (user.interests) {
            if (user.interests.length > 0) {
              routeName = "MainStructure"
            }
          }
          let actionToDispatch = StackActions.reset({
            index: 0,
            actions: [
              NavigationActions.navigate({ routeName: routeName }),
            ]
          })
          dispatch({ type: types.USER, data: user });
          navigation.dispatch(actionToDispatch)
        }
      } else if (error) {
        dispatch({ type: types.LOADER, data: false });
        if (error == -1) {
          let actionToDispatch = StackActions.reset({
            index: 0,
            actions: [
              NavigationActions.navigate({
                routeName: 'Signup',
                params: {
                  fbData: data
                }
              }),
            ]
          })
          navigation.dispatch(actionToDispatch)
        } else {
          if (error.shouldVerify) {
            dispatch({ type: types.SIGNUP_SUCESS, data: data });
          }
          errorCB(error)
        }
      }
    });
  };
}

export function getFacebookPostsCms(oldData, errorCB, refreshList = false) {
  return (dispatch) => {
    AsyncStorage.getItem('user').then((jsonUser) => {
      if (jsonUser) {
        let user = JSON.parse(jsonUser)
        if (oldData == null) {
          if (refreshList == false) {
            dispatch({ type: types.LOADER, data: true });
          }
        }
        api.getFacebookPostsCms(user.token, oldData, function (success, data, error) {
          if (success) {
            dispatch({ type: types.LOADER, data: false });
            let finalData = data
            if (oldData) {
              if (finalData.posts) {
                finalData.posts = [...oldData.posts, ...finalData.posts]
              }
            }
            dispatch({ type: types.FACEBOOK_POSTS_CMS, data: finalData });
          } else if (error) {
            dispatch({ type: types.LOADER, data: false });
            errorCB(error)
          }
        });
      }
    })
      .catch((e) => { })
  };
}

export function getFacebookPosts(oldData, errorCB, refreshList = false) {
  return (dispatch) => {
    if (oldData == null) {
      if (refreshList == false) {
        dispatch({ type: types.LOADER, data: true });
      }
    }
    api.getFacebookPosts(oldData, function (success, data, error) {
      if (success) {
        dispatch({ type: types.LOADER, data: false });
        let finalData = data
        if (oldData) {
          if (finalData.data) {
            finalData.data = [...oldData.data, ...finalData.data]
          }
        }
        dispatch({ type: types.FACEBOOK_POSTS, data: finalData });
      } else if (error) {
        dispatch({ type: types.LOADER, data: false });
        errorCB(error)
      }
    });
  };
}

export function loginViaFacebookIdForNonActiveSplash(facebookId, successCB, errorCB) {
  return (dispatch) => {
    api.loginViaFacebookId(facebookId, function (success, data, error) {
      if (success) {
        if (data !== null) {
          dispatch({ type: types.LOGIN_SUCCESS, data: data });
          successCB()
        }
      } else if (error) {
        errorCB(error)
      }
    });
  };
}

export function loginViaFacebookId(facebookId, navigation, errorCB) {
  return (dispatch) => {
    dispatch({ type: types.LOADER, data: true });
    api.loginViaFacebookId(facebookId, function (success, data, error) {
      if (success) {
        dispatch({ type: types.LOADER, data: false });
        if (data !== null) {
          dispatch({ type: types.LOGIN_SUCCESS, data: data });
          let user = data
          var routeName = 'Interests'
          if (user.interests) {
            if (user.interests.length > 0) {
              routeName = 'MainStructure'
            }
          }
          let actionToDispatch = StackActions.reset({
            index: 0,
            actions: [
              NavigationActions.navigate({ routeName: routeName }),
            ]
          })
          dispatch({ type: types.USER, data: user });
          navigation.dispatch(actionToDispatch)
        }
      } else if (error) {
        dispatch({ type: types.LOADER, data: false });
        errorCB(error)
      }
    });
  };
}

export function forgetPassword(number, successCB, errorCB) {
  return (dispatch) => {
    dispatch({ type: types.LOADER, data: true });
    api.forgetPassword(number, function (success, data, error) {
      if (success) {
        dispatch({ type: types.LOADER, data: false });
        successCB();
      } else if (error) {
        dispatch({ type: types.LOADER, data: false });
        errorCB(error)
      }
    });
  };
}

export function forgetPasswordAction(newPassword, code, navigation, errorCB) {
  return (dispatch) => {
    dispatch({ type: types.LOADER, data: true });
    api.forgetPasswordAction(newPassword, code, function (success, data, error) {
      if (success) {
        dispatch({ type: types.LOADER, data: false });
        dispatch({ type: types.LOGIN_SUCCESS, data: data });
        let user = data
        var routeName = 'Interests'
        if (user.interests) {
          if (user.interests.length > 0) {
            routeName = 'MainStructure'
          }
        }
        let actionToDispatch = StackActions.reset({
          index: 0,
          actions: [
            NavigationActions.navigate({ routeName: routeName }),
          ]
        })
        dispatch({ type: types.USER, data: user });
        navigation.dispatch(actionToDispatch)

      } else if (error) {
        dispatch({ type: types.LOADER, data: false });
        errorCB(error)
      }
    });
  };
}

export function getProductsForCategory(category, offset, successCB, errorCB) {
  return (dispatch) => {
    AsyncStorage.getItem('user').then((jsonUser) => {
      if (jsonUser) {
        let user = JSON.parse(jsonUser)
        api.getProductsForCategory(user.token, category, offset, function (success, data, error) {
          if (success) {
            successCB(data);
          } else if (error) {
            errorCB(error)
          }
        });
      }
    })
      .catch((e) => { })
  };
}

export function getProductsForCategoryDetails(category, id, offset, successCB, errorCB) {
  return (dispatch) => {
    AsyncStorage.getItem('user').then((jsonUser) => {
      if (jsonUser) {
        let user = JSON.parse(jsonUser)
        if (offset == null) {
          dispatch({ type: types.LOADER, data: true });
        }
        api.getProductsForCategoryDetails(user.token, category, id, offset, function (success, data, error) {
          if (success) {
            dispatch({ type: types.LOADER, data: false });
            dispatch({ type: types.CATEGORY_DETAILS_PRODUCTS, data: data });
            successCB();
          } else if (error) {
            dispatch({ type: types.LOADER, data: false });
            errorCB(error)
          }
        });
      }
    })
      .catch((e) => { })
  };
}

export function getProductDetails(id, successCB, errorCB) {
  return (dispatch) => {
    dispatch({ type: types.LOADER, data: true });
    AsyncStorage.getItem('user').then((jsonUser) => {
      if (jsonUser) {
        let user = JSON.parse(jsonUser)
        api.getProductDetails(user.token, id, function (success, data, error) {
          if (success) {
            dispatch({ type: types.LOADER, data: false });
            if (data.details) {
              for (var i = 0; i < data.details.length; i++) {
                data.details[i]["expanded"] = false
              }
            }
            dispatch({ type: types.PRODUCT_DETAILS, data: data });
            successCB();
          } else if (error) {
            dispatch({ type: types.LOADER, data: false });
            errorCB(error)
          }
        });
      }
    })
      .catch((e) => { })
  };
}

export function applayForJob(firstName, lastName, email, mobile, vacancy, experienceYears, salaryExpected, province, city, moreInfo, cvFile, successCB, errorCB) {
  return (dispatch) => {
    dispatch({ type: types.LOADER, data: true });
    AsyncStorage.getItem('user').then((jsonUser) => {
      if (jsonUser) {
        let user = JSON.parse(jsonUser)
        api.applayForJob(user.token, firstName, lastName, email, mobile, vacancy, experienceYears, salaryExpected, province, city, moreInfo, cvFile, function (success, data, error) {
          if (success) {
            dispatch({ type: types.LOADER, data: false });
            successCB();
          } else if (error) {
            dispatch({ type: types.LOADER, data: false });
            console.log('error', error)
            errorCB(error)
          }
        });
      }
    })
      .catch((e) => { console.log('error', e) })
  };
}

export function sendContactUs(firstName, lastName, email, mobile, subject, message, successCB, errorCB) {
  return (dispatch) => {
    dispatch({ type: types.LOADER, data: true });
    AsyncStorage.getItem('user').then((jsonUser) => {
      if (jsonUser) {
        let user = JSON.parse(jsonUser)
        api.sendContactUs(user.token, firstName, lastName, email, mobile, subject, message, function (success, data, error) {
          if (success) {
            dispatch({ type: types.LOADER, data: false });
            successCB();
          } else if (error) {
            dispatch({ type: types.LOADER, data: false });
            errorCB(error)
          }
        });
      }
    })
      .catch((e) => { })
  };
}

export function editPassword(oldPassword, newPassword, successCB, errorCB) {
  return (dispatch) => {
    dispatch({ type: types.LOADER, data: true });
    AsyncStorage.getItem('user').then((jsonUser) => {
      if (jsonUser) {
        let user = JSON.parse(jsonUser)
        api.editPassword(user.token, oldPassword, newPassword, function (success, data, error) {
          if (success) {
            dispatch({ type: types.LOADER, data: false });
            successCB();
          } else if (error) {
            dispatch({ type: types.LOADER, data: false });
            errorCB(error)
          }
        });
      }
    })
      .catch((e) => { })
  };
}

export function deleteAccount(successCB, errorCB) {
  return (dispatch) => {
    dispatch({ type: types.LOADER, data: true });
    AsyncStorage.getItem('user').then((jsonUser) => {
      if (jsonUser) {
        let user = JSON.parse(jsonUser)
        api.deleteAccount(user.token, function (success, data, error) {
          if (success) {
            dispatch({ type: types.LOADER, data: false });
            successCB();
          } else if (error) {
            dispatch({ type: types.LOADER, data: false });
            errorCB(error)
          }
        });
      }
    })
      .catch((e) => { })
  };
}

export function salesmanCallLog(salesmanId) {
  return (dispatch) => {
    AsyncStorage.getItem('user').then((jsonUser) => {
      if (jsonUser) {
        let user = JSON.parse(jsonUser)
        api.salesmanCallLog(salesmanId, user.token, function (success, data, error) {
          if (success) {
          } else if (error) {
          }
        });
      }
    })
      .catch((e) => { })
  };
}

export const getCurrentLocation = (successCB, errorCB) => async dispatch => {
  try {
    const granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION);

    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      dispatch({ type: types.LOADER, data: true });
      Geolocation.getCurrentPosition(
        (position) => {
          if (position.coords) {
            let coords = { latitude: position.coords.latitude, longitude: position.coords.longitude }
            dispatch({ type: types.CURRENT_LOCATION, data: coords });
            successCB(coords);
          } else {
            dispatch({ type: types.LOADER, data: false });
            let error = { errorMessage: "حدث خطأ أثناء تحديد موقعك الرجاء إعادة المحاولة", shouldRetry: false, shouldVerify: false, shouldLogout: false }
            errorCB(error)
          }
        },
        (error) => {
          dispatch({ type: types.LOADER, data: false });
          let errorMessage = null
          if (error.code == 2) {
            errorMessage = { errorMessage: " الرجاء تشغيل خدمة المواقع من الجهاز ", shouldRetry: false, shouldVerify: false, shouldLogout: false }
          } else {
            errorMessage = { errorMessage: "حدث خطأ في الاتصال. الرجاء إعادة المحاولة", shouldRetry: true, shouldVerify: false, shouldLogout: false }
          }
          errorCB(errorMessage)
        }
      )
    }
    else {
      let error = { errorMessage: " الرجاء اعطاء صلاحية خدمة المواقع من إعدادات الجهاز لهذا التطبيق", shouldRetry: false, shouldVerify: false, shouldLogout: false }
      errorCB(error)
    }
  } catch (err) {
    console.log('err', err)
  }
}

export function getWeather(location, successCB, errorCB) {
  return (dispatch) => {
    dispatch({ type: types.LOADER, data: true });
    api.getWeather(location, function (success, data, error) {
      if (success) {
        if (data.length > 0 ) {
          data = data.sort(data.dt);
          data.sort((a, b) => a.dt - b.dt);
          let currentWeather = [];
          let currentTime = moment();
          if (currentTime.get('hour') < moment.unix(data[0].dt).utc().get('hour')) {
            currentTime = moment.unix(data[0].dt).utc()
          }
          for (let i = 0; i < data.length; i++) {
            let date = moment.unix(data[i].dt).utc();
            let nextDate = moment.unix(data[i].dt).utc().add(3, 'h');
            if ((currentTime.get('hour') >= date.get('hour')) && (currentTime.get('hour') < nextDate.get('hour'))) {
              data[i].dt = moment.unix(data[i].dt).utc();
              data[i].main.temp = Math.round(data[i].main.temp);
              data[i].main.temp_min = Math.round(data[i].main.temp_min);
              data[i].main.temp_max = Math.round(data[i].main.temp_max);
              data[i].weather[0].icon = "http://openweathermap.org/img/w/" + data[i].weather[0].icon + ".png";
              editCurrentWeather = {
                time: data[i].dt,
                temp_main: data[i].main.temp,
                temp_min: data[i].main.temp_min,
                temp_max: data[i].main.temp_max,
                humidity: data[i].main.humidity,
                description: data[i].weather[0].description,
                icon: data[i].weather[0].icon
              }
              currentWeather.push(editCurrentWeather);
            }
          }
          dispatch({ type: types.WEATHER_INFORMATION, data: currentWeather });
          dispatch({ type: types.LOADER, data: false });
          successCB();
        }
      } else if (error) {
        dispatch({ type: types.LOADER, data: false });
        errorCB(error)
      }
    });
  };
}


export function share() {
  return (dispatch) => {
    AsyncStorage.getItem('user').then((jsonUser) => {
      if (jsonUser) {
        let user = JSON.parse(jsonUser)
        api.share(user.token, function (success, data, error) {
          if (success) {
          } else if (error) {
          }
        });
      }
    })
      .catch((e) => { })
  };
}

export function editProfileAppVersion(areaSize, specialization) {
  return (dispatch) => {
    AsyncStorage.getItem('user').then((jsonUser) => {
      let user = JSON.parse(jsonUser)
      api.editProfile(user.token, user.firstName, user.lastName, user.occupation, user.province, user.city, user.gender, user.birthdate, null, user.interests, areaSize, specialization, function (success, data, error) {
        if (success) {
        } else if (error) {
        }
      });
    }).catch((e) => { })
  };
}

export function newProductWatchingDone(productId) {
  return (dispatch) => {
    AsyncStorage.getItem('user').then((jsonUser) => {
      if (jsonUser) {
        let user = JSON.parse(jsonUser)
        for (let i = 0; i < user.productsIdWatching.length; i++) {
          if (productId === user.productsIdWatching[i]) {
            return
          }
        }
        user.productsIdWatching.push(productId)
        AsyncStorage.setItem('user', JSON.stringify(user));
        dispatch({ type: types.USER, data: user });
      } else {
        dispatch({ type: types.USER, data: null });
      }
    })
      .catch((e) => {
        dispatch({ type: types.USER, data: null });
      })
  };
}

export function CategoryChecker(productsWatching, productsIdNew, categorysName) {
  return (dispatch) => {
    AsyncStorage.getItem('user').then((jsonUser) => {
      if (jsonUser) {
        let user = JSON.parse(jsonUser)
        let checker = (arr, target) => target.every(v => arr.includes(v));
        for (let i = 0; i < user.categoryChecker.length; i++) {
          if (categorysName === user.categoryChecker[i]) {
            return
          }
        }
        if (checker(productsWatching, productsIdNew)) {
          user.categoryChecker.push(categorysName)
        }
        AsyncStorage.setItem('user', JSON.stringify(user));
        dispatch({ type: types.USER, data: user });
      } else {
        dispatch({ type: types.USER, data: null });
      }
    })
      .catch((e) => {
        dispatch({ type: types.USER, data: null });
      })
  };
}

export function getVacanciesCareers() {
  return (dispatch) => {
    AsyncStorage.getItem('job_vacancies').then((jsonVacancies) => {
      if (jsonVacancies) {
        var vacancies = JSON.parse(jsonVacancies)
        AsyncStorage.getItem('user').then((jsonUser) => {
          if (jsonUser) {
            let user = JSON.parse(jsonUser)
            dispatch({ type: types.VACANCIES, data: vacancies });
          } else {
            dispatch({ type: types.VACANCIES, data: null });
          }
        })
      } else {
        dispatch({ type: types.VACANCIES, data: null });
      }
    }).catch((e) => { dispatch({ type: types.VACANCIES, data: null }) })
  };
}

export function getAllFertilizerGuides(oldData, errorCB, refreshList = false) {
  return (dispatch) => {
    AsyncStorage.getItem('user').then((jsonUser) => {
      if (jsonUser) {
        let user = JSON.parse(jsonUser)
        if (oldData == null) {
          if (refreshList == false) {
            dispatch({ type: types.LOADER, data: true });
          }
        }
        api.getAllFertilizerGuides(user.token, oldData, function (success, data, error) {
          if (success) {
            dispatch({ type: types.LOADER, data: false });
            console.log('data', data);
            let finalData = data
            if (oldData) {
              if (finalData.models) {
                finalData.models = [...oldData.models, ...finalData.models]
              }
            }
            dispatch({ type: types.FERTILIZER_GUIDES, data: finalData });
          } else if (error) {
            dispatch({ type: types.LOADER, data: false });
            errorCB(error)
          }
        });
      }
    })
      .catch((e) => { })
  };
}

export function getFertilizDetailsItem(id, successCB, errorCB) {
  return (dispatch) => {
    dispatch({ type: types.LOADER, data: true });
    AsyncStorage.getItem('user').then((jsonUser) => {
      if (jsonUser) {
        let user = JSON.parse(jsonUser)
        api.getFertilizDetailsItem(user.token, id, function (success, data, error) {
          if (success) {
            dispatch({ type: types.LOADER, data: false });
            dispatch({ type: types.FERTILIZ_DETAILS, data: data.guide });
            successCB();
          } else if (error) {
            dispatch({ type: types.LOADER, data: false });
            errorCB(error)
          }
        });
      }
    })
      .catch((e) => { })
  };
}

export function registerPushNotification(deviceToken) {
  return (dispatch) => {
    AsyncStorage.getItem('user').then((jsonUser) => {
      if (jsonUser) {
        let user = JSON.parse(jsonUser)
        let deviceTokenToUse = deviceToken
        if (deviceTokenToUse == null) {
          deviceTokenToUse = user.notification_token
        }
        api.registerPushNotification(user.token, deviceTokenToUse, function (success, data, error) {
          if (success) {
            user["notification_token"] = deviceTokenToUse
            user["notificationEnabled"] = true
            AsyncStorage.setItem('user', JSON.stringify(user));
            dispatch({ type: types.USER, data: user });
          } else if (error) {
            dispatch({ type: types.USER, data: null });
          }
        });
      }
    })
      .catch((e) => {
        dispatch({ type: types.USER, data: null });
      })
  };
}

export function unRegisterPushNotification() {
  return (dispatch) => {
    AsyncStorage.getItem('user').then((jsonUser) => {
      if (jsonUser) {
        let user = JSON.parse(jsonUser)
        api.unRegisterPushNotification(user.token, function (success, data, error) {
          if (success) {
            user["notificationEnabled"] = false
            AsyncStorage.setItem('user', JSON.stringify(user));
            dispatch({ type: types.USER, data: user });
          } else if (error) {
            dispatch({ type: types.USER, data: null });
          }
        });
      }
    })
      .catch((e) => {
        dispatch({ type: types.USER, data: null });
      })
  };
}

export function getNotifications(oldData, errorCB, refreshList = false) {
  return (dispatch) => {
    AsyncStorage.getItem('user').then((jsonUser) => {
      if (jsonUser) {
        let user = JSON.parse(jsonUser)
        if (oldData == null) {
          if (refreshList == false) {
            dispatch({ type: types.LOADER, data: true });
          }
        }
        api.getNotifications(user.token, oldData, function (success, data, error) {
          if (success) {
            dispatch({ type: types.LOADER, data: false });
            let finalData = data
            if (oldData) {
              if (finalData.notifications) {
                finalData.notifications = [...oldData.notifications, ...finalData.notifications]
              }
            }
            dispatch({ type: types.ALL_NOTIFICATIONS, data: finalData });
          } else if (error) {
            dispatch({ type: types.LOADER, data: false });
            errorCB(error)
          }
        });
      }
    })
      .catch((e) => { })
  };
}

export function setVacancie(vacancie, successCB) {
  return (dispatch) => {
    AsyncStorage.getItem('user').then((jsonUser) => {
      if (jsonUser) {
        let user = JSON.parse(jsonUser)
        user["vacancie"] = vacancie.id
        user["vacancie_name"] = vacancie.title
        AsyncStorage.setItem('user', JSON.stringify(user));
        dispatch({ type: types.USER, data: user });
        successCB()
      } else {
        dispatch({ type: types.USER, data: null });
      }
    }).catch((e) => { dispatch({ type: types.USER, data: null }) })
  };
}

export function setVacancieEmpty() {
  return (dispatch) => {
    AsyncStorage.getItem('user').then((jsonUser) => {
      if (jsonUser) {
        let user = JSON.parse(jsonUser)
        user["vacancie"] = ''
        user["vacancie_name"] = ''
        AsyncStorage.setItem('user', JSON.stringify(user));
        dispatch({ type: types.USER, data: user });
      } else {
        dispatch({ type: types.USER, data: null });
      }
    }).catch((e) => { dispatch({ type: types.USER, data: null }) })
  };
}
