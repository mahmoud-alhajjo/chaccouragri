import AsyncStorage from '@react-native-community/async-storage';

import createReducer from '../Lib/createReducer'
import * as types from '../Actions/types'


export const Reducer = createReducer({}, {
  [types.LOADER](state, action) {
    var load = action.data;
    state = Object.assign({}, state, {load: load });
    return state;
  },
  [types.USER](state, action) {
    let user = action.data;
    state = Object.assign({}, state, {user: user});
    return state;
  },
  [types.TOGGLE_ABOUTUS_VIEW](state, action) {
    var toggle = action.data;
    state = Object.assign({}, state, {isAboutUsOpened: toggle });
    return state;
  },
  [types.TOGGLE_PROFILE_VIEW](state, action) {
    var toggle = action.data;
    state = Object.assign({}, state, {isProfileOpened: toggle });
    return state;
  },
  [types.META_DATA_SUCCESS](state, action) {
    let data = action.data;
    let dataKeys = Object.keys(data)
    for(let i = 0; i < dataKeys.length; i++){
      let key = dataKeys[i]
      let value = data[dataKeys[i]]
      AsyncStorage.setItem(key, JSON.stringify(value));
    }
    state = Object.assign({}, state, { appVerion: data.app_verion});
    return state;
  },
  [types.OCCUPATIONS](state, action) {
    let occupations = action.data;
    state = Object.assign({}, state, {occupations: occupations});
    return state;
  },
  [types.PROVINCE](state, action) {
    let province = action.data;
    state = Object.assign({}, state, {province: province});
    return state;
  },
  [types.CITY](state, action) {
    let cities = action.data;
    state = Object.assign({}, state, {cities: cities});
    return state;
  },
  [types.GENDER](state, action) {
    let gender = action.data;
    state = Object.assign({}, state, {gender: gender});
    return state;
  },
  [types.AREA_SIZE](state, action) {
    let areaSize = action.data;
    state = Object.assign({}, state, {areaSize: areaSize});
    return state;
  },
  [types.SPECIALIZATION](state, action) {
    let specialization = action.data;
    state = Object.assign({}, state, {specialization: specialization});
    return state;
  },
  [types.CONTACT_US_SUBJECTS](state, action) {
    let subjects = action.data;
    state = Object.assign({}, state, {subjects: subjects});
    return state;
  },
  [types.SALESMAN](state, action) {
    let salesman = action.data;
    state = Object.assign({}, state, {salesman: salesman});
    return state;
  },
  [types.MAIN_INTERESTS](state, action) {
    let mainInterests = action.data;
    state = Object.assign({}, state, {mainInterests: mainInterests});
    return state;
  },
  [types.SUB_INTERESTS](state, action) {
    let subInterests = action.data;
    state = Object.assign({}, state, {subInterests: subInterests});
    return state;
  },
  [types.SIGNUP_SUCESS](state, action) {
    let nonActiveUser = action.data;
    AsyncStorage.setItem('nonActiveUser', JSON.stringify(nonActiveUser));
    state = Object.assign({}, state, {nonActiveUser: nonActiveUser});
    return state;
  },
  [types.NONE_ACTIVE_USER](state, action) {
    let nonActiveUser = action.data;
    state = Object.assign({}, state, {nonActiveUser: nonActiveUser});
    return state;
  },
  [types.REMOVE_NONE_ACTIVE_USER](state, action) {
    AsyncStorage.removeItem('nonActiveUser');
    state = Object.assign({}, state, null);
    return state;
  },
  [types.TOGGLE_VERIFICATION_VIEW](state, action) {
    var toggle = action.data;
    state = Object.assign({}, state, {isVerificationViewOpened: toggle });
    return state;
  },
  [types.GO_TO_NOTIFICATION](state, action) {
    var toggle = action.data;
    state = Object.assign({}, state, {notification: toggle });
    return state;
  },
  [types.LOGIN_SUCCESS](state, action) {
    let user = action.data;
    user["productsIdWatching"] = []
    user["categoryChecker"] = []
    user["notificationEnabled"] = true
    AsyncStorage.getItem('user').then((jsonUser) => {
      if(jsonUser) {
        let oldUser = JSON.parse(jsonUser)
        if(oldUser.productsIdWatching) {
          user["productsIdWatching"] = oldUser.productsIdWatching
        }
        if(oldUser.categoryChecker) {
          user["categoryChecker"] = oldUser.categoryChecker
        }
        if(oldUser.notifications) {
          user["notifications"] = oldUser.notifications
        }
        if(oldUser.notification_token) {
          user["notification_token"] = oldUser.notification_token
        }
        if(oldUser.notificationEnabled != null) {
          user["notificationEnabled"] = oldUser.notificationEnabled
        }
      }
      AsyncStorage.setItem('user', JSON.stringify(user));
    })
    .catch((e) => {
      AsyncStorage.setItem('user', JSON.stringify(user));
    })
    state = Object.assign({}, state, null);
    return state;
  },
  [types.EDIT_PROFILE](state, action) {
    let user = action.data;
    user["productsIdWatching"] = []
    user["categoryChecker"] = []
    user["notificationEnabled"] = false
    AsyncStorage.getItem('user').then((jsonUser) => {
      if(jsonUser) {
        let oldUser = JSON.parse(jsonUser)
        if(oldUser.productsIdWatching) {
          user["productsIdWatching"] = oldUser.productsIdWatching
        }
        if(oldUser.categoryChecker) {
          user["categoryChecker"] = oldUser.categoryChecker
        }
        if(oldUser.notifications) {
          user["notifications"] = oldUser.notifications
        }
        if(oldUser.notification_token) {
          user["notification_token"] = oldUser.notification_token
        }
        if(oldUser.notificationEnabled != null) {
          user["notificationEnabled"] = oldUser.notificationEnabled
        }
      }
      AsyncStorage.setItem('user', JSON.stringify(user));
    })
    .catch((e) => {
      AsyncStorage.setItem('user', JSON.stringify(user));
    })
    state = Object.assign({}, state, {user: user});
    return state;
  },
  [types.CATEGORY_DETAILS_PRODUCTS](state, action) {
    var cateogryPorducts = action.data;
    state = Object.assign({}, state, {cateogryPorducts: cateogryPorducts });
    return state;
  },
  [types.PRODUCT_DETAILS](state, action) {
    var productDetails = action.data;
    state = Object.assign({}, state, {productDetails: productDetails });
    return state;
  },
  [types.FACEBOOK_POSTS](state, action) {
    let posts = action.data;
    for (var i = 0; i < posts.data.length; i++) {
      let item = posts.data[i]
      item["cellType"] = "small"
      let postImage = null
      if(item.attachments) {
        if(item.attachments.data) {
          if(item.attachments.data.length > 0) {
            let firstAttachment = null
            for (var j = 0; j < item.attachments.data.length; j++) {
              if(item.attachments.data[j].type == 'photo' || item.attachments.data[j].type == 'album') {
                firstAttachment = item.attachments.data[j]
                break
              }
            }
            if(firstAttachment) {
              item["linkWeb"] = firstAttachment.url
              if(firstAttachment.media) {
                if(firstAttachment.media.image) {
                  if(firstAttachment.media.image.src) {
                    postImage = firstAttachment.media.image.src
                  }
                }
              } else if(firstAttachment.subattachments) {
                  if(firstAttachment.subattachments.data) {
                    if(firstAttachment.subattachments.data.length > 0) {
                      let subFirstAttachemnt = null
                      for (var j = 0; j < firstAttachment.subattachments.data.length; j++) {
                        if(firstAttachment.subattachments.data[j].type == 'photo') {
                          subFirstAttachemnt = firstAttachment.subattachments.data[j]
                          break
                        }
                      }

                      if(subFirstAttachemnt) {
                        if(subFirstAttachemnt.media) {
                          if(subFirstAttachemnt.media.image) {
                            if(subFirstAttachemnt.media.image.src) {
                              postImage = subFirstAttachemnt.media.image.src
                            }
                          }
                        }
                      }
                    }
                  }
                }
            }
          }
        }
      }
      item['postImage'] = postImage

    }

    for (var i = 0; i < posts.data.length; i = i+3) {
      let randomValue = posts.data[Math.floor(Math.random() * (posts.data.length - 1))];
      randomValue.cellType = 'big'
    }
    state = Object.assign({}, state, {fbPosts: posts});
    return state;
  },
  [types.FACEBOOK_POSTS_CMS](state, action) {
    let posts = action.data;
    for (var i = 0; i < posts.posts.length; i++) {
      let item = posts.posts[i]
      item["cellType"] = "small"
    }
    for (var i = 0; i < posts.posts.length; i = i+3) {
      let randomValue = posts.posts[Math.floor(Math.random() * (posts.posts.length - 1))];
      randomValue.cellType = 'big'
    }
    state = Object.assign({}, state, {fbPosts: posts });
    return state;
  },
  [types.LOGOUT](state, action) {
    AsyncStorage.clear();
    state = Object.assign({}, null, null);
    return state;
  },
  [types.LOGGEDOUT](state, action) {
    var loggedOut = action.data;
    state = Object.assign({}, state, {loggedOut: loggedOut });
    return state;
  },
  [types.CURRENT_LOCATION](state, action) {
    let userLocation = action.data;
    state = Object.assign({}, state, { userLocation: userLocation });
    return state;
  },
  [types.WEATHER_INFORMATION](state, action) {
    let weatherInformation = action.data;
    state = Object.assign({}, state, { weatherInformation: weatherInformation });
    return state;
  },
  [types.VACANCIES](state, action) {
    let vacancies = action.data;
    state = Object.assign({}, state, {vacancies: vacancies});
    return state;
  },
  [types.FERTILIZER_GUIDES](state, action) {
    let fertilizerGuides = action.data;
    state = Object.assign({}, state, {fertilizerGuides: fertilizerGuides});
    return state;
  },
  [types.FERTILIZ_DETAILS](state, action) {
    let fertilizerDetails = action.data;
    state = Object.assign({}, state, {fertilizerDetails: fertilizerDetails});
    return state;
  },
  [types.ALL_NOTIFICATIONS](state, action) {
    let notifications = action.data;
    state = Object.assign({}, state, {notifications: notifications});
    return state;
  },
});
