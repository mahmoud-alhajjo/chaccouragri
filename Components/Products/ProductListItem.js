/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  Text,
} from 'react-native';

import mainStyles from '../../css/MainStyle';
import colors from '../../css/Colors';
import * as fonts from '../../css/Fonts';
import ResponsiveImage from '../Helpers/ResponsiveImage'

import * as Animatable from 'react-native-animatable';
import Dimensions from 'Dimensions';
import LinearGradient from 'react-native-linear-gradient';

import {connect} from 'react-redux';
import { bindActionCreators } from 'redux'
import { ActionCreators } from '../../Actions'

export  class ProductListItem extends Component {
  
  itemNew = false

  constructor() {
    super();
    this.state = {
    }
  }

  render() {
    let productImageUrl = this.props.item.thumbnail ? this.props.item.thumbnail : ""
    let productImage = productImageUrl != "" ? {uri: productImageUrl} : require('../../Images/dummy.jpg')
    let stripNewProduct = null
    
    if (this.props.item.is_new === "1") {
        stripNewProduct = <View style={styles.stripNewProduct}>
                              <Text style={styles.textNewProduct}>جديد</Text>
                              <View style={styles.triangleCorner}></View>
                          </View>

        for (let i = 0; i < this.props.user.productsIdWatching.length; i++) {
            if (this.props.item.id === this.props.user.productsIdWatching[i]) {
                stripNewProduct = null
                break
            }
        }
    }
 
    let size = null
    if((this.props.item.package_size) || (this.props.item.carton_size)){
      if(this.props.item.package_size) {
        size = this.props.item.package_size
      } else if (this.props.item.carton_size) {
        size = this.props.item.carton_size
      }
    }
    return (
      <Animatable.View animation="fadeInRightBig" delay={this.props.index * 100}>
        <TouchableOpacity style={[styles.container]} onPress={() => this.props.itemClick(this.props.item)}>
          <View style={styles.backgroundImage}>
            <ResponsiveImage style={mainStyles.flex} source={productImage} />
          </View>
          <LinearGradient
            colors={["rgba(0, 0, 0, 0.0)", "rgba(0, 0, 0, 0.29)", "rgba(0, 0, 0, 0.8)"]}
            start={{x: 0.0, y: 0.0}}
            end={{x: 0.0, y: 1.0}}
            style={{height: "100%", width:"100%", justifyContent:'flex-end', padding: 18, borderRadius: 10}}
          >
            <Text style={styles.productName}>
              {this.props.item.name_ar}
            </Text>
            <Text style={styles.productNameEn}>
              {this.props.item.name}
            </Text>
          </LinearGradient>
          {stripNewProduct}
        </TouchableOpacity>
      </Animatable.View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: Dimensions.get('window').height * 0.299,
    borderRadius: 10,
    marginBottom: 15,
    marginLeft: 18,
    marginRight: 18,
    backgroundColor: colors.slate,
    overflow: 'hidden',
  },
  backgroundImage: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
  productName: {
    fontFamily: fonts.Bold,
    fontSize: 20,
    lineHeight: 30,
    letterSpacing: 0,
    textAlign: "right",
    color: colors.white
  },
  productNameEn: {
    marginTop: 10,
    fontFamily: "LucidaGrande",
    fontWeight: "bold",
    fontSize: 20,
    lineHeight: 30,
    letterSpacing: 0,
    textAlign: "right",
    color: colors.white
  },
  descriptionText: {
    fontFamily: fonts.Medium,
    fontSize: 16,
    lineHeight: 26,
    letterSpacing: 0,
    textAlign: "right",
    color: colors.white
  },
  stripNewProduct: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: 80,
    height:80
  },
  triangleCorner: {
    backgroundColor: 'transparent',
    borderStyle: 'solid',
    borderRightWidth: 80,
    borderTopWidth: 80,
    borderRightColor: 'transparent',
    borderTopColor: colors.orange
  },
  textNewProduct: {  
    position: 'absolute',
    top: 20,
    left: -10,
    width: 60,
    transform: [{ rotate: '315deg'}],
    fontSize: 20,
    color: colors.white,
    fontFamily: fonts.Medium,
    zIndex: 100
  }
});


function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps(state) {
  return {
    user: state.Reducer.user,
    cateogryPorducts: state.Reducer.cateogryPorducts
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductListItem);
