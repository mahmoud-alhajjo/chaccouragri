/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  FlatList,
  RefreshControl,
} from 'react-native';

import CategoryListItem from './CategoryListItem'
import ProductCategories from './ProductCategories'
import ConfirmationPopup from '../Popup/ConfirmationPopup'
import MainProductsLoader from './MainProductsLoader'

import mainStyles from '../../css/MainStyle';
import colors from '../../css/Colors';
import * as AppConstants from '../../Config/AppConstants';

import Masonry from 'react-native-masonry-layout';
import Dimensions from 'Dimensions';
import Modal from "react-native-modal";
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux'
import { ActionCreators } from '../../Actions'
import Spinner from 'react-native-spinkit'

export class CategoryList extends Component {

  confirmVerificationButton = false
  getList = true

  constructor() {
    super();
    this.state = {
      data: null,
      toggleProductCategories: false,
      isRefreshing: false,
      getNextData: false,

      errorModal: false,
      errorMessage: "",
      requestError: null,
    }
  }

  componentDidMount() {
    this.getList = true
    this.props.getUserFromLocal()    
    setTimeout(() => {
      this.props.getProductsForCategory(this.props.productType, null, this.onGetDataSucess, this.onGetDataFailure)
    }, 250);
  }

  onGetDataSucess = (data) => {
    for (var i = 0; i < data.results.length; i++) {
      let currentRow = (Math.ceil(i / 2))
      data.results[i]["index"] = i
      data.results[i]["height"] = i % 2 == 0 ? Dimensions.get('window').height * 0.361 : currentRow % 2 == 0 ? Dimensions.get('window').height * 0.235 : Dimensions.get('window').height * 0.403
    }
    this.setState({
      data: data,
      isRefreshing: false
    }, () => {
      if(this.refs.list) {
        this.refs.list.clear();
      }
      setTimeout(() => {
        if(this.refs.list) {
          this.refs.list.addItems(data.results);
        }
      }, 250);
    });
  }

  onGetDataFailure = (error) => {
    this.setState({data: null, errorMessage:error.errorMessage, requestError: error, errorModal: true, isRefreshing: false})
  }

  onGetDataCategoryPorductSucess = () => {
    this.openProductCategoriesAction()
  }

  onGetDataCategoryPorductFailure = (error) => {
    this.setState({errorMessage:error.errorMessage, requestError: error, errorModal: true})
  }

  itemClicked = (item) => {
    this.getList = false
    this.props.logEvent(AppConstants.Porduct, {category: this.props.productType.charAt(0).toUpperCase() + this.props.productType.slice(1), productName: item.name})
    this.props.getProductsForCategoryDetails(this.props.productType, item.id, null, this.onGetDataCategoryPorductSucess, this.onGetDataCategoryPorductFailure)
  }

  openProductCategoriesAction = () => {
    this.setState({toggleProductCategories: true})
  }

  closeProductCategoriesAction = () => {
    this.setState({toggleProductCategories: false})
  }

  confirmVerificationPopup = () => {
    this.confirmVerificationButton = true
    this.setState({errorModal: false})
  }

  closeVerificationPopup = () => {
    this.confirmVerificationButton = false
    this.setState({errorModal: false})
  }

  onErrorModalHide = () => {
    if(this.confirmVerificationButton) {
      if(this.state.requestError) {
        if(this.state.requestError.shouldRetry) {
          if(this.getList) {
            this.props.getProductsForCategory(this.props.productType, null, this.onGetDataSucess, this.onGetDataFailure)
          } else {
            this.props.getProductsForCategoryDetails(this.props.productType, item.id, null, this.onGetDataCategoryPorductSucess, this.onGetDataCategoryPorductFailure)
          }
        } else if(this.state.requestError.shouldLogout) {
          this.props.logoutAndDeleteAllData(this.props.navigation)
        }
      }
    }
  }

  refreshList = () => {
    this.getList = true
    this.setState({isRefreshing: true})
    this.props.getProductsForCategory(this.props.productType, null, this.onGetDataSucess, this.onGetDataFailure)
  }

  loadNextPage = (event) => {
    const scrollHeight = Math.floor( event.nativeEvent.contentOffset.y + event.nativeEvent.layoutMeasurement.height );
    const height = Math.floor( event.nativeEvent.contentSize.height );
    if (scrollHeight >= height - 10) {
      if((this.state.data.offset) && (!this.state.getNextData)) {
        this.setState({
          getNextData: true
        }, () => {
          this.getList = true
          this.props.getProductsForCategory(this.props.productType, this.state.data.offset, this.onGetNextPageSucess, this.onGetNextPageFailure)
        });
      }
    }
  }

  onGetNextPageSucess = (data) => {
    for (var i = 0; i < data.results.length; i++) {
      let currentRow = (Math.ceil(i / 2))
      data.results[i]["index"] = i
      data.results[i]["height"] = i % 2 == 0 ? Dimensions.get('window').height * 0.361 : currentRow % 2 == 0 ? Dimensions.get('window').height * 0.235 : Dimensions.get('window').height * 0.403
    }
    let dataToList = data.results
    data.results = [...this.state.data.results, data.results]
    this.setState({
      data: data,
      getNextData: false
    }, () => {
      if(this.refs.list) {
        this.refs.list.addItems(dataToList);
      }
    });

  }

  onGetNextPageFailure = (error) => {
    this.setState({errorMessage:error.errorMessage, requestError: error, errorModal: true, getNextData: false})
  }

  renderItem = (item) => {
    return (
      <CategoryListItem item={item} row={(Math.ceil(item.index / 2))} index={item.index} totalCount={this.state.data ? this.state.data.results.length : 0} itemClick={(item) => this.itemClicked(item)} />
    )
  }

  render() {
    return (
      <View style={[mainStyles.flex, styles.container]}>
        <View style={[mainStyles.flex, {display: this.state.data == null ? 'flex' : 'none'}]}>
          <MainProductsLoader />
        </View>
        <Masonry
          ref="list"
          renderItem={item => this.renderItem(item)}
          keyExtractor={(item, index) => index.toString()}
          showsVerticalScrollIndicator={false}
          columns={2}
          containerStyle={{display: this.state.data == null ? 'none' : 'flex'}}
          removeClippedSubviews={true}
          onScroll={this.loadNextPage}
          refreshControl={
                          ((this.state.data != null) || (this.state.requestError != null)) ?
                          <RefreshControl
                              tintColor={colors.fernGreen}
                              colors={[colors.fernGreen, colors.fernGreenTwo]}
                              refreshing={this.state.isRefreshing}
                              onRefresh={this.refreshList}
                          /> : null
                        }
          style={{transform: [{ scaleX: -1 }]}}
        />
        <View style={{justifyContent: 'center', alignItems:'center', display: this.state.getNextData ? 'flex' : 'none'}}>
          <Spinner isVisible={this.state.getNextData} size={40} color={colors.fernGreen} type={"ThreeBounce"} />
        </View>
        <Modal
            transparent={true}
            onBackButtonPress={() => this.closeProductCategoriesAction()}
            style={{marginLeft:0, marginRight:0, marginTop:0, marginBottom:0}}
            isVisible={this.state.toggleProductCategories}
            animationIn={"bounceInUp"}
            animationOut={"bounceOutDown"}
            animationInTiming={900}
            animationOutTiming={900}
          >
          <ProductCategories productType={this.props.productType} closeAction={this.closeProductCategoriesAction} />
        </Modal>
        <Modal
            transparent={true}
            style={{justifyContent: 'center', backgroundColor: 'transparent'}}
            isVisible={this.state.errorModal}
            animationIn={"bounceInUp"}
            animationOut={"bounceOutDown"}
            animationInTiming={1000}
            animationOutTiming={1000}
            onModalHide={() => this.onErrorModalHide()}
          >
          <ConfirmationPopup
            mainTitle={this.state.requestError ? "عذراً" : "تهانينا"}
            description={this.state.errorMessage}
            oneButton={this.state.requestError ? !this.state.requestError.shouldRetry : true}
            confirmText={this.state.requestError ? this.state.requestError.shouldRetry ? "إعادة" : "إغلاق" : "متابعة"}
            rejectText={"إغلاق"}
            confirm={this.confirmVerificationPopup}
            cancel={this.closeVerificationPopup} />
        </Modal>
      </View>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps(state) {
  return {
    cateogryPorducts: state.Reducer.cateogryPorducts
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(CategoryList);

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.paleGrey
  }
});
