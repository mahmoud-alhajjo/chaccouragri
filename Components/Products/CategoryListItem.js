/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  Text,
} from 'react-native';

import mainStyles from '../../css/MainStyle';
import colors from '../../css/Colors';
import * as fonts from '../../css/Fonts';
import ResponsiveImage from '../Helpers/ResponsiveImage'

import * as Animatable from 'react-native-animatable';
import Dimensions from 'Dimensions';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
import { ActionCreators } from '../../Actions'

export class CategoryListItem extends Component {
  stripNewCategory = null
  constructor() {
    super();
    this.state = {

    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.item.is_new === "1") {
      this.stripNewCategory = <View style={styles.stripNewCategory}>
        <Text style={styles.textNewCategory}>جديد</Text>
        <View style={styles.triangleCorner}></View>
      </View>
      if (this.props.user) {
        if (this.props.user.categoryChecker) {
          for (let i = 0; i < this.props.user.categoryChecker.length; i++) {
            if (this.props.item.name === this.props.user.categoryChecker[i]) {
              this.stripNewCategory = null
              break
            }
          }
        }
      }
      if (prevProps) {
        if (prevProps.user.categoryChecker) {
          for (let i = 0; i < prevProps.user.categoryChecker.length; i++) {
            if (prevProps.item.name === prevProps.user.categoryChecker[i]) {
              this.stripNewCategory = null
              break
            }
          }
        }
      }
    }
  }

  render() {

    let productImageUrl = this.props.item.thumbnail ? this.props.item.thumbnail : ""
    let productImage = productImageUrl != "" ? { uri: productImageUrl } : require('../../Images/dummy.jpg')


    return (
      <Animatable.View animation="fadeInUpBig" delay={this.props.index * 100}>
        <TouchableOpacity
          style={[
            styles.container,
            {
              marginRight: this.props.item.index % 2 != 0 ? 6 : 3,
              marginLeft: this.props.item.index % 2 == 0 ? 6 : 3,
              marginTop: this.props.item.row === 0 ? 8 : 4,
              marginBottom: (this.props.item.row === Math.ceil(((this.props.totalCount / 2)) - 1)) ? 8 : 4,
              height: this.props.item.height,
            }
          ]}
          onPress={() => this.props.itemClick(this.props.item)}
        >
          <ResponsiveImage style={styles.productImage} source={productImage} />
          {this.stripNewCategory}
          <View style={styles.productTextView}>
            <Text numberOfLines={2} style={styles.productText}>
              {this.props.item.arabic_name}
            </Text>
          </View>
        </TouchableOpacity>
      </Animatable.View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: (Dimensions.get('window').width - 18) / 2,
    borderRadius: 4,
    backgroundColor: colors.white,
    shadowColor: "rgba(45, 45, 84, 0.16)",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowRadius: 4,
    shadowOpacity: 1,
    elevation: 5,
    overflow: 'hidden',
    transform: [{ scaleX: -1 }]
  },
  productImage: {
    borderTopLeftRadius: 4,
    borderTopRightRadius: 4,
    height: "75%",
    width: "100%",
  },
  productText: {
    fontFamily: fonts.Medium,
    fontSize: 18,
    letterSpacing: 0,
    textAlign: "center",
    color: colors.slate
  },
  productTextView: {
    alignItems: 'center',
    justifyContent: 'center',
    maxHeight: "25%",
    paddingLeft: 9,
    paddingRight: 9,
    paddingTop: 10,
    paddingBottom: 10,
  },
  stripNewCategory: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: 60,
    height: 60
  },
  triangleCorner: {
    backgroundColor: 'transparent',
    borderStyle: 'solid',
    borderRightWidth: 60,
    borderTopWidth: 60,
    borderRightColor: 'transparent',
    borderTopColor: colors.orange
  },
  textNewCategory: {
    position: 'absolute',
    top: 12,
    left: -5,
    width: 45,
    transform: [{ rotate: '315deg' }],
    fontSize: 18,
    color: colors.white,
    fontFamily: fonts.Medium,
    zIndex: 100
  }
});

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps(state) {
  return {
    user: state.Reducer.user,
    cateogryPorducts: state.Reducer.cateogryPorducts
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(CategoryListItem);