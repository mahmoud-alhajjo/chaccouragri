/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
} from 'react-native';

import mainStyles from '../../css/MainStyle';
import colors from '../../css/Colors';

import Dimensions from 'Dimensions';
import ContentLoader from 'react-native-content-loader'
import {Circle, Rect} from 'react-native-svg'

export default class MainProductsLoader extends Component {

  constructor() {
    super();
    this.state = {
    }
  }

  render() {
    return (
      <View style={[mainStyles.flex, styles.container]}>
        <View style={[mainStyles.rowFlex, {justifyContent: 'space-between'}]}>
          <View>
            <ContentLoader primaryColor={colors.fernGreen}
                           primaryColor={colors.fernGreenTwo}
                           duration={1500}
                           height={Dimensions.get('window').height * 0.361}
                           width={(Dimensions.get('window').width - 18) / 2}>
                <Rect x="0" y="0" rx="0" ry="4" width="100%" height="100%"/>
            </ContentLoader>
            <View style={{height:4}} />
            <ContentLoader primaryColor={colors.fernGreen}
                             primaryColor={colors.fernGreenTwo}
                             duration={1500}
                             height={Dimensions.get('window').height * 0.361}
                             width={(Dimensions.get('window').width - 18) / 2}>
                <Rect x="0" y="0" rx="4" ry="4" width="100%" height="100%"/>
            </ContentLoader>
            <View style={{height:4}} />
            <ContentLoader primaryColor={colors.fernGreen}
                           primaryColor={colors.fernGreenTwo}
                           duration={1500}
                           height={Dimensions.get('window').height * 0.361}
                           width={(Dimensions.get('window').width - 18) / 2}>
                <Rect x="0" y="0" rx="4" ry="4" width="100%" height="100%"/>
            </ContentLoader>
          </View>
          <View style={{width:3}} />
          <View>
            <ContentLoader primaryColor={colors.fernGreen}
                             primaryColor={colors.fernGreenTwo}
                             duration={1500}
                             height={Dimensions.get('window').height * 0.235}
                             width={(Dimensions.get('window').width - 18) / 2}>
                <Rect x="0" y="0" rx="4" ry="4" width="100%" height="100%"/>
            </ContentLoader>
            <View style={{height:4}} />
            <ContentLoader primaryColor={colors.fernGreen}
                             primaryColor={colors.fernGreenTwo}
                             duration={1500}
                             height={Dimensions.get('window').height * 0.403}
                             width={(Dimensions.get('window').width - 18) / 2}>
                <Rect x="0" y="0" rx="4" ry="4" width="100%" height="100%"/>
            </ContentLoader>
            <View style={{height:4}} />
            <ContentLoader primaryColor={colors.fernGreen}
                               primaryColor={colors.fernGreenTwo}
                               duration={1500}
                               height={Dimensions.get('window').height * 0.235}
                               width={(Dimensions.get('window').width - 18) / 2}>
                  <Rect x="0" y="0" rx="4" ry="4" width="100%" height="100%"/>
            </ContentLoader>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginLeft: 6,
    marginRight: 6,
    marginTop: 8,
    marginBottom: 8,
  }
});
