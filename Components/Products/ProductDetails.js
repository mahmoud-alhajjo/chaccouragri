/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  FlatList,
  Platform,
  LayoutAnimation,
  UIManager,
  ScrollView,
  Text,
  Image,
  TouchableOpacity,
  Linking
} from 'react-native';

import mainStyles from '../../css/MainStyle';
import colors from '../../css/Colors';
import * as fonts from '../../css/Fonts';
import * as constants from '../../Config/AppConstants';

import ProductDetailsItem from './ProductDetailsItem'
import ResponsiveImage from '../Helpers/ResponsiveImage'

import Swiper from 'react-native-swiper';
import Dimensions from 'Dimensions';
import LinearGradient from 'react-native-linear-gradient';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
import { ActionCreators } from '../../Actions'

export class ProductDetails extends Component {

  constructor() {
    super();
    if (Platform.OS === 'android') {
      UIManager.setLayoutAnimationEnabledExperimental(true);
    }
    this.state = {
      refresh: false,
    };
  }

  componentDidMount() {
    if (this.props.productDetails.is_new === "1") {
      this.props.newProductWatchingDone(this.props.productDetails.id)
    }
  }

  renderPagination = (index, total) => {
    if (total === 1) {
      return null
    }
    var dots = []
    for (var i = 0; i < total; i++) {
      dots.push(<View key={i} style={{ width: 10, height: 10, backgroundColor: colors.slate, borderRadius: 5, opacity: index == i ? 1 : 0.5, marginRight: (i == (total - 1)) ? null : 20 }} />)
    }
    return (
      <LinearGradient
        colors={["rgba(255, 255, 255, 0.0)", "rgba(255, 255, 255, 1.0)"]}
        start={{ x: 0.0, y: 0.0 }}
        end={{ x: 0.0, y: 1.0 }}
        style={{ position: 'absolute', left: 0, right: 0, bottom: 0, height: 50, justifyContent: 'flex-end', alignItems: 'center', padding: 10 }}
      >
        <View style={mainStyles.rowFlex}>
          {dots}
        </View>
      </LinearGradient>
    )
  }

  itemClick = (item, index) => {
    if (item.type != constants.TypeFile) {
      LayoutAnimation.configureNext({
        duration: 250,
        update: {
          type: LayoutAnimation.Types.linear,
        }
      });
      let data = this.props.productDetails.details
      data[index].expanded = !data[index].expanded
      this.setState({ refresh: true })

    } else {
      let itemUrl = item.value
      Linking.canOpenURL(itemUrl).then(supported => {
        if (supported) {
          Linking.openURL(itemUrl);
        }
      });
    }
  }

  backAction = () => {
    this.props.closeAction()
  }

  render() {
    let productsIdNew = []
    let productsWatching = this.props.user.productsIdWatching
    if (this.props.cateogryPorducts) {
      for (let i = 0; i < this.props.cateogryPorducts.result.products.length; i++) {
        if (this.props.cateogryPorducts.result.products[i].is_new === "1") {
          productsIdNew.push(this.props.cateogryPorducts.result.products[i].id)
        }
      }
      if (this.props.cateogryPorducts.result.is_new === "1") {
        this.props.CategoryChecker(productsWatching, productsIdNew, this.props.cateogryPorducts.result.name)
      }
    }

    let images = null
    if (this.props.productDetails.images.length != 0) {
      images = this.props.productDetails.images.map((item) => {
        let productImage = { uri: item }
        return (
          <ResponsiveImage key={item} style={mainStyles.flex} source={productImage} />
        )
      });
      images = images.reverse()
    }
    return (
      <View style={[mainStyles.flex, styles.container]}>
        <View style={styles.headerView}>
          <View style={[mainStyles.rowFlex, { alignItems: 'center', justifyContent: 'space-between' }]}>
            <View style={{ width: 70 }} />
            <Text style={[mainStyles.flex, styles.headerText]}>
              {this.props.productDetails.name_ar}
            </Text>
            <TouchableOpacity style={[styles.closeButton, { alignItems: 'flex-end' }]} onPress={this.backAction}>
              <Image resizeMode={'contain'} style={styles.closeIcon} source={require('../../Images/close.png')} />
            </TouchableOpacity>
          </View>
          <Text style={styles.englishHeaderText}>
            {this.props.productDetails.name_en}
          </Text>
        </View>
        <ScrollView>
          {
            images ?
              <View>
                <Swiper index={this.props.productDetails.images.length - 1} height={Dimensions.get('window').height * 0.65} autoplay={false} showsButtons={false} showsPagination={true} loop={true} scrollEnabled={true} renderPagination={(index, total, context) => this.renderPagination(index, total)} >
                  {images}
                </Swiper>
              </View> : null
          }
          <FlatList
            ref={(ref) => { this.flatListRef = ref; }}
            data={this.props.productDetails.details}
            extraData={this.state}
            renderItem={({ item, index }) =>
              <ProductDetailsItem item={item} index={index} totalCount={this.props.productDetails.details.length} itemClick={(item, index) => this.itemClick(item, index)} />
            }
            showsVerticalScrollIndicator={false}
            keyExtractor={(item, index) => index.toString()}
          />
        </ScrollView>
      </View>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps(state) {
  return {
    productDetails: state.Reducer.productDetails,
    user: state.Reducer.user,
    cateogryPorducts: state.Reducer.cateogryPorducts
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductDetails);


const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.white
  },
  headerView: {
    paddingTop: 26,
    paddingBottom: 17,
    backgroundColor: colors.whiteTwo
  },
  headerText: {
    fontFamily: fonts.Bold,
    fontSize: 30,
    lineHeight: 30,
    letterSpacing: 0,
    textAlign: "center",
    color: colors.fernGreen
  },
  englishHeaderText: {
    marginTop: 6,
    fontFamily: "LucidaGrande",
    fontSize: 22,
    fontWeight: "normal",
    fontStyle: "normal",
    lineHeight: 30,
    letterSpacing: 0,
    textAlign: "center",
    color: colors.coolGrey
  },
  closeButton: {
    width: 70,
    height: 45,
    justifyContent: 'center',
  },
  closeIcon: {
    marginRight: 19,
    width: 14,
    height: 14,
    tintColor: colors.fernGreen,
  },
});
