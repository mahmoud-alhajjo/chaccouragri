/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
} from 'react-native';

import mainStyles from '../../css/MainStyle';
import colors from '../../css/Colors';
import * as fonts from '../../css/Fonts';
import * as AppConstants from '../../Config/AppConstants';

import Dimensions from 'Dimensions';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux'
import { ActionCreators } from '../../Actions'

export class ProductTabs extends Component {

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this._listener = this.props.scrollValue.addListener(this.setAnimationValue.bind(this));
  }

  setAnimationValue({ value, }) {
    this.mainTabView.setNativeProps({
      style: {
        left: value * ((Dimensions.get('window').width)/3),
      },
    });
  }

  goToPage = (index) => {
    if(index == 0) {
      this.props.logEvent(AppConstants.MainCategory, {"category": 'Pesticides'})
    } else if (index == 1) {
      this.props.logEvent(AppConstants.MainCategory, {"category": 'Fertilizers'})
    } else if (index == 2) {
      this.props.logEvent(AppConstants.MainCategory, {"category": 'Seeds'})
    }
    this.props.goToPage(index)
  }

  render() {
    return (
      <View style={[mainStyles.rowFlex, tabsStyles.container]}>
        <View ref={(ref) => { this.mainTabView = ref; }} style={[tabsStyles.topSelectedView, {left: 0}]} />
        <TouchableOpacity style={tabsStyles.tabView} onPress={() => this.goToPage(0)}>
          <Text style={[tabsStyles.tabText, this.props.activeTab == 0 ? tabsStyles.selectedTabText : tabsStyles.notSelectedTabText]}>
            مبيدات
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={tabsStyles.tabView} onPress={() => this.goToPage(1)}>
          <Text style={[tabsStyles.tabText, this.props.activeTab == 1 ? tabsStyles.selectedTabText : tabsStyles.notSelectedTabText]}>
            أسمدة
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={[tabsStyles.tabView, {flexDirection: 'row'}]} onPress={() => this.goToPage(2)}>
          <Text style={[tabsStyles.tabText, this.props.activeTab == 2 ? tabsStyles.selectedTabText : tabsStyles.notSelectedTabText]}>
            بذور
          </Text>
        </TouchableOpacity>
      </View>
    );
  }

}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps(state) {
  return {
    categoryInfo: state.Reducer.categoryInfo
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductTabs);

const tabsStyles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'space-between',
    height: 45,
    backgroundColor: colors.fernGreenTwo
  },
  topSelectedView: {
    position: 'absolute',
    bottom:0,
    width:'33.33%',
    height: 3,
    backgroundColor: colors.white
  },
  tabText: {
    fontFamily: fonts.Bold,
    fontSize: 14,
    letterSpacing: 0,
    textAlign: "center",
    color: colors.white
  },
  selectedTabText: {
    opacity: 1,
  },
  notSelectedTabText: {
    opacity: 0.4,
  },
  tabView: {
    width:(Dimensions.get('window').width)/3,
    height:"100%",
    alignItems: 'center',
    justifyContent: 'center'
  }
});
