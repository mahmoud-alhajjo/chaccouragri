/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
} from 'react-native';

import mainStyles from '../../css/MainStyle';
import colors from '../../css/Colors';
import * as fonts from '../../css/Fonts';
import Header from '../Main Structure/Header'

import CategoryList from './CategoryList'
import ProductTabs from './ProductTabs'
import * as constants from '../../Config/AppConstants';
import * as AppConstants from '../../Config/AppConstants';

import Dimensions from 'Dimensions';
import ScrollableTabView from 'react-native-scrollable-tab-view'
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux'
import { ActionCreators } from '../../Actions'

export class MainProducts extends Component {

  constructor() {
    super();
    this.state = {
    }
  }

  componentDidMount() {
    this.props.logEvent(AppConstants.MainCategory, {"category": 'Seeds'})
  }

  render() {
    return (
      <View style={[mainStyles.flex, styles.container]}>
        <Header navigation={this.props.navigation} />
          <ScrollableTabView ref={(ref) => { this.pagerView = ref; }} initialPage={2} locked={false} renderTabBar={() => <ProductTabs />}>
            <CategoryList productType={constants.Pesticide} />
            <CategoryList productType={constants.Fertilizer} />
            <CategoryList productType={constants.Seeds} />
          </ScrollableTabView>
      </View>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps(state) {
  return {
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(MainProducts);

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.paleGrey
  }
});
