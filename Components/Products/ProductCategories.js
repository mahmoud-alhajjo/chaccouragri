/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  ScrollView,
  TouchableOpacity,
  Image,
  Text,
  FlatList,
  Animated
} from 'react-native';

import mainStyles from '../../css/MainStyle';
import colors from '../../css/Colors';
import * as fonts from '../../css/Fonts';
import ProductListItem from './ProductListItem'
import ProductDetails from './ProductDetails'
import ResponsiveImage from '../Helpers/ResponsiveImage'
import ConfirmationPopup from '../Popup/ConfirmationPopup'
import * as AppConstants from '../../Config/AppConstants';

import Dimensions from 'Dimensions';
import LinearGradient from 'react-native-linear-gradient';
import Modal from "react-native-modal";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
import { ActionCreators } from '../../Actions'
import Spinner from 'react-native-spinkit'

  HEADER_MAX_HIEGHT = Dimensions.get('window').height * 0.358
  HEADER_MIN_HIEGHT = 150

export class ProductCategories extends Component {

  confirmVerificationButton = false
  getProductDetailsFlag = false
  productId = null

  constructor() {
    super();
    this.state = {
      mainTitleTextWidth: null,
      toggleProductDetails: false,

      getNextData: false,

      errorModal: false,
      errorMessage: "",
      requestError: null,

      scrollY: new Animated.Value(0)
    }
  }

  backAction = () => {
    this.props.closeAction()
  }

  onLayout = event => {
    if (this.state.mainTitleTextWidth) return
    let width = event.nativeEvent.layout.width
    this.setState({ mainTitleTextWidth: width })
  }

  itemClicked = (item) => {
    this.productId = item.id
    this.getProductDetailsFlag = true
    
    this.props.logEvent(AppConstants.PorductDetails, { product: item.name })
    this.props.getProductDetails(item.id, this.onGetPorductDetailsSucess, this.onGetPorductDetailsFailure)
  }

  onGetPorductDetailsSucess = () => {
    this.getProductDetailsFlag = false
    this.productId = null
    this.openProductDetailsAction()
  }

  onGetPorductDetailsFailure = (error) => {
    this.setState({ errorMessage: error.errorMessage, requestError: error, errorModal: true })
  }

  openProductDetailsAction = () => {
    this.setState({ toggleProductDetails: true })
  }

  closeProductDetailsAction = () => {
    this.setState({ toggleProductDetails: false })
  }

  loadNextPage = (event) => {
    const scrollHeight = Math.floor(event.nativeEvent.contentOffset.y + event.nativeEvent.layoutMeasurement.height);
    const height = Math.floor(event.nativeEvent.contentSize.height);
    if (scrollHeight >= height - 10) {
      if ((this.props.cateogryPorducts.offset) && (!this.state.getNextData)) {
        this.setState({
          getNextData: true
        }, () => {
          this.getProductDetailsFlag = false
          this.productId = null 
          this.props.getProductsForCategoryDetails(this.props.productType, this.props.cateogryPorducts.result.id, this.props.cateogryPorducts.offset, this.onGetNextCategoryPorductSucess, this.onGetNextCategoryPorductFailure)
        });
      }
    }
  }

  onGetNextCategoryPorductSucess = () => {
    this.setState({ getNextData: false })
  }

  onGetNextCategoryPorductFailure = (error) => {
    this.setState({ getNextData: false, errorMessage: error.errorMessage, requestError: error, errorModal: true })
  }

  confirmVerificationPopup = () => {
    this.confirmVerificationButton = true
    this.setState({ errorModal: false })
  }

  closeVerificationPopup = () => {
    this.confirmVerificationButton = false
    this.setState({ errorModal: false })
  }

  onErrorModalHide = () => {
    if (this.confirmVerificationButton) {
      if (this.state.requestError) {
        if (this.state.requestError.shouldRetry) {
          if (this.getProductDetailsFlag) {
            if (this.productId) {
              this.props.getProductDetails(this.productId, this.onGetPorductDetailsSucess, this.onGetPorductDetailsFailure)
            }
          } else {
            this.props.getProductsForCategoryDetails(this.props.productType, this.props.data.result.id, this.props.data.offset, this.onGetNextCategoryPorductSucess, this.onGetNextCategoryPorductFailure)
          }
        } else if (this.state.requestError.shouldLogout) {
          this.backAction()
          this.props.logoutAndDeleteAllData(this.props.navigation)
        }
      }
    }
  }

  render() {

    const headerHeight = this.state.scrollY.interpolate({
      inputRange: [0, HEADER_MAX_HIEGHT - HEADER_MIN_HIEGHT],
      outputRange: [HEADER_MAX_HIEGHT, HEADER_MIN_HIEGHT],
      extrapolate: 'clamp'
    })

    const fontSizeTitle = this.state.scrollY.interpolate({
      inputRange: [0, 100],
      outputRange: [30 , 25],
      extrapolate: 'clamp'
    })

    let productImageUrl = this.props.cateogryPorducts.result.image ? this.props.cateogryPorducts.result.image : ""
    let productImage = productImageUrl != "" ? { uri: productImageUrl } : require('../../Images/dummy.jpg')
    return (
      <View style={[mainStyles.flex, styles.container]}>
        <Animated.View style={[styles.mainimageContainer, { height: headerHeight }]}>
          <View style={styles.imageContainer}>
            <ResponsiveImage style={mainStyles.flex} source={productImage} />
          </View>
          <LinearGradient
            colors={["rgba(0, 0, 0, 0.0)", "rgba(0, 0, 0, 0.4)"]}
            start={{ x: 0.0, y: 0.0 }}
            end={{ x: 0.0, y: 1.0 }}
            style={{ flex: 1, justifyContent: 'space-between' }}
          >
            <View style={{ alignItems: 'flex-end' }}>
              <TouchableOpacity style={[styles.closeButton, { alignItems: 'flex-end' }]} onPress={this.backAction}>
                <Image resizeMode={'contain'} style={styles.closeIcon} source={require('../../Images/close.png')} />
              </TouchableOpacity>
            </View>
            <View style={{ alignItems: 'flex-end', marginRight: 54, marginBottom: 45 }}>
              <Animated.Text onLayout={this.onLayout} style={[styles.productTitleText, { fontSize: fontSizeTitle }]}>
                {this.props.cateogryPorducts.result.arabic_name}
              </Animated.Text>
              <LinearGradient
                colors={[colors.fernGreen, colors.fernGreenTwo]}
                start={{ x: 0.0, y: 0.0 }}
                end={{ x: 0.0, y: 1.0 }}
                style={[styles.greenLineView, { width: 35 }]}
              />
            </View>
          </LinearGradient>
        </Animated.View>
        <Animated.ScrollView
          style={[styles.container]}
          contentContainerStyle={styles.container}
          scrollEventThrottle={16}
          onScroll={Animated.event(
            [{ nativeEvent: { contentOffset: { y: this.state.scrollY } } }],
            {
              useNativeDriver: false,
              listener: event => {
                this.loadNextPage(event)
              },
            },
          )}
        >
          <Text style={styles.descriptionText}>
            {this.props.cateogryPorducts.result.description}
          </Text>
          {
            this.props.cateogryPorducts.result.products.length !== 0 ?
              <View style={{ marginBottom: 25 }}>
                <Text style={styles.productsTitle}>
                  المنتجات
              </Text>
                <FlatList
                  ref={(ref) => { this.flatListRef = ref; }}
                  data={this.props.cateogryPorducts.result.products}
                  extraData={this.props.cateogryPorducts}
                  renderItem={({ item, index }) =>
                    <ProductListItem index={index} totalCount={this.props.cateogryPorducts.result.products.length} item={item} itemClick={(item) => this.itemClicked(item)} />
                  }
                  keyExtractor={(item, index) => index.toString()}
                  onScroll={this.loadNextPage}
                  style={{ marginTop: 21 }}
                />
                <View style={{ justifyContent: 'center', alignItems: 'center', display: this.state.getNextData ? 'flex' : 'none' }}>
                  <Spinner isVisible={this.state.getNextData} size={40} color={colors.fernGreen} type={"ThreeBounce"} />
                </View>
              </View> : null
          }
        </Animated.ScrollView>
        <Modal
          transparent={true}
          onBackButtonPress={() => this.closeProductDetailsAction()}
          style={{ marginLeft: 0, marginRight: 0, marginTop: 0, marginBottom: 0 }}
          isVisible={this.state.toggleProductDetails}
          animationIn={"bounceInUp"}
          animationOut={"bounceOutDown"}
          animationInTiming={900}
          animationOutTiming={900}
        >
          <ProductDetails closeAction={this.closeProductDetailsAction} />
        </Modal>
        <Modal
          transparent={true}
          style={{ justifyContent: 'center', backgroundColor: 'transparent' }}
          isVisible={this.state.errorModal}
          animationIn={"bounceInUp"}
          animationOut={"bounceOutDown"}
          animationInTiming={1000}
          animationOutTiming={1000}
          onModalHide={() => this.onErrorModalHide()}
        >
          <ConfirmationPopup
            mainTitle={"عذراً"}
            description={this.state.errorMessage}
            oneButton={this.state.requestError ? !this.state.requestError.shouldRetry : true}
            confirmText={this.state.requestError ? this.state.requestError.shouldRetry ? "إعادة" : "إغلاق" : "متابعة"}
            rejectText={"إغلاق"}
            confirm={this.confirmVerificationPopup}
            cancel={this.closeVerificationPopup} />
        </Modal>

      </View>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps(state) {
  return {
    cateogryPorducts: state.Reducer.cateogryPorducts,
    user: state.Reducer.user
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductCategories);


const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.white,
  },
  closeButton: {
    width: 70,
    height: 52,
    justifyContent: 'flex-start',
  },
  closeIcon: {
    marginTop: 36,
    marginRight: 18,
    width: 14,
    height: 14,
    tintColor: colors.white
  },
  imageContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
  mainimageContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    zIndex: 10000,
  },
  productTitleText: {
    fontFamily: fonts.Bold,
    letterSpacing: 0,
    textAlign: "right",
    color: colors.white
  },
  greenLineView: {
    marginTop: 20,
    width: 38.3,
    height: 2.3,
    borderRadius: 90,
    shadowColor: "rgba(68, 149, 52, 0.59)",
    shadowOffset: {
      width: 0,
      height: 0
    },
    shadowRadius: 9,
    shadowOpacity: 1,
    elevation: 5,
  },

  descriptionText: {
    marginTop: Dimensions.get('window').height * 0.358,
    marginBottom: 25,
    marginLeft: 20,
    marginRight: 20,
    fontFamily: fonts.Medium,
    fontSize: 11,
    lineHeight: 22,
    letterSpacing: 0,
    textAlign: "right",
    color: colors.battleshipGrey
  },
  productsTitle: {
    marginLeft: 18,
    marginRight: 18,
    fontFamily: fonts.Bold,
    fontSize: 15,
    letterSpacing: 0,
    textAlign: "right",
    color: colors.fernGreen
  }
});
