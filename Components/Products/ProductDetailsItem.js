/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  Image,
} from 'react-native';

import mainStyles from '../../css/MainStyle';
import colors from '../../css/Colors';
import * as fonts from '../../css/Fonts';
import * as constants from '../../Config/AppConstants';
import ResponsiveImage from '../Helpers/ResponsiveImage'

import * as Animatable from 'react-native-animatable';
import Modal from "react-native-modal";
import ImageViewer from 'react-native-image-zoom-viewer';

export default class ProductDetailsItem extends Component {

  constructor(props) {
    super(props);
    this.state = {
      toggleImageZoomModal: false,
    };
  }

  renderDescription = () => {
    var viewToRender = null
    if(this.props.item.type == constants.TypeText) {
      viewToRender = <Text style={itemStyles.answerText}>
                      {this.props.item.value}
                    </Text>
    } else if (this.props.item.type == constants.TypeImage) {
      let productImageUrl = this.props.item.value ? this.props.item.value : ""
      let productImage = productImageUrl != "" ? {uri: productImageUrl} : require('../../Images/dummy.jpg')
      viewToRender = <TouchableOpacity onPress={this.zoomImage}>
                      <ResponsiveImage style={{width: "100%", height: (this.props.item.height / 5)}} source={productImage} resizeMode={'contain'} />
                    </TouchableOpacity>
    }
    return viewToRender
  }

  zoomImage = () => {
    this.setState({toggleImageZoomModal: true})
  }

  closeImageZoomAction = () => {
    this.setState({toggleImageZoomModal: false})
  }

  renderHeaderForZoomGallery = () => {
    return(
      <TouchableOpacity onPress={this.closeImageZoomAction} style={itemStyles.closeButton}>
        <Image resizeMode={'contain'} style={{width:13, height:13, tintColor:colors.white}} source={require('../../Images/close.png')}/>
      </TouchableOpacity>
    )
  }

  render() {
    return (
      <Animatable.View animation="fadeInDownBig" delay={this.props.index * 100} style={{overflow:'hidden'}}>
        <TouchableOpacity onPress={() => this.props.itemClick(this.props.item, this.props.index)}>
          <View style={[mainStyles.rowFlex, itemStyles.question]}>
            {
              this.props.item.type == constants.TypeFile ? <Image resizeMode={'contain'} style={itemStyles.arrowIcon} source={require('../../Images/download.png')} /> :
              <Text style={itemStyles.plusText}>
                {this.props.item.expanded ? "-" : "+"}
              </Text>
            }
            <Text style={itemStyles.questionText}>
              {this.props.item.title}
            </Text>
          </View>
        </TouchableOpacity>
        <View style={[
            itemStyles.answerView,
            {overflow:'hidden', height: this.props.item.expanded ? null : 0, paddingLeft: this.props.item.expanded ? 30 : 0, paddingRight: this.props.item.expanded ? 30 : 0, paddingTop: this.props.item.expanded ? 19 : 0, paddingBottom: this.props.item.expanded ? 19 : 0, borderBottomWidth: this.props.item.expanded ? 1 : 0}
          ]}
        >
          {this.props.item.expanded ? this.renderDescription() : null}
        </View>
        {
          this.props.item.type == constants.TypeImage ?  <Modal
                                                          transparent={true}
                                                          onBackButtonPress={() => this.closeImageZoomAction()}
                                                          style={{marginLeft:0, marginRight:0, marginTop:0, marginBottom:0}}
                                                          isVisible={this.state.toggleImageZoomModal}
                                                          animationIn={"bounceInUp"}
                                                          animationOut={"bounceOutDown"}
                                                          animationInTiming={900}
                                                          animationOutTiming={900}
                                                        >
                                                          <ImageViewer
                                                            imageUrls={
                                                              [
                                                                {
                                                                  url: this.props.item.value
                                                                }
                                                              ]
                                                            }
                                                            enableSwipeDown={true}
                                                            renderIndicator={(currentIndex, allSize) => null}
                                                            onSwipeDown={() => this.closeImageZoomAction()}
                                                            renderHeader={(currentIndex) => this.renderHeaderForZoomGallery()}
                                                          />
                                                        </Modal> : null
        }
      </Animatable.View>
    );
  }
}


const itemStyles = StyleSheet.create({
  question: {
    paddingLeft: 21,
    paddingRight: 29,
    paddingTop: 15,
    paddingBottom: 15,
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: colors.white,
    shadowColor: "#e3e3e3",
    shadowOffset: {
      width: 0,
      height: -1
    },
    shadowRadius: 0,
    shadowOpacity: 1,
    overflow:'hidden',
    borderStyle: 'solid',
    borderBottomWidth: 1,
    borderBottomColor: "rgb(220,220,220)",
  },
  questionText: {
    marginLeft: 23,
    fontFamily: fonts.Light,
    fontSize: 20,
    letterSpacing: -0.13,
    textAlign: "right",
    color: colors.slate
  },
  minusText: {
    fontFamily: "LucidaGrande",
    fontWeight: "bold",
    fontSize: 40,
    fontStyle: "normal",
    letterSpacing: 0,
    textAlign: "left",
    color: "rgb(220,220,220)"
  },
  plusText: {
    fontFamily: "LucidaGrande",
    fontWeight: "bold",
    fontSize: 32,
    fontStyle: "normal",
    letterSpacing: 0,
    textAlign: "left",
    color: "rgb(220,220,220)"
  },
  answerView:{
    backgroundColor: colors.white,
    borderStyle: 'solid',
    borderBottomColor: "rgb(220,220,220)",
  },
  answerText: {
    fontFamily: fonts.Light,
    fontSize: 14.5,
    letterSpacing: -0.13,
    lineHeight: 22,
    textAlign: "right",
    color: colors.slate
  },
  arrowIcon: {
    width: 22,
    height: 22,
    tintColor: "rgb(220,220,220)",
  },
  closeButton: {
    marginTop: 20,
    marginRight: 15,
    padding: 10,
    alignItems: 'flex-end',
    position: 'absolute',
    top: 0,
    right: 0,
    width: 40,
    height: 40,
    zIndex: 1000,
  }
});
