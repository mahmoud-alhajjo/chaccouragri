/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  Keyboard,
  Image,
  TouchableOpacity,
} from 'react-native';

import mainStyles from '../../css/MainStyle';
import colors from '../../css/Colors';
import Header from '../Main Structure/Header'
import * as fonts from '../../css/Fonts';
import OverlayButton from '../Helpers/OverlayButton'
import SelectionPopup from '../Popup/SelectionPopup'
import * as constants from '../../Config/AppConstants';
import Validator from '../Helpers/Validator'
import ConfirmationPopup from '../Popup/ConfirmationPopup'

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
import { ActionCreators } from '../../Actions'
import Modal from "react-native-modal";
import { DocumentPicker, DocumentPickerUtil } from 'react-native-document-picker';

export class Careers extends Component {

  confirmVerificationButton = false
  popupType = null

  nameY = 0
  lastNameY = 0
  emailY = 0
  mobileY = 0
  vacanciesY = 0
  experienceYearsY = 0
  salaryExpectedY = 0
  provinceY = 0
  cityY = 0
  moreInfoY = 0

  constructor(props) {
    super(props);

    let provinceObj = null
    if ((props.user.province) && (props.user.province_name)) {
      provinceObj = {
        id: props.user.province,
        name: props.user.province_name
      }
    }

    let cityObj = null
    if ((props.user.city) && (props.user.city_name) && (props.user.province)) {
      cityObj = {
        id: props.user.city,
        name: props.user.city_name,
        province_id: props.user.province,
      }
    }

    let vacancieObj = null
    if ((props.user.vacancie) && (props.user.vacancie_name)) {
      vacancieObj = {
        id: props.user.vacancie,
        name: props.user.vacancie_name
      }
    }

    this.state = {
      selectionModal: false,

      firstName: props.user.first_name,
      lastName: props.user.last_name,
      email: null,
      mobile: props.user.phone_number,
      vacancie: vacancieObj,
      experienceYears: null,
      salaryExpected: null,
      province: provinceObj,
      city: cityObj,
      moreInfo: null,
      cvFile: null,

      nameError: "",
      lastNameError: "",
      emailError: "",
      mobileError: "",
      experienceYearsError: "",
      salaryExpectedError: "",
      provinceError: "",
      cityError: "",
      cvFileError: "",

      errorModal: false,
      errorMessage: "",
      requestError: null,
    }
  }

  componentDidMount() {
    this.keyboardDidHideSub = Keyboard.addListener('keyboardDidHide', this.keyboardDidHide);
  }

  componentDidUpdate(prevProps, prevState) {
    let vacancieObj = null
   
      if ((this.props.user.vacancie) && (this.props.user.vacancie_name)) {
        vacancieObj = {
          id: this.props.user.vacancie,
          name: this.props.user.vacancie_name
        }
        if (this.state.vacancie == null || (this.state.vacancie != null && this.state.vacancie == prevState.vacancie && prevProps.user != this.props.user ))
          this.setState({ vacancie: vacancieObj })
      }
  }

  componentWillUnmount() {
    this.keyboardDidHideSub.remove();
    this.props.setVacancieEmpty();
  }

  keyboardDidHide = (event) => {
    this.blurTextFields()
  };

  dismissKeyboard = () => {
    Keyboard.dismiss
    this.blurTextFields()
  }

  blurTextFields = () => {
    this.refs.lastNameInput.blur()
    this.refs.firstNameInput.blur()
    this.refs.mobileInput.blur()
    this.refs.experienceYearsInput.blur()
    this.refs.salaryExpectedInput.blur()
    this.refs.emailInput.blur()
    this.refs.moreInfoInput.blur()
  }

  getCVAction = () => {
    DocumentPicker.show({
      filetype: [DocumentPickerUtil.pdf()],
    }, (error, res) => {
      if (res) {
        if (res.uri && res.type && res.fileName) {
          this.setState({
            cvFile: res,
            cvFileError: null
          })
        }
      }
    });
  }

  sendAction = () => {
    let nameError = Validator.getNameError(this.state.firstName)
    let lastNameError = Validator.getLastNameError(this.state.lastName)
    let emailError = Validator.getEmailError(this.state.email)
    let mobileError = Validator.getMobileError(this.state.mobile)
    let experienceYearsError = Validator.getExperienceYearsError(this.state.experienceYears)
    let salaryExpectedError = Validator.getSalaryExpectedError(this.state.salaryExpected)
    let provinceError = Validator.getProvinceError(this.state.province)
    let cityError = Validator.getCityError(this.state.city)
    let cvFileError = null
    if (this.state.cvFile == null) {
      cvFileError = "الرجاء تحميل السيرة الذاتية"
    }

    this.setState({
      nameError: nameError,
      lastNameError: lastNameError,
      emailError: emailError,
      mobileError: mobileError,
      experienceYearsError: experienceYearsError,
      salaryExpectedError: salaryExpectedError,
      provinceError: provinceError,
      cityError: cityError,
      cvFileError: cvFileError
    }, this.scrollToFirstErrorField)

    if ((nameError == null) && (lastNameError == null) && (emailError == null) && (mobileError == null) && (experienceYearsError == null) && (salaryExpectedError == null) && (provinceError == null) && (cityError == null) && (cvFileError == null)) {
      this.dismissKeyboard()
      this.props.applayForJob(this.state.firstName, this.state.lastName, this.state.email, this.state.mobile, this.state.vacancie == null ? null : this.state.vacancie.id, this.state.experienceYears, this.state.salaryExpected, this.state.province.id, this.state.city.id, this.state.moreInfo, this.state.cvFile, this.onApplyForJobSucess, this.onApplyForJobFailure)
    }
  }

  onApplyForJobSucess = () => {
    let message = "لقد تم ارسال طلبك بنجاح."
    this.setState({ errorMessage: message, requestError: null, errorModal: true })
  }

  onApplyForJobFailure = (error) => {
    this.setState({ errorMessage: error.errorMessage, requestError: error, errorModal: true })
  }

  scrollToFirstErrorField = () => {
    if (this.state.nameError != null) {
      this.scroll.props.scrollToPosition(0, this.nameY - 10)
      this.refs.firstNameInput.focus()
    } else if (this.state.lastNameError != null) {
      this.scroll.props.scrollToPosition(0, this.lastNameY - 10)
      this.refs.lastNameInput.focus()
    } else if (this.state.emailError != null) {
      this.scroll.props.scrollToPosition(0, this.emailY - 10)
      this.refs.emailInput.focus()
    } else if (this.state.mobileError != null) {
      this.scroll.props.scrollToPosition(0, this.mobileY - 10)
      this.refs.mobileInput.focus()
    } else if (this.state.experienceYearsError != null) {
      this.scroll.props.scrollToPosition(0, this.experienceYearsY - 10)
      this.refs.experienceYearsInput.focus()
    } else if (this.state.salaryExpectedError != null) {
      this.scroll.props.scrollToPosition(0, this.salaryExpectedY - 10)
      this.refs.salaryExpectedInput.focus()
    } else if (this.state.provinceError != null) {
      this.scroll.props.scrollToPosition(0, this.provinceY - 10)
    } else if (this.state.cityError != null) {
      this.scroll.props.scrollToPosition(0, this.cityY - 10)
    } else if (this.state.cvFileError != null) {
      this.scroll.props.scrollToPosition(0, this.cvFileY - 10)
    }
  }

  onGetCitiesList = () => {
    this.popupType = constants.City
    this.setState({ selectionModal: true })
  }

  cityClicked = () => {
    if (this.state.province) {
      this.props.getCities(this.state.province.id, this.onGetCitiesList)
    } else {
      let provinceError = Validator.getProvinceError(this.state.province)
      this.setState({ provinceError: provinceError })
    }
  }

  onGetProvinceList = () => {
    this.popupType = constants.Province
    this.setState({ selectionModal: true })
  }

  closeSelectionPopupAction = () => {
    this.setState({ selectionModal: false })
  }

  handlePopupSelection = (selectedItem, popupType) => {
    if (popupType == constants.Province) {
      this.setState({ province: selectedItem, provinceError: "", city: null, selectionModal: false })
    } else if (popupType == constants.City) {
      this.setState({ city: selectedItem, cityError: "", selectionModal: false })
    } else if (popupType == constants.Vacancies) {
      this.setState({ vacancie: selectedItem, selectionModal: false })
    }
  }

  getListForSelectionPopup = () => {
    let popupType = this.popupType
    if (popupType == constants.Province) {
      return this.props.province
    } else if (popupType == constants.City) {
      return this.props.cities
    } else if (popupType == constants.Vacancies) {
      let vacancies = null
      vacancies = [{ id: -1, name: "بدون فرص" }, ...this.props.vacancies]
      return vacancies
    }
    return null
  }

  getSelectedItemSelectionPopup = () => {
    let popupType = this.popupType
    if (popupType == constants.Province) {
      return this.state.province
    } else if (popupType == constants.City) {
      return this.state.city
    } else if (popupType == constants.Vacancies) {
      return this.state.vacancie
    }
    return null
  }

  onSelectionModalHide = () => {
    this.popupType = null
  }

  handleChangeText = (event, field) => {
    if (field == 'firstName') {
      this.setState({ firstName: event })
    } else if (field == 'lastName') {
      this.setState({ lastName: event })
    } else if (field == 'email') {
      this.setState({ email: event })
    } else if (field == 'mobile') {
      this.setState({ mobile: event })
    } else if (field == 'experienceYears') {
      this.setState({ experienceYears: event })
    } else if (field == 'salaryExpected') {
      this.setState({ salaryExpected: event })
    } else if (field == 'moreInfo') {
      this.setState({ moreInfo: event })
    }
  }

  renderTextError = (error) => {
    return (
      <Text style={styles.errorText}>
        {error}
      </Text>
    )
  }

  renderTextErrorCV = (error) => {
    return (
      <Text style={styles.errorTextCV}>
        {error}
      </Text>
    )
  }

  confirmVerificationPopup = () => {
    this.confirmVerificationButton = true
    this.setState({ errorModal: false })
  }

  onGetVacancies = () => {
    this.popupType = constants.Vacancies
    this.setState({ selectionModal: true })
  }

  closeVerificationPopup = () => {
    this.confirmVerificationButton = false
    this.setState({ errorModal: false })
  }

  onErrorModalHide = () => {
    if (this.confirmVerificationButton) {
      if (this.state.requestError) {
        if (this.state.requestError.shouldRetry) {
          this.props.applayForJob(this.state.firstName, this.state.lastName, this.state.email, this.state.mobile, this.state.vacancie == null ? null : this.state.vacancie.id, this.state.experienceYears, this.state.salaryExpected, this.state.province.id, this.state.city.id, this.state.moreInfo, this.state.cvFile, this.onApplyForJobSucess, this.onApplyForJobFailure)
        } else if (this.state.requestError.shouldLogout) {
          this.props.logoutAndDeleteAllData(this.props.navigation)
        }
      } else {
        this.props.navigation.goBack()
      }
    }
  }

  render() {
    let vacancie = this.props.vacancies.length > 0 ?
      <View style={[styles.mainInputField, { justifyContent: null, alignItems: null }]}>
        <View style={[mainStyles.rowFlex, mainStyles.flex, { justifyContent: 'space-between', alignItems: 'center' }]}>
          <Image style={styles.downArrow} source={require('../../Images/downArrow.png')} />
          <View style={[styles.singleTextInputView, { marginLeft: 16 }]}
            onLayout={event => {
              const layout = event.nativeEvent.layout;
              this.vacanciesY = layout.y
            }}
          >
            <TextInput
              ref="vacancies"
              placeholder={"فرص العمل"}
              underlineColorAndroid={"transparent"}
              placeholderTextColor={colors.slate}
              blurOnSubmit={false}
              selectionColor={colors.slate}
              style={[mainStyles.flex, styles.textInput]}
              editable={false}
              value={this.state.vacancie ? this.state.vacancie.name ? this.state.vacancie.name : this.state.vacancie.title : ""}
            />
          </View>
        </View>
        <TouchableOpacity onPress={() => this.onGetVacancies()} style={{ position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, zIndex: 1000 }} />
      </View> : null

    return (
      <View style={[mainStyles.flex, styles.container]}>
        <KeyboardAwareScrollView innerRef={ref => { this.scroll = ref }} style={{ backgroundColor: colors.white }} contentContainerStyle={{ backgroundColor: colors.white }} bounces={false} enableResetScrollToCoords={true} scrollEnabled={true} enableOnAndroid={true}>
          <View style={styles.descriptionView}>
            <Text style={styles.descriptionText}>
              كشركة رائدة في القطاع الزراعي السوري، نؤمن أولاً بالموارد البشرية، ونسعى دائماً للبحث عن المواهب الجديدة المتميزة للانضمام إلى فريقنا، حيث أن الثقافة والخبرة والمهارات لفريق العمل تضمن نمونا ونجاحنا واستمرارنا.{"\n"}إن كنت خريج جامعي جديد أو ذو خبرة سواء في مجال الزراعة أو المبيعات أو الإدارة، يسرنا التعرف عليك.{"\n"}بكل بساطة إملأ الاستمارة المذكورة أدناه وقم بتحميل السيرة الذاتية الخاصة بك، وسنقوم بالاتصال بك قريباً.{"\n"}
            </Text>
          </View>
          <View style={styles.inputView}>
            <View style={[mainStyles.rowFlex, styles.mainInputField]}>
              <View style={[styles.doubleTextInputView, { paddingRight: 16, borderStyle: "solid", borderRightWidth: 1, borderRightColor: colors.silver }]}
                onLayout={event => {
                  const layout = event.nativeEvent.layout;
                  this.lastNameY = layout.y
                }}
              >
                <TextInput
                  ref="lastNameInput"
                  placeholder={"اسم العائلة"}
                  underlineColorAndroid={"transparent"}
                  placeholderTextColor={colors.slate}
                  returnKeyType={"next"}
                  autoCapitalize={"words"}
                  autoCorrect={false}
                  maxLength={50}
                  blurOnSubmit={false}
                  selectionColor={colors.slate}
                  style={[mainStyles.flex, styles.textInput]}
                  onChangeText={((event) => this.handleChangeText(event, 'lastName'))}
                  value={this.state.lastName ? this.state.lastName : ""}
                  onSubmitEditing={(event) => this.refs.emailInput.focus()}
                />
                {(this.state.lastNameError == "" || this.state.lastNameError == null) ? null : this.renderTextError(this.state.lastNameError)}
              </View>
              <View style={[styles.doubleTextInputView, { paddingLeft: 16 }]}
                onLayout={event => {
                  const layout = event.nativeEvent.layout;
                  this.nameY = layout.y
                }}
              >
                <TextInput
                  ref="firstNameInput"
                  placeholder={"الاسم"}
                  underlineColorAndroid={"transparent"}
                  placeholderTextColor={colors.slate}
                  returnKeyType={"next"}
                  autoCapitalize={"words"}
                  autoCorrect={false}
                  maxLength={50}
                  blurOnSubmit={false}
                  selectionColor={colors.slate}
                  style={[mainStyles.flex, styles.textInput]}
                  onChangeText={((event) => this.handleChangeText(event, 'firstName'))}
                  value={this.state.firstName ? this.state.firstName : ""}
                  onSubmitEditing={(event) => this.refs.lastNameInput.focus()}
                />
                {(this.state.nameError == "" || this.state.nameError == null) ? null : this.renderTextError(this.state.nameError)}
              </View>
            </View>
            <View style={[mainStyles.rowFlex, styles.mainInputField]}>
              <View style={styles.singleTextInputView}
                onLayout={event => {
                  const layout = event.nativeEvent.layout;
                  this.emailY = layout.y
                }}
              >
                <TextInput
                  ref="emailInput"
                  placeholder={"البريد الالكتروني"}
                  underlineColorAndroid={"transparent"}
                  placeholderTextColor={colors.slate}
                  autoCapitalize={"none"}
                  returnKeyType={"next"}
                  maxLength={60}
                  blurOnSubmit={false}
                  keyboardType={"email-address"}
                  selectionColor={colors.slate}
                  style={[mainStyles.flex, styles.textInput]}
                  onChangeText={((event) => this.handleChangeText(event, 'email'))}
                  value={this.state.email ? this.state.email : ""}
                  onSubmitEditing={(event) => this.refs.mobileInput.focus()}
                />
                {(this.state.emailError == "" || this.state.emailError == null) ? null : this.renderTextError(this.state.emailError)}
              </View>
            </View>
            <View style={[mainStyles.rowFlex, styles.mainInputField]}>
              <View style={styles.singleTextInputView}
                onLayout={event => {
                  const layout = event.nativeEvent.layout;
                  this.mobileY = layout.y
                }}
              >
                <TextInput
                  ref="mobileInput"
                  placeholder={"رقم الجوال"}
                  underlineColorAndroid={"transparent"}
                  placeholderTextColor={colors.slate}
                  returnKeyType={"next"}
                  maxLength={10}
                  blurOnSubmit={false}
                  keyboardType={"phone-pad"}
                  selectionColor={colors.slate}
                  style={[mainStyles.flex, styles.textInput]}
                  onChangeText={((event) => this.handleChangeText(event, 'mobile'))}
                  value={this.state.mobile ? this.state.mobile : ""}
                  onSubmitEditing={(event) =>  this.refs.experienceYearsInput.focus() }
                />
                {(this.state.mobileError == "" || this.state.mobileError == null) ? null : this.renderTextError(this.state.mobileError)}
              </View>
            </View>
            {vacancie}
            <View style={[mainStyles.rowFlex, styles.mainInputField]}>
              <View style={styles.singleTextInputView}
                onLayout={event => {
                  const layout = event.nativeEvent.layout;
                  this.experienceYearsY = layout.y
                }}
              >
                <TextInput
                  ref="experienceYearsInput"
                  placeholder={"سنين الخبرة"}
                  underlineColorAndroid={"transparent"}
                  placeholderTextColor={colors.slate}
                  returnKeyType={"next"}
                  maxLength={2}
                  blurOnSubmit={false}
                  keyboardType={"phone-pad"}
                  selectionColor={colors.slate}
                  style={[mainStyles.flex, styles.textInput]}
                  onChangeText={((event) => this.handleChangeText(event, 'experienceYears'))}
                  value={this.state.experienceYears ? this.state.experienceYears : ""}
                  onSubmitEditing={(event) => this.refs.salaryExpectedInput.focus()}
                />
                {(this.state.experienceYearsError == "" || this.state.experienceYearsError == null) ? null : this.renderTextError(this.state.experienceYearsError)}
              </View>
            </View>
            <View style={[mainStyles.rowFlex, styles.mainInputField]}>
              <View style={styles.singleTextInputView}
                onLayout={event => {
                  const layout = event.nativeEvent.layout;
                  this.salaryExpectedY = layout.y
                }}
              >
                <TextInput
                  ref="salaryExpectedInput"
                  placeholder={"الراتب المتوقع"}
                  underlineColorAndroid={"transparent"}
                  placeholderTextColor={colors.slate}
                  returnKeyType={"next"}
                  maxLength={7}
                  blurOnSubmit={false}
                  keyboardType={"phone-pad"}
                  selectionColor={colors.slate}
                  style={[mainStyles.flex, styles.textInput]}
                  onChangeText={((event) => this.handleChangeText(event, 'salaryExpected'))}
                  value={this.state.salaryExpected ? this.state.salaryExpected : ""}
                  onSubmitEditing={(event) => this.dismissKeyboard()}
                />
                {(this.state.salaryExpectedError == "" || this.state.salaryExpectedError == null) ? null : this.renderTextError(this.state.salaryExpectedError)}
              </View>
            </View>
            <View style={[mainStyles.rowFlex, styles.mainInputField]}>
              <View style={[styles.doubleTextInputView, { paddingRight: 16, borderStyle: "solid", borderRightWidth: 1, borderRightColor: colors.silver }]}>
                <View style={[mainStyles.flex, mainStyles.rowFlex, { alignItems: 'center' }]}
                  onLayout={event => {
                    const layout = event.nativeEvent.layout;
                    this.cityY = layout.y
                  }}
                >
                  <Image style={styles.downArrow} source={require('../../Images/downArrow.png')} />
                  <TextInput
                    ref="cityInput"
                    placeholder={"المنطقة"}
                    underlineColorAndroid={"transparent"}
                    placeholderTextColor={colors.slate}
                    blurOnSubmit={false}
                    selectionColor={colors.slate}
                    style={[mainStyles.flex, styles.textInput]}
                    editable={false}
                    value={this.state.city ? this.state.city.name : ""}
                    multiline={true}
                  />
                </View>
                {(this.state.cityError == "" || this.state.cityError == null) ? null : this.renderTextError(this.state.cityError)}
                <TouchableOpacity onPress={() => this.cityClicked()} style={{ position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, zIndex: 1000 }} />
              </View>
              <View style={[styles.doubleTextInputView, { paddingLeft: 16 }]}>
                <View style={[mainStyles.flex, mainStyles.rowFlex, { alignItems: 'center' }]}
                  onLayout={event => {
                    const layout = event.nativeEvent.layout;
                    this.provinceY = layout.y
                  }}
                >
                  <Image style={styles.downArrow} source={require('../../Images/downArrow.png')} />
                  <TextInput
                    ref="provinceInput"
                    placeholder={"محافظة"}
                    underlineColorAndroid={"transparent"}
                    placeholderTextColor={colors.slate}
                    blurOnSubmit={false}
                    selectionColor={colors.slate}
                    style={[mainStyles.flex, styles.textInput]}
                    editable={false}
                    value={this.state.province ? this.state.province.name : ""}
                    multiline={true}
                  />
                </View>
                {(this.state.provinceError == "" || this.state.provinceError == null) ? null : this.renderTextError(this.state.provinceError)}
                <TouchableOpacity onPress={() => this.props.getProvince(this.onGetProvinceList)} style={{ position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, zIndex: 1000 }} />
              </View>
            </View>
            <View style={[mainStyles.rowFlex, styles.mainInputFieldNoBorder]}>
              <View style={styles.singleTextInputView}>
                <TextInput
                  ref="moreInfoInput"
                  placeholder={"معلومات إضافية"}
                  textAlignVertical={'top'}
                  underlineColorAndroid={"transparent"}
                  placeholderTextColor={colors.slate}
                  multiline={true}
                  numberOfLines={4}
                  autoCapitalize={"none"}
                  autoCorrect={false}
                  blurOnSubmit={false}
                  selectionColor={colors.slate}
                  onChangeText={((event) => this.handleChangeText(event, 'moreInfo'))}
                  style={[mainStyles.flex, styles.textInput]}
                />
              </View>
            </View>
          </View>
          <View style={{ marginLeft: 36, marginRight: 36, marginTop: 10, marginBottom: 25 }}
            onLayout={event => {
              const layout = event.nativeEvent.layout;
              this.cvFileY = layout.y
            }}
          >
            <OverlayButton buttonColors={[colors.blush, colors.darkPeach]} buttonBackgroundColor={colors.darkPeach} buttonTextColor={colors.white} clickButton={this.getCVAction} style={mainStyles.flex} text={"تحميل السيرة الذاتية"} customStyle={styles.uploadCVButton} textButtonStyle={styles.textButton} />
            {(this.state.cvFileError == "" || this.state.cvFileError == null) ? null : this.renderTextErrorCV(this.state.cvFileError)}
            {this.state.cvFile ?
              <Text style={[styles.errorTextCV, { color: colors.slate }]}>
                {this.state.cvFile.fileName}
              </Text> : null
            }
          </View>
          <View style={{ marginLeft: 36, marginRight: 36, marginTop: 10, marginBottom: 30 }}>
            <OverlayButton buttonColors={[colors.fernGreen, colors.fernGreenTwo]} buttonBackgroundColor={colors.fernGreen} buttonTextColor={colors.white} clickButton={this.sendAction} style={mainStyles.flex} text={"ارسال"} customStyle={styles.sendButton} textButtonStyle={styles.textButton} />
          </View>
        </KeyboardAwareScrollView>
        <Modal
          transparent={true}
          onBackButtonPress={() => this.closeSelectionPopupAction()}
          onBackdropPress={() => this.closeSelectionPopupAction()}
          style={{ justifyContent: 'center', alignItems: 'center', backgroundColor: 'transparent' }}
          isVisible={this.state.selectionModal}
          animationIn={"bounceInUp"}
          animationOut={"bounceOutDown"}
          animationInTiming={1000}
          animationOutTiming={1000}
          onModalHide={() => this.onSelectionModalHide()}
        >
          <SelectionPopup popupType={this.popupType} selectedItem={this.getSelectedItemSelectionPopup()} list={this.getListForSelectionPopup()} cancel={this.closeSelectionPopupAction} confirm={(item) => this.handlePopupSelection(item, this.popupType)} />
        </Modal>
        <Modal
          transparent={true}
          style={{ justifyContent: 'center', backgroundColor: 'transparent' }}
          isVisible={this.state.errorModal}
          animationIn={"bounceInUp"}
          animationOut={"bounceOutDown"}
          animationInTiming={1000}
          animationOutTiming={1000}
          onModalHide={() => this.onErrorModalHide()}
        >
          <ConfirmationPopup
            mainTitle={this.state.requestError ? "عذراً" : "تهانينا"}
            description={this.state.errorMessage}
            oneButton={this.state.requestError ? !this.state.requestError.shouldRetry : true}
            confirmText={this.state.requestError ? this.state.requestError.shouldRetry ? "إعادة" : "إغلاق" : "إغلاق"}
            rejectText={"إغلاق"}
            confirm={this.confirmVerificationPopup}
            cancel={this.closeVerificationPopup} />
        </Modal>
      </View>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps(state) {
  return {
    province: state.Reducer.province,
    cities: state.Reducer.cities,
    user: state.Reducer.user,
    vacancies: state.Reducer.vacancies
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Careers);


const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.white
  },
  descriptionView: {
    marginTop: 35,
    marginLeft: 19,
    marginRight: 19,
    marginBottom: 14.2,
  },
  descriptionText: {
    fontFamily: fonts.Medium,
    fontSize: 11,
    lineHeight: 17,
    letterSpacing: 0,
    textAlign: "right",
    lineHeight: 22,
    color: colors.slate
  },
  inputView: {
    marginLeft: 36,
    marginRight: 36,
    height: 600,
    borderRadius: 4,
    borderStyle: "solid",
    borderWidth: 1,
    borderColor: colors.silverTwo
  },
  textInput: {
    paddingTop: 0,
    paddingBottom: 0,
    fontFamily: fonts.Light,
    fontSize: 15,
    fontWeight: "300",
    letterSpacing: 0,
    textAlign: "right",
    color: colors.slate
  },
  downArrow: {
    width: 12,
    height: 6
  },
  mainInputField: {
    borderStyle: "solid",
    borderBottomWidth: 1,
    borderBottomColor: colors.silver,
    minHeight: 67.5,
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingLeft: 16,
    paddingRight: 16,
  },
  mainInputFieldNoBorder: {
    height: 151,
    paddingTop: 18,
    paddingBottom: 18,
    paddingLeft: 16,
    paddingRight: 16,
  },
  doubleTextInputView: {
    height: '100%',
    width: '50%',
  },
  singleTextInputView: {
    height: '100%',
    flex: 1,
  },
  uploadCVButton: {
    height: 47,
    borderRadius: 4,
    shadowColor: "rgba(0, 0, 0, 0.21)",
    shadowOffset: {
      width: 7,
      height: -8
    },
    shadowRadius: 26,
    shadowOpacity: 1,
    elevation: 3,
  },
  textButton: {
    fontFamily: fonts.Medium,
    fontSize: 18,
    letterSpacing: 0,
    textAlign: "center",
  },
  sendButton: {
    height: 65,
    borderRadius: 4,
    shadowColor: "rgba(0, 0, 0, 0.21)",
    shadowOffset: {
      width: 7,
      height: -8
    },
    shadowRadius: 26,
    shadowOpacity: 1,
    elevation: 3,
  },
  errorText: {
    fontFamily: fonts.Light,
    fontSize: 12,
    letterSpacing: 0,
    textAlign: "right",
    color: colors.darkishRed,
    marginBottom: 6,
  },
  errorTextCV: {
    fontFamily: fonts.Light,
    fontSize: 12,
    letterSpacing: 0,
    textAlign: "center",
    color: colors.darkishRed,
    marginTop: 10,
  }
});