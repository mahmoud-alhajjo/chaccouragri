/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  FlatList
} from 'react-native';

import mainStyles from '../../css/MainStyle';
import colors from '../../css/Colors';
import * as fonts from '../../css/Fonts';
import OverlayButton from '../Helpers/OverlayButton';
import * as AppConstants from '../../Config/AppConstants';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
import { ActionCreators } from '../../Actions'
import * as Animatable from 'react-native-animatable';

export class VacanciesCareersItem extends Component {

  constructor() {
    super();
    this.state = {
    }
  }


  applyjobAction = () => {
    this.props.setVacancie(this.props.item, this.onsetVacanciesuccess)
  }

  onsetVacanciesuccess = () => {
    this.props.goToPage()
  }

  renderDescription = () => {
    let viewToRender = null
    if (this.props.item.title) {
      viewToRender =
        <View>
          <Text style={itemStyles.answerText}>
            {this.props.item.requirements}
          </Text>
          <View style={{ marginLeft: 50, marginRight: 50, marginTop: 15, marginBottom: 15 }}>
            <OverlayButton
              buttonColors={[colors.fernGreen, colors.fernGreenTwo]}
              buttonBackgroundColor={colors.fernGreen}
              buttonTextColor={colors.white}
              clickButton={this.applyjobAction}
              style={mainStyles.flex}
              text={"التقدم لعمل"}
              customStyle={itemStyles.loginButton}
              textButtonStyle={itemStyles.loginText}
            />
          </View>
        </View>
    }
    return viewToRender;
  }

  render() {
    return (
      <Animatable.View animation="fadeInDownBig" delay={this.props.index * 100} style={{ overflow: 'hidden' }}>
        <TouchableOpacity onPress={() => this.props.itemClick(this.props.item, this.props.index)}>
          <View style={[mainStyles.rowFlex, itemStyles.question]}>
            {
              <Text style={itemStyles.plusText}>
                {this.props.item.expanded ? "-" : "+"}
              </Text>
            }
            <Text style={itemStyles.questionText}>
              {this.props.item.title}
            </Text>
          </View>
        </TouchableOpacity>
        <View style={[
          itemStyles.answerView,
          { overflow: 'hidden', height: this.props.item.expanded ? null : 0, paddingLeft: this.props.item.expanded ? 30 : 0, paddingRight: this.props.item.expanded ? 30 : 0, paddingTop: this.props.item.expanded ? 19 : 0, paddingBottom: this.props.item.expanded ? 19 : 0, borderBottomWidth: this.props.item.expanded ? 1 : 0 }
        ]}
        >
          {this.props.item.expanded ? this.renderDescription() : null}
        </View>
      </Animatable.View>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps(state) {
  return {
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(VacanciesCareersItem);



const itemStyles = StyleSheet.create({
  question: {
    paddingLeft: 21,
    paddingRight: 29,
    paddingTop: 15,
    paddingBottom: 15,
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: colors.white,
    shadowColor: "#e3e3e3",
    shadowOffset: {
      width: 0,
      height: -1
    },
    shadowRadius: 0,
    shadowOpacity: 1,
    overflow: 'hidden',
    borderStyle: 'solid',
    borderBottomWidth: 1,
    borderBottomColor: "rgb(220,220,220)",
  },
  questionText: {
    marginLeft: 23,
    fontFamily: fonts.Light,
    fontSize: 20,
    letterSpacing: -0.13,
    textAlign: "right",
    color: colors.slate
  },
  minusText: {
    fontFamily: "LucidaGrande",
    fontWeight: "bold",
    fontSize: 40,
    fontStyle: "normal",
    letterSpacing: 0,
    textAlign: "left",
    color: "rgb(220,220,220)"
  },
  plusText: {
    fontFamily: "LucidaGrande",
    fontWeight: "bold",
    fontSize: 32,
    fontStyle: "normal",
    letterSpacing: 0,
    textAlign: "left",
    color: "rgb(220,220,220)"
  },
  answerView: {
    backgroundColor: colors.white,
    borderStyle: 'solid',
    borderBottomColor: "rgb(220,220,220)",
  },
  answerText: {
    fontFamily: fonts.Light,
    fontSize: 14.5,
    letterSpacing: -0.13,
    lineHeight: 22,
    textAlign: "right",
    color: colors.slate
  },
  arrowIcon: {
    width: 22,
    height: 22,
    tintColor: "rgb(220,220,220)",
  },
  closeButton: {
    marginTop: 20,
    marginRight: 15,
    padding: 10,
    alignItems: 'flex-end',
    position: 'absolute',
    top: 0,
    right: 0,
    width: 40,
    height: 40,
    zIndex: 1000,
  },
  loginButton: {
    height: 45,
    borderRadius: 4,
    shadowColor: "rgba(0, 0, 0, 0.21)",
    shadowOffset: {
      width: 7,
      height: -8
    },
    shadowRadius: 26,
    shadowOpacity: 1,
    elevation: 5,
  },
  loginText: {
    fontFamily: fonts.Medium,
    fontSize: 14,
    letterSpacing: 0,
    textAlign: "center",
  },
});
