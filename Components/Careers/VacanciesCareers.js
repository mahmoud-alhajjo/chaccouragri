/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  LayoutAnimation,
  UIManager,
  FlatList
} from 'react-native';

import mainStyles from '../../css/MainStyle';
import colors from '../../css/Colors';
import * as fonts from '../../css/Fonts';
 
import VacanciesCareersItem from './VacanciesCareersItem'
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux'
import { ActionCreators } from '../../Actions'

export class VacanciesCareers extends Component {

  constructor() {
    super();
    if(Platform.OS === 'android'){
      UIManager.setLayoutAnimationEnabledExperimental(true);
    }
    this.state = {
      refresh: false,
    };
  }

  componentDidMount () {
    this.props.getUserFromLocal()
    this.props.getVacanciesCareers()
  }

  itemClick = (item, index) => {
    if(item.title) {
      LayoutAnimation.configureNext({
        duration: 250,
        update: {
          type: LayoutAnimation.Types.linear,
        }
      });
      let data = this.props.vacancies
      data[index].expanded = !data[index].expanded
      this.setState({refresh: true})
    }
  }

  render() {
    let vacancies = null 
    if(this.props.vacancies)
    {
      vacancies= this.props.vacancies
    }    
    return (
      <View style={[mainStyles.flex, styles.container]}>
          {
            vacancies ? vacancies.length > 0 ?   <FlatList
                                        data={vacancies}
                                        renderItem={({ item, index }) => 
                                        <VacanciesCareersItem item={item} index={index}  itemClick={(item, index) => this.itemClick(item, index)} goToPage={ () => this.props.goToPage && this.props.goToPage() }/>
                                          }
                                        keyExtractor={(item, index) => index.toString()}
                                        style={{marginTop: 15}}
                                      /> : <View style={[mainStyles.flex, {justifyContent: 'center', alignItems: 'center'}]}><Text style={{fontFamily: fonts.Light, fontSize: 15, textAlign: "center", color: colors.slate}}>لايوجد فرص عمل بمنطقتك حالياً</Text></View>
                                         : null
          }
      </View>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps(state) {
  return {
    vacancies: state.Reducer.vacancies
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(VacanciesCareers);

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.white
  }
});
