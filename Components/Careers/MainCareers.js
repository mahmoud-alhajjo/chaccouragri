/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
} from 'react-native';

import mainStyles from '../../css/MainStyle';
import colors from '../../css/Colors';
import * as fonts from '../../css/Fonts';
import Header from '../Main Structure/Header'

import CareersTap from './CareersTap'
import Careers from './Careers'
import VacanciesCareers from './VacanciesCareers'
import * as AppConstants from '../../Config/AppConstants';

import ScrollableTabView from 'react-native-scrollable-tab-view'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
import { ActionCreators } from '../../Actions'

export class MainCareers extends Component {

  constructor() {
    super();
    this.state = {
    }
  }

  componentDidMount() {
    this.props.logEvent(AppConstants.MainCareer, { "careers": 'vacancies' })
  }

  goToPage(pageId) {
    this.tabView.goToPage(pageId);
  }

  render() {
    return (
      <View style={[mainStyles.flex, styles.container]}>
        <Header navigation={this.props.navigation} />
        <ScrollableTabView ref={(tabView) => { this.tabView = tabView}} initialPage={1} locked={false} renderTabBar={() => <CareersTap />}>
          <Careers navigation={this.props.navigation} />
          <VacanciesCareers tabView={this.tabView} goToPage={ () => this.goToPage(0) } />
        </ScrollableTabView>
      </View>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps(state) {
  return {
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(MainCareers);

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.paleGrey
  }
});
