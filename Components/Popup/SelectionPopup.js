/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  TouchableOpacity,
  ScrollView
} from 'react-native';

import mainStyles from '../../css/MainStyle';
import colors from '../../css/Colors';
import OverlayButton from '../Helpers/OverlayButton'
import * as fonts from '../../css/Fonts';
import * as constants from '../../Config/AppConstants';

export default class SelectionPopup extends Component {


  constructor(props) {
    super(props);
    var obj = null
    if (props.selectedItem) {
      obj = props.list.find((item) => item.id === props.selectedItem.id)
    }
    this.state = {
      selectedItem: obj
    }
  }

  handleSelection = (item) => {
    this.setState({ selectedItem: item })
  }

  render() {
    let listItems = null

    if (this.props.list) {
      listItems = this.props.list.map((item) => {
        return <TouchableOpacity onPress={() => this.handleSelection(item)} key={item.id} style={{ height: 40, justifyContent: 'center', backgroundColor: this.state.selectedItem ? item.id == this.state.selectedItem.id ? colors.whiteTwo : null : null }}><Text style={styles.description}>{item.name ? item.name : item.title }</Text></TouchableOpacity>
      });
    }

    var title = ""
    let popupType = this.props.popupType
    if (popupType == constants.Occupations) {
      title = "اختر المهنة"
    } else if (popupType == constants.Province) {
      title = "اختر المحافظة"
    } else if (popupType == constants.City) {
      title = "اختر المنطقة"
    } else if (popupType == constants.Gender) {
      title = "اختر الجنس"
    } else if (popupType == constants.ContactUsSubjects) {
      title = "اختر الموضوع"
    } else if (popupType == constants.Vacancies) {
      title = "فرص العمل"
    }
    return (
      <View style={{ width: '85%', maxHeight: "70%", justifyContent: 'center' }}>
        <View style={styles.container}>
          <View style={[mainStyles.rowFlex, styles.mainView]}>
            <View style={{ width: 40, height: 40 }}></View>
            <View style={[mainStyles.flex, { minHeight: 40, justifyContent: 'center' }]}>
              <Text style={styles.title}>{title}</Text>
            </View>
            <TouchableOpacity onPress={this.props.cancel} style={styles.closeButton}>
              <Image resizeMode={'contain'} style={{ width: 13, height: 13, tintColor: colors.black }} source={require('../../Images/close.png')} />
            </TouchableOpacity>
          </View>
          <ScrollView style={{ width: '100%' }}>
            {listItems}
          </ScrollView>
          <View style={{ height: 50, width: '100%', marginTop: 15, paddingLeft: 12, paddingRight: 12 }}>
            <OverlayButton disabled={this.state.selectedItem ? false : true} buttonBackgroundColorOpacity={this.state.selectedItem ? 1 : 0.3} buttonBackgroundColor={colors.fernGreen} buttonTextColorOpacity={this.state.selectedItem ? 1 : 0.3} buttonTextColor={colors.white} clickButton={this.props.confirm.bind(this, this.state.selectedItem)} style={mainStyles.flex} text="تم" customStyle={styles.loginButton} textButtonStyle={styles.loginText} />
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    borderRadius: 12.5,
    overflow: 'visible',
    alignItems: 'center',
    paddingTop: 12,
    paddingBottom: 12,
    marginTop: 0,
    marginBottom: 0,
    backgroundColor: colors.white,
  },
  description: {
    fontSize: 16,
    color: colors.slate,
    marginLeft: 16,
    marginRight: 16,
    fontFamily: fonts.Light,
    textAlign: "right",
  },
  title: {
    fontFamily: fonts.Medium,
    fontSize: 18,
    textAlign: "center",
    color: colors.black,

  },
  mainView: {
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: 6,
  },
  closeImage: {
    alignItems: 'center',
    paddingLeft: 5,
    paddingRight: 5,
    paddingTop: 5,
    paddingBottom: 5,
    position: 'absolute',
    right: -13,
    zIndex: 1000,
    backgroundColor: colors.whiteTwo,
    borderRadius: 100,
  },
  loginButton: {
    height: 40,
    borderRadius: 4,
    shadowColor: "rgba(0, 0, 0, 0.21)",
    shadowOffset: {
      width: 7,
      height: -8
    },
    shadowRadius: 26,
    shadowOpacity: 1,
    elevation: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  loginText: {
    fontFamily: fonts.Medium,
    fontSize: 18,
    letterSpacing: 0,
    textAlign: "center",
  },
  closeButton: {
    justifyContent: 'center',
    padding: 10,
    width: 40,
    height: 40,
  }
});
