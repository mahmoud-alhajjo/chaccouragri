/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    Image,
    TouchableOpacity,

} from 'react-native';

import mainStyles from '../../css/MainStyle';
import colors from '../../css/Colors';
import OverlayButton from '../Helpers/OverlayButton'
import * as fonts from '../../css/Fonts';

export default class ConfirmationPopup extends Component {

    constructor() {
        super();
        this.state = {

        };
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.mainView}>
                    <Text style={styles.title}>{this.props.mainTitle ? this.props.mainTitle : ""}</Text>
                    <Text>{"\n"}</Text>
                    <Text style={styles.description}>{this.props.description}</Text>
                </View>
                <View style={[mainStyles.rowFlex,{height: 50, justifyContent: 'space-between'}]}>
                    {
                      this.props.oneButton ? null : <View style={[mainStyles.flex, {marginRight: 12, height: 50}]}>
                                                      <OverlayButton buttonBackgroundColor={colors.white} buttonTextColor={colors.battleshipGrey} clickButton={this.props.cancel} style={mainStyles.flex} text={this.props.rejectText ? this.props.rejectText : "لا"} customStyle={styles.loginButton} textButtonStyle={styles.loginText} />
                                                    </View>
                    }
                    <View style={[mainStyles.flex, {marginLeft: this.props.oneButton ? 0 : 12, height: 50}]}>
                        <OverlayButton buttonBackgroundColor={this.props.warning ? colors.scarlet : colors.fernGreen} buttonTextColor={colors.white} clickButton={this.props.confirm} style={mainStyles.flex} text={this.props.confirmText ? this.props.confirmText : "نعم"} customStyle={styles.loginButton} textButtonStyle={styles.loginText}/>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      borderRadius: 12.5,
      overflow:'hidden',
      alignItems: 'center',
      paddingTop: 24,
      paddingBottom: 24,
      paddingLeft: 24,
      paddingRight: 24,
      marginLeft: 24,
      marginRight: 24,
      backgroundColor: colors.white,
    },
    description: {
      fontSize: 14,
      fontFamily: fonts.Light,
      textAlign: "center",
      color: colors.black
    },
    title: {
      fontFamily: fonts.Bold,
      fontSize: 17,
      textAlign: "center",
      color: colors.slate
    },
    mainView:{
      paddingTop: 15,
      marginBottom: 39,
    },
    loginButton: {
      height: 40,
      borderRadius: 4,
      shadowColor: "rgba(0, 0, 0, 0.21)",
      shadowOffset: {
        width: 7,
        height: -8
      },
      shadowRadius: 26,
      shadowOpacity: 1,
      elevation: 5,
      justifyContent: 'center',
      alignItems: 'center',
    },
    loginText: {
      fontFamily: fonts.Medium,
      fontSize: 18,
      letterSpacing: 0,
      textAlign: "center",
    },
});
