import React, { Component } from 'react';
import { View, Text, StyleSheet, ImageBackground, FlatList, Image} from 'react-native';
import colors from '../../css/Colors';
import * as fonts from '../../css/Fonts';
import Header from '../Main Structure/Header';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import { ActionCreators } from '../../Actions';
import LinearGradient from 'react-native-linear-gradient';
import moment from 'moment';
import Dimensions from 'Dimensions';

import Modal from "react-native-modal";
import ConfirmationPopup from '../Popup/ConfirmationPopup';

class Weather extends Component {

  confirmVerificationButton = false
  locationFlag = true

  Images = [ require('../../Images/weather1.jpg'),
             require('../../Images/weather2.jpg'),
             require('../../Images/weather3.jpg'),
             require('../../Images/weather4.jpg'),
             require('../../Images/weather5.jpg'),
             require('../../Images/weather6.jpg'),
             require('../../Images/weather7.jpg'),
             require('../../Images/weather8.jpg'),
             require('../../Images/weather9.jpg'),
             require('../../Images/weather10.jpg'),
           ]

  constructor(props) {
    super(props);
    this.state = {
      errorModal: false,
      errorMessage: "",
      requestError: null,
      randomImage: require('../../Images/dummy.jpg')
  }
}


  componentDidMount(){
    this.locationFlag = true
    this.props.getCurrentLocation(this.onGetCurrentLocationSuccess, this.onGetCurrentLocationFailure);
  }

  onGetCurrentLocationSuccess = (location) =>{
    this.locationFlag = false
    this.setState({errorMessage:"", requestError:null})
    this.props.getWeather(location,this.onGetWeatherSuccess,this.onGetWeatherFailure)
  }

  onGetCurrentLocationFailure = (error) =>{
    this.setState({errorMessage:error.errorMessage, requestError: error, errorModal: true})
  }

  onGetWeatherSuccess = () => {
    this.setState({errorMessage:"", requestError:null, randomImage: this.randomImage()})
  }

  onGetWeatherFailure = (error) =>{
    this.setState({errorMessage:error.errorMessage, requestError: error, errorModal: true})
  }

  confirmErrorPopup = () => {
    this.confirmVerificationButton = true
    this.setState({errorModal: false})
  }

  closeErrorPopup = () => {
    this.confirmVerificationButton = false
    this.setState({errorModal: false})
  }

  onErrorModalHide = () => {
    if(this.confirmVerificationButton) {
      if(this.state.requestError) {
        if(this.state.requestError.shouldRetry) {
          if(this.locationFlag) {
            this.props.getCurrentLocation(this.onGetCurrentLocationSuccess, this.onGetCurrentLocationFailure);
          } else {
            this.props.getWeather(this.props.userLocation, this.onGetWeatherSuccess, this.onGetWeatherFailure, true)
          }
        }
      }
    }
  }

  renderItem = (item) => {
    return (
      <View style={itemStyles.container}>
        <Text style={[itemStyles.ItemText, { color:colors.coolGrey }]}>{item.time.format("dddd")}</Text>
        <Image style={itemStyles.itemImage} source={{uri: item.icon}}/>
        <Text style={[itemStyles.ItemText, { color:colors.coolBlue }]}>{item.temp_max}</Text>
        <Text style={[itemStyles.ItemText, { marginTop: 10, color:colors.coolGrey }]}>{item.temp_min}</Text>
      </View>
      )
  }

  randomImage = () => {
    return this.Images[Math.floor(Math.random()*this.Images.length)]
  }

  render() {
    let currentDay = null
    let otherDays = []
    const { weatherInformation } = this.props;
    if(weatherInformation){
      currentDay = weatherInformation[0]
      otherDays = weatherInformation.slice(1, weatherInformation.length);
      otherDays.reverse();
    }
        return(
            <View style={{ flex: 1 }}>
              <Header navigation={this.props.navigation} />
              <View style={{ height: 250 }}>
                  <Image
                      style={ styles.WeatherImage }
                      source={ this.state.randomImage }
                  />
                  <LinearGradient
                    colors={["rgba(0, 0, 0, 0.0)", "rgba(0, 0, 0, 0.5)"]}
                    start={{ x: 0.0, y: 0.0 }}
                    end={{ x: 0.0, y: 1.0 }}
                    style={styles.WeatherOverlay}
                  >
                    <View style={{width:'50%', paddingLeft: 12}}>
                        <Text style={[styles.WeatherText, styles.WeatherMediumText ]} >{currentDay ? currentDay.temp_max +"°"  : ""}</Text>
                        <Text style={[styles.WeatherText, styles.WeatherMediumText, { marginBottom: 10 } ]} >{currentDay ? currentDay.temp_min +"°" : ""}</Text>
                        <Text style={[styles.WeatherText, styles.WeatherLightText ]} >{currentDay ? "  الرطوبة  " + currentDay.humidity +"%": ""}</Text>
                    </View>
                    <View style={{width:'50%',  paddingRight: 12}}>
                        <Text style={[styles.WeatherText, styles.WeatherBoldText ]}>{currentDay ? currentDay.temp_main+"°" : ""}</Text>
                        <Text style={[styles.WeatherText, styles.WeatherLightText, { textAlign: 'right' }]}>{currentDay ? currentDay.time.format("dddd, MMM Do") : ""}</Text>
                    </View>
                  </LinearGradient>
              </View>
              <FlatList
                  data={otherDays}
                  horizontal
                  renderItem={({ item }) => this.renderItem(item)}
                  keyExtractor={(item, index) => index.toString()}
                  style={{marginTop: 15}}
              />
              <Modal
                transparent={true}
                style={{justifyContent: 'center', backgroundColor: 'transparent'}}
                isVisible={this.state.errorModal}
                animationIn={"bounceInUp"}
                animationOut={"bounceOutDown"}
                animationInTiming={1000}
                animationOutTiming={1000}
                onModalHide={() => this.onErrorModalHide()}>
                  <ConfirmationPopup
                    mainTitle={"عذراً"}
                    description={this.state.errorMessage}
                    oneButton={this.state.requestError ? !this.state.requestError.shouldRetry : true}
                    confirmText={this.state.requestError ? this.state.requestError.shouldRetry ? "إعادة" : "إغلاق" : "إغلاق"}
                    rejectText={"إغلاق"}
                    confirm={this.confirmErrorPopup}
                    cancel={this.closeErrorPopup} />
              </Modal>
            </View>
        );
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(ActionCreators, dispatch);
  }

  function mapStateToProps(state) {
    return {
      userLocation: state.Reducer.userLocation,
      weatherInformation: state.Reducer.weatherInformation,
    };
  }

const styles = StyleSheet.create({
  WeatherImage: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: (Dimensions.get('window').width),
    height: 250
  },
  WeatherOverlay: {
    height:250,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "flex-end"
  },
  WeatherText: {
    letterSpacing: 0,
    textAlign: "left",
    color: colors.white,
  },
  WeatherBoldText: {
    fontSize: 55,
    textAlign: 'right',
    fontFamily: fonts.Bold
  },
  WeatherMediumText: {
    fontSize: 17,
    fontFamily: fonts.Medium
  },
  WeatherLightText: {
    fontSize: 16,
    marginBottom: 10,
    fontFamily: fonts.Light
  }
})

const itemStyles = StyleSheet.create({
  container: {
    width: (Dimensions.get('window').width) / 4,
    alignItems: "center"
  },
  ItemText: {
    fontSize: 15,
    fontFamily: fonts.Medium
  },
  itemImage: {
    width: 40,
    height: 40
  }
})

export default connect(mapStateToProps, mapDispatchToProps)(Weather);
