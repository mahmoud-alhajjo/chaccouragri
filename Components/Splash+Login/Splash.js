/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Image,
  Linking,
  Text,
  ImageBackground,
  LayoutAnimation, UIManager, Platform 
} from 'react-native';

import Dimensions from 'Dimensions';
import mainStyles from '../../css/MainStyle';
import CustomStatusBar from '../Main Structure/CustomStatusBar'
import ConfirmationPopup from '../Popup/ConfirmationPopup'
import colors from '../../css/Colors';
import * as fonts from '../../css/Fonts';
import * as Animatable from 'react-native-animatable';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux'
import { ActionCreators } from '../../Actions'
import Modal from "react-native-modal";
import DeviceInfo from 'react-native-device-info';

export class Splash extends Component {

  mainAnimationFinished = false
  requestDone = false
  shouldGetUserProfile = false
  metaDataRequestError = false
  getProfileRequestError = false
  confirmCloseGetProfilePopup = false

  constructor() {
    super();
    this.state = {
      errorModal: false,
      errorMessage: "",
      getProfileErrorModal: false,
      requestError: null,
      loginErrorModal: false,

      imageWidthAnimation :244,
      imageHeightAnimation :143,
      marginTopAnimation: 0,
      opacityAnimation : 0,
      justifyContent: 'center',
      display: "none"
    }
    if (Platform.OS === 'android')
    {
      UIManager.setLayoutAnimationEnabledExperimental(true)
    }
  }

  startAnimationForUpdateVersion = () => {
      LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
      this.setState({ justifyContent: 'flex-start', 
                      display: "flex", 
                      imageWidthAnimation: 200, 
                      imageHeightAnimation: 100, 
                      opacityAnimation: 1, 
                      marginTopAnimation: 30
                    });
  }
  
  componentDidMount () {
    this.props.getMetaData(this.onGetMetaDataSucess, this.onGetMetaDataError)
    this.props.getUserFromLocal()
  }
  
  // UNSAFE_componentWillMount() {
  //   this.props.getUserFromLocal()
  // }

  onLogoAnimationDone = () => {
    this.mainAnimationFinished = true
    if(this.requestDone) {
      this.refs.logoImage.stopAnimation()
      if(this.props.appVerion > DeviceInfo.getVersion()) {
        this.startAnimationForUpdateVersion()
      } 
      else if(this.shouldGetUserProfile) {
          this.props.navigateToHomeFromSplashForUser(this.props.navigation)
          this.props.editProfileAppVersion(this.props.user.area_size, this.props.user.specialization)
      } else {
        this.refs.logoImage.stopAnimation()
        this.props.navigateToOnBoardingFromSplash(this.props.navigation)
      }
    } else {
      this.refs.logoImage.pulse(1500)
      .then(endState => endState.finished ? this.onLogoAnimationDone() : null)
    }
  }

  onGetMetaDataSucess = (getUserProfile) => {
    this.metaDataRequestError = false
    this.shouldGetUserProfile = getUserProfile
    if(getUserProfile){
      this.props.getProfile(this.onGetProfileSucess, this.onGetProfileFailure)
    } else {
      this.props.getNoneActiveUser(this.onGetNonActiveUser)
    }
  }

  onGetMetaDataError = (error) => {
    this.setState({errorMessage:error.errorMessage, errorModal: true})
    this.metaDataRequestError = true
  }

  onGetNonActiveUser = () => {
    if(this.props.nonActiveUser !== null) {
      this.setLoginAction()
    } else {
      this.requestDone = true
    }
  }

  setLoginAction = () => {
    this.shouldGetUserProfile = true
    let nonActiveUser = this.props.nonActiveUser
    if(nonActiveUser.password) {
      this.loginAction()
    } else if(nonActiveUser.fbData) {
      this.loginFbAction()
    } else if(nonActiveUser.fbId) {
      this.loginFbAction()
    }
  }

  loginAction = () => {
    let nonActiveUser = this.props.nonActiveUser
    this.props.loginForNonActiveSplash(nonActiveUser.mobile, nonActiveUser.password, this.onLoginSuccess, this.onLoginFailure)
  }

  loginFbAction = () => {
    let nonActiveUser = this.props.nonActiveUser
    if(nonActiveUser.fbData){
      this.props.loginViaFacebookIdForNonActiveSplash(nonActiveUser.fbData.fbId, this.onLoginSuccess, this.onFacebookLoginFailure)
    } else {
      this.props.loginViaFacebookIdForNonActiveSplash(nonActiveUser.fbId, this.onLoginSuccess, this.onFacebookLoginFailure)
    }
  }

  onLoginSuccess = () => {
    console.log("Sucess")
    this.requestDone = true
  }

  onLoginFailure = (error) => {
    if(error.shouldVerify) {
      this.requestDone = true
      this.shouldGetUserProfile = false
    } else {
      this.setState({errorMessage:error.errorMessage, requestError: error, loginErrorModal: true})
    }
  }

  onFacebookLoginFailure = (error) => {
    if(error.shouldVerify) {
      this.requestDone = true
      this.shouldGetUserProfile = false
    } else {
      this.setState({errorMessage:error.errorMessage, requestError: error, loginErrorModal: true})
    }
  }

  closePopupAction = () => {
    this.setState({errorModal: false})
  }

  onModalHide = () => {
    if(this.metaDataRequestError) {
      this.props.getMetaData(this.onGetMetaDataSucess, this.onGetMetaDataError)
    }
  }

  onGetProfileSucess = () => {
    this.getProfileRequestError = false
    this.requestDone = true
  }

  onGetProfileFailure = (error) => {
    this.setState({requestError: error, getProfileErrorModal: true})
    this.getProfileRequestError = true
  }

  closeGetProfilePopupAction = () => {
    this.confirmCloseGetProfilePopup = false
    this.setState({getProfileErrorModal: false})
  }

  confirmGetProfilePopupAction = () => {
    this.confirmCloseGetProfilePopup = true
    this.setState({getProfileErrorModal: false})
  }

  onGetProfileModalHide = () => {
    if(this.state.requestError) {
      if(this.state.requestError.shouldRetry) {
        this.props.getProfile(this.onGetProfileSucess, this.onGetProfileFailure)
      } else if (this.state.requestError.shouldVerify) {
        this.props.navigateToOnBoardingFromSplash(this.props.navigation)
      } else if (this.state.requestError.shouldLogout) {
        this.props.logoutAndDeleteAllData(this.props.navigation)
      }
    }
  }

  closeLoginErrorPopupAction = () => {
    this.confirmErrorModal = false
    this.setState({loginErrorModal: false})
  }

  confirmLoginErrorPopupAction = () => {
    this.confirmErrorModal = true
    this.setState({loginErrorModal: false})
  }

  onLoginErrorModalHide = () => {
    if(this.confirmErrorModal){
      if(this.state.requestError) {
        if(this.state.requestError.shouldRetry) {
          if(this.isLogin){
            this.loginAction()
          } else {
            this.loginFbAction()
          }
        } else if (this.state.requestError.shouldLogout) {
          this.props.logoutAndDeleteAllData(this.props.navigation)
        } else if (this.state.requestError.shouldVerify) {
          this.props.navigateToOnBoardingFromSplash(this.props.navigation)
        }
      }
    }
  }

  render() {
    let newVersion = this.props.appVerion;
    let { justifyContent, display, imageWidthAnimation, 
          imageHeightAnimation, opacityAnimation, 
           marginTopAnimation } = this.state;

    return (
      <ImageBackground source={require('../../Images/splash.jpg')} style={mainStyles.flex}>
        <CustomStatusBar isHidden />
        <View style={[ styles.container,{justifyContent: justifyContent}]}>
          <Animatable.Image resizeMode="contain" ref="logoImage" easing="linear" animation="bounceInDown" duration={1500} onAnimationEnd={this.onLogoAnimationDone} source={require('../../Images/chaccourLogo2.png')} style={[styles.logo, {width: imageWidthAnimation, height: imageHeightAnimation, marginTop: marginTopAnimation}]} easing="linear"/>
        </View>
        <View style={styles.textImage}>
        <Text style={[styles.textfadeAnim , {display: display, opacity: opacityAnimation} ]}>
            { `الرجاء تحديث التطبيق إلى الإصدار ${newVersion} من الروابط التالية  `}
            <Text style={{ textDecorationLine: 'underline' }} onPress={()=> Linking.openURL('https://play.google.com/store/apps/details?id=com.chaccour_agri.app')}>متجر جوجل</Text>
            <Text>  أو  </Text>      
            <Text style={{ textDecorationLine: 'underline' }} onPress={()=> Linking.openURL('http://chaccour-agri.com/app.apk')}>من الرابط المباشر</Text>
          </Text>
        </View>
        <Modal
            transparent={true}
            style={{justifyContent: 'center', backgroundColor: 'transparent'}}
            isVisible={this.state.errorModal}
            animationIn={"bounceInUp"}
            animationOut={"bounceOutDown"}
            animationInTiming={1000}
            animationOutTiming={1000}
            onModalHide={() => this.onModalHide()}
          >
          <ConfirmationPopup
            mainTitle={"عذراً"}
            description={this.state.errorMessage}
            oneButton
            confirmText={"إعادة المحاولة"}
            confirm={this.closePopupAction}
          />
        </Modal>
        <Modal
            transparent={true}
            style={{justifyContent: 'center', backgroundColor: 'transparent'}}
            isVisible={this.state.getProfileErrorModal}
            animationIn={"bounceInUp"}
            animationOut={"bounceOutDown"}
            animationInTiming={1000}
            animationOutTiming={1000}
            onModalHide={() => this.onGetProfileModalHide()}
        >
          <ConfirmationPopup
            mainTitle={"عذراً"}
            description={this.state.requestError ? this.state.requestError.errorMessage : ""}
            oneButton
            confirmText={this.state.requestError ? this.state.requestError.shouldRetry ? "إعادة" : this.state.requestError.shouldVerify ? "إغلاق" : "إغلاق" : "إغلاق"}
            confirm={this.confirmGetProfilePopupAction}
            cancel={this.closeGetProfilePopupAction} />
        </Modal>
        <Modal
            transparent={true}
            style={{justifyContent: 'center', backgroundColor: 'transparent'}}
            isVisible={this.state.loginErrorModal}
            animationIn={"bounceInUp"}
            animationOut={"bounceOutDown"}
            animationInTiming={1000}
            animationOutTiming={1000}
            onModalHide={() => this.onLoginErrorModalHide()}
        >
          <ConfirmationPopup
            mainTitle={"عذراً"}
            description={this.state.errorMessage}
            oneButton={this.state.requestError ? this.state.requestError.shouldRetry ? false : this.state.requestError.shouldVerify ? false : true : true}
            confirmText={this.state.requestError ? this.state.requestError.shouldRetry ? "إعادة" :  this.state.requestError.shouldVerify ? "متابعة" : "إغلاق" : "إغلاق"}
            rejectText={"إغلاق"}
            confirm={this.confirmLoginErrorPopupAction}
            cancel={this.closeLoginErrorPopupAction} />
        </Modal>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },
  logo: {
    width: 244,
    height: 143,
    alignSelf: 'center'
  },
  textImage: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    justifyContent: 'center',
    alignItems: 'center'
  },
  textfadeAnim: {
    marginLeft: 16,
    marginRight: 16,
    alignSelf: "center",
    textAlign: "center",
    display: 'none',
    fontSize: 22,
    lineHeight: 40,
    color: colors.white,
    fontFamily: fonts.Medium
  }
});

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps(state) {
  return {
    nonActiveUser: state.Reducer.nonActiveUser,
    appVerion:state.Reducer.appVerion,
    user: state.Reducer.user
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Splash);
