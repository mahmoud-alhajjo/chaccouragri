/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Image,
  Text,
  TouchableOpacity,
  TextInput,
  Keyboard,
} from 'react-native';

import mainStyles from '../../css/MainStyle';
import CustomStatusBar from '../Main Structure/CustomStatusBar'
import colors from '../../css/Colors';
import * as fonts from '../../css/Fonts';
import OverlayButton from '../Helpers/OverlayButton'
import ResetPassword from './ResetPassword'
import Validator from '../Helpers/Validator'
import ConfirmationPopup from '../Popup/ConfirmationPopup'

import Dimensions from 'Dimensions';
import * as Animatable from 'react-native-animatable';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Modal from "react-native-modal";
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux'
import { ActionCreators } from '../../Actions'

export class Login extends Component {

  mobileY = 0
  passwordY = 0

  confirmErrorModal = false
  closeResetPasswordModalType = null
  resetPasswordMobile = null
  isLogin = true

  constructor() {
    super();
    this.state = {
      resetPasswordModal: false,
      mobile: null,
      password: null,
      mobileError: "",
      passwordError: "",

      errorModal: false,
      errorMessage: "",
      requestError: null,
    }
  }

  componentDidMount () {
    this.keyboardDidHideSub = Keyboard.addListener('keyboardDidHide', this.keyboardDidHide);
  }

  componentWillUnmount() {
    this.keyboardDidHideSub.remove();
  }

  keyboardDidHide = (event) => {
    this.blurTextFields()
  };

  dismissKeyboard = () => {
    Keyboard.dismiss
    this.blurTextFields()
  }

  blurTextFields = () => {
    this.refs.mobileInput.blur()
    this.refs.passwordInput.blur()
  }

  fbLoginAction = () => {
    this.isLogin = false
    this.props.loginViaFacebook(this.props.navigation, this.onFacebookLoginFailure)
  }

  onFacebookLoginFailure = (error) => {
    this.setState({errorMessage:error.errorMessage, requestError: error, errorModal: true})
  }

  loginAction = () => {
    let mobileError = Validator.getMobileError(this.state.mobile)
    let passwordError = Validator.getPasswordErrorLogin(this.state.password)

    this.setState({
      mobileError:mobileError,
      passwordError:passwordError,
    }, this.scrollToFirstErrorField)

    if((mobileError == null) && (passwordError == null)) {
      this.isLogin = true
      this.props.login(this.state.mobile, this.state.password, this.props.navigation, this.onLoginFailure)
      this.dismissKeyboard()
    }
  }

  onLoginFailure = (error) => {
    this.setState({errorMessage:error.errorMessage, requestError: error, errorModal: true})
  }

  closeErrorPopupAction = () => {
    this.confirmErrorModal = false
    this.setState({errorModal: false})
  }

  confirmErrorPopupAction = () => {
    this.confirmErrorModal = true
    this.setState({errorModal: false})
  }

  onErrorModalHide = () => {
    if(this.confirmErrorModal){
      if(this.state.requestError) {
        if(this.state.requestError.shouldRetry) {
          if(this.isLogin) {
            this.loginAction()
          } else {
            this.fbLoginAction()
          }
        } else if (this.state.requestError.shouldVerify) {
          this.props.navigation.navigate('Verification', {nonActiveUser: this.props.nonActiveUser})
        }
      }
    }
  }

  scrollToFirstErrorField = () => {
    if(this.state.mobileError != null) {
      this.scroll.props.scrollToPosition(0, this.mobileY - 10)
      this.refs.mobileInput.focus()
    } else if(this.state.passwordError != null) {
      this.scroll.props.scrollToPosition(0, this.passwordY - 10)
      this.refs.passwordInput.focus()
    }
  }

  resetPasswordAction = () => {
    this.dismissKeyboard()
    this.setState({resetPasswordModal: true})
  }

  newAccountAction = () => {
    this.props.navigation.navigate('Signup')
  }

  backAction = () => {
    this.props.navigation.goBack()
  }

  closeResetPasswordAction = (type, mobile) => {
    this.closeResetPasswordModalType = type
    this.resetPasswordMobile = mobile
    this.setState({resetPasswordModal: false})
  }

  handleChangeText = (event, field) => {
    if(field == 'mobile') {
      this.setState({mobile: event})
    } else if(field == 'password') {
      this.setState({password: event})
    }
  }

  renderTextError = (error) => {
    return(
      <Text style={styles.errorText}>
        {error}
      </Text>
    )
  }

  onResetPasswordModalHide = () => {
    if(this.closeResetPasswordModalType) {
      if(this.closeResetPasswordModalType == 'Verification') {
        if(this.resetPasswordMobile) {
          this.props.navigation.navigate('Verification', {nonActiveUser: {mobile: this.resetPasswordMobile}})
        }
      }
    }
  }

  render() {
    return (
      <View style={[mainStyles.flex, styles.container]}>
        <CustomStatusBar isHidden={false} isDark />
        <View style={[mainStyles.rowFlex, {alignItems: 'center',justifyContent: 'space-between'}]}>
          <View style={{width: 70}} />
          <Text style={styles.headerText}>
            تسجيل الدخول
          </Text>
          <TouchableOpacity style={[styles.closeButton, {alignItems: 'flex-end'}]} onPress={this.backAction}>
            <Image resizeMode={'contain'} style={styles.closeIcon} source={require('../../Images/close.png')} />
          </TouchableOpacity>
        </View>
        <KeyboardAwareScrollView innerRef={ref => {this.scroll = ref}} style={{backgroundColor: colors.white}} contentContainerStyle={{backgroundColor: colors.white}} bounces={false} enableResetScrollToCoords={true} scrollEnabled={true} enableOnAndroid={true}>
          <Animatable.View style={mainStyles.flex} animation="fadeInDownBig">
            <View style={styles.textInputView}>
              <View style={styles.textInputInnerView}
                    onLayout={event => {
                      const layout = event.nativeEvent.layout;
                      this.mobileY = layout.y
                    }}
              >
                <TextInput
                  ref="mobileInput"
                  placeholder={"رقم الجوال"}
                  underlineColorAndroid ={"transparent"}
                  placeholderTextColor={colors.slate}
                  returnKeyType={"next"}
                  maxLength={10}
                  blurOnSubmit={false}
                  keyboardType={"phone-pad"}
                  selectionColor={colors.slate}
                  style={[mainStyles.flex, styles.textInput]}
                  onChangeText={((event) => this.handleChangeText(event, 'mobile'))}
                  value={this.state.mobile ? this.state.mobile : ""}
                  onSubmitEditing={(event) => this.refs.passwordInput.focus()}
                />
                {(this.state.mobileError == "" || this.state.mobileError == null) ? null : this.renderTextError(this.state.mobileError)}
              </View>
              <View style={{width: '100%', height: 1, backgroundColor: colors.silver}} />
              <View style={styles.textInputInnerView}
                    onLayout={event => {
                      const layout = event.nativeEvent.layout;
                      this.passwordY = layout.y
                    }}
              >
                <TextInput
                  ref="passwordInput"
                  placeholder={"كلمة المرور"}
                  underlineColorAndroid ={"transparent"}
                  placeholderTextColor={colors.slate}
                  returnKeyType={"done"}
                  autoCapitalize={"none"}
                  autoCorrect={false}
                  maxLength={50}
                  blurOnSubmit={false}
                  secureTextEntry={true}
                  selectionColor={colors.slate}
                  style={[mainStyles.flex, styles.textInput]}
                  onChangeText={((event) => this.handleChangeText(event, 'password'))}
                  value={this.state.password ? this.state.password : ""}
                  onSubmitEditing={(event) => this.loginAction()}
                />
                {(this.state.passwordError == "" || this.state.passwordError == null) ? null : this.renderTextError(this.state.passwordError)}
              </View>
            </View>
            <View style={{marginLeft: 36, marginRight: 36, marginTop: 21}}>
              <OverlayButton buttonColors={[colors.fernGreen, colors.fernGreenTwo]} buttonBackgroundColor={colors.fernGreen} buttonTextColor={colors.white} clickButton={this.loginAction} style={mainStyles.flex} text={"تسجيل دخول"} customStyle={styles.loginButton} textButtonStyle={styles.loginText} />
            </View>
            <View style={{marginLeft: 36, marginRight: 36, marginTop: 13}}>
              <OverlayButton buttonBackgroundColor={colors.coolBlue} buttonTextColor={colors.white} clickButton={this.fbLoginAction} style={mainStyles.flex} text={"تسجيل عن طريق حساب الفيسبوك"} customStyle={styles.fbButton} source={require('../../Images/fbLogo.png')} iconStyle={styles.fbImage} textButtonStyle={styles.fbText} />
            </View>
            <TouchableOpacity onPress={this.resetPasswordAction} style={{marginTop: 15, minHeight: 35, justifyContent: 'center'}}>
              <Text style={styles.resetPasswordText}>
                نسيت معلومات الدخول؟
              </Text>
            </TouchableOpacity>
            <View style={{marginLeft: 36, marginRight: 36, marginTop: 15}}>
              <OverlayButton buttonColors={[colors.blush, colors.darkPeach]} buttonBackgroundColor={colors.darkPeach} buttonTextColor={colors.white} clickButton={this.newAccountAction} style={mainStyles.flex} text={"إنشاء حساب جديد"} customStyle={styles.loginButton} textButtonStyle={styles.textButton} />
            </View>
          </Animatable.View>
        </KeyboardAwareScrollView>
        <Modal
            transparent={true}
            onBackButtonPress={() => this.closeResetPasswordAction()}
            style={{justifyContent: 'center', backgroundColor: 'transparent'}}
            isVisible={this.state.resetPasswordModal}
            animationIn={"bounceInUp"}
            animationOut={"bounceOutDown"}
            animationInTiming={1000}
            animationOutTiming={1000}
            onModalHide={() => this.onResetPasswordModalHide()}
          >
          <ResetPassword cancel={this.closeResetPasswordAction} />
        </Modal>
        <Modal
            transparent={true}
            style={{justifyContent: 'center', backgroundColor: 'transparent'}}
            isVisible={this.state.errorModal}
            animationIn={"bounceInUp"}
            animationOut={"bounceOutDown"}
            animationInTiming={1000}
            animationOutTiming={1000}
            onModalHide={() => this.onErrorModalHide()}
        >
          <ConfirmationPopup
            mainTitle={"عذراً"}
            description={this.state.errorMessage}
            oneButton={this.state.requestError ? this.state.requestError.shouldRetry ? false : this.state.requestError.shouldVerify ? false : true : true}
            confirmText={this.state.requestError ? this.state.requestError.shouldRetry ? "إعادة" :  this.state.requestError.shouldVerify ? "متابعة" : "إغلاق" : "إغلاق"}
            rejectText={"إغلاق"}
            confirm={this.confirmErrorPopupAction}
            cancel={this.closeErrorPopupAction} />
        </Modal>
      </View>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps(state) {
  return {
    nonActiveUser: state.Reducer.nonActiveUser,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.white
  },
  headerText: {
    fontFamily: fonts.Medium,
    fontSize: 20,
    letterSpacing: 0,
    textAlign: "center",
    color: colors.charcoalGreyTwo
  },
  closeButton: {
    width:70,
    height:45,
    justifyContent: 'center',
  },
  closeIcon: {
    marginRight: 19,
    width: 14,
    height: 14,
  },
  chaccorIcon: {
    marginTop: 43,
    alignSelf: 'center',
    width: Dimensions.get('window').width * 0.24,
    height: Dimensions.get('window').height * 0.134,
  },
  textInputView: {
    marginTop: 30,
    marginLeft: 36,
    marginRight: 36,
    height: 133,
    borderRadius: 4,
    borderStyle: "solid",
    borderWidth: 1,
    borderColor: colors.silverTwo,
  },
  textInputInnerView: {
    height: '50%',
    paddingLeft: 16,
    paddingRight: 16,
  },
  textInput: {
    paddingTop: 0,
    paddingBottom: 0,
    fontFamily: fonts.Light,
    fontSize: 15,
    letterSpacing: 0,
    textAlign: "right",
    color: colors.slate
  },
  fbButton: {
    height: 65,
    borderRadius: 3,
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: "rgba(0, 0, 0, 0.2)",
    shadowOffset: {
      width: 0,
      height: 6
    },
    shadowRadius: 10,
    shadowOpacity: 1,
    elevation: 5,
  },
  fbText: {
    fontFamily: fonts.Medium,
    fontSize: 14,
    letterSpacing: 0,
    textAlign: "center",
  },
  fbImage: {
    marginTop: 10,
    marginRight: 11,
    width: 22,
    height: 34.6,
  },
  loginButton: {
    height: 65,
    borderRadius: 4,
    shadowColor: "rgba(0, 0, 0, 0.21)",
    shadowOffset: {
      width: 7,
      height: -8
    },
    shadowRadius: 26,
    shadowOpacity: 1,
    elevation: 5,
  },
  loginText: {
    fontFamily: fonts.Medium,
    fontSize: 18,
    letterSpacing: 0,
    textAlign: "center",
  },
  resetPasswordText: {
    fontFamily: fonts.Medium,
    fontSize: 13,
    letterSpacing: 0,
    textAlign: "center",
    color: colors.coolGrey
  },
  newAccountText: {
    fontFamily: fonts.Bold,
    fontSize: 18,
    letterSpacing: 0,
    textAlign: "center",
    color: colors.slate
  },
  errorText: {
    fontFamily: fonts.Light,
    fontSize: 12,
    letterSpacing: 0,
    textAlign: "right",
    color: colors.darkishRed,
    marginBottom: 6,
  },
  textButton: {
    fontFamily: fonts.Medium,
    fontSize: 18,
    letterSpacing: 0,
    textAlign: "center",
  },
});
