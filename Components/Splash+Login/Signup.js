/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Image,
  Text,
  TouchableOpacity,
  TextInput,
  Keyboard,
  PermissionsAndroid,
  ImageBackground
} from 'react-native';

import mainStyles from '../../css/MainStyle';
import CustomStatusBar from '../Main Structure/CustomStatusBar'
import colors from '../../css/Colors';
import * as fonts from '../../css/Fonts';
import OverlayButton from '../Helpers/OverlayButton'
import SelectionPopup from '../Popup/SelectionPopup'
import * as constants from '../../Config/AppConstants';
import Validator from '../Helpers/Validator'
import ConfirmationPopup from '../Popup/ConfirmationPopup'

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import DateTimePicker from 'react-native-modal-datetime-picker';
import Moment from 'moment';
import * as Animatable from 'react-native-animatable';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux'
import { ActionCreators } from '../../Actions'
import Modal from "react-native-modal";

export class Signup extends Component {

  popupType = null

  nameY = 0
  lastNameY = 0
  occuptionY = 0
  areaSizeY = 0
  mobileY = 0
  provinceY = 0
  cityY = 0
  genderY = 0
  birthDateY = 0
  passwordY = 0

  registerError = false
  retryRegister = false
  constructor(props) {
    super(props);

    var fbData = null
    if(props.navigation.state.params !== undefined) {
      if(props.navigation.state.params.fbData){
        fbData = props.navigation.state.params.fbData
      }
    }
    this.state = {
      profilePic: fbData ? fbData.profilePic : null,
      profilePic64: fbData ? fbData.profilePic64 : null,
      isDateTimePickerVisible: false,
      birthdate: null,
      birthdateString: "",
      selectionModal: false,

      firstName: fbData ? fbData.firstName : null,
      lastName: fbData ? fbData.lastName : null,
      occupation: null,
      areaSize: null,
      specialization: null,
      mobile: null,
      province: null,
      city: null,
      gender: null,
      password: null,

      nameError:"",
      lastNameError:"",
      occuptionError:"",
      areaSizeError: "",
      specializationError: "",
      mobileError:"",
      provinceError:"",
      cityError: "",
      genderError: "",
      birthDateError: "",
      passwordError: "",

      errorModal: false,
      errorMessage: "",
      requestError: null,
    }
  }

  componentDidMount () {
    this.keyboardDidHideSub = Keyboard.addListener('keyboardDidHide', this.keyboardDidHide);
  }

  componentWillUnmount() {
    this.keyboardDidHideSub.remove();
  }

  keyboardDidHide = (event) => {
    this.blurTextFields()
  };

  dismissKeyboard = () => {
    Keyboard.dismiss
    this.blurTextFields()
  }

  blurTextFields = () => {
    this.refs.lastNameInput.blur()
    this.refs.firstNameInput.blur()
    this.refs.mobileInput.blur()
    if(this.refs.passwordInput) {
      this.refs.passwordInput.blur()
    }
  }

  signupAction = () => {

    let fbData = null
    if(this.props.navigation.state.params !== undefined) {
      fbData = this.props.navigation.state.params.fbData
    }

    let nameError = Validator.getNameError(this.state.firstName)
    let lastNameError = Validator.getLastNameError(this.state.lastName)
    let occuptionError = Validator.getOccuptionError(this.state.occupation)
    let mobileError = Validator.getMobileError(this.state.mobile)
    let provinceError = Validator.getProvinceError(this.state.province)
    let cityError = Validator.getCityError(this.state.city)
    let genderError = Validator.getGenderError(this.state.gender)
    let birthDateError = Validator.getBirthDateError(this.state.birthdate)
    let passwordError = Validator.getPasswordError(this.state.password)

    if(fbData) {
      passwordError = null
    }

    let areaSizeError = null
    if(this.state.occupation) {
      if(this.state.occupation.name == "مزارع") {
        if(this.state.areaSize == null){
          areaSizeError = "الرجاء ادخال مساحة الأرض"
        }
      }
    }

    let specializationError = null
    if(this.state.occupation) {
      if((this.state.occupation.name == "طالب جامعي في الهندسة الزراعية") || (this.state.occupation.name == "دكتور / أستاذ في كلية الزراعة")) {
        if(this.state.specialization == null){
          specializationError = "الرجاء اختيار الاختصاص"
        }
      }
    }

    this.setState({
      nameError:nameError,
      lastNameError:lastNameError,
      occuptionError:occuptionError,
      areaSizeError:areaSizeError,
      specializationError: specializationError,
      mobileError:mobileError,
      provinceError:provinceError,
      cityError: cityError,
      genderError: genderError,
      birthDateError: birthDateError,
      passwordError: passwordError,
    }, this.scrollToFirstErrorField)

    if ((nameError == null) && (lastNameError == null) && (occuptionError == null) && (areaSizeError == null) && (specializationError == null) && (mobileError == null) && (provinceError == null) && (cityError == null) && (genderError == null) && (birthDateError == null) && (passwordError == null)) {
      let areaSizeId = null
      if(this.state.areaSize) {
        areaSizeId = this.state.areaSize.id
      }
      let specializationId = null
      if(this.state.specialization) {
        specializationId = this.state.specialization.id
      }
      this.props.signup(this.state.profilePic64, this.state.firstName, this.state.lastName, this.state.occupation.id, this.state.mobile, this.state.province.id, this.state.city.id, this.state.gender.id, this.state.birthdateString, this.state.password, fbData, areaSizeId, specializationId, this.onSignupSucess, this.onSignupFailure)
      this.dismissKeyboard()
    }
  }

  onSignupSucess = () => {
    this.registerError = false
    let message = "تم انشاء الحساب بشكل صحيح. اضغط على متابعة لإكمال عملية التسجيل."
    this.setState({errorMessage:message, requestError:null, errorModal: true})
  }

  onSignupFailure = (error) => {
    this.registerError = true
    this.setState({errorMessage:error.errorMessage, requestError: error, errorModal: true})
  }

  scrollToFirstErrorField = () => {
    if(this.state.nameError != null) {
      this.scroll.props.scrollToPosition(0, this.nameY - 10)
      this.refs.firstNameInput.focus()
    } else if(this.state.lastNameError != null) {
      this.scroll.props.scrollToPosition(0, this.lastNameY - 10)
      this.refs.lastNameInput.focus()
    } else if(this.state.occuptionError != null) {
      this.scroll.props.scrollToPosition(0, this.occuptionY - 10)
    } else if(this.state.areaSizeError != null) {
      this.scroll.props.scrollToPosition(0, this.areaSizeY - 10)
    } else if(this.state.specializationError != null) {
      this.scroll.props.scrollToPosition(0, this.areaSizeY - 10)
    } else if(this.state.mobileError != null) {
      this.scroll.props.scrollToPosition(0, this.mobileY - 10)
      this.refs.mobileInput.focus()
    } else if(this.state.provinceError != null) {
      this.scroll.props.scrollToPosition(0, this.provinceY - 10)
    } else if(this.state.cityError != null) {
      this.scroll.props.scrollToPosition(0, this.cityY - 10)
    } else if(this.state.genderError != null) {
      this.scroll.props.scrollToPosition(0, this.genderY - 10)
    } else if(this.state.birthDateError != null) {
      this.scroll.props.scrollToPosition(0, this.birthDateY - 10)
    } else if(this.state.passwordError != null) {
      this.scroll.props.scrollToPosition(0, this.passwordY - 10)
      this.refs.passwordInput.focus()
    }
  }

  backAction = () => {
    this.props.navigateToOnBoardingFromSplash(this.props.navigation)
  }

  openImageGallery = () => {
    var ImagePicker = require('react-native-image-picker');
    var options = {
      title: 'اختيار صورة الحساب',
      takePhotoButtonTitle: 'التقاط صورة',
      chooseFromLibraryButtonTitle: "الاختيار من المكتبة",
      cancelButtonTitle: "إلغاء",
      mediaType: 'photo',
      allowsEditing: true,
      maxWidth: 400,
      maxHeight: 200,
    };
    ImagePicker.showImagePicker(options, response  => {
      if (response.didCancel) {
      } else if (response.error) {
        alert(response.error)
      } else if (response.customButton) {
      } else {
          let source = { uri: response.uri };
          setTimeout(() => {
            this.setState({
              profilePic: source,
              profilePic64: response.data
            });
          }, 500);
      }
    });
  }

  selectProfilePic = async () => {
    try {
      let granted = await PermissionsAndroid.requestMultiple([PermissionsAndroid.PERMISSIONS.CAMERA, PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE])
      if ((granted[PermissionsAndroid.PERMISSIONS.CAMERA] === PermissionsAndroid.RESULTS.GRANTED) && (granted[PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE] === PermissionsAndroid.RESULTS.GRANTED)){
        this.openImageGallery()
      } else {
        alert('الرجاء إعطاء صلاحية للكاميرا')
      }
    } catch (err) {
    }
  }


  onGetOccupationsList = () => {
    this.popupType = constants.Occupations
    this.setState({selectionModal: true})
  }

  onGetAreaSizesList = () => {
    this.popupType = constants.AreaSize
    this.setState({selectionModal: true})
  }

  onGetSpecializationList = () => {
    this.popupType = constants.Specialization
    this.setState({selectionModal: true})
  }

  onGetCitiesList = () => {
    this.popupType = constants.City
    this.setState({selectionModal: true})
  }

  cityClicked = () => {
    if(this.state.province) {
      this.props.getCities(this.state.province.id, this.onGetCitiesList)
    } else {
      let provinceError = Validator.getProvinceError(this.state.province)
      this.setState({provinceError:provinceError})
    }
  }

  onGetProvinceList = () => {
    this.popupType = constants.Province
    this.setState({selectionModal: true})
  }

  datePickerAction = () => {
    this.showDateTimePicker()
  }

  onGetGenderList = () => {
    this.popupType = constants.Gender
    this.setState({selectionModal: true})
  }

  showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });

  hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });

  handleDatePicked = (date) => {
    this.setState({
      birthdate: date,
      birthdateString: Moment(date).locale('en').format('DD-MM-YYYY'),
      birthDateError: "",
    }, this.hideDateTimePicker)
  }

  closeSelectionPopupAction = () => {
    this.setState({selectionModal: false})
  }

  handlePopupSelection = (selectedItem, popupType) => {
    if(popupType == constants.Occupations) {
      this.setState({occupation:selectedItem, occuptionError:"", areaSize: null, specialization: null, selectionModal: false})
    } else if(popupType == constants.AreaSize) {
      this.setState({areaSize:selectedItem, areaSizeError:"", selectionModal: false})
    } else if(popupType == constants.Specialization) {
      this.setState({specialization:selectedItem, specializationError:"", selectionModal: false})
    } else if(popupType == constants.Province) {
      this.setState({province:selectedItem, provinceError:"", city: null, selectionModal: false})
    } else if(popupType == constants.City) {
     this.setState({city:selectedItem, cityError:"", selectionModal: false})
    } else if(popupType == constants.Gender) {
     this.setState({gender:selectedItem, genderError:"", selectionModal: false})
    }
  }

  getListForSelectionPopup = () => {
    let popupType = this.popupType
    if(popupType == constants.Occupations){
      return this.props.occupations
    } else if(popupType == constants.AreaSize){
      return this.props.areaSize
    } else if(popupType == constants.Specialization){
      return this.props.specialization
    } else if(popupType == constants.Province){
      return this.props.province
    } else if(popupType == constants.City){
     return this.props.cities
    } else if(popupType == constants.Gender){
     return this.props.gender
    }
    return null
  }

  getSelectedItemSelectionPopup = () => {
    let popupType = this.popupType
    if(popupType == constants.Occupations){
      return this.state.occupation
    } else if(popupType == constants.AreaSize){
      return this.state.areaSize
    } else if(popupType == constants.Specialization){
      return this.state.specialization
    } else if(popupType == constants.Province){
      return this.state.province
    } else if(popupType == constants.City){
      return this.state.city
    } else if(popupType == constants.Gender){
      return this.state.gender
    }
    return null
  }

  onSelectionModalHide = () => {
    this.popupType = null
  }

  handleChangeText = (event, field) => {
    if(field == 'firstName') {
      this.setState({firstName: event})
    } else if(field == 'lastName') {
      this.setState({lastName: event})
    } else if(field == 'mobile') {
      this.setState({mobile: event})
    } else if(field == 'password') {
      this.setState({password: event})
    }
  }

  renderTextError = (error) => {
    return(
      <Text style={styles.errorText}>
        {error}
      </Text>
    )
  }

  onErrorModalHide = () => {
    var fbData = null
    if(this.props.navigation.state.params !== undefined) {
      fbData = this.props.navigation.state.params.fbData
    }

    if(this.registerError) {
      if(this.retryRegister) {
        if(this.state.requestError) {
          if(this.state.requestError.shouldRetry) {
            this.signupAction()
          } else if (this.state.requestError.shouldVerify) {
            this.props.navigation.navigate('Verification', {nonActiveUser: {mobile: this.state.mobile, password: this.state.password, fbData: fbData}})
          }
        }
      }
    } else {
      this.props.navigation.navigate('Verification', {nonActiveUser: {mobile: this.state.mobile, password: this.state.password, fbData: fbData}})
    }
  }

  closePopupAction = () => {
    this.retryRegister = false
    this.setState({errorModal: false})
  }

  retryRegisterAction = () => {
    this.retryRegister = true
    this.setState({errorModal: false})
  }

  render() {
    var fbData = null
    if(this.props.navigation.state.params !== undefined) {
      fbData = this.props.navigation.state.params.fbData
    }
    let areaSizeField = null
    if(this.state.occupation) {
      if(this.state.occupation.name == "مزارع") {
        areaSizeField = <View style={[styles.mainInputField, {justifyContent:null, alignItems: null}]}>
                        <View style={[mainStyles.rowFlex, mainStyles.flex, {justifyContent:'space-between', alignItems: 'center'}]}>
                          <Image style={styles.downArrow} source={require('../../Images/downArrow.png')}/>
                          <View style={[styles.singleTextInputView, {marginLeft: 16}]}
                                onLayout={event => {
                                  const layout = event.nativeEvent.layout;
                                  this.areaSizeY = layout.y
                                }}
                          >
                            <TextInput
                              ref="jobInput"
                              placeholder={"مساحة الأرض المزروعة (دنم)"}
                              underlineColorAndroid ={"transparent"}
                              placeholderTextColor={colors.slate}
                              blurOnSubmit={false}
                              selectionColor={colors.slate}
                              style={[mainStyles.flex, styles.textInput]}
                              editable={false}
                              value={this.state.areaSize ? this.state.areaSize.name : ""}
                            />
                          </View>
                        </View>
                        {(this.state.areaSizeError == "" || this.state.areaSizeError == null) ? null : this.renderTextError(this.state.areaSizeError)}
                        <TouchableOpacity onPress={() => this.props.getAreaSize(this.onGetAreaSizesList)} style={{position: 'absolute', top: 0, left: 0, right: 0, bottom: 0}}/>
                      </View>
      }
    }
    let specializationField = null
    if(this.state.occupation) {
      if((this.state.occupation.name == "طالب جامعي في الهندسة الزراعية") || (this.state.occupation.name == "دكتور / أستاذ في كلية الزراعة")) {
        specializationField = <View style={[styles.mainInputField, {justifyContent:null, alignItems: null}]}>
                        <View style={[mainStyles.rowFlex, mainStyles.flex, {justifyContent:'space-between', alignItems: 'center'}]}>
                          <Image style={styles.downArrow} source={require('../../Images/downArrow.png')}/>
                          <View style={[styles.singleTextInputView, {marginLeft: 16}]}
                                onLayout={event => {
                                  const layout = event.nativeEvent.layout;
                                  this.areaSizeY = layout.y
                                }}
                          >
                            <TextInput
                              ref="jobInput"
                              placeholder={"الاختصاص"}
                              underlineColorAndroid ={"transparent"}
                              placeholderTextColor={colors.slate}
                              blurOnSubmit={false}
                              selectionColor={colors.slate}
                              style={[mainStyles.flex, styles.textInput]}
                              editable={false}
                              value={this.state.specialization ? this.state.specialization.name : ""}
                            />
                          </View>
                        </View>
                        {(this.state.specializationError == "" || this.state.specializationError == null) ? null : this.renderTextError(this.state.specializationError)}
                        <TouchableOpacity onPress={() => this.props.getSpecialization(this.onGetSpecializationList)} style={{position: 'absolute', top: 0, left: 0, right: 0, bottom: 0}}/>
                      </View>
      }
    }
    let passwordField = <View style={[mainStyles.rowFlex, styles.mainInputFieldNoBorder]}>
                    <View style={styles.singleTextInputView}
                          onLayout={event => {
                            const layout = event.nativeEvent.layout;
                            this.passwordY = layout.y
                          }}
                    >
                      <TextInput
                        ref="passwordInput"
                        placeholder={"كلمة المرور"}
                        underlineColorAndroid ={"transparent"}
                        placeholderTextColor={colors.slate}
                        returnKeyType={"done"}
                        autoCapitalize={"none"}
                        autoCorrect={false}
                        maxLength={50}
                        blurOnSubmit={false}
                        secureTextEntry={true}
                        selectionColor={colors.slate}
                        style={[mainStyles.flex, styles.textInput]}
                        onChangeText={((event) => this.handleChangeText(event, 'password'))}
                        value={this.state.password ? this.state.password : ""}
                        onSubmitEditing={(event) => this.signupAction()}
                      />
                      {(this.state.passwordError == "" || this.state.passwordError == null) ? null : this.renderTextError(this.state.passwordError)}
                    </View>
                  </View>

    if(fbData) {
      passwordField = null
    }
    return (
      <View style={[mainStyles.flex, styles.container]}>
        <CustomStatusBar isHidden={false} isDark />
        <View style={[mainStyles.rowFlex, {alignItems: 'center',justifyContent: 'space-between'}]}>
          <View style={{width: 70}} />
          <Text style={styles.headerText}>
            إنشاء حساب جديد
          </Text>
          <TouchableOpacity style={[styles.closeButton, {alignItems: 'flex-end'}]} onPress={this.backAction}>
            <Image resizeMode={'contain'} style={styles.closeIcon} source={require('../../Images/close.png')} />
          </TouchableOpacity>
        </View>
        <KeyboardAwareScrollView innerRef={ref => {this.scroll = ref}} style={{backgroundColor: colors.white}} contentContainerStyle={{backgroundColor: colors.white}} bounces={false} enableResetScrollToCoords={true} scrollEnabled={true} enableOnAndroid={true}>
          <Animatable.View style={mainStyles.flex} animation="fadeInDownBig">
            <TouchableOpacity style={styles.profilePicMainView} onPress={this.selectProfilePic}>
              {
                this.state.profilePic ? null :  <Image resizeMode={'contain'} style={styles.cameraPic} source={require('../../Images/imgCamera.png')}/>
              }
              <ImageBackground source={require('../../Images/imgPlaceholderBg.png')} style={styles.profilePicView}>
                <Image style={this.state.profilePic ? styles.profilePic : styles.profilePicEmpty} source={this.state.profilePic ? this.state.profilePic : require('../../Images/imgPlaceholder.png')}/>
              </ImageBackground>
            </TouchableOpacity>
            <View style={styles.inputView}>
              <View style={[mainStyles.rowFlex, styles.mainInputField]}>
                <View style={[styles.doubleTextInputView, {paddingRight: 16, borderStyle: "solid", borderRightWidth: 1, borderRightColor: colors.silver}]}
                      onLayout={event => {
                        const layout = event.nativeEvent.layout;
                        this.lastNameY = layout.y
                      }}
                >
                  <TextInput
                    ref="lastNameInput"
                    placeholder={"اسم العائلة"}
                    underlineColorAndroid ={"transparent"}
                    placeholderTextColor={colors.slate}
                    returnKeyType={"next"}
                    autoCapitalize={"words"}
                    autoCorrect={false}
                    maxLength={50}
                    blurOnSubmit={false}
                    selectionColor={colors.slate}
                    style={[mainStyles.flex, styles.textInput]}
                    onChangeText={((event) => this.handleChangeText(event, 'lastName'))}
                    value={this.state.lastName ? this.state.lastName : ""}
                    onSubmitEditing={(event) => this.dismissKeyboard()}
                  />
                  {(this.state.lastNameError == "" || this.state.lastNameError == null) ? null : this.renderTextError(this.state.lastNameError)}
                </View>
                <View style={[styles.doubleTextInputView, {paddingLeft: 16}]}
                      onLayout={event => {
                        const layout = event.nativeEvent.layout;
                        this.nameY = layout.y
                      }}
                >
                  <TextInput
                    ref="firstNameInput"
                    placeholder={"الاسم"}
                    underlineColorAndroid ={"transparent"}
                    placeholderTextColor={colors.slate}
                    returnKeyType={"next"}
                    autoCapitalize={"words"}
                    autoCorrect={false}
                    maxLength={50}
                    blurOnSubmit={false}
                    selectionColor={colors.slate}
                    style={[mainStyles.flex, styles.textInput]}
                    onChangeText={((event) => this.handleChangeText(event, 'firstName'))}
                    value={this.state.firstName ? this.state.firstName : ""}
                    onSubmitEditing={(event) => this.refs.lastNameInput.focus()}
                  />
                  {(this.state.nameError == "" || this.state.nameError == null) ? null : this.renderTextError(this.state.nameError)}
                </View>
              </View>
              <View style={[styles.mainInputField, {justifyContent:null, alignItems: null}]}>
                <View style={[mainStyles.rowFlex, mainStyles.flex, {justifyContent:'space-between', alignItems: 'center'}]}>
                  <Image style={styles.downArrow} source={require('../../Images/downArrow.png')}/>
                  <View style={[styles.singleTextInputView, {marginLeft: 16}]}
                        onLayout={event => {
                          const layout = event.nativeEvent.layout;
                          this.occuptionY = layout.y
                        }}
                  >
                    <TextInput
                      ref="jobInput"
                      placeholder={"المهنة"}
                      underlineColorAndroid ={"transparent"}
                      placeholderTextColor={colors.slate}
                      blurOnSubmit={false}
                      selectionColor={colors.slate}
                      style={[mainStyles.flex, styles.textInput]}
                      editable={false}
                      value={this.state.occupation ? this.state.occupation.name : ""}
                    />
                  </View>
                </View>
                {(this.state.occuptionError == "" || this.state.occuptionError == null) ? null : this.renderTextError(this.state.occuptionError)}
                <TouchableOpacity onPress={() => this.props.getOccupations(this.onGetOccupationsList)} style={{position: 'absolute', top: 0, left: 0, right: 0, bottom: 0}}/>
              </View>
              {areaSizeField}
              {specializationField}
              <View style={[mainStyles.rowFlex, styles.mainInputField]}>
                <View style={styles.singleTextInputView}
                      onLayout={event => {
                        const layout = event.nativeEvent.layout;
                        this.mobileY = layout.y
                      }}
                >
                  <TextInput
                    ref="mobileInput"
                    placeholder={"رقم الجوال"}
                    underlineColorAndroid ={"transparent"}
                    placeholderTextColor={colors.slate}
                    returnKeyType={"next"}
                    maxLength={10}
                    blurOnSubmit={false}
                    keyboardType={"phone-pad"}
                    selectionColor={colors.slate}
                    style={[mainStyles.flex, styles.textInput]}
                    onChangeText={((event) => this.handleChangeText(event, 'mobile'))}
                    value={this.state.mobile ? this.state.mobile : ""}
                    onSubmitEditing={(event) => this.dismissKeyboard()}
                  />
                  {(this.state.mobileError == "" || this.state.mobileError == null) ? null : this.renderTextError(this.state.mobileError)}
                </View>
              </View>
              <View style={[mainStyles.rowFlex, styles.mainInputField]}>
                <View style={[styles.doubleTextInputView, {paddingRight: 16, borderStyle: "solid", borderRightWidth: 1, borderRightColor: colors.silver}]}>
                  <View style={[mainStyles.flex, mainStyles.rowFlex, {alignItems:'center'}]}
                        onLayout={event => {
                          const layout = event.nativeEvent.layout;
                          this.cityY = layout.y
                        }}
                  >
                    <Image style={styles.downArrow} source={require('../../Images/downArrow.png')}/>
                    <TextInput
                      ref="cityInput"
                      placeholder={"المنطقة"}
                      underlineColorAndroid ={"transparent"}
                      placeholderTextColor={colors.slate}
                      blurOnSubmit={false}
                      selectionColor={colors.slate}
                      style={[mainStyles.flex, styles.textInput]}
                      editable={false}
                      value={this.state.city ? this.state.city.name : ""}
                    />
                  </View>
                  {(this.state.cityError == "" || this.state.cityError == null) ? null : this.renderTextError(this.state.cityError)}
                  <TouchableOpacity onPress={() => this.cityClicked()} style={{position: 'absolute', top: 0, left: 0, right: 0, bottom: 0}}/>
                </View>
                <View style={[styles.doubleTextInputView, {paddingLeft: 16}]}>
                  <View style={[mainStyles.flex, mainStyles.rowFlex, {alignItems:'center'}]}
                        onLayout={event => {
                          const layout = event.nativeEvent.layout;
                          this.provinceY = layout.y
                        }}
                  >
                    <Image style={styles.downArrow} source={require('../../Images/downArrow.png')}/>
                    <TextInput
                      ref="provinceInput"
                      placeholder={"محافظة"}
                      underlineColorAndroid ={"transparent"}
                      placeholderTextColor={colors.slate}
                      blurOnSubmit={false}
                      selectionColor={colors.slate}
                      style={[mainStyles.flex, styles.textInput]}
                      editable={false}
                      value={this.state.province ? this.state.province.name : ""}
                    />
                  </View>
                  {(this.state.provinceError == "" || this.state.provinceError == null) ? null : this.renderTextError(this.state.provinceError)}
                  <TouchableOpacity onPress={() => this.props.getProvince(this.onGetProvinceList)} style={{position: 'absolute', top: 0, left: 0, right: 0, bottom: 0}}/>
                </View>
              </View>
              <View style={[mainStyles.rowFlex, styles.mainInputField]}>
                <View style={[styles.doubleTextInputView, {paddingRight: 16, borderStyle: "solid", borderRightWidth: 1, borderRightColor: colors.silver}]}>
                  <View style={[mainStyles.flex, mainStyles.rowFlex, {alignItems:'center'}]}
                        onLayout={event => {
                          const layout = event.nativeEvent.layout;
                          this.birthDateY = layout.y
                        }}
                  >
                    <Image style={styles.downArrow} source={require('../../Images/downArrow.png')}/>
                    <TextInput
                      ref="birthDateInput"
                      placeholder={"تاريخ الميلاد"}
                      underlineColorAndroid ={"transparent"}
                      placeholderTextColor={colors.slate}
                      blurOnSubmit={false}
                      selectionColor={colors.slate}
                      style={[mainStyles.flex, styles.textInput]}
                      editable={false}
                      value={this.state.birthdateString}
                    />
                  </View>
                  {(this.state.birthDateError == "" || this.state.birthDateError == null) ? null : this.renderTextError(this.state.birthDateError)}
                  <TouchableOpacity onPress={this.datePickerAction} style={{position: 'absolute', top: 0, left: 0, right: 0, bottom: 0}}/>
                </View>
                <View style={[styles.doubleTextInputView, {paddingLeft: 16}]}>
                  <View style={[mainStyles.flex, mainStyles.rowFlex, {alignItems:'center'}]}
                        onLayout={event => {
                          const layout = event.nativeEvent.layout;
                          this.genderY = layout.y
                        }}
                  >
                    <Image style={styles.downArrow} source={require('../../Images/downArrow.png')}/>
                    <TextInput
                      ref="genderInput"
                      placeholder={"الجنس"}
                      underlineColorAndroid ={"transparent"}
                      placeholderTextColor={colors.slate}
                      blurOnSubmit={false}
                      selectionColor={colors.slate}
                      style={[mainStyles.flex, styles.textInput]}
                      editable={false}
                      value={this.state.gender ? this.state.gender.name : ""}
                    />
                  </View>
                  {(this.state.genderError == "" || this.state.genderError == null) ? null : this.renderTextError(this.state.genderError)}
                  <TouchableOpacity onPress={() => this.props.getGender(this.onGetGenderList)} style={{position: 'absolute', top: 0, left: 0, right: 0, bottom: 0}}/>
                </View>
              </View>
              {passwordField}
            </View>
            <View style={{marginLeft: 36, marginRight: 36, marginTop: 25, marginBottom: 25}}>
              <OverlayButton buttonColors={[colors.blush, colors.darkPeach]} buttonBackgroundColor={colors.darkPeach} buttonTextColor={colors.white} clickButton={this.signupAction} style={mainStyles.flex} text={"إنشاء حساب جديد"} customStyle={styles.loginButton} textButtonStyle={styles.loginText} />
            </View>
          </Animatable.View>
        </KeyboardAwareScrollView>
        <DateTimePicker
          isVisible={this.state.isDateTimePickerVisible}
          onConfirm={this.handleDatePicked}
          onCancel={this.hideDateTimePicker}
          maximumDate={Moment().locale('en').toDate()}
          minimumDate={Moment().locale('en').subtract(100, 'y').toDate()}
          locale={'ar'}
          date={this.state.birthdate ? this.state.birthdate : Moment().locale('en').subtract(40, 'y').toDate()}
        />
        <Modal
            transparent={true}
            onBackButtonPress={() => this.closeSelectionPopupAction()}
            onBackdropPress={() => this.closeSelectionPopupAction()}
            style={{justifyContent: 'center', alignItems: 'center', backgroundColor: 'transparent'}}
            isVisible={this.state.selectionModal}
            animationIn={"bounceInUp"}
            animationOut={"bounceOutDown"}
            animationInTiming={1000}
            animationOutTiming={1000}
            onModalHide={() => this.onSelectionModalHide()}
          >
          <SelectionPopup popupType={this.popupType} selectedItem={this.getSelectedItemSelectionPopup()} list={this.getListForSelectionPopup()} cancel={this.closeSelectionPopupAction} confirm={(item) => this.handlePopupSelection(item, this.popupType)} />
        </Modal>
        <Modal
            transparent={true}
            style={{justifyContent: 'center', backgroundColor: 'transparent'}}
            isVisible={this.state.errorModal}
            animationIn={"bounceInUp"}
            animationOut={"bounceOutDown"}
            animationInTiming={1000}
            animationOutTiming={1000}
            onModalHide={() => this.onErrorModalHide()}
          >
          <ConfirmationPopup
            mainTitle={this.registerError ? "عذراً" : "تهانينا"}
            description={this.state.errorMessage}
            oneButton={this.state.requestError ? this.state.requestError.shouldRetry ? false : this.state.requestError.shouldVerify ? false : true : true}
            confirmText={this.registerError ? this.state.requestError.shouldRetry ? "إعادة" : this.state.requestError.shouldVerify ? "متابعة" : "إغلاق" : "متابعة"}
            rejectText={"إغلاق"}
            confirm={this.retryRegisterAction}
            cancel={this.closePopupAction} />
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.white
  },
  headerText: {
    fontFamily: fonts.Medium,
    fontSize: 20,
    letterSpacing: 0,
    textAlign: "center",
    color: colors.charcoalGreyTwo
  },
  closeButton: {
    width:70,
    height:45,
    justifyContent: 'center',
  },
  closeIcon: {
    marginRight: 19,
    width: 14,
    height: 14,
  },
  profilePicMainView: {
    marginTop: 18,
    alignItems:'center',
    justifyContent: 'center',
    width: 90,
    height: 90,
    alignSelf: 'center'
  },
  profilePicView: {
    width: 90,
    height: 90,
    justifyContent: 'center',
    alignItems: 'center',
  },
  profilePicEmpty: {
    width: 32,
    height: 32,
    marginBottom: 8,
  },
  profilePic: {
    width: 68,
    height: 69,
    borderRadius: 34.5,
    marginBottom: 8.5,
  },
  cameraPic: {
    width: 23,
    height: 23,
    position: 'absolute',
    top: 0,
    right: 7,
    zIndex: 1000,
  },
  inputView: {
    marginTop: 13,
    marginLeft: 36,
    marginRight: 36,
    borderRadius: 4,
    borderStyle: "solid",
    borderWidth: 1,
    borderColor: colors.silverTwo
  },
  textInput: {
    paddingTop: 0,
    paddingBottom: 0,
    fontFamily: fonts.Light,
    fontSize: 15,
    fontWeight: "300",
    letterSpacing: 0,
    textAlign: "right",
    color: colors.slate
  },
  downArrow: {
    width: 12,
    height: 6
  },
  mainInputField: {
    borderStyle: "solid",
    borderBottomWidth: 1,
    borderBottomColor: colors.silver,
    height: 67.5,
    justifyContent:'space-between',
    alignItems: 'center',
    paddingLeft: 16,
    paddingRight: 16,
  },
  mainInputFieldNoBorder: {
    height: 67.5,
    justifyContent:'space-between',
    alignItems: 'center',
    paddingLeft: 16,
    paddingRight: 16,
  },
  doubleTextInputView: {
    height: '100%',
    width:'50%',
  },
  singleTextInputView: {
    height: '100%',
    flex: 1,
  },
  loginButton: {
    height: 65,
    borderRadius: 4,
    shadowColor: "rgba(0, 0, 0, 0.21)",
    shadowOffset: {
      width: 7,
      height: -8
    },
    shadowRadius: 26,
    shadowOpacity: 1,
    elevation: 5,
  },
  loginText: {
    fontFamily: fonts.Medium,
    fontSize: 18,
    letterSpacing: 0,
    textAlign: "center",
  },
  errorText: {
    fontFamily: fonts.Light,
    fontSize: 12,
    letterSpacing: 0,
    textAlign: "right",
    color: colors.darkishRed,
    marginBottom: 6,
  }
});

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps(state) {
  return {
    occupations: state.Reducer.occupations,
    province: state.Reducer.province,
    cities: state.Reducer.cities,
    gender: state.Reducer.gender,
    areaSize: state.Reducer.areaSize,
    specialization: state.Reducer.specialization,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Signup);
