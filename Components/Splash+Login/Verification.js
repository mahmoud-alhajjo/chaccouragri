/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Image,
  Text,
  TouchableOpacity,
  PixelRatio
} from 'react-native';

import mainStyles from '../../css/MainStyle';
import CustomStatusBar from '../Main Structure/CustomStatusBar'
import colors from '../../css/Colors';
import * as fonts from '../../css/Fonts';
import OverlayButton from '../Helpers/OverlayButton'
import ConfirmationPopup from '../Popup/ConfirmationPopup'

import Dimensions from 'Dimensions';
import * as Animatable from 'react-native-animatable';
import Modal from "react-native-modal";
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux'
import { ActionCreators } from '../../Actions'

export class Verification extends Component {

  backActionFlag = false
  confirmVerificationButton = false
  confirmErrorModal = false
  isLogin = true
  fromResend = false

  constructor() {
    super();
    this.state = {
      inputNumber: "",
      backMessage: "من انك لا تريد تأكيد حسابك؟\n\nفي هذه الحالة سيتم حذف المعلومات التي ادخلتها مما يضطرك الى إعادة ادخالها مرة اخرى",
      backModal: false,

      contuctModal: false,

      errorModal: false,
      errorMessage: "",
      requestError: null,
      verificationCodeError: false,
      loginErrorModal: false,
    }
  }

  componentDidMount() {
    this.props.toggleVerificationView(true)
  }

  componentWillUnmount() {
    this.props.toggleVerificationView(false)
  }

  backAction = () => {
    let nonActiveUser = this.props.navigation.state.params.nonActiveUser
    if(nonActiveUser.password == null && nonActiveUser.fbData == null) {
      this.props.navigation.goBack()
    } else {
      this.setState({backModal: true})
    }
  }

  buttonClicked = (number) => {
    let verificationCode = this.state.inputNumber
    if(number === "") {
      this.setState({inputNumber: ""})
    } else {
      if(verificationCode.length < 4) {
        verificationCode = verificationCode + number
        this.setState({inputNumber: verificationCode})
      }
    }
  }

  newAccountAction = () => {
    this.fromResend = false
    let verificationCode = this.state.inputNumber
    let nonActiveUser = this.props.navigation.state.params.nonActiveUser
    if(verificationCode.length == 4) {
      this.setState({verificationCodeError: false})
      this.props.verifyAccount(verificationCode, nonActiveUser.mobile, this.onActivationSucess, this.onActivationFailure)
    } else {
      this.setState({verificationCodeError: true})
    }
  }

  loginAction = () => {
    this.isLogin = true
    let nonActiveUser = this.props.navigation.state.params.nonActiveUser
    this.props.login(nonActiveUser.mobile, nonActiveUser.password, this.props.navigation, this.onLoginFailure)
  }

  loginFbAction = () => {
    this.isLogin = false
    let nonActiveUser = this.props.navigation.state.params.nonActiveUser
    if(nonActiveUser.fbData){
      this.props.loginViaFacebookId(nonActiveUser.fbData.fbId, this.props.navigation, this.onFacebookLoginFailure)
    } else {
      this.props.loginViaFacebookId(nonActiveUser.fbId, this.props.navigation, this.onFacebookLoginFailure)
    }
  }

  onActivationSucess = () => {
    let nonActiveUser = this.props.navigation.state.params.nonActiveUser
    if(nonActiveUser.password) {
      this.setState({requestError:null})
      this.loginAction()
    } else if(nonActiveUser.fbData) {
      this.setState({requestError:null})
      this.loginFbAction()
    } else if(nonActiveUser.fbId) {
      this.setState({requestError:null})
      this.loginFbAction()
    }
    else {
      let message = "اضغط متابعة لإدخال كلمة المرور الجديدة"
      this.setState({errorMessage:message, requestError:null, errorModal: true})
    }
  }

  onActivationFailure = (error) => {
    this.setState({errorMessage:error.errorMessage, requestError: error, errorModal: true})
  }

  onLoginFailure = (error) => {
    this.setState({errorMessage:error.errorMessage, requestError: error, loginErrorModal: true})
  }

  onFacebookLoginFailure = (error) => {
    this.setState({errorMessage:error.errorMessage, requestError: error, loginErrorModal: true})
  }

  closePopupAction = () => {
    this.backActionFlag = false
    this.setState({backModal: false})
  }

  confirmBackAction = () => {
    this.backActionFlag = true
    this.setState({backModal: false})
  }

  onBackModalHide = () => {
    if(this.backActionFlag) {
      this.props.backToOnBoardingFromVerification(this.props.navigation)
    }
  }

  confirmContactPopup = () => {
    this.setState({contuctModal: false})
  }

  confirmVerificationPopup = () => {
    this.confirmVerificationButton = true
    this.setState({errorModal: false})
  }

  closeVerificationPopup = () => {
    this.confirmVerificationButton = false
    this.setState({errorModal: false})
  }

  onErrorModalHide = () => {
    if(this.confirmVerificationButton) {
      if(this.state.requestError) {
        if(this.state.requestError.shouldRetry) {
          this.newAccountAction()
        } else if(this.state.requestError.shouldLogout) {
          this.props.backToOnBoardingFromVerification(this.props.navigation)
        }
      } else {
        if(!this.fromResend) {
          let nonActiveUser = this.props.navigation.state.params.nonActiveUser
          if(nonActiveUser.password == null && nonActiveUser.fbData == null) {
            this.props.navigation.navigate('EditPassword', {fromVerification: true, code: this.state.inputNumber})
          }
        }
      }
    }
  }

  closeLoginErrorPopupAction = () => {
    this.confirmErrorModal = false
    this.setState({loginErrorModal: false})
  }

  confirmLoginErrorPopupAction = () => {
    this.confirmErrorModal = true
    this.setState({loginErrorModal: false})
  }

  onLoginErrorModalHide = () => {
    if(this.confirmErrorModal){
      if(this.state.requestError) {
        if(this.state.requestError.shouldRetry) {
          if(this.isLogin){
            this.loginAction()
          } else {
            this.loginFbAction()
          }
        } else if (this.state.requestError.shouldLogout) {
          this.props.logoutAndDeleteAllData(this.props.navigation)
        }
      }
    }
  }

  contactUsAction = () => {
    this.setState({contuctModal: true})
  }

  // resendCodeAction = () => {
  //   this.fromResend = true
  //   let nonActiveUser = this.props.navigation.state.params.nonActiveUser
  //   this.props.resendCode(nonActiveUser.mobile, this.onResendSucess, this.onResendFailure)
  // }

  // onResendSucess = () => {
  //   let message = "تم إعادة ارسال رمز التأكيد بنجاح"
  //   this.setState({errorMessage:message, requestError:null, errorModal: true})
  // }

  // onResendFailure = (error) => {
  //   this.setState({errorMessage:error.errorMessage, requestError: error, errorModal: true})
  // }

 

  render() {
    let oldDevice = false
    if(PixelRatio.get() <= 1.5){
      oldDevice = true
    }
    let confirmButtonText = "إنشاء حساب"
    let nonActiveUser = this.props.navigation.state.params.nonActiveUser
    if(nonActiveUser.password == null && nonActiveUser.fbData == null) {
      confirmButtonText = "تأكيد الحساب"
    }
    let retryButton = <TouchableOpacity onPress={this.contactUsAction} style={{justifyContent: 'center', alignItems: 'center', marginTop: 15}}>
                        <Text style={styles.resendText}>
                          لم يتم إرسال رمز التأكيد؟ اتصل بنا
                        </Text>
                      </TouchableOpacity>
    let codeErrorText = <Text style={styles.errorText}>
                          الرمز يجب ان يكون 4 أرقام
                        </Text>
    return (
      <View style={[mainStyles.flex, styles.container]}>
        <CustomStatusBar backgroundColor={colors.fernGreenTwo} isHidden={false} />
        <View style={[mainStyles.rowFlex, {backgroundColor: colors.fernGreenTwo, alignItems: 'center',justifyContent: 'space-between'}]}>
          <View style={{width: 70}} />
          <Text style={styles.headerText}>
            تأكيد الحساب
          </Text>
          <TouchableOpacity style={[styles.closeButton, {alignItems: 'flex-end'}]} onPress={this.backAction}>
            <Image resizeMode={'contain'} style={styles.closeIcon} source={require('../../Images/close.png')} />
          </TouchableOpacity>
        </View>
        <View style={[styles.inputNumberView, {paddingTop: this.state.inputNumber === "" ? 43 : 20}]}>
          <Text style={[styles.inputNumberText, {minHeight: oldDevice ? 36 : 72.5, fontSize: oldDevice ? 30 : 50}]}>
            {this.state.inputNumber}
          </Text>
          <View style={{height:1, width: '50%', marginTop: oldDevice ? -10 : -20, backgroundColor: colors.silver}} />
          <Text style={styles.noteInputText}>
            الرجاء دخال الرمز المرسل عن طريق{"\n"}الرسالة النصية
          </Text>
          {this.state.verificationCodeError ? codeErrorText : null}
        </View>
        <View style={styles.numbersInput}>
          <View style={mainStyles.rowFlex}>
            <TouchableOpacity onPress={() => this.buttonClicked("1")} style={styles.numberButton}>
              <Text style={[styles.numberText, {fontSize: oldDevice ? 22 : 32}]}>
                1
              </Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.buttonClicked("2")}  style={styles.numberButton}>
              <Text style={[styles.numberText, {fontSize: oldDevice ? 22 : 32}]}>
                2
              </Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.buttonClicked("3")} style={styles.numberButton}>
              <Text style={[styles.numberText, {fontSize: oldDevice ? 22 : 32}]}>
                3
              </Text>
            </TouchableOpacity>
          </View>
          <View style={mainStyles.rowFlex}>
            <TouchableOpacity onPress={() => this.buttonClicked("4")}  style={styles.numberButton}>
              <Text style={[styles.numberText, {fontSize: oldDevice ? 22 : 32}]}>
                4
              </Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.buttonClicked("5")}  style={styles.numberButton}>
              <Text style={[styles.numberText, {fontSize: oldDevice ? 22 : 32}]}>
                5
              </Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.buttonClicked("6")}  style={styles.numberButton}>
              <Text style={[styles.numberText, {fontSize: oldDevice ? 22 : 32}]}>
                6
              </Text>
            </TouchableOpacity>
          </View>
          <View style={mainStyles.rowFlex}>
            <TouchableOpacity onPress={() => this.buttonClicked("7")}  style={styles.numberButton}>
              <Text style={[styles.numberText, {fontSize: oldDevice ? 22 : 32}]}>
                7
              </Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.buttonClicked("8")}  style={styles.numberButton}>
              <Text style={[styles.numberText, {fontSize: oldDevice ? 22 : 32}]}>
                8
              </Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.buttonClicked("9")}  style={styles.numberButton}>
              <Text style={[styles.numberText, {fontSize: oldDevice ? 22 : 32}]}>
                9
              </Text>
            </TouchableOpacity>
          </View>
          <View style={mainStyles.rowFlex}>
            <TouchableOpacity onPress={() => this.buttonClicked(",")} style={styles.numberButton}>
              <Text style={[styles.numberText, {fontSize: oldDevice ? 22 : 32}]}>
                ,
              </Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.buttonClicked("0")} style={styles.numberButton}>
              <Text style={[styles.numberText, {fontSize: oldDevice ? 22 : 32}]}>
                0
              </Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.buttonClicked("")} style={styles.numberButton}>
              <Text style={[styles.numberText, {fontSize: oldDevice ? 22 : 32}]}>
                C
              </Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={{marginLeft: 36, marginRight: 36, marginTop: 15}}>
          <OverlayButton buttonColors={[colors.fernGreen, colors.fernGreenTwo]} buttonBackgroundColor={colors.fernGreen} buttonTextColor={colors.white} clickButton={this.newAccountAction} style={mainStyles.flex} text={confirmButtonText} customStyle={styles.loginButton} textButtonStyle={styles.loginText} />
        </View>
        {retryButton}
        <Modal
            transparent={true}
            style={{justifyContent: 'center', backgroundColor: 'transparent'}}
            isVisible={this.state.backModal}
            animationIn={"bounceInUp"}
            animationOut={"bounceOutDown"}
            animationInTiming={1000}
            animationOutTiming={1000}
            onModalHide={() => this.onBackModalHide()}
          >
          <ConfirmationPopup mainTitle={"هل أنت متأكد"} description={this.state.backMessage} confirmText={"نعم"} rejectText={"لا"} confirm={this.confirmBackAction} cancel={this.closePopupAction} />
        </Modal>
        <Modal
            transparent={true}
            style={{justifyContent: 'center', backgroundColor: 'transparent'}}
            isVisible={this.state.errorModal}
            animationIn={"bounceInUp"}
            animationOut={"bounceOutDown"}
            animationInTiming={1000}
            animationOutTiming={1000}
            onModalHide={() => this.onErrorModalHide()}
          >
          <ConfirmationPopup
            mainTitle={this.state.requestError ? "عذراً" : "تهانينا"}
            description={this.state.errorMessage}
            oneButton={this.state.requestError ? !this.state.requestError.shouldRetry : true}
            confirmText={this.state.requestError ? this.state.requestError.shouldRetry ? "إعادة" : "إغلاق" : "متابعة"}
            rejectText={"إغلاق"}
            confirm={this.confirmVerificationPopup}
            cancel={this.closeVerificationPopup} />
        </Modal>
        <Modal
            transparent={true}
            style={{justifyContent: 'center', backgroundColor: 'transparent'}}
            isVisible={this.state.contuctModal}
            animationIn={"bounceInUp"}
            animationOut={"bounceOutDown"}
            animationInTiming={1000}
            animationOutTiming={1000}
          >
          <ConfirmationPopup
            mainTitle={"اتصل بنا"}
            description={"الرجاء إرسال رقمك عبر تطبيق واتس أب أو التلغرام إلى الرقم التالي : 379 309 0947 و سنقوم بارسال رمز التفعيل الخاص بك"}
            oneButton={true}
            confirmText={"إغلاق"}
            confirm={this.confirmContactPopup} />
        </Modal>
        <Modal
            transparent={true}
            style={{justifyContent: 'center', backgroundColor: 'transparent'}}
            isVisible={this.state.loginErrorModal}
            animationIn={"bounceInUp"}
            animationOut={"bounceOutDown"}
            animationInTiming={1000}
            animationOutTiming={1000}
            onModalHide={() => this.onLoginErrorModalHide()}
        >
          <ConfirmationPopup
            mainTitle={"عذراً"}
            description={this.state.errorMessage}
            oneButton={this.state.requestError ? this.state.requestError.shouldRetry ? false : this.state.requestError.shouldVerify ? false : true : true}
            confirmText={this.state.requestError ? this.state.requestError.shouldRetry ? "إعادة" :  this.state.requestError.shouldVerify ? "متابعة" : "إغلاق" : "إغلاق"}
            rejectText={"إغلاق"}
            confirm={this.confirmLoginErrorPopupAction}
            cancel={this.closeLoginErrorPopupAction} />
        </Modal>
      </View>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps(state) {
  return {

  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Verification);

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.white
  },
  headerText: {
    fontFamily: fonts.Medium,
    fontSize: 20,
    letterSpacing: 0,
    textAlign: "center",
    color: colors.white
  },
  closeButton: {
    width:70,
    height:45,
    justifyContent: 'center',
  },
  closeIcon: {
    marginRight: 19,
    width: 14,
    height: 14,
    tintColor: colors.white
  },
  inputNumberView: {
    paddingBottom: 29,
    alignItems: 'center',
    backgroundColor: colors.fernGreenTwo
  },
  inputNumberText: {
    fontFamily: "LucidaGrande",
    fontWeight: "normal",
    fontStyle: "normal",
    letterSpacing: 0,
    textAlign: "center",
    color: colors.white
  },
  noteInputText: {
    marginTop: 19,
    fontFamily: fonts.Light,
    fontSize: 15,
    fontWeight: "300",
    letterSpacing: 0,
    textAlign: "center",
    color: colors.white
  },
  numbersInput: {
    marginTop: 10,
    paddingLeft: 33,
    paddingRight: 33,
  },
  numberButton: {
    paddingTop: 0,
    paddingBottom: 0,
    paddingLeft: 25,
    paddingRight: 25,
    alignItems: 'center',
    justifyContent: 'center',
    width: '33.33%',
  },
  numberText: {
    fontFamily: "LucidaGrande",
    fontWeight: "normal",
    fontStyle: "normal",
    letterSpacing: 0,
    textAlign: "center",
    color: "rgba(155, 155, 155, 0.6)"
  },
  loginButton: {
    height: 65,
    borderRadius: 4,
    shadowColor: "rgba(0, 0, 0, 0.21)",
    shadowOffset: {
      width: 7,
      height: -8
    },
    shadowRadius: 26,
    shadowOpacity: 1,
    elevation: 5,
  },
  loginText: {
    fontFamily: fonts.Medium,
    fontSize: 18,
    letterSpacing: 0,
    textAlign: "center",
  },
  errorText: {
    fontFamily: fonts.Light,
    fontSize: 12,
    letterSpacing: 0,
    textAlign: "right",
    color: colors.darkishRed,
    marginTop: 6,
  },
  resendText: {
    fontFamily: fonts.Bold,
    fontSize: 15,
    fontStyle: "normal",
    lineHeight: 26,
    letterSpacing: 0,
    textAlign: "center",
    textDecorationLine: 'underline',
    color: colors.darkishRed
  }
});
