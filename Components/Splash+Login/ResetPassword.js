/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TextInput,
  Keyboard,
  Image,
  TouchableOpacity,
  Text
} from 'react-native';

import mainStyles from '../../css/MainStyle';
import colors from '../../css/Colors';
import * as fonts from '../../css/Fonts';
import OverlayButton from '../Helpers/OverlayButton'
import Validator from '../Helpers/Validator'
import ConfirmationPopup from '../Popup/ConfirmationPopup'

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import * as Animatable from 'react-native-animatable';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux'
import { ActionCreators } from '../../Actions'
import Modal from "react-native-modal";

export class ResetPassword extends Component {

  confirmErrorModal = false

  constructor(props) {
    super(props);
    this.state = {
      mobile: null,
      mobileError: "",

      errorModal: false,
      errorMessage: "",
      requestError: null,
    };
  }

  componentDidMount () {
    this.keyboardDidHideSub = Keyboard.addListener('keyboardDidHide', this.keyboardDidHide);
  }

  componentWillUnmount() {
    this.keyboardDidHideSub.remove();
  }

  keyboardDidHide = (event) => {
    this.blurTextFields()
  };

  dismissKeyboard = () => {
    Keyboard.dismiss
    this.blurTextFields()
  }

  blurTextFields = () => {
    this.refs.mobileInput.blur()
  }

  changePasswordAction = () => {
    let mobileError = Validator.getMobileError(this.state.mobile)
    this.setState({
      mobileError:mobileError,
    }, this.scrollToFirstErrorField)

    if(mobileError == null) {
      this.props.forgetPassword(this.state.mobile, this.onResetPasswordSucess, this.onResetPasswordFailure)
      this.dismissKeyboard()
    }
  }

  onResetPasswordSucess = () => {
    let message = "تم ارسال طلبك ب نجاح. اضغط على متابعة لإكمال إعادة ضبط كلمة المرور"
    this.setState({errorMessage: message, requestError: null, errorModal: true})
  }

  onResetPasswordFailure = (error) => {
    this.setState({errorMessage:error.errorMessage, requestError: error, errorModal: true})
  }

  scrollToFirstErrorField = () => {
    if(this.state.mobileError != null) {
      this.refs.mobileInput.focus()
    }
  }

  handleChangeText = (event, field) => {
    if(field == 'mobile') {
      this.setState({mobile: event})
    }
  }

  renderTextError = (error) => {
    return(
      <Text style={styles.errorText}>
        {error}
      </Text>
    )
  }

  closeErrorPopupAction = () => {
    this.confirmErrorModal = false
    this.setState({errorModal: false})
  }

  confirmErrorPopupAction = () => {
    this.confirmErrorModal = true
    this.setState({errorModal: false})
  }

  onErrorModalHide = () => {
    if(this.state.requestError) {
      if(this.confirmErrorModal){
        if(this.state.requestError) {
          if(this.state.requestError.shouldRetry) {
            this.changePasswordAction()
          } else if (this.state.requestError.shouldVerify) {
            this.props.cancel('Verification', this.state.mobile)
          }
        }
      }
    } else {
      this.props.cancel('Verification', this.state.mobile)
    }
  }

  render() {
    return (
      <View style={{marginLeft: 20, marginRight:20, borderRadius: 10, backgroundColor: colors.white, overflow:'hidden'}}>
        <TouchableOpacity onPress={this.props.cancel} style={styles.closeButton}>
          <Image resizeMode={'contain'} source={require('../../Images/close.png')}/>
        </TouchableOpacity>
        <View style={styles.textInputView}>
          <View style={styles.textInputInnerView}>
            <TextInput
              ref="mobileInput"
              placeholder={"رقم الجوال"}
              underlineColorAndroid ={"transparent"}
              placeholderTextColor={colors.slate}
              returnKeyType={"done"}
              maxLength={10}
              blurOnSubmit={false}
              keyboardType={"phone-pad"}
              selectionColor={colors.slate}
              style={[mainStyles.flex, styles.textInput]}
              onChangeText={((event) => this.handleChangeText(event, 'mobile'))}
              value={this.state.mobile ? this.state.mobile : ""}
              onSubmitEditing={(event) => this.changePasswordAction()}
            />
          </View>
        </View>
        {(this.state.mobileError == "" || this.state.mobileError == null) ? null : this.renderTextError(this.state.mobileError)}
        <View style={{marginLeft: 16, marginRight: 16, marginBottom: 21}}>
          <OverlayButton buttonColors={[colors.fernGreen, colors.fernGreenTwo]} buttonBackgroundColor={colors.fernGreen} buttonTextColor={colors.white} clickButton={this.changePasswordAction} style={mainStyles.flex} text={"إعادة ضبط كلمة المرور"} customStyle={styles.loginButton} textButtonStyle={styles.loginText} />
        </View>
        <Modal
            transparent={true}
            style={{justifyContent: 'center', backgroundColor: 'transparent'}}
            isVisible={this.state.errorModal}
            animationIn={"bounceInUp"}
            animationOut={"bounceOutDown"}
            animationInTiming={1000}
            animationOutTiming={1000}
            onModalHide={() => this.onErrorModalHide()}
        >
          <ConfirmationPopup
            mainTitle={this.state.requestError ? "عذراً" : "تهانينا"}
            description={this.state.errorMessage}
            oneButton={this.state.requestError ? this.state.requestError.shouldRetry ? false : this.state.requestError.shouldVerify ? false : true : true}
            confirmText={this.state.requestError ? this.state.requestError.shouldRetry ? "إعادة" : this.state.requestError.shouldVerify ? "متابعة" : "إغلاق" : "متابعة"}
            rejectText={"إغلاق"}
            confirm={this.confirmErrorPopupAction}
            cancel={this.closeErrorPopupAction} />
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
  },
  textInputView: {
    marginTop: 40,
    marginLeft: 16,
    marginRight: 16,
    marginBottom: 20,
    height: 66.5,
    borderRadius: 4,
    borderStyle: "solid",
    borderWidth: 1,
    borderColor: colors.silverTwo
  },
  textInputInnerView: {
    height: '100%',
    paddingLeft: 16,
    paddingRight: 16,
  },
  textInput: {
    paddingTop: 0,
    paddingBottom: 0,
    fontFamily: fonts.Light,
    fontSize: 15,
    letterSpacing: 0,
    textAlign: "right",
    color: colors.slate
  },
  loginButton: {
    height: 65,
    borderRadius: 4,
    shadowColor: "rgba(0, 0, 0, 0.21)",
    shadowOffset: {
      width: 7,
      height: -8
    },
    shadowRadius: 26,
    shadowOpacity: 1,
    elevation: 5,
  },
  loginText: {
    fontFamily: fonts.Medium,
    fontSize: 18,
    letterSpacing: 0,
    textAlign: "center",
  },
  closeButton: {
    padding: 10,
    alignItems: 'flex-end',
    position: 'absolute',
    top: 0,
    right: 0,
    width: 40,
    height: 40,
    zIndex: 1000,
  },
  errorText: {
    fontFamily: fonts.Light,
    fontSize: 12,
    letterSpacing: 0,
    textAlign: "right",
    color: colors.darkishRed,
    marginLeft: 16,
    marginRight: 16,
    marginBottom: 6,
  }
});

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps(state) {
  return {
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ResetPassword);
