/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Image,
  Text,
  TouchableOpacity,
  PixelRatio
} from 'react-native';

import mainStyles from '../../css/MainStyle';
import CustomStatusBar from '../Main Structure/CustomStatusBar'
import colors from '../../css/Colors';
import * as fonts from '../../css/Fonts';

import Dimensions from 'Dimensions';
import * as Animatable from 'react-native-animatable';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux'
import { ActionCreators } from '../../Actions'

let oldDevice = false
if(PixelRatio.get() <= 1.5){
  oldDevice = true
}

export class OnBoarding extends Component {

  fromLanuch = true
  constructor() {
    super();
    this.state = {
    }
  }

  componentDidMount() {
    this.props.getNoneActiveUser(this.onGetNonActiveUser)
  }

  onGetNonActiveUser = () => {
    if(this.props.nonActiveUser !== null) {
      this.props.navigation.navigate('Verification', {nonActiveUser: this.props.nonActiveUser})
    } else if (!this.fromLanuch) {
      this.props.navigation.navigate('Signup')
    }
  }

  signupAction = () => {
    this.fromLanuch = false
    this.props.getNoneActiveUser(this.onGetNonActiveUser)
  }

  loginAction = () => {
    this.props.navigation.navigate('Login')
  }

  onAnimationDone = (index) => {
    setTimeout(() => {
      switch (index) {
        case 1:
          if(this.refs.img1){
            this.refs.img1.pulse(2000)
            .then(endState => endState.finished ? this.onAnimationDone(1) : null)
          }
          break;
        case 2:
          if(this.refs.img2) {
            this.refs.img2.pulse(2000)
            .then(endState => endState.finished ? this.onAnimationDone(2) : null)
          }
          break;
        case 3:
          if(this.refs.img3){
            this.refs.img3.pulse(2000)
            .then(endState => endState.finished ? this.onAnimationDone(3) : null)
          }
          break;
        case 4:
          if(this.refs.img4){
            this.refs.img4.pulse(2000)
            .then(endState => endState.finished ? this.onAnimationDone(4) : null)
          }
          break;
        case 5:
          if(this.refs.img5) {
            this.refs.img5.pulse(2000)
            .then(endState => endState.finished ? this.onAnimationDone(5) : null)
          }
          break;
        case 6:
          if(this.refs.img6) {
            this.refs.img6.pulse(2000)
            .then(endState => endState.finished ? this.onAnimationDone(6) : null)
          }
          break;
        default:
          break;
      }
    }, index * 100);
  }

  render() {
    return (
      <View style={[mainStyles.flex, styles.container]}>
        <CustomStatusBar isHidden />
        <View style={mainStyles.flex}>
          <Animatable.Image ref="img1" easing="linear" animation="zoomIn" delay={1 * 100} onAnimationEnd={() => this.onAnimationDone(1)} resizeMode={'contain'} source={require('../../Images/on-boarding1.png')} style={styles.onBoarding1}/>
          <Animatable.Image ref="img2" easing="linear" animation="zoomIn" delay={2 * 100} onAnimationEnd={() => this.onAnimationDone(2)} resizeMode={'contain'} source={require('../../Images/on-boarding3.png')} style={styles.onBoarding3}/>
          <Animatable.Image ref="img3" easing="linear" animation="zoomIn" delay={3 * 100} onAnimationEnd={() => this.onAnimationDone(3)} resizeMode={'contain'} source={require('../../Images/on-boarding2.png')} style={styles.onBoarding2}/>
          <Animatable.Image ref="img4" easing="linear" animation="zoomIn" delay={4 * 100} onAnimationEnd={() => this.onAnimationDone(4)} resizeMode={'contain'} source={require('../../Images/on-boarding4.png')} style={styles.onBoarding4}/>
          <Animatable.Image ref="img5" easing="linear" animation="zoomIn" delay={5 * 100} onAnimationEnd={() => this.onAnimationDone(5)} resizeMode={'contain'} source={require('../../Images/on-boarding5.png')} style={styles.onBoarding5}/>
          <Animatable.Image ref="img6" easing="linear" animation="zoomIn" delay={6 * 100} onAnimationEnd={() => this.onAnimationDone(6)} resizeMode={'contain'} source={require('../../Images/on-boarding6.png')} style={styles.onBoarding6}/>
        </View>
        <Animatable.View animation="fadeInDownBig">
          <Text style={styles.companyName}>
            شركة شكور الزراعية
          </Text>
          <Text style={styles.descriptionCompany}>
            الرواد في مجال الزراعة منذ عام 1919
          </Text>
          <View style={[mainStyles.rowFlex, styles.buttonView]}>
            <TouchableOpacity style={{minHeight: 50, justifyContent: 'center'}} onPress={this.loginAction}>
              <Text style={styles.buttonText}>
                تسجيل الدخول
              </Text>
            </TouchableOpacity>
            <TouchableOpacity style={{minHeight: 50, justifyContent: 'center'}} onPress={this.signupAction}>
              <Text style={styles.buttonText}>
                إنشاء حساب
              </Text>
            </TouchableOpacity>
          </View>
        </Animatable.View>
      </View>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps(state) {
  return {
    nonActiveUser: state.Reducer.nonActiveUser,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(OnBoarding);

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.white,
  },
  onBoarding1: {
    position: 'absolute',
    top: -8,
    left: 0,
    width: oldDevice ? Dimensions.get('window').width * 0.368 : Dimensions.get('window').width * 0.378,
    height: oldDevice ? Dimensions.get('window').height * 0.177 : Dimensions.get('window').height * 0.187,
  },
  onBoarding3: {
    position: 'absolute',
    top: -8,
    right: 0,
    width: oldDevice ? Dimensions.get('window').width * 0.264 : Dimensions.get('window').width * 0.274,
    height: oldDevice ? Dimensions.get('window').height * 0.21 : Dimensions.get('window').height * 0.22,
  },
  onBoarding2: {
    position: 'absolute',
    top: oldDevice ? Dimensions.get('window').height * 0.10 : Dimensions.get('window').height * 0.11,
    left: oldDevice ? Dimensions.get('window').width * 0.368 : Dimensions.get('window').width * 0.378,
    width: oldDevice ? Dimensions.get('window').width * 0.35 : Dimensions.get('window').width * 0.36,
    height: oldDevice ? Dimensions.get('window').height * 0.192 : Dimensions.get('window').height * 0.202,
  },
  onBoarding4: {
    position: 'absolute',
    top: oldDevice ? (Dimensions.get('window').height * 0.177) + 15 : (Dimensions.get('window').height * 0.187) + 30,
    left: 0,
    width: oldDevice ? Dimensions.get('window').width * 0.39 : Dimensions.get('window').width * 0.4,
    height: oldDevice ? Dimensions.get('window').height * 0.255 : Dimensions.get('window').height * 0.265,
  },
  onBoarding5: {
    position: 'absolute',
    top: oldDevice ? ((Dimensions.get('window').height * 0.21) + 44) : ((Dimensions.get('window').height * 0.22) + 69),
    right: 0,
    width: oldDevice ? Dimensions.get('window').width * 0.534 : Dimensions.get('window').width * 0.544,
    height: oldDevice ? Dimensions.get('window').height * 0.394 : Dimensions.get('window').height * 0.404,
  },
  onBoarding6: {
    position: 'absolute',
    top: oldDevice ? ((Dimensions.get('window').height * 0.177) + 15 + (Dimensions.get('window').height * 0.255) + 22) : ((Dimensions.get('window').height * 0.187) + 30 + (Dimensions.get('window').height * 0.265) + 12),
    left: 28,
    width: oldDevice ? Dimensions.get('window').width * 0.35 : Dimensions.get('window').width * 0.36,
    height: oldDevice ? Dimensions.get('window').height * 0.192 : Dimensions.get('window').height * 0.202,
  },
  companyName: {
    marginTop: 20,
    fontFamily: fonts.Medium,
    fontSize: 26,
    fontStyle: "normal",
    letterSpacing: 0,
    textAlign: "center",
    color: colors.charcoalGrey
  },
  descriptionCompany: {
    marginTop: 10,
    opacity: 0.9,
    fontFamily: fonts.Light,
    fontSize: 16,
    fontWeight: "300",
    fontStyle: "normal",
    lineHeight: 26,
    letterSpacing: 0,
    textAlign: "center",
    color: colors.warmGrey
  },
  buttonView: {
    paddingLeft: 40,
    paddingRight: 40,
    marginTop: 17,
    marginBottom: 15,
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  buttonText: {
    fontFamily: fonts.Bold,
    fontSize: 16,
    fontStyle: "normal",
    letterSpacing: 0,
    textAlign: "right",
    color: colors.mediumGreen
  }
});
