/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Image,
  Text,
  TouchableOpacity,
  ImageBackground,
} from 'react-native';

import mainStyles from '../../css/MainStyle';
import colors from '../../css/Colors';
import * as fonts from '../../css/Fonts';
import * as AppConstants from '../../Config/AppConstants';

import LinearGradient from 'react-native-linear-gradient';
import Communications from 'react-native-communications';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux'
import { ActionCreators } from '../../Actions'

export class Header extends Component {

  constructor() {
    super();
    this.state = {
    }
  }

  openMenuAction = () => {
    this.props.navigation.openDrawer();
  }

  openFbProfileAction = () => {
    let socialLink = "https://www.facebook.com/chaccour.agriculturalcompany"
    if(socialLink){
      this.props.logEvent(AppConstants.SocialMedia, {type: 'Facebook'})
      Communications.web(socialLink)
    }
  }

  goBack = () => {
    this.props.navigation.goBack()
  }

  openProfileAction = () => {
    this.props.toggleProfileView(true)
  }

  getScreenName = () => {
    var screenName = ""
    let routeName = this.props.navigation.state.routeName
    if(routeName == "Products") {
      screenName = "المنتجات"
    } else if(routeName == "LatestNews") {
      screenName = "آخر الأخبار"
    } else if(routeName == "Notifications") {
      screenName = "إشعارات"
    } else if(routeName == "Careers") {
      screenName = "بحث عن عمل"
    } else if(routeName == "ContactUs") {
      screenName = "اتصل بنا"
    } else if(routeName == "Weather") {
      screenName = "الطقس"
    } else if(routeName == "FertilizationGuide") {
      screenName = "دليل التسميد"
    }
    else if(routeName == "FertilizationDetailsItem") {
      screenName = this.props.fertilizeName
    }
    else if(routeName == "FertilizationDetails") {
      screenName = this.props.fertilizeType
    }
    return screenName
  }

  render() {
    let leftButton = <View style={{width: 70}} />
    let menuIcon =   <TouchableOpacity style={[styles.menuButton, {alignItems: 'flex-end'}]} onPress={this.openMenuAction}>
                        <Image resizeMode={'contain'} style={styles.menuIcon} source={require('../../Images/hamburger.png')} />
                      </TouchableOpacity>
    let notificationView = null
    if(this.props.navigation.state.routeName === "LatestNews") {
      leftButton = <TouchableOpacity style={[styles.menuButton, {alignItems: 'flex-start'}]} onPress={this.openFbProfileAction}>
                    <Image style={styles.fbIcon} source={require('../../Images/fbLogo.png')} />
                  </TouchableOpacity>
    }
    if(this.props.navigation.state.routeName === "Notifications") {
      var unReadnotificationsCount = 0
      var profilePic = null
      if(this.props.user){
        if(this.props.user.photo){
          profilePic = {uri: this.props.user.photo}
        }
        if(this.props.user.notifications) {
          unReadnotificationsCount = this.props.user.notifications.filter((item) => {
            return item.read == false
          }).length
        }
      }
      if(unReadnotificationsCount != 0) {
        notificationView = <View style={{marginRight:10.5, paddingTop: 2, paddingBottom:2, paddingLeft: 9, paddingRight: 9, justifyContent: 'center', alignItems:'center', borderRadius: 100, backgroundColor: colors.freshGreen}}>
                                  <Text style={{fontFamily: fonts.Bold, fontSize: 11, letterSpacing: 0, textAlign: "center", color: colors.white}}>
                                    {unReadnotificationsCount}
                                  </Text>
                               </View>
      }

      leftButton = <TouchableOpacity style={[styles.menuButton, {alignItems: 'flex-start'}]} onPress={this.openProfileAction}>
                    <ImageBackground source={require('../../Images/imgPlaceholderBg.png')} style={styles.profilePicView}>
                      <Image style={profilePic ? styles.profilePic : styles.profilePicEmpty} source={profilePic ? profilePic : require('../../Images/imgPlaceholder.png')}/>
                    </ImageBackground>
                  </TouchableOpacity>
    }
    if(this.props.navigation.state.routeName === "FertilizationDetailsItem" || this.props.navigation.state.routeName === "FertilizationDetails") {
      leftButton = <TouchableOpacity style={[styles.menuButton, {alignItems: 'flex-start'}]} onPress={this.goBack}>
                    <Image style={styles.fbIcon} source={require('../../Images/ArrowLeft.png')} />
                  </TouchableOpacity>
      menuIcon = <View style={{width: 70}} />
    }

    return (
      <LinearGradient
        colors={[colors.fernGreen, colors.fernGreenTwo]}
        start={{x: 0.0, y: 0.0}}
        end={{x: 0.0, y: 1.0}}
        style={[mainStyles.rowFlex, styles.container]}
      >
        {leftButton}
        <View style={[mainStyles.rowFlex ,mainStyles.flex, {height:'100%',alignItems: 'center', justifyContent:'center'}]}>
          {notificationView}
          <Text style={styles.headerText}>
            {this.getScreenName()}
          </Text>
        </View>
        {menuIcon}
      </LinearGradient>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps(state) {
  return {
    user: state.Reducer.user,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Header);

const styles = StyleSheet.create({
  container: {
    height: 54,
    shadowColor: "rgba(11, 11, 11, 0.27)",
    shadowOffset: {
      width: 30,
      height: -6
    },
    shadowRadius: 30,
    shadowOpacity: 1,
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  headerText: {
    fontFamily: fonts.Medium,
    fontSize: 20,
    letterSpacing: 0,
    textAlign: "center",
    color: colors.white
  },
  menuButton: {
    width:70,
    height:54,
    justifyContent: 'center',
  },
  menuIcon: {
    marginRight: 12,
    width: 24,
    height: 24,
  },
  fbIcon: {
    marginTop: 8,
    marginLeft: 12,
    width: 27,
    height: 36,
  },
  profilePic: {
    width: 29,
    height: 29,
    borderRadius: 14.5,
  },
  profilePicEmpty: {
    width: 15,
    height: 15,
    marginBottom: 6,
  },
  profilePicView: {
    marginLeft: 12,
    width: 31,
    height: 31,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
