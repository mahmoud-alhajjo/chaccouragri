/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Image,
  Text,
  TouchableOpacity,
  BackHandler,
  Keyboard,
  DeviceEventEmitter,
  Animated,
  Easing,
  I18nManager
} from 'react-native';

import mainStyles from '../../css/MainStyle';
import CustomStatusBar from '../Main Structure/CustomStatusBar'
import colors from '../../css/Colors';
import * as fonts from '../../css/Fonts';
import * as AppConstants from '../../Config/AppConstants';

import Dimensions from 'Dimensions';
import {
  createStackNavigator,
  createDrawerNavigator,
} from 'react-navigation';
import LinearGradient from 'react-native-linear-gradient';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
import { ActionCreators } from '../../Actions'
import Modal from "react-native-modal";
import Spinner from 'react-native-spinkit'
import * as Animatable from 'react-native-animatable';
import PushNotification from 'react-native-push-notification';

import CustomDrawerContentComponent from './CustomDrawerContentComponent'

import Splash from '../Splash+Login/Splash'
import OnBoarding from '../Splash+Login/OnBoarding'
import Login from '../Splash+Login/Login'
import Signup from '../Splash+Login/Signup'
import Verification from '../Splash+Login/Verification'
import Interests from '../Profile/Interests'

import MainProducts from '../Products/MainProducts'
import AboutUs from '../Static Pages/AboutUs'
import LatestNews from '../LatestNews/LatestNews'
import Notifications from '../Notifications/Notifications'
import MainCareers from '../Careers/MainCareers'
import ContactUs from '../ContactUs/ContactUs'
import Weather from '../Weather/Weather'
import Profile from '../Profile/Profile'
import EditPassword from '../Profile/EditPassword'
import EditProfile from '../Profile/EditProfile'
import Privacy from '../Static Pages/Privacy'
import FertilizationGuide from '../FertilizationGuide/FertilizationGuide'
import FertilizationDetailsItem from '../FertilizationGuide/FertilizationDetailsItem'
import FertilizationDetails from '../FertilizationGuide/FertilizationDetails'


const ProfileNavigator = createStackNavigator({
  Profile: {
    screen: Profile,
    navigationOptions: ({ navigation }) => ({
      header: null,
    }),
  },
  Interests: {
    screen: Interests,
    navigationOptions: ({ navigation }) => ({
      header: null,
    }),
  },
  EditPassword: {
    screen: EditPassword,
    navigationOptions: ({ navigation }) => ({
      header: null,
    }),
  },
  EditProfile: {
    screen: EditProfile,
    navigationOptions: ({ navigation }) => ({
      header: null,
    }),
  },
  Privacy: {
    screen: Privacy,
    navigationOptions: ({ navigation }) => ({
      header: null,
    }),
  },
},
  {
    mode: 'card',
    initialRouteName: 'Profile',
    headerMode: 'screen',
  }
);

const FertilizationGuideContainer = createStackNavigator({
  FertilizationGuide: {
    screen: FertilizationGuide,
  },
  FertilizationDetails: {
    screen: FertilizationDetails,
  },
  FertilizationDetailsItem: {
    screen: FertilizationDetailsItem,
  },
},
  {
    initialRouteName: 'FertilizationGuide',
    headerMode: 'none',
    transitionConfig: () => ({
      containerStyle: {
        backgroundColor: colors.fernGreen,
      },
      transitionSpec: {
        duration: 350,
      }
    })
  }
)


FertilizationGuideContainer.navigationOptions = ({ navigation }) => {
  let drawerLockMode = 'unlocked';
  if (navigation.state.index > 0) {
    drawerLockMode = 'locked-closed';
  }
  return {
    drawerLockMode,
  };
};

const DrawerScreens = {
  Products: {
    screen: MainProducts,
  },
  LatestNews: {
    screen: LatestNews,
  },
  Notifications: {
    screen: Notifications,
  },
  Careers: {
    screen: MainCareers,
  },
  ContactUs: {
    screen: ContactUs,
  },
  Weather: {
    screen: Weather,
  },
  FertilizationGuide: {
    screen: FertilizationGuideContainer,
  },
}


let isArabicLang = false
if (I18nManager.localeIdentifier.toLowerCase().indexOf("ar") > -1) {
  isArabicLang = true
}

const DrawerOptions = {
  drawerWidth: Dimensions.get('window').width,
  drawerPosition: isArabicLang ? 'left' : 'right',
  contentComponent: (props) => (
    <CustomDrawerContentComponent {...props} />
  )
}

const NavigationDrawer = createDrawerNavigator(DrawerScreens, DrawerOptions)

const RootNavigator = createStackNavigator({
  Splash: {
    screen: Splash,
    navigationOptions: ({ navigation }) => ({
      header: null,
    }),
  },
  OnBoarding: {
    screen: OnBoarding,
    navigationOptions: ({ navigation }) => ({
      header: null,
    }),
  },
  Login: {
    screen: Login,
    navigationOptions: ({ navigation }) => ({
      header: null,
    }),
  },
  Signup: {
    screen: Signup,
    navigationOptions: ({ navigation }) => ({
      header: null,
    }),
  },
  Verification: {
    screen: Verification,
    navigationOptions: ({ navigation }) => ({
      header: null,
    }),
  },
  Interests: {
    screen: Interests,
    navigationOptions: ({ navigation }) => ({
      header: null,
    }),
  },
  EditPassword: {
    screen: EditPassword,
    navigationOptions: ({ navigation }) => ({
      header: null,
    }),
  },
  MainStructure: {
    screen: NavigationDrawer,
    navigationOptions: ({ navigation }) => ({
      header: null,
    }),
  },
},
  {
    mode: 'card',
    initialRouteName: 'Splash',
    headerMode: 'screen',
  }
);

export class MainApp extends Component {

  registedPushNotification = false

  constructor() {
    super();
    this.state = {
    }
  }

  configNotification = () => {
    PushNotification.configure({
      onRegister: this.onRegister,
      onNotification: this.onReceived,
      senderID: "374354561657",
    });
  }

  onRegister = (token) => {
    console.log('token', token);
    this.props.registerPushNotification(token.token)
  }

  onReceived = (notification) => {
    if (notification) {
      if (notification.foreground) {
        console.log('onReceived', notification);
        var isNotificationScreen = false
        if (this.rootNavigator) {
          if (this.rootNavigator._navigation) {
            if (this.rootNavigator._navigation._childrenNavigation) {
              let dataKeys = Object.keys(this.rootNavigator._navigation._childrenNavigation)
              if (dataKeys.length > 0) {
                if (this.rootNavigator._navigation._childrenNavigation[dataKeys[0]].state.index == 2) {
                  isNotificationScreen = true
                }
              }
            }
          }
        }
        this.props.addNotificationToUser(notification, false, isNotificationScreen)
      }
        if (!notification.foreground) {
          this.props.logEvent(AppConstants.NotificationOpened, null)
          this.props.addNotificationToUser(notification, true, false)
          this.props.goToNotificationScreen(true)
          PushNotification.cancelAllLocalNotifications()
        }
    }
  }


  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
  }

  componentDidUpdate(prevProps) {
    if (this.props.loggedOut) {
      this.registedPushNotification = false
      this.props.logoutAndDeleteAllData(this.rootNavigator._navigation)
    } else if (this.props.user) {
      if (this.props.user.notificationEnabled) {
        if (!this.registedPushNotification) {
          this.configNotification()
          this.registedPushNotification = true
        }
      }
    }
  }

  onBackPress = () => {
    if (this.props.load) {
      return true
    } else if (this.props.isVerificationViewOpened) {
      return true
    }
    return false
  }

  handleNavigationState = (previous, next, action) => {
    if (action.type === 'Navigation/DRAWER_OPENED') {
      Keyboard.dismiss()
    }
  }

  render() {
    return (
      <View style={[mainStyles.flex, { backgroundColor: colors.white }]}>
        <RootNavigator ref={(ref) => { this.rootNavigator = ref; }} onNavigationStateChange={this.handleNavigationState} />
        <Modal
          transparent={true}
          onBackButtonPress={() => this.props.toggleAboutUsView(false)}
          style={{ marginLeft: 0, marginRight: 0, marginTop: 0, marginBottom: 0 }}
          isVisible={this.props.isAboutUsOpened}
          animationIn={"bounceInUp"}
          animationOut={"bounceOutDown"}
          animationInTiming={1000}
          animationOutTiming={1000}
        >
          <AboutUs />
        </Modal>
        <Modal
          transparent={true}
          onBackButtonPress={() => this.props.toggleProfileView(false)}
          style={{ marginLeft: 0, marginRight: 0, marginTop: 0, marginBottom: 0 }}
          isVisible={this.props.isProfileOpened}
          animationIn={"bounceInUp"}
          animationOut={"bounceOutDown"}
          animationInTiming={1000}
          animationOutTiming={1000}
        >
          <ProfileNavigator />
        </Modal>
        <Modal
          transparent={true}
          style={{ marginLeft: 0, marginRight: 0, marginTop: 0, marginBottom: 0, justifyContent: 'center', alignItems: 'center' }}
          isVisible={this.props.load}
          animationIn={"fadeIn"}
          animationOut={"fadeOut"}
          animationInTiming={600}
          animationOutTiming={600}
          backdropOpacity={0.4}
        >
          <Spinner isVisible={this.props.load} size={80} color={colors.fernGreen} type={"FadingCircleAlt"} />
        </Modal>
      </View>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps(state) {
  return {
    isAboutUsOpened: state.Reducer.isAboutUsOpened,
    isProfileOpened: state.Reducer.isProfileOpened,
    isVerificationViewOpened: state.Reducer.isVerificationViewOpened,
    load: state.Reducer.load,
    loggedOut: state.Reducer.loggedOut,
    user: state.Reducer.user,
    notifications: state.Reducer.notifications
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(MainApp);
