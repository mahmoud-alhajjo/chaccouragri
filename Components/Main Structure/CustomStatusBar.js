/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StatusBar
} from 'react-native';

import colors from '../../css/Colors';

export default class CustomStatusBar extends Component {

  constructor(props) {
    super(props);
  }

  getBackgroundColor = () => {
    if (this.props.backgroundColor) {
      return this.props.backgroundColor
    }
    return colors.white
  }

  getBarStyle = () => {
    if(this.props.isDark) {
      return 'dark-content'
    }
    return 'light-content'
  }

  setHidden = () => {
    if(this.props.isHidden) {
      return true
    }
    return false
  }

  render() {
    return (
      <StatusBar
        backgroundColor = {this.getBackgroundColor()}
        barStyle= {this.getBarStyle()}
        animated={true}
        hidden={this.setHidden()}
      />
    );
  }
}
