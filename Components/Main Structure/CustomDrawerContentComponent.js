/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Image,
  Text,
  TouchableOpacity,
  Share,
  ImageBackground,
  ScrollView,
  PixelRatio
} from 'react-native';

import mainStyles from '../../css/MainStyle';
import CustomStatusBar from '../Main Structure/CustomStatusBar'
import colors from '../../css/Colors';
import * as fonts from '../../css/Fonts';
import * as AppConstants from '../../Config/AppConstants';

import Dimensions from 'Dimensions';

import LinearGradient from 'react-native-linear-gradient';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux'
import { ActionCreators } from '../../Actions'

export class CustomDrawerContentComponent extends Component {

  constructor(props) {
    super(props);
    this.state = {
    }
  }

  componentDidUpdate(prevProps) {
    if(this.props.notification) {
      let routeIndex = prevProps.navigation.state.index
      if(routeIndex != 2) {
        prevProps.navigation.navigate('Notifications')
      } else {
        if(prevProps.navigation.state.isDrawerOpen) {
          prevProps.navigation.closeDrawer()
        }
      }
    }
  }

  menuItemAction = (item) => {
    if(item === "Share") {
      this.props.share();
      this.props.logEvent(AppConstants.ShareAction, null)
      Share.share({message: 'حمل على جوالك تطبيق دليل مواد شركة شكور الزراعية عن طريق متجر جوجل: https://play.google.com/store/apps/details?id=com.chaccour_agri.app \n \n أو مباشرة عن طريق الرابط: http://chaccour-agri.com/app.apk', title: 'شركة شكور الزراعية'},)
    } else if(item === "AboutUs") {
      this.props.logEvent(AppConstants.AboutUsPage, null)
      this.props.navigation.closeDrawer()
      this.props.toggleAboutUsView(true)
    } else if(item === "Profile") {
      this.props.logEvent(AppConstants.ProfilePage, null)
      this.props.navigation.closeDrawer()
      this.props.toggleProfileView(true)
    } else {
      if(item == "Products") {
        this.props.logEvent(AppConstants.ProductPage, null)
      } else if(item == "LatestNews") {
        this.props.logEvent(AppConstants.LatestNewsPage, null)
      } else if(item == "Notifications") {
        this.props.logEvent(AppConstants.NotificationsPage, null)
      } else if(item == "Careers") {
        this.props.logEvent(AppConstants.CareersPage, null)
      } else if(item == "ContactUs") {
        this.props.logEvent(AppConstants.ContactUsPage, null)
      } else if(item == "Weather") {
        this.props.logEvent(AppConstants.WeatherPage, null)
      }
      else if(item == "FertilizationGuide") {
        this.props.logEvent(AppConstants.FertilizationGuidePage, null)
      }
      this.props.navigation.closeDrawer()
      this.props.navigation.navigate(item)
    }
  }


  render () {
    var fullName = ""
    var provinceName = ""
    var profilePic = null
    var unReadnotificationsCount = 0

    if(this.props.user){
      fullName = this.props.user.first_name + " " + this.props.user.last_name
      provinceName = this.props.user.province_name
      if(this.props.user.photo){
        profilePic = {uri: this.props.user.photo}
      }
      if(this.props.user.notifications) {
        unReadnotificationsCount = this.props.user.notifications.filter((item) => {
          return item.read == false
        }).length
      }
    }
    let activeView = <LinearGradient
                      colors={[colors.fern, colors.apple]}
                      start={{x: 0.0, y: 0.0}}
                      end={{x: 0.0, y: 1.0}}
                      style={styles.activeView}
                      >
                      </LinearGradient>
    let notificationView = null
    if(unReadnotificationsCount != 0) {
      notificationView = <View style={{marginRight:13, paddingTop: 3, paddingBottom:3, paddingLeft: 12, paddingRight: 12, justifyContent: 'center', alignItems:'center', borderRadius: 100, backgroundColor: colors.freshGreen}}>
                                <Text style={{fontFamily: fonts.Bold, fontSize: 15, letterSpacing: 0, textAlign: "center", color: colors.white}}>
                                  {unReadnotificationsCount}
                                </Text>
                          </View>
    }
    return (
      <ImageBackground style={[mainStyles.flex, {justifyContent: 'center'}]} source={require('../../Images/menuBg.jpg')}>
        <ScrollView>
          <CustomStatusBar backgroundColor={colors.fernGreen} />
          <View style={styles.itemsView}>
            <TouchableOpacity style={[mainStyles.rowFlex, {marginBottom: 10, minHeight: 50, alignItems:'center', justifyContent:'flex-end'}]} onPress={() => this.menuItemAction("Products")}>
              <Text style={styles.itemText}>
                المنتجات
              </Text>
              {this.props.activeItemKey === "Products" ? activeView : null}
            </TouchableOpacity>
            <TouchableOpacity style={[mainStyles.rowFlex, {marginBottom: 10, minHeight: 50, alignItems:'center', justifyContent:'flex-end'}]} onPress={() => this.menuItemAction("AboutUs")}>
              <Text style={styles.itemText}>
                من نحن
              </Text>
              {this.props.activeItemKey === "AboutUs" ? activeView : null}
            </TouchableOpacity>
            <TouchableOpacity style={[mainStyles.rowFlex, {marginBottom: 10, minHeight: 50, alignItems:'center', justifyContent:'flex-end'}]} onPress={() => this.menuItemAction("LatestNews")}>
              <Text style={styles.itemText}>
                آخر الأخبار
              </Text>
              {this.props.activeItemKey === "LatestNews" ? activeView : null}
            </TouchableOpacity>
            <TouchableOpacity style={[mainStyles.rowFlex, {marginBottom: 10, minHeight: 50, alignItems:'center', justifyContent:'flex-end'}]} onPress={() => this.menuItemAction("Notifications")}>
              {notificationView}
              <Text style={styles.itemText}>
                إشعارات
              </Text>
              {this.props.activeItemKey === "Notifications" ? activeView : null}
            </TouchableOpacity>
            <TouchableOpacity style={[mainStyles.rowFlex, {marginBottom: 10, minHeight: 50, alignItems:'center', justifyContent:'flex-end'}]} onPress={() => this.menuItemAction("Careers")}>
              <Text style={styles.itemText}>
                بحث عن عمل
              </Text>
              {this.props.activeItemKey === "Careers" ? activeView : null}
            </TouchableOpacity>
            <TouchableOpacity style={[mainStyles.rowFlex, {marginBottom: 10, minHeight: 50, alignItems:'center', justifyContent:'flex-end'}]} onPress={() => this.menuItemAction("FertilizationGuide")}>
              <Text style={styles.itemText}>
                    دليل التسميد
              </Text>
              {this.props.activeItemKey === "FertilizationGuide" ? activeView : null}
            </TouchableOpacity>
            <TouchableOpacity style={[mainStyles.rowFlex, {marginBottom: 10, minHeight: 50, alignItems:'center', justifyContent:'flex-end'}]} onPress={() => this.menuItemAction("Weather")}>
              <Text style={styles.itemText}>
                    الطقس
              </Text>
              {this.props.activeItemKey === "Weather" ? activeView : null}
            </TouchableOpacity>
            <TouchableOpacity style={[mainStyles.rowFlex, {marginBottom: 10, minHeight: 50, alignItems:'center', justifyContent:'flex-end'}]} onPress={() => this.menuItemAction("ContactUs")}>
              <Text style={styles.itemText}>
                اتصل بنا
              </Text>
              {this.props.activeItemKey === "ContactUs" ? activeView : null}
            </TouchableOpacity>
            <TouchableOpacity style={[mainStyles.rowFlex, {minHeight: 50, alignItems:'center', justifyContent:'flex-end'}]} onPress={() => this.menuItemAction("Share")}>
              <Text style={styles.itemText}>
                شارك التطبيق
              </Text>
            </TouchableOpacity>
          </View>
          <View style={{marginLeft: 39, marginRight: 39, marginBottom: 27}}>
            <TouchableOpacity onPress={() => this.menuItemAction("Profile")}>
              <ImageBackground source={require('../../Images/imgPlaceholderBg.png')} style={styles.profilePicView}>
                <Image style={profilePic ? styles.profilePic : styles.profilePicEmpty} source={profilePic ? profilePic : require('../../Images/imgPlaceholder.png')}/>
              </ImageBackground>
              <Text style={styles.nameText}>
                {fullName}
              </Text>
              <Text style={styles.provinceText}>
                {provinceName}
              </Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </ImageBackground>
    );
  }
};

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps(state) {
  return {
    user: state.Reducer.user,
    notification: state.Reducer.notification,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(CustomDrawerContentComponent);

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.white
  },
  backgroundImage: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
  },
  itemsView: {
    marginTop: 40,
    marginRight: 58,
    marginLeft: 58,
    marginBottom: 44,
  },
  itemText: {
    fontFamily: fonts.Medium,
    fontSize: 22,
    letterSpacing: 0,
    textAlign: "right",
    color: colors.white
  },
  activeView: {
    marginLeft: 19,
    width: 2,
    height: 29,
    borderRadius: 90,
    shadowColor: colors.fernGreen,
    shadowOffset: {
      width: 0,
      height: 0
    },
    shadowRadius: 9,
    shadowOpacity: 1,
    elevation: 5,
  },
  profilePic: {
    width: 46,
    height: 46,
    borderRadius: 23,
  },
  profilePicEmpty: {
    width: 18,
    height: 18,
    marginBottom: 8,
  },
  profilePicView: {
    width: 46,
    height: 46,
    justifyContent: 'center',
    alignItems: 'center',
  },
  nameText: {
    marginTop: 7,
    fontFamily: fonts.Medium,
    fontSize: 20,
    letterSpacing: 0,
    textAlign: "left",
    color: colors.white
  },
  provinceText: {
    marginTop: 2,
    fontFamily: fonts.Medium,
    fontSize: 12,
    letterSpacing: 0,
    textAlign: "left",
    color: colors.cloudyBlue
  }
});
