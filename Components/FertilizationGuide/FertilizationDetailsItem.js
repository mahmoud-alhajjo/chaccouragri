/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  FlatList,
  LayoutAnimation,
  UIManager,
} from 'react-native';

import mainStyles from '../../css/MainStyle';
import colors from '../../css/Colors';
import * as fonts from '../../css/Fonts';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
import { ActionCreators } from '../../Actions'
import * as Animatable from 'react-native-animatable';
import Header from '../Main Structure/Header';

export class FertilizationDetailsItem extends Component {
  constructor() {
    super();
    if (Platform.OS === 'android') {
      UIManager.setLayoutAnimationEnabledExperimental(true);
    }
    this.state = {
      refresh: false,
    }
  }

  itemClicked = (item, index) => {
    if (item.stage) {
      LayoutAnimation.configureNext({
        duration: 250,
        update: {
          type: LayoutAnimation.Types.linear,
        }
      });
      let data = this.props.fertilizerDetails.stages
      data[index].expanded = !data[index].expanded
      this.setState({ refresh: true })
    }
  }

  FertilizationDetailsAction = (item) => {
    this.props.navigation.navigate('FertilizationDetails', {
      stageFertilizer: item
    });
  }

  renderItem = (item, index) => {
    return (
      <Animatable.View animation="fadeInDownBig" delay={index * 100} style={{ overflow: 'hidden' }} >
        {item.stage.trim() != "" ? <TouchableOpacity onPress={() => this.itemClicked(item, index)}>
          <View style={[mainStyles.rowFlex, itemStyles.question]}>
            {
              <Text style={itemStyles.plusText}>
                {item.expanded ? "-" : "+"}
              </Text>
            }
            <Text style={itemStyles.questionText}>
              {item.stage}
            </Text>
          </View>
        </TouchableOpacity> : null}
        {
          item.stage ?
            item.stageFertilizers.map((stageFertilizer) => {
              let itemFertilizer = stageFertilizer
              return <TouchableOpacity key={stageFertilizer.id} onPress={() => { this.props.navigation.navigate('FertilizationDetails', { stageFertilizers: itemFertilizer }) }}>
                <View style={[
                  itemStyles.answerView,
                  { overflow: 'hidden', height: item.expanded ? null : 0, paddingLeft: item.expanded ? 30 : 0, paddingRight: item.expanded ? 30 : 0, paddingTop: item.expanded ? 19 : 0, paddingBottom: item.expanded ? 19 : 0, borderBottomWidth: item.expanded ? 1 : 0 }
                ]}
                >
                  {item.expanded ?
                    <Text style={itemStyles.answerText}>
                      <Text style={{color: colors.red, fontWeight: 'bold', fontSize: 16}}>
                        {stageFertilizer.between_relation == 'OR' ? 'أو  ' : stageFertilizer.between_relation == 'AND' ? 'و  ' : null }
                      </Text>
                      {stageFertilizer.fertilizer_name}
                    </Text>
                    : null}
                </View>
              </TouchableOpacity>
            }) : null
        }
      </Animatable.View>
    )
  }



  render() {
    return (
      <View style={[mainStyles.flex, styles.container]}>
        <Header navigation={this.props.navigation} fertilizeName={this.props.fertilizerDetails.productInfo.name_ar} />
       { this.props.fertilizerDetails.stages.length > 0 ?  <FlatList
          data={this.props.fertilizerDetails.stages}
          renderItem={({ item, index }) => this.renderItem(item, index)}
          keyExtractor={(item, index) => index.toString()}
          style={{ marginTop: 15 }}
        /> : <View style={[mainStyles.flex, { justifyContent: 'center', alignItems: 'center' }]}><Text style={{ fontFamily: fonts.Light, fontSize: 15, textAlign: "center", color: colors.slate }}>لايوجد أسمدة حالياً</Text></View>
        }
      </View>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps(state) {
  return {
    fertilizerDetails: state.Reducer.fertilizerDetails,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(FertilizationDetailsItem);



const itemStyles = StyleSheet.create({
  question: {
    paddingLeft: 21,
    paddingRight: 29,
    paddingTop: 15,
    paddingBottom: 15,
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: colors.white,
    shadowColor: "#e3e3e3",
    shadowOffset: {
      width: 0,
      height: -1
    },
    shadowRadius: 0,
    shadowOpacity: 1,
    overflow: 'hidden',
    borderStyle: 'solid',
    borderBottomWidth: 1,
    borderBottomColor: "rgb(220,220,220)",
  },
  questionText: {
    marginLeft: 23,
    fontFamily: fonts.Light,
    fontSize: 20,
    letterSpacing: -0.13,
    textAlign: "right",
    color: colors.slate
  },
  minusText: {
    fontFamily: "LucidaGrande",
    fontWeight: "bold",
    fontSize: 40,
    fontStyle: "normal",
    letterSpacing: 0,
    textAlign: "left",
    color: "rgb(220,220,220)"
  },
  plusText: {
    fontFamily: "LucidaGrande",
    fontWeight: "bold",
    fontSize: 32,
    fontStyle: "normal",
    letterSpacing: 0,
    textAlign: "left",
    color: "rgb(220,220,220)"
  },
  answerView: {
    backgroundColor: colors.white,
    borderStyle: 'solid',
    borderBottomColor: "rgb(220,220,220)",
  },
  answerText: {
    fontFamily: fonts.Light,
    fontSize: 14.5,
    letterSpacing: -0.13,
    lineHeight: 22,
    textAlign: "right",
    color: colors.slate
  },
  arrowIcon: {
    width: 22,
    height: 22,
    tintColor: "rgb(220,220,220)",
  },
  closeButton: {
    marginTop: 20,
    marginRight: 15,
    padding: 10,
    alignItems: 'flex-end',
    position: 'absolute',
    top: 0,
    right: 0,
    width: 40,
    height: 40,
    zIndex: 1000,
  }
});

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.white,
  }
})