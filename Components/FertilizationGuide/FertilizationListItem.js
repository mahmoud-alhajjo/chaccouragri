/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  FlatList,
  LayoutAnimation,
  UIManager
} from 'react-native';

import mainStyles from '../../css/MainStyle';
import colors from '../../css/Colors';
import * as fonts from '../../css/Fonts';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
import { ActionCreators } from '../../Actions'
import ConfirmationPopup from '../Popup/ConfirmationPopup'
import * as Animatable from 'react-native-animatable';

import ResponsiveImage from '../Helpers/ResponsiveImage'
import Dimensions from 'Dimensions';
import LinearGradient from 'react-native-linear-gradient';
import Modal from "react-native-modal";

export class FertilizationListItem extends Component {

  confirmVerificationButton = false

  constructor() {
    super();
    if (Platform.OS === 'android') {
      UIManager.setLayoutAnimationEnabledExperimental(true);
    }
    this.state = {
      errorModal: false,
      errorMessage: "",
    }
  }

  itemClicked = (item, index) => {
    this.props.getFertilizDetailsItem(item.id, this.onGetgetFertilizDetailsItemSuccess, this.onGetgetFertilizDetailsItemFailure)
  }

  onGetgetFertilizDetailsItemSuccess = (error) => {
    this.props.navigation.navigate('FertilizationDetailsItem')
  }

  onGetgetFertilizDetailsItemFailure = (error) => {
    this.setState({ errorMessage: error.errorMessage, requestError: error, errorModal: true, isRefreshing: false, getNextData: false })
  }

  confirmVerificationPopup = () => {
    this.confirmVerificationButton = true
    this.setState({ errorModal: false })
  }

  closeVerificationPopup = () => {
    this.confirmVerificationButton = false
    this.setState({ errorModal: false })
  }

  onErrorModalHide = () => {
    if (this.confirmVerificationButton) {
      this.props.getFertilizDetailsItem(this.props.item.id, this.onGetgetFertilizDetailsItemSuccess, this.onGetgetFertilizDetailsItemFailure)
    }
  }

  render() {
    let productImageUrl = this.props.item.productInfo.thumbnail ? this.props.item.productInfo.thumbnail : ""
    let productImage = productImageUrl != "" ? { uri: productImageUrl } : require('../../Images/dummy.jpg')
    return (
      <Animatable.View animation="fadeInRightBig" delay={this.props.index * 100}>
        <TouchableOpacity style={[styles.container]} onPress={() => this.itemClicked(this.props.item, this.props.index)}>
          <View style={styles.backgroundImage}>
            <ResponsiveImage style={mainStyles.flex} source={productImage} />
          </View>
          <LinearGradient
            colors={["rgba(0, 0, 0, 0.0)", "rgba(0, 0, 0, 0.29)", "rgba(0, 0, 0, 0.8)"]}
            start={{ x: 0.0, y: 0.0 }}
            end={{ x: 0.0, y: 1.0 }}
            style={{ height: "100%", width: "100%", justifyContent: 'flex-end', padding: 18, borderRadius: 10 }}
          >
            <Text style={styles.productName}>
              {this.props.item.productInfo.name_ar}
            </Text>
            <Text style={styles.productNameEn}>
              {this.props.item.productInfo.name}
            </Text>
          </LinearGradient>
        </TouchableOpacity>
        <Modal
          transparent={true}
          style={{ justifyContent: 'center', backgroundColor: 'transparent' }}
          isVisible={this.state.errorModal}
          animationIn={"bounceInUp"}
          animationOut={"bounceOutDown"}
          animationInTiming={1000}
          animationOutTiming={1000}
          onModalHide={() => this.onErrorModalHide()}
        >
          <ConfirmationPopup
            mainTitle={"عذراً"}
            description={this.state.errorMessage}
            confirmText={"إعادة"}
            rejectText={"إغلاق"}
            confirm={this.confirmVerificationPopup}
            cancel={this.closeVerificationPopup} />
        </Modal>
      </Animatable.View>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps(state) {
  return {
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(FertilizationListItem);


const styles = StyleSheet.create({
  container: {
    height: Dimensions.get('window').height * 0.299,
    borderRadius: 10,
    marginBottom: 15,
    marginLeft: 18,
    marginRight: 18,
    backgroundColor: colors.slate,
    overflow: 'hidden',
  },
  backgroundImage: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
  productName: {
    fontFamily: fonts.Bold,
    fontSize: 20,
    lineHeight: 30,
    letterSpacing: 0,
    textAlign: "right",
    color: colors.white
  },
  productNameEn: {
    marginTop: 10,
    fontFamily: "LucidaGrande",
    fontWeight: "bold",
    fontSize: 20,
    lineHeight: 30,
    letterSpacing: 0,
    textAlign: "right",
    color: colors.white
  },
  descriptionText: {
    fontFamily: fonts.Medium,
    fontSize: 16,
    lineHeight: 26,
    letterSpacing: 0,
    textAlign: "right",
    color: colors.white
  },
  stripNewProduct: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: 80,
    height: 80
  },
  triangleCorner: {
    backgroundColor: 'transparent',
    borderStyle: 'solid',
    borderRightWidth: 80,
    borderTopWidth: 80,
    borderRightColor: 'transparent',
    borderTopColor: colors.mediumGreen
  },
  textNewProduct: {
    position: 'absolute',
    top: 20,
    left: -10,
    width: 60,
    transform: [{ rotate: '315deg' }],
    fontSize: 20,
    color: colors.white,
    fontFamily: fonts.Medium,
    zIndex: 100
  }
});