import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  FlatList,
  LayoutAnimation,
  UIManager
} from 'react-native';

import mainStyles from '../../css/MainStyle';
import colors from '../../css/Colors';
import * as fonts from '../../css/Fonts';
import OverlayButton from '../Helpers/OverlayButton';
import * as AppConstants from '../../Config/AppConstants';
import ProductDetails from '../Products/ProductDetails';
import ConfirmationPopup from '../Popup/ConfirmationPopup';

import Dimensions from 'Dimensions';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
import { ActionCreators } from '../../Actions'
import * as Animatable from 'react-native-animatable';
import Header from '../Main Structure/Header';
import Modal from "react-native-modal";

class FertilizationDetails extends Component {

  confirmVerificationButton = false
  getProductDetailsFlag = false
  productId = null

  constructor(props) {
    super();

    fertilizerName = ''
    mechanismOfUseFertilizer = []
    fertilizerId = ''

    if (Platform.OS === 'android') {
      UIManager.setLayoutAnimationEnabledExperimental(true);
    }

    if (props.navigation.state.params) {
      const { stageFertilizers } = props.navigation.state.params
      const { rate_of_use, mode_of_use, reason_of_use, fertilizer_name, fertilizer_id } = stageFertilizers
      fertilizerName = fertilizer_name
      fertilizerId = fertilizer_id
      if (rate_of_use.trim() != "") mechanismOfUseFertilizer.push({ rate_of_use: rate_of_use })
      if (mode_of_use.trim() != "") mechanismOfUseFertilizer.push({ mode_of_use: mode_of_use })
      if (reason_of_use.trim() != "") mechanismOfUseFertilizer.push({ reason_of_use: reason_of_use })
      // mechanismOfUseFertilizer = [{ rate_of_use: rate_of_use }, { mode_of_use: mode_of_use }, { reason_of_use: reason_of_use }]
    }

    this.state = {
      refresh: false,
      toggleProductDetails: false,
      errorModal: false,
      errorMessage: "",
      requestError: null,
    };
  }

  backAction = () => {
    this.props.closeAction()
  }

  showFertilizer = () => {
    this.productId = fertilizerId
    this.getProductDetailsFlag = true

    this.props.logEvent(AppConstants.PorductDetails, { product: fertilizerName })
    this.props.getProductDetails(fertilizerId, this.onGetPorductDetailsSucess, this.onGetPorductDetailsFailure)
  }

  onGetPorductDetailsSucess = () => {
    this.getProductDetailsFlag = false
    this.productId = null
    this.openProductDetailsAction()
  }

  onGetPorductDetailsFailure = (error) => {
    this.setState({ errorMessage: error.errorMessage, requestError: error, errorModal: true })
  }

  openProductDetailsAction = () => {
    this.setState({ toggleProductDetails: true })
  }


  closeProductDetailsAction = () => {
    this.setState({ toggleProductDetails: false })
  }
  confirmVerificationPopup = () => {
    this.confirmVerificationButton = true
    this.setState({ errorModal: false })
  }

  closeVerificationPopup = () => {
    this.confirmVerificationButton = false
    this.setState({ errorModal: false })
  }

  onErrorModalHide = () => {
    if (this.confirmVerificationButton) {
      if (this.state.requestError) {
        if (this.state.requestError.shouldRetry) {
          if (this.getProductDetailsFlag) {
            if (this.productId) {
              this.props.getProductDetails(this.productId, this.onGetPorductDetailsSucess, this.onGetPorductDetailsFailure)
            }
          }
        } else if (this.state.requestError.shouldLogout) {
          this.backAction()
          this.props.logoutAndDeleteAllData(this.props.navigation)
        }
      }
    }
  }

  itemClicked = (item, index) => {
    if (item) {
      LayoutAnimation.configureNext({
        duration: 250,
        update: {
          type: LayoutAnimation.Types.linear,
        }
      });
      let data = mechanismOfUseFertilizer
      data[index].expanded = !data[index].expanded
      this.setState({ refresh: true })
    }
  }

  renderItem = (item, index) => {
    let title = item.rate_of_use ? 'معدل الاستخدام' : item.mode_of_use ? 'طريقة التسميد' : item.reason_of_use ? 'الغاية من استعمال السماد' : null
    let subtitel = item.rate_of_use ? item.rate_of_use : item.mode_of_use ? item.mode_of_use : item.reason_of_use ? item.reason_of_use : null
    return (
      <Animatable.View animation="fadeInDownBig" delay={index * 100} style={{ overflow: 'hidden' }}>
        <TouchableOpacity onPress={() => this.itemClicked(item, index)}>
          <View style={[mainStyles.rowFlex, itemStyles.question]}>
            {
              <Text style={itemStyles.plusText}>
                {item.expanded ? "-" : "+"}
              </Text>
            }
            <Text style={itemStyles.questionText}>
              {title}
            </Text>
          </View>
        </TouchableOpacity>
        <View style={[
          itemStyles.answerView,
          { overflow: 'hidden', height: item.expanded ? null : 0, paddingLeft: item.expanded ? 30 : 0, paddingRight: item.expanded ? 30 : 0, paddingTop: item.expanded ? 19 : 0, paddingBottom: item.expanded ? 19 : 0, borderBottomWidth: item.expanded ? 1 : 0 }
        ]}
        >
          {item.expanded ?
            <Text style={itemStyles.answerText}>
              {subtitel}
            </Text>
            : null}
        </View>
      </Animatable.View>
    )
  }

  render() {
    let detailsFertilizerButton = null
    if (fertilizerId) {
      detailsFertilizerButton =
        <View style={{ marginLeft: 36, marginRight: 36, marginTop: 21, marginBottom: 21 }}>

          <OverlayButton
            buttonColors={[colors.fernGreen, colors.fernGreenTwo]}
            buttonBackgroundColor={colors.fernGreen}
            buttonTextColor={colors.white}
            clickButton={this.showFertilizer}
            style={mainStyles.flex}
            text={"تفاصيل السماد"}
            customStyle={itemStyles.loginButton}
            textButtonStyle={itemStyles.loginText}
          />
        </View>
    }

    return (
      <View style={[mainStyles.flex, styles.container]}>
        <Header navigation={this.props.navigation} fertilizeType={fertilizerName} />
        {mechanismOfUseFertilizer ? mechanismOfUseFertilizer.length > 0 ? <FlatList
          data={mechanismOfUseFertilizer}
          renderItem={({ item, index }) => this.renderItem(item, index)}
          keyExtractor={(item, index) => index.toString()}
          style={{ marginTop: 15 }}
        /> : <View style={[mainStyles.flex, { justifyContent: 'center', alignItems: 'center' }]}><Text style={{ fontFamily: fonts.Light, fontSize: 15, textAlign: "center", color: colors.slate }}>لايوجد تفاصيل حالياً</Text></View>
          : null
        }
        {detailsFertilizerButton}
        <Modal
          transparent={true}
          onBackButtonPress={() => this.closeProductDetailsAction()}
          style={{ marginLeft: 0, marginRight: 0, marginTop: 0, marginBottom: 0 }}
          isVisible={this.state.toggleProductDetails}
          animationIn={"bounceInUp"}
          animationOut={"bounceOutDown"}
          animationInTiming={900}
          animationOutTiming={900}
        >
          <ProductDetails closeAction={this.closeProductDetailsAction} />
        </Modal>
        <Modal
          transparent={true}
          style={{ justifyContent: 'center', backgroundColor: 'transparent' }}
          isVisible={this.state.errorModal}
          animationIn={"bounceInUp"}
          animationOut={"bounceOutDown"}
          animationInTiming={1000}
          animationOutTiming={1000}
          onModalHide={() => this.onErrorModalHide()}
        >
          <ConfirmationPopup
            mainTitle={"عذراً"}
            description={this.state.errorMessage}
            oneButton={this.state.requestError ? !this.state.requestError.shouldRetry : true}
            confirmText={this.state.requestError ? this.state.requestError.shouldRetry ? "إعادة" : "إغلاق" : "متابعة"}
            rejectText={"إغلاق"}
            confirm={this.confirmVerificationPopup}
            cancel={this.closeVerificationPopup} />
        </Modal>
      </View>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps(state) {
  return {
    cateogryPorducts: state.Reducer.cateogryPorducts,
  };
}


const itemStyles = StyleSheet.create({
  question: {
    paddingLeft: 21,
    paddingRight: 29,
    paddingTop: 15,
    paddingBottom: 15,
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: colors.white,
    shadowColor: "#e3e3e3",
    shadowOffset: {
      width: 0,
      height: -1
    },
    shadowRadius: 0,
    shadowOpacity: 1,
    overflow: 'hidden',
    borderStyle: 'solid',
    borderBottomWidth: 1,
    borderBottomColor: "rgb(220,220,220)",
  },
  questionText: {
    marginLeft: 23,
    fontFamily: fonts.Light,
    fontSize: 20,
    letterSpacing: -0.13,
    textAlign: "right",
    color: colors.slate
  },
  minusText: {
    fontFamily: "LucidaGrande",
    fontWeight: "bold",
    fontSize: 40,
    fontStyle: "normal",
    letterSpacing: 0,
    textAlign: "left",
    color: "rgb(220,220,220)"
  },
  plusText: {
    fontFamily: "LucidaGrande",
    fontWeight: "bold",
    fontSize: 32,
    fontStyle: "normal",
    letterSpacing: 0,
    textAlign: "left",
    color: "rgb(220,220,220)"
  },
  answerView: {
    backgroundColor: colors.white,
    borderStyle: 'solid',
    borderBottomColor: "rgb(220,220,220)",
  },
  answerText: {
    fontFamily: fonts.Light,
    fontSize: 14.5,
    letterSpacing: -0.13,
    lineHeight: 22,
    textAlign: "right",
    color: colors.slate
  },
  arrowIcon: {
    width: 22,
    height: 22,
    tintColor: "rgb(220,220,220)",
  },
  closeButton: {
    marginTop: 20,
    marginRight: 15,
    padding: 10,
    alignItems: 'flex-end',
    position: 'absolute',
    top: 0,
    right: 0,
    width: 40,
    height: 40,
    zIndex: 1000,
  },
  loginButton: {
    height: 65,
    borderRadius: 4,
    shadowColor: "rgba(0, 0, 0, 0.21)",
    shadowOffset: {
      width: 7,
      height: -8
    },
    shadowRadius: 26,
    shadowOpacity: 1,
    elevation: 5,
  },
  loginText: {
    fontFamily: fonts.Medium,
    fontSize: 18,
    letterSpacing: 0,
    textAlign: "center",
  },
});

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.white
  }
})


export default connect(mapStateToProps, mapDispatchToProps)(FertilizationDetails);
