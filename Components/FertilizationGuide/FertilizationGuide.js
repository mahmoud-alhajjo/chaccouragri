import React, { Component } from 'react';
import { View, Text, StyleSheet, FlatList, RefreshControl, UIManager } from 'react-native';
import mainStyles from '../../css/MainStyle';
import colors from '../../css/Colors';
import * as fonts from '../../css/Fonts';
import Header from '../Main Structure/Header';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { ActionCreators } from '../../Actions';
import ConfirmationPopup from '../Popup/ConfirmationPopup'
import FertilizationListItem from './FertilizationListItem';

import Modal from "react-native-modal";
import Spinner from 'react-native-spinkit'

class FertilizationGuide extends Component {

  confirmVerificationButton = false
  getList = true

  constructor() {
    super();
    if (Platform.OS === 'android') {
      UIManager.setLayoutAnimationEnabledExperimental(true);
    }
    this.state = {
      isRefreshing: false,
      getNextData: false,
      errorModal: false,
      errorMessage: "",
    };
  }

  componentDidUpdate(prevProps) {
    if (this.props.fertilizerGuides !== prevProps.fertilizerGuides) {
      this.setState({ isRefreshing: false, getNextData: false })
    }
  }

  componentDidMount() {
    this.getList = true
    this.props.getAllFertilizerGuides(null, this.onGetAllFertilizerGuidesFailure)
  }

  onGetAllFertilizerGuidesFailure = (error) => {
    this.setState({ errorMessage: error.errorMessage, requestError: error, errorModal: true, isRefreshing: false, getNextData: false })
  }

  confirmVerificationPopup = () => {
    this.confirmVerificationButton = true
    this.setState({ errorModal: false })
  }

  closeVerificationPopup = () => {
    this.confirmVerificationButton = false
    this.setState({ errorModal: false })
  }

  onErrorModalHide = () => {
    if (this.confirmVerificationButton) {
      if (this.getList) {
        this.props.getAllFertilizerGuides(null, this.onGetAllFertilizerGuidesFailure)
      } else {
        this.setState({ getNextData: true })
        this.props.getAllFertilizerGuides(this.props.fertilizerGuides, this.onGetAllFertilizerGuidesFailure)
      }
    }
  }

  refreshList = () => {
    this.getList = true
    this.setState({ isRefreshing: true })
    this.props.getAllFertilizerGuides(null, this.onGetAllFertilizerGuidesFailure, true)
  }

  loadNextPage = (event) => {
    const scrollHeight = Math.floor(event.nativeEvent.contentOffset.y + event.nativeEvent.layoutMeasurement.height);
    const height = Math.floor(event.nativeEvent.contentSize.height);
    if (scrollHeight >= height) {
      if ((this.props.fertilizerGuides.offset) && (!this.state.getNextData)) {
        this.setState({
          getNextData: true
        }, () => {
          this.getList = false
          this.props.getAllFertilizerGuides(this.props.fertilizerGuides, this.onGetAllFertilizerGuidesFailure)
        });
      }
    }
  }

  render() {
    return (
      <View style={[mainStyles.flex, styles.container]}>
        <Header navigation={this.props.navigation} />
        {
          this.props.fertilizerGuides ? this.props.fertilizerGuides.models ? <FlatList
            ref={(ref) => { this.flatListRef = ref; }}
            data={this.props.fertilizerGuides.models}
            extraData={this.props.fertilizerGuides}
            renderItem={({ item, index }) =>
              <FertilizationListItem navigation={this.props.navigation} item={item} index={index} totalCount={this.props.fertilizerGuides.models.length} />
            }
            keyExtractor={(item, index) => index.toString()}
            showsVerticalScrollIndicator={false}
            contentContainerStyle={{ paddingTop: 15 }}
            removeClippedSubviews={true}
            onMomentumScrollEnd={this.loadNextPage}
            refreshControl={
              ((this.props.fertilizerGuides != null) || (this.state.requestError != null)) ?
                <RefreshControl
                  tintColor={colors.fernGreen}
                  colors={[colors.fernGreen, colors.fernGreenTwo]}
                  refreshing={this.state.isRefreshing}
                  onRefresh={this.refreshList}
                /> : null
            }
          /> : <View style={[mainStyles.flex, { justifyContent: 'center', alignItems: 'center' }]}><Text style={{ fontFamily: fonts.Light, fontSize: 15, textAlign: "center", color: colors.slate }}>لايوجد دليل أسمدة حالياً</Text></View>
            : null
        }
        <View style={{ justifyContent: 'center', alignItems: 'center', display: this.state.getNextData ? 'flex' : 'none' }}>
          <Spinner isVisible={this.state.getNextData} size={40} color={colors.fernGreen} type={"ThreeBounce"} />
        </View>
        <Modal
          transparent={true}
          style={{ justifyContent: 'center', backgroundColor: 'transparent' }}
          isVisible={this.state.errorModal}
          animationIn={"bounceInUp"}
          animationOut={"bounceOutDown"}
          animationInTiming={1000}
          animationOutTiming={1000}
          onModalHide={() => this.onErrorModalHide()}
        >
          <ConfirmationPopup
            mainTitle={"عذراً"}
            description={this.state.errorMessage}
            confirmText={"إعادة"}
            rejectText={"إغلاق"}
            confirm={this.confirmVerificationPopup}
            cancel={this.closeVerificationPopup} />
        </Modal>
      </View>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps(state) {
  return {
    fertilizerGuides: state.Reducer.fertilizerGuides,
  };
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.white,
  }
})


export default connect(mapStateToProps, mapDispatchToProps)(FertilizationGuide);
