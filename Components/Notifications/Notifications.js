/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  FlatList,
  Text,
  RefreshControl
} from 'react-native';

import mainStyles from '../../css/MainStyle';
import colors from '../../css/Colors';
import * as fonts from '../../css/Fonts';
import Header from '../Main Structure/Header'
import NotificationItem from './NotificationItem'
import ConfirmationPopup from '../Popup/ConfirmationPopup'

import PushNotification from 'react-native-push-notification';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
import { ActionCreators } from '../../Actions'
import Modal from "react-native-modal";
import Spinner from 'react-native-spinkit'

export class Notifications extends Component {

  confirmVerificationButton = false
  getList = true

  constructor(props) {
    super(props);
    this.state = {
      isRefreshing: false,
      getNextData: false,

      errorModal: false,
      errorMessage: "",
      requestError: null,
    }
  }

  componentDidMount() {
    PushNotification.cancelAllLocalNotifications()
    this.props.goToNotificationScreen(false)
    this.props.setUserNotificationRead(null)
    this.props.getNotifications(null, this.onGetNotificationsFailure)
  }

  onGetNotificationsFailure = (error) => {
    this.setState({ errorMessage: error.errorMessage, requestError: error, errorModal: true, isRefreshing: false, getNextData: false })
  }

  componentDidUpdate(prevProps) {
    if (this.props.notifications !== prevProps.notifications) {
      this.setState({ isRefreshing: false, getNextData: false })
    }
    if (this.props.user) {
      if (this.props.user.notifications) {
        for (var i = 0; i < this.props.user.notifications.length; i++) {
          this.props.user.notifications[i].read = true
        }
        prevProps.goToNotificationScreen(false)
      }
    }
  }

  confirmVerificationPopup = () => {
    this.confirmVerificationButton = true
    this.setState({ errorModal: false })
  }

  closeVerificationPopup = () => {
    this.confirmVerificationButton = false
    this.setState({ errorModal: false })
  }

  onErrorModalHide = () => {
    if (this.confirmVerificationButton) {
      if (this.getList) {
        this.props.getNotifications(null, this.onGetNotificationsFailure)
      } else {
        this.setState({ getNextData: true })
        this.props.getNotifications(this.props.notifications, this.onGetNotificationsFailure)
      }
    }
  }

  refreshList = () => {
    this.getList = true
    this.setState({ isRefreshing: true })
    this.props.getNotifications(null, this.onGetNotificationsFailure, true)
  }

  loadNextPage = (event) => {
    const scrollHeight = Math.floor(event.nativeEvent.contentOffset.y + event.nativeEvent.layoutMeasurement.height);
    const height = Math.floor(event.nativeEvent.contentSize.height);
    if (scrollHeight >= height) {
      if ((this.props.notifications.offset) && (!this.state.getNextData)) {
        this.setState({
          getNextData: true
        }, () => {
          this.getList = false
          this.props.getNotifications(this.props.notifications, this.onGetNotificationsFailure)
        });
      }
    }
  }

  render() {    
    return (
      <View style={[mainStyles.flex, styles.container]}>
        <Header navigation={this.props.navigation} />
        {
          this.props.notifications ? this.props.notifications.notifications.length > 0 ? <FlatList
            ref={(ref) => { this.flatListRef = ref; }}
            data={this.props.notifications.notifications}
            extraData={this.props.notifications}
            renderItem={({ item, index }) =>
              <NotificationItem item={item} index={index} totalCount={this.props.notifications.notifications.length} />
            }
            keyExtractor={(item, index) => index.toString()}
            showsVerticalScrollIndicator={false}
            removeClippedSubviews={true}
            onMomentumScrollEnd={this.loadNextPage}
            refreshControl={
              ((this.props.notifications != null) || (this.state.requestError != null)) ?
                <RefreshControl
                  tintColor={colors.fernGreen}
                  colors={[colors.fernGreen, colors.fernGreenTwo]}
                  refreshing={this.state.isRefreshing}
                  onRefresh={this.refreshList}
                /> : null
            }
          /> : <View style={[mainStyles.flex, { justifyContent: 'center', alignItems: 'center' }]}><Text style={{ fontFamily: fonts.Light, fontSize: 15, textAlign: "center", color: colors.slate }}>لا يوجدك لديك إشعارات بعد</Text></View>
            :  null
        }
        <View style={{ justifyContent: 'center', alignItems: 'center', display: this.state.getNextData ? 'flex' : 'none' }}>
          <Spinner isVisible={this.state.getNextData} size={40} color={colors.fernGreen} type={"ThreeBounce"} />
        </View>
        <Modal
          transparent={true}
          style={{ justifyContent: 'center', backgroundColor: 'transparent' }}
          isVisible={this.state.errorModal}
          animationIn={"bounceInUp"}
          animationOut={"bounceOutDown"}
          animationInTiming={1000}
          animationOutTiming={1000}
          onModalHide={() => this.onErrorModalHide()}
        >
          <ConfirmationPopup
            mainTitle={"عذراً"}
            description={this.state.errorMessage}
            confirmText={"إعادة"}
            rejectText={"إغلاق"}
            confirm={this.confirmVerificationPopup}
            cancel={this.closeVerificationPopup} />
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.white
  }
});

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps(state) {
  return {
    user: state.Reducer.user,
    notifications: state.Reducer.notifications,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Notifications);
