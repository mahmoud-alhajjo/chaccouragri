/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  TouchableOpacity
} from 'react-native';

import mainStyles from '../../css/MainStyle';
import colors from '../../css/Colors';
import * as fonts from '../../css/Fonts';
import ResponsiveImage from '../Helpers/ResponsiveImage'

import Dimensions from 'Dimensions';
import * as Animatable from 'react-native-animatable';
import Moment from 'moment';
import 'moment/locale/ar'

export default class NotificationItem extends Component {

  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    let notificationImageItem = this.props.item.image ? { uri: this.props.item.image } : null
    let notificationWithImage =
      <Animatable.View animation="fadeInDownBig" style={[styles.container, { marginTop: this.props.index == 0 ? 26.5 : 13.25, marginBottom: this.props.index == (this.props.totalCount - 1) ? 26.25 : 13.25 }]} delay={this.props.index * 100}>
        <View style={[mainStyles.rowFlex, { marginTop: this.props.index == 0 ? 12 : 4, marginBottom: this.props.index == (this.props.totalCount - 1) ? 12 : 4 }]} >
          <View style={[mainStyles.flex, { paddingTop: 6, paddingBottom: 6 }]}>
            <Text style={[mainStyles.flex, styles.mainTitleText]}>
              {this.props.item.title}
            </Text>
            <Text style={styles.descriptionText}>
              {this.props.item.message}
            </Text>
            <View>
              <Text style={styles.timeText}>
                {Moment.unix(this.props.item.created_at).locale('ar').format('YYYY, MMMM DD')}
              </Text>
              <Text style={styles.timeText}>
                {Moment.unix(this.props.item.created_at).locale('ar').format('hh:mm a')}
              </Text>
            </View>
          </View>
          {
            notificationImageItem ? <ResponsiveImage style={styles.itemImage} source={notificationImageItem} /> : null
          }
        </View>
        <View style={styles.seprator} />
      </Animatable.View>

    let notificationWithoutImage =
      <Animatable.View animation="fadeInDownBig" style={[styles.container, { marginTop: this.props.index == 0 ? 26.5 : 13.25, marginBottom: this.props.index == (this.props.totalCount - 1) ? 26.25 : 13.25 }]} delay={this.props.index * 100}>
        <View style={[mainStyles.rowFlex, mainStyles.flex, { justifyContent: 'space-between', alignItems: 'center' }]}>
          <View>
            <Text style={styles.timeText}>
              {Moment.unix(this.props.item.created_at).locale('ar').format('YYYY, MMMM DD')}
            </Text>
            <Text style={styles.timeText}>
              {Moment.unix(this.props.item.created_at).locale('ar').format('hh:mm a')}
            </Text>
          </View>
          <Text style={[mainStyles.flex, styles.mainTitleText]}>
            {this.props.item.title}
          </Text>
        </View>
        <Text style={styles.descriptionText}>
          {this.props.item.message}
        </Text>
        <View style={styles.seprator} />
      </Animatable.View>

    return (
      this.props.item.image ? notificationWithImage : notificationWithoutImage
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginRight: 20,
    marginLeft: 20,
    paddingLeft: 9.5,
    backgroundColor: colors.white
  },
  timeText: {
    fontSize: 10,
    marginRight: 5,
    fontFamily: fonts.Light,
    letterSpacing: 0,
    textAlign: "right",
    color: colors.cloudyBlue
  },
  mainTitleText: {
    fontFamily: fonts.Medium,
    fontSize: 15,
    letterSpacing: 0,
    textAlign: "right",
    color: colors.slate
  },
  descriptionText: {
    marginTop: 13,
    marginBottom: 14,
    fontFamily: fonts.Medium,
    fontSize: 10,
    lineHeight: 17,
    letterSpacing: 0,
    textAlign: "right",
    color: colors.battleshipGrey
  },
  seprator: {
    height: 1.5,
    width: '100%',
    backgroundColor: colors.silver
  },
  itemImage: {
    marginLeft: 17,
    borderRadius: 2,
    width: Dimensions.get('window').width * 0.256,
    height: Dimensions.get('window').height * 0.127
  },
  dateText: {
    fontSize: 10,
    fontFamily: fonts.Light,
    letterSpacing: 0,
    textAlign: "left",
    color: colors.cloudyBlue,
    marginRight: 5,
  }
});
