/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';

export default class Validator {

  static isEmailValid(email) {
    let reg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return reg.test(email);
  }

  static isNameValid(name, maxLength = 2) {
    if(name.length >= maxLength){
      return true
    } else {
      return false
    }
  }


  static isMobileValid(mobile) {
    if(mobile.length == 10){
      let reg = /^(09)[0-9]/;
      return reg.test(mobile);
    } else {
      return false
    }
  }

  static isExperienceYears(experienceYears) {
    if(experienceYears.length <= 2){
      let reg = /^[0-9]*$/;
      return reg.test(experienceYears);
    } else {
      return false
    }
  }

  static isSalaryExpected(salaryExpected) {
    if(salaryExpected.length <= 7){
      let reg = /^[0-9]*$/;
      return reg.test(salaryExpected);
    } else {
      return false
    }
  }

  static isPasswordValid(password) {
    let reg = /^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{6,})/
    return reg.test(password);
  }

  static isCodeValid(code) {
    if(code.length == 4) {
      return true
    }
    return false
  }

  static getEmailError(email) {
    if ((email == null) || (email == "")) {
      return "الرجاء ادخال البريد الاكتروني"
    } else {
      if (Validator.isEmailValid(email)) {
        return null
      } else {
        return "الرجاء ادخال بريد الكتروني صالح"
      }
    }
  }

  static getPasswordErrorLogin(password){
    if ((password == null) || (password == "")) {
      return "الرجاء ادخال كلمة المرور"
    } else {
      return null
    }
  }

  static getNameError(name) {
    if ((name == null) || (name == "")) {
      return "الرجاء ادخال الاسم"
    } else {
      if (Validator.isNameValid(name)) {
        return null
      } else {
        return "الرجاء ادخال اسم صحيح"
      }
    }
  }

  static getLastNameError(lastName) {
    if ((lastName == null) || (lastName == "")) {
      return "الرجاء ادخال اسم العائلة"
    } else {
      if (Validator.isNameValid(lastName)) {
        return null
      } else {
        return "الرجاء ادخال اسم عائلة صحيح"
      }
    }
  }


  static getMobileError(mobile) {
    if ((mobile == null) || (mobile == "")) {
      return "الرجاء ادخال رقم الجوال"
    } else {
      if (Validator.isMobileValid(mobile)) {
        return null
      } else {
        return "الرجاء ادخال رقم جوال سوري صالح يبدأ ب 09"
      }
    }
  }

  static getExperienceYearsError(experienceYears) {
    if ((experienceYears == null) || (experienceYears == "")) {
      return "الرجاء ادخال سنين الخبرة"
    } else {
      if (Validator.isExperienceYears(experienceYears)) {
        return null
      } else {
        return "الرجاء ادخال رقم سنين الخبرة مكون من خانتين"
      }
    }
  }

  static getSalaryExpectedError(salaryExpected) {
    if ((salaryExpected == null) || (salaryExpected == "")) {
      return "الرجاء ادخال الراتب المتوقع"
    } else {
      if (Validator.isSalaryExpected(salaryExpected)) {
        return null
      } else {
        return "الرجاء ادخال رقم الراتب المتوقع"
      }
    }
  }

  static getPasswordError(password) {
    if ((password == null) || (password == "")) {
      return "الرجاء ادخال كلمة المرور"
    } else {
      if (Validator.isPasswordValid(password)) {
        return null
      } else {
        return "الرجاء ادخال كلمة مرور مكونة من 6 محارف على الأقل مع وجود رقم واحد على الأقل"
      }
    }
  }

  static getNewPasswordError(oldPassword, newPassword) {
    if ((newPassword == null) || (newPassword == "")) {
      return "الرجاء ادخال كلمة المرور"
    } else {
      if(oldPassword != newPassword) {
        if ((oldPassword == null) || (oldPassword == "")) {
          return ""
        } else {
          if (Validator.isPasswordValid(newPassword)) {
            return null
          } else {
            return "الرجاء ادخال كلمة مرور قوية"
          }
        }
      } else if (oldPassword == newPassword) {
        return "كلمة المرور الجديدة لا يمكن ان تكون نفس كلمة المرور القديمة"
      } else {
        return ""
      }
    }
  }

  static getRepeatPasswordError(password, repeatedPassword) {
    if ((password == null) || (password == "")) {
      return null
    } else {
      if (password == repeatedPassword) {
        return null
      } else {
        return "كلمة المرور المعادة يجب ان تكون نفس كلمة المرور"
      }
    }
  }

  static getOccuptionError(occupation){
    if (occupation == null) {
      return "الرجاء اختيار المهنة"
    } else {
      return null
    }
  }

  static getProvinceError(province){
    if (province == null) {
      return "الرجاء اختيار المحافظة"
    } else {
      return null
    }
  }

  static getCityError(city){
    if (city == null) {
      return "الرجاء اختيار المنطقة"
    } else {
      return null
    }
  }

  static getGenderError(gender){
    if (gender == null) {
      return "الرجاء اختيار الجنس"
    } else {
      return null
    }
  }

  static getBirthDateError(birthdate){
    if ((birthdate == null) || (birthdate == "")) {
      return "الرجاء ادخال تاريخ الميلاد"
    } else {
      return null
    }
  }

  static getVerificationError(code){
    if ((code == null) || (code == "")) {
      return "الرجاء ادخال رمز التحقق المرسل"
    } else {
      if (Validator.isCodeValid(code)) {
        return null
      } else {
        return "الرجاء ادخال رمز تحقق صالح المكون من 4 أرقام"
      }
    }
  }
}