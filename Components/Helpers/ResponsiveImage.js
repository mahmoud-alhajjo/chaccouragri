import React, { Component } from 'react';
import {
  View,
  Image,
  Animated
} from 'react-native';

export default class ResponsiveImage extends Component {

  constructor(props){
    super(props);
    this.state = {
      imageLoaded:false,
      thumbnailOpacity: new Animated.Value(1)
    }
  }

  onLoad(){
    Animated.timing(this.state.thumbnailOpacity,{
        toValue: 0,
        duration : 250
    }).start()

  }

  render() {
    let resizeMode = 'cover'
    if(this.props.resizeMode) {
      resizeMode = this.props.resizeMode
    }
    return (
      <View
        style={this.props.style}
      >
        <Animated.Image
          style={{height:'100%', width:'100%', opacity:this.state.thumbnailOpacity}}
          source={require('../../Images/dummy.jpg')}
          resizeMode={'cover'}
        />
        <Animated.Image
          style={{height:'100%', width:'100%', position:'absolute'}}
          source={this.props.source}
          onLoad = {(event)=>this.onLoad(event)}
          resizeMode={resizeMode}
        />
      </View>
    );
  }
}
