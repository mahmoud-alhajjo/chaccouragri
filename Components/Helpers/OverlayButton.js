import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableHighlight,
  Image
} from 'react-native';

import mainStyles from '../../css/MainStyle';

import LinearGradient from 'react-native-linear-gradient';

export default class OverlayButton extends Component {

  constructor(props) {
    super(props);
    this.state = {pressStatus: false};
  }

  onHideUnderlay() {
    this.setState({pressStatus: false});
  }

  onShowUnderlay() {
    this.setState({pressStatus: true});
  }

  clickButton = () => {
    this.props.clickButton()
  }

  renderButton = () => {
    pressStyle = {backgroundColor: this.props.buttonTextColor, opacity:this.props.buttonTextColorOpacity ? this.props.buttonTextColorOpacity : 1, borderWidth:1, borderColor: this.props.buttonBackgroundColor}
    nonPressStyle = {backgroundColor: this.props.buttonBackgroundColor, borderWidth:0, opacity:this.props.buttonBackgroundColorOpacity ? this.props.buttonBackgroundColorOpacity : 1}
    textPressStyle = {color: this.props.buttonBackgroundColor}
    textNonPressStyle = {color: this.props.buttonTextColor}
    customStyle = this.props.customStyle

    return (
        <TouchableHighlight
          disabled={this.props.disabled ? this.props.disabled : false}
          activeOpacity={1}
          onPress={this.clickButton}
          underlayColor = {this.props.buttonTextColor}
          onHideUnderlay={this.onHideUnderlay.bind(this)}
          onShowUnderlay={this.onShowUnderlay.bind(this)}
          style={[customStyle ? customStyle : styles.nextButton, (this.state.pressStatus ? pressStyle : nonPressStyle )]}
          >
           <View style={[mainStyles.rowFlex, styles.buttonInfo]}>
            {this.props.source ? <Image style={[this.props.iconStyle, {tintColor: (this.state.pressStatus ? this.props.buttonBackgroundColor : this.props.buttonTextColor)}]} source={this.props.source}/> : null}
            <Text style={[this.props.textButtonStyle ? this.props.textButtonStyle : styles.nextButtonText , this.state.pressStatus ? textPressStyle : textNonPressStyle]}>
              {this.props.text}
            </Text>
          </View>
        </TouchableHighlight>
    );
  }

  renderLinearButton = () => {

    pressStyle = {opacity:this.props.buttonTextColorOpacity ? this.props.buttonTextColorOpacity : 1, borderWidth:1, borderColor: this.props.buttonBackgroundColor}
    nonPressStyle = {opacity:this.props.buttonBackgroundColorOpacity ? this.props.buttonBackgroundColorOpacity : 1, borderWidth:0}
    textPressStyle = {color: this.props.buttonBackgroundColor}
    textNonPressStyle = {color: this.props.buttonTextColor}
    customStyle = this.props.customStyle

    return (
      <LinearGradient
        colors={this.state.pressStatus ? [this.props.buttonTextColor, this.props.buttonTextColor] : this.props.buttonColors}
        start={{x: 0.0, y: 0.0}}
        end={{x: 0.0, y: 1.0}}
        style={[customStyle ? customStyle : styles.nextButton, (this.state.pressStatus ? pressStyle : nonPressStyle )]}
      >
      <TouchableHighlight
        disabled={this.props.disabled ? this.props.disabled : false}
        activeOpacity={1}
        onPress={this.clickButton}
        underlayColor = {"transparent"}
        onHideUnderlay={this.onHideUnderlay.bind(this)}
        onShowUnderlay={this.onShowUnderlay.bind(this)}
        style={{width:'100%', height:'100%', justifyContent: 'center'}}
        >
         <View style={[mainStyles.rowFlex, styles.buttonInfo]}>
          {this.props.source ? <Image style={[this.props.iconStyle, {tintColor: (this.state.pressStatus ? this.props.buttonBackgroundColor : this.props.buttonTextColor)}]} source={this.props.source}/> : null}
          <Text style={[this.props.textButtonStyle ? this.props.textButtonStyle : styles.nextButtonText , this.state.pressStatus ? textPressStyle : textNonPressStyle]}>
            {this.props.text}
          </Text>
        </View>
      </TouchableHighlight>
      </LinearGradient>
    )
  }

  render() {
    let viewToRender = null
    if(this.props.buttonColors) {
      viewToRender = this.renderLinearButton()
    } else {
      viewToRender = this.renderButton()
    }
    return(
      viewToRender
    )
  }
}


const styles = StyleSheet.create({
  nextButton: {
    height: 48,
    justifyContent: 'center',
    borderRadius:100,
  },
  nextButtonText: {
    fontSize: 13,
    fontWeight: "bold",
    fontStyle: "normal",
    letterSpacing: 0,
    textAlign: "center",
  },
  buttonInfo: {
    justifyContent: 'center',
    alignItems: 'center',
  }
});
