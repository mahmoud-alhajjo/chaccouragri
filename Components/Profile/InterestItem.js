/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Image,
  Text,
  TouchableOpacity,
} from 'react-native';

import mainStyles from '../../css/MainStyle';
import colors from '../../css/Colors';
import * as fonts from '../../css/Fonts';
import ResponsiveImage from '../Helpers/ResponsiveImage'

import * as Animatable from 'react-native-animatable';
import Dimensions from 'Dimensions';

export default class InterestItem extends Component {

  constructor() {
    super();
    this.state = {

    }
  }

  render() {
    let productImageUrl = this.props.item.thumbnail ? this.props.item.thumbnail : ""
    let productImage = productImageUrl != "" ? {uri: productImageUrl} : require('../../Images/dummy.jpg')

    let selectedView = <View style={{backgroundColor: "rgba(81, 148, 68, 0.6)", position: 'absolute', bottom: 0, left: 0, right: 0, top: 0, justifyContent: 'center', alignItems:'center', zIndex: 1000}}>
                        <Image style={styles.choseImage} source={require('../../Images/chose.png')} />
                      </View>
    return (
      <Animatable.View
        style={[
          styles.container,
          { marginRight: this.props.index % 2 != 0 ? 6 : 3,
            marginLeft: this.props.index % 2 == 0 ? 6 : 3,
            marginTop: (this.props.index === 0 || this.props.index === 1) ? 0 : 6.5,
            marginBottom: ((this.props.index === this.props.totalCount - 1) || (this.props.index === this.props.totalCount - 2)) ? 13 : 6.5,
          }
        ]}
        animation="fadeInUpBig"
        delay={this.props.index * 100}>
        <TouchableOpacity
          onPress={() => this.props.itemClick(this.props.item, this.props.index)}
          style={{transform: [{ scaleX: -1 }]}}
        >
          {this.props.item.selected ? selectedView : null}
          <ResponsiveImage style={styles.productImage} source={productImage} />
          <View style={styles.productTextView}>
            <Text numberOfLines={2} style={styles.productText}>
              {this.props.item.name}
            </Text>
          </View>
        </TouchableOpacity>
      </Animatable.View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: (Dimensions.get('window').width - 18) / 2,
    height: Dimensions.get('window').height * 0.235,
    borderRadius: 4,
    backgroundColor: colors.white,
    shadowColor: "rgba(45, 45, 84, 0.16)",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowRadius: 4,
    shadowOpacity: 1,
    elevation: 5,
    overflow: 'hidden',
  },
  productImage: {
    borderTopLeftRadius: 4,
    borderTopRightRadius: 4,
    height:"80%",
    width: "100%",
  },
  productText: {
    fontFamily: fonts.Medium,
    fontSize: 18,
    letterSpacing: 0,
    textAlign: "center",
    color: colors.slate,
  },
  productTextView: {
    alignItems: 'center',
    justifyContent: 'center',
    maxHeight:"20%",
    paddingLeft: 9,
    paddingRight: 9,
    paddingTop: 10,
    paddingBottom: 10,
  },
  choseImage: {
    width: 70,
    height: 70,
    alignSelf: 'center'
  }
});
