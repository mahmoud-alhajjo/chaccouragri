/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Image,
  Text,
  TouchableOpacity,
  TextInput,
  Keyboard,
  PermissionsAndroid,
  ImageBackground
} from 'react-native';

import mainStyles from '../../css/MainStyle';
import colors from '../../css/Colors';
import * as fonts from '../../css/Fonts';
import OverlayButton from '../Helpers/OverlayButton'
import SelectionPopup from '../Popup/SelectionPopup'
import * as constants from '../../Config/AppConstants';
import Validator from '../Helpers/Validator'
import ConfirmationPopup from '../Popup/ConfirmationPopup'

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import DateTimePicker from 'react-native-modal-datetime-picker';
import Moment from 'moment';
import * as Animatable from 'react-native-animatable';
import Modal from "react-native-modal";
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux'
import { ActionCreators } from '../../Actions'

export class EditProfile extends Component {

  confirmVerificationButton = false

  popupType = null

  nameY = 0
  lastNameY = 0
  occuptionY = 0
  areaSizeY = 0
  mobileY = 0
  provinceY = 0
  cityY = 0
  genderY = 0
  birthDateY = 0

  constructor(props) {
    super(props);

    let provinceObj = null
    if((props.user.province) && (props.user.province_name)) {
      provinceObj = {
        id: props.user.province,
        name: props.user.province_name
      }
    }

    let cityObj = null
    if((props.user.city) && (props.user.city_name) && (props.user.province)) {
      cityObj = {
        id: props.user.city,
        name: props.user.city_name,
        province_id: props.user.province,
      }
    }

    let ocupationObj = null
    if((props.user.occupation) && (props.user.occupation_name)) {
      ocupationObj = {
        id: props.user.occupation,
        name: props.user.occupation_name
      }
    }

    let genderObj = null
    if((props.user.gender)) {
      genderObj = {
        id: props.user.gender,
        name: props.user.gender == "male" ? "ذكر" : "انثى"
      }
    }

    let areaSizeObj = null
    if(props.user.area_size) {
      let id = props.user.area_size
      var name = ""
      if(props.user.area_size == "<50") {
        name = "أقل من 50 دنم"
      } else if (props.user.area_size == "50-100") {
        name = "100 - 50 دنم"
      } else if (props.user.area_size == "100-150") {
        name = "150 - 100 دنم"
      } else if (props.user.area_size == ">200") {
        name = "أكبر من 200 دنم"
      }
      areaSizeObj = {
        id: id,
        name: name,
      }
    }

    let specializationObj = null
    if(props.user.specialization) {
      let id = props.user.specialization
      var name = ""
      if(props.user.specialization == "قسم الاقتصاد الزراعي") {
        name = "قسم الاقتصاد الزراعي"
      } else if (props.user.specialization == "قسم الإنتاج الحيواني") {
        name = "قسم الإنتاج الحيواني"
      } else if (props.user.specialization == "قسم العلوم الأساسية") {
        name = "قسم العلوم الأساسية"
      } else if (props.user.specialization == "قسم علوم الأغذية") {
        name = "قسم علوم الأغذية"
      } else if (props.user.specialization == "قسم علوم البستنة") {
        name = "قسم علوم البستنة"
      } else if (props.user.specialization == "قسم علوم التربة") {
        name = "قسم علوم التربة"
      } else if (props.user.specialization == "قسم المحاصيل الحقلية") {
        name = "قسم المحاصيل الحقلية"
      } else if (props.user.specialization == "قسم الموارد الطبيعية المتجددة والبيئة") {
        name = "قسم الموارد الطبيعية المتجددة والبيئة"
      } else if (props.user.specialization == "قسم الهندسة الريفية") {
        name = "قسم الهندسة الريفية"
      } else if (props.user.specialization == "قسم وقاية النبات") {
        name = "قسم وقاية النبات"
      }
      specializationObj = {
        id: id,
        name: name,
      }
    }

    this.state = {
      profilePic: props.user.photo ? {uri: this.props.user.photo} : null,
      profilePic64: null,
      isDateTimePickerVisible: false,
      birthdate: Moment(props.user.birthdate, 'DD-MM-YYYY').locale('en').toDate(),
      birthdateString: props.user.birthdate,

      selectionModal: false,

      firstName: props.user.first_name,
      lastName: props.user.last_name,
      occupation: ocupationObj,
      areaSize: areaSizeObj,
      specialization: specializationObj,
      mobile: props.user.phone_number,
      province: provinceObj,
      city: cityObj,
      gender: genderObj,

      nameError:"",
      lastNameError:"",
      occuptionError:"",
      mobileError:"",
      provinceError:"",
      cityError: "",
      genderError: "",
      birthDateError: "",

      errorModal: false,
      errorMessage: "",
      requestError: null,

    }
  }

  componentDidMount () {
    this.keyboardDidHideSub = Keyboard.addListener('keyboardDidHide', this.keyboardDidHide);
  }

  componentWillUnmount() {
    this.keyboardDidHideSub.remove();
  }

  keyboardDidHide = (event) => {
    this.blurTextFields()
  };

  dismissKeyboard = () => {
    Keyboard.dismiss
    this.blurTextFields()
  }

  blurTextFields = () => {
    this.refs.lastNameInput.blur()
    this.refs.firstNameInput.blur()
  }

  editProfileAction = () => {

    let nameError = Validator.getNameError(this.state.firstName)
    let lastNameError = Validator.getLastNameError(this.state.lastName)
    let occuptionError = Validator.getOccuptionError(this.state.occupation)
    let mobileError = Validator.getMobileError(this.state.mobile)
    let provinceError = Validator.getProvinceError(this.state.province)
    let cityError = Validator.getCityError(this.state.city)
    let genderError = Validator.getGenderError(this.state.gender)
    let birthDateError = Validator.getBirthDateError(this.state.birthdate)

    let areaSizeError = null
    if(this.state.occupation) {
      if(this.state.occupation.name == "مزارع") {
        if(this.state.areaSize == null){
          areaSizeError = "الرجاء ادخال مساحة الأرض"
        }
      }
    }

    let specializationError = null
    if(this.state.occupation) {
      if((this.state.occupation.name == "طالب جامعي في الهندسة الزراعية") || (this.state.occupation.name == "دكتور / أستاذ في كلية الزراعة")) {
        if(this.state.specialization == null){
          specializationError = "الرجاء اختيار الاختصاص"
        }
      }
    }

    this.setState({
      nameError:nameError,
      lastNameError:lastNameError,
      occuptionError:occuptionError,
      areaSizeError:areaSizeError,
      specializationError: specializationError,
      mobileError:mobileError,
      provinceError:provinceError,
      cityError: cityError,
      genderError: genderError,
      birthDateError: birthDateError,
    }, this.scrollToFirstErrorField)

    if ((nameError == null) && (lastNameError == null) && (occuptionError == null) && (areaSizeError == null) && (specializationError == null) && (mobileError == null) && (provinceError == null) && (cityError == null) && (genderError == null) && (birthDateError == null) ) {
      let areaSizeId = null
      if(this.state.areaSize) {
        areaSizeId = this.state.areaSize.id
      }
      let specializationId = null
      if(this.state.specialization) {
        specializationId = this.state.specialization.id
      }
      this.props.editProfile(this.state.firstName, this.state.lastName, this.state.occupation.id, this.state.province.id, this.state.city.id, this.state.gender.id, this.state.birthdateString, this.state.profilePic64, null, areaSizeId, specializationId, this.onEditProfileSucess, this.onEditProfileFailure)
      this.dismissKeyboard()
    }
  }

  onEditProfileSucess = () => {
    let message = "تم تعديل معلوماتك الشخصية بنجاح"
    this.setState({errorMessage:message, requestError:null, errorModal: true})
  }

  onEditProfileFailure = (error) => {
    this.setState({errorMessage:error.errorMessage, requestError: error, errorModal: true})
  }

  scrollToFirstErrorField = () => {
    if(this.state.nameError != null) {
      this.scroll.props.scrollToPosition(0, this.nameY - 10)
      this.refs.firstNameInput.focus()
    } else if(this.state.lastNameError != null) {
      this.scroll.props.scrollToPosition(0, this.lastNameY - 10)
      this.refs.lastNameInput.focus()
    } else if(this.state.occuptionError != null) {
      this.scroll.props.scrollToPosition(0, this.occuptionY - 10)
    } else if(this.state.areaSizeError != null) {
      this.scroll.props.scrollToPosition(0, this.areaSizeY - 10)
    } else if(this.state.specializationError != null) {
      this.scroll.props.scrollToPosition(0, this.areaSizeY - 10)
    }  else if(this.state.mobileError != null) {
      this.scroll.props.scrollToPosition(0, this.mobileY - 10)
      this.refs.mobileInput.focus()
    } else if(this.state.provinceError != null) {
      this.scroll.props.scrollToPosition(0, this.provinceY - 10)
    } else if(this.state.cityError != null) {
      this.scroll.props.scrollToPosition(0, this.cityY - 10)
    } else if(this.state.genderError != null) {
      this.scroll.props.scrollToPosition(0, this.genderY - 10)
    } else if(this.state.birthDateError != null) {
      this.scroll.props.scrollToPosition(0, this.birthDateY - 10)
    }
  }

  backAction = () => {
    this.props.navigation.goBack()
  }

  openImageGallery = () => {
    var ImagePicker = require('react-native-image-picker');
    var options = {
      title: 'اختيار صورة الحساب',
      takePhotoButtonTitle: 'التقاط صورة',
      chooseFromLibraryButtonTitle: "الاختيار من المكتبة",
      cancelButtonTitle: "إلغاء",
      mediaType: 'photo',
      allowsEditing: true,
      maxWidth: 400,
      maxHeight: 200,
    };
    ImagePicker.showImagePicker(options, response  => {
      if (response.didCancel) {
      } else if (response.error) {
        alert(response.error)
      } else if (response.customButton) {
      } else {
          let source = { uri: response.uri };
          setTimeout(() => {
            this.setState({
              profilePic: source,
              profilePic64: response.data
            });
          }, 500);
      }
    });
  }

  selectProfilePic = async () => {
    try {
      let granted = await PermissionsAndroid.requestMultiple([PermissionsAndroid.PERMISSIONS.CAMERA, PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE])
      if ((granted[PermissionsAndroid.PERMISSIONS.CAMERA] === PermissionsAndroid.RESULTS.GRANTED) && (granted[PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE] === PermissionsAndroid.RESULTS.GRANTED)){
        this.openImageGallery()
      } else {
        alert('الرجاء إعطاء صلاحية للكاميرا')
      }
    } catch (err) {
    }
  }

  onGetOccupationsList = () => {
    this.popupType = constants.Occupations
    this.setState({selectionModal: true})
  }

  onGetAreaSizesList = () => {
    this.popupType = constants.AreaSize
    this.setState({selectionModal: true})
  }

  onGetSpecializationList = () => {
    this.popupType = constants.Specialization
    this.setState({selectionModal: true})
  }

  onGetCitiesList = () => {
    this.popupType = constants.City
    this.setState({selectionModal: true})
  }

  cityClicked = () => {
    if(this.state.province) {
      this.props.getCities(this.state.province.id, this.onGetCitiesList)
    } else {
      let provinceError = Validator.getProvinceError(this.state.province)
      this.setState({provinceError:provinceError})
    }
  }

  onGetProvinceList = () => {
    this.popupType = constants.Province
    this.setState({selectionModal: true})
  }

  datePickerAction = () => {
    this.showDateTimePicker()
  }

  onGetGenderList = () => {
    this.popupType = constants.Gender
    this.setState({selectionModal: true})
  }

  showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });

  hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });

  handleDatePicked = (date) => {
    this.setState({
      birthdate: date,
      birthdateString: Moment(date).locale('en').format('DD/MM/YYYY')
    }, this.hideDateTimePicker)
  }

  closeSelectionPopupAction = () => {
    this.setState({selectionModal: false})
  }

  handlePopupSelection = (selectedItem, popupType) => {
    if(popupType == constants.Occupations) {
      this.setState({occupation:selectedItem, occuptionError:"", areaSize: null, specialization: null, selectionModal: false})
    } else if(popupType == constants.AreaSize) {
      this.setState({areaSize:selectedItem, areaSizeError:"", selectionModal: false})
    } else if(popupType == constants.Specialization) {
      this.setState({specialization:selectedItem, specializationError:"", selectionModal: false})
    } else if(popupType == constants.Province) {
      this.setState({province:selectedItem, provinceError:"", city: null, selectionModal: false})
    } else if(popupType == constants.City) {
     this.setState({city:selectedItem, cityError:"", selectionModal: false})
    } else if(popupType == constants.Gender) {
     this.setState({gender:selectedItem, genderError:"", selectionModal: false})
    }
  }

  getListForSelectionPopup = () => {
    let popupType = this.popupType
    if(popupType == constants.Occupations){
      return this.props.occupations
    } else if(popupType == constants.AreaSize){
      return this.props.areaSize
    } else if(popupType == constants.Specialization){
      return this.props.specialization
    }  else if(popupType == constants.Province){
      return this.props.province
    } else if(popupType == constants.City){
     return this.props.cities
    } else if(popupType == constants.Gender){
     return this.props.gender
    }
    return null
  }

  getSelectedItemSelectionPopup = () => {
    let popupType = this.popupType
    if(popupType == constants.Occupations){
      return this.state.occupation
    } else if(popupType == constants.AreaSize){
      return this.state.areaSize
    } else if(popupType == constants.Specialization){
      return this.state.specialization
    } else if(popupType == constants.Province){
      return this.state.province
    } else if(popupType == constants.City){
      return this.state.city
    } else if(popupType == constants.Gender){
      return this.state.gender
    }
    return null
  }

  onSelectionModalHide = () => {
    this.popupType = null
  }

  handleChangeText = (event, field) => {
    if(field == 'firstName') {
      this.setState({firstName: event})
    } else if(field == 'lastName') {
      this.setState({lastName: event})
    } else if(field == 'mobile') {
      this.setState({mobile: event})
    }
  }

  renderTextError = (error) => {
    return(
      <Text style={styles.errorText}>
        {error}
      </Text>
    )
  }

  confirmVerificationPopup = () => {
    this.confirmVerificationButton = true
    this.setState({errorModal: false})
  }

  closeVerificationPopup = () => {
    this.confirmVerificationButton = false
    this.setState({errorModal: false})
  }

  onErrorModalHide = () => {
    if(this.confirmVerificationButton) {
      if(this.state.requestError) {
        if(this.state.requestError.shouldRetry) {
          this.editProfileAction()
        } else if(this.state.requestError.shouldLogout) {
          this.props.logoutAndDeleteAllData(null)
        }
      } else {
        this.props.navigation.goBack()
      }
    }
  }

  render() {
    let areaSizeField = null
    if(this.state.occupation) {
      if(this.state.occupation.name == "مزارع") {
        areaSizeField = <View style={[styles.mainInputField, {justifyContent:null, alignItems: null}]}>
                        <View style={[mainStyles.rowFlex, mainStyles.flex, {justifyContent:'space-between', alignItems: 'center'}]}>
                          <Image style={styles.downArrow} source={require('../../Images/downArrow.png')}/>
                          <View style={[styles.singleTextInputView, {marginLeft: 16}]}
                                onLayout={event => {
                                  const layout = event.nativeEvent.layout;
                                  this.areaSizeY = layout.y
                                }}
                          >
                            <TextInput
                              ref="jobInput"
                              placeholder={"مساحة الأرض المزروعة (دنم)"}
                              underlineColorAndroid ={"transparent"}
                              placeholderTextColor={colors.slate}
                              blurOnSubmit={false}
                              selectionColor={colors.slate}
                              style={[mainStyles.flex, styles.textInput]}
                              editable={false}
                              value={this.state.areaSize ? this.state.areaSize.name : ""}
                            />
                          </View>
                        </View>
                        {(this.state.areaSizeError == "" || this.state.areaSizeError == null) ? null : this.renderTextError(this.state.areaSizeError)}
                        <TouchableOpacity onPress={() => this.props.getAreaSize(this.onGetAreaSizesList)} style={{position: 'absolute', top: 0, left: 0, right: 0, bottom: 0}}/>
                      </View>
      }
    }
    let specializationField = null
    if(this.state.occupation) {
      if((this.state.occupation.name == "طالب جامعي في الهندسة الزراعية") || (this.state.occupation.name == "دكتور / أستاذ في كلية الزراعة")) {
        specializationField = <View style={[styles.mainInputField, {justifyContent:null, alignItems: null}]}>
                        <View style={[mainStyles.rowFlex, mainStyles.flex, {justifyContent:'space-between', alignItems: 'center'}]}>
                          <Image style={styles.downArrow} source={require('../../Images/downArrow.png')}/>
                          <View style={[styles.singleTextInputView, {marginLeft: 16}]}
                                onLayout={event => {
                                  const layout = event.nativeEvent.layout;
                                  this.areaSizeY = layout.y
                                }}
                          >
                            <TextInput
                              ref="jobInput"
                              placeholder={"الاختصاص"}
                              underlineColorAndroid ={"transparent"}
                              placeholderTextColor={colors.slate}
                              blurOnSubmit={false}
                              selectionColor={colors.slate}
                              style={[mainStyles.flex, styles.textInput]}
                              editable={false}
                              value={this.state.specialization ? this.state.specialization.name : ""}
                            />
                          </View>
                        </View>
                        {(this.state.specializationError == "" || this.state.specializationError == null) ? null : this.renderTextError(this.state.specializationError)}
                        <TouchableOpacity onPress={() => this.props.getSpecialization(this.onGetSpecializationList)} style={{position: 'absolute', top: 0, left: 0, right: 0, bottom: 0}}/>
                      </View>
      }
    }
    return (
      <View style={[mainStyles.flex, styles.container]}>
        <View style={[mainStyles.rowFlex, {marginTop:25, alignItems: 'center',justifyContent: 'space-between'}]}>
          <View style={{width: 70}} />
          <Text style={[mainStyles.flex, styles.headerText]}>
            تعديل المعلومات الشخصية
          </Text>
          <TouchableOpacity style={[styles.closeButton, {alignItems: 'flex-end'}]} onPress={this.backAction}>
            <Image resizeMode={'contain'} style={styles.closeIcon} source={require('../../Images/close.png')} />
          </TouchableOpacity>
        </View>
        <KeyboardAwareScrollView innerRef={ref => {this.scroll = ref}} style={{backgroundColor: colors.white}} contentContainerStyle={{backgroundColor: colors.white}} bounces={false} enableResetScrollToCoords={true} scrollEnabled={true} enableOnAndroid={true}>
          <Animatable.View style={mainStyles.flex} animation="fadeInDownBig">
            <TouchableOpacity style={styles.profilePicMainView} onPress={this.selectProfilePic}>
              {
                this.state.profilePic ? null :  <Image resizeMode={'contain'} style={styles.cameraPic} source={require('../../Images/imgCamera.png')}/>
              }
              <ImageBackground source={require('../../Images/imgPlaceholderBg.png')} style={styles.profilePicView}>
                <Image style={this.state.profilePic ? styles.profilePic : styles.profilePicEmpty} source={this.state.profilePic ? this.state.profilePic : require('../../Images/imgPlaceholder.png')}/>
              </ImageBackground>
            </TouchableOpacity>
            <View style={styles.inputView}>
              <View style={[mainStyles.rowFlex, styles.mainInputField]}>
                <View style={[styles.doubleTextInputView, {paddingRight: 16, borderStyle: "solid", borderRightWidth: 1, borderRightColor: colors.silver}]}
                      onLayout={event => {
                        const layout = event.nativeEvent.layout;
                        this.lastNameY = layout.y
                      }}
                >
                  <TextInput
                    ref="lastNameInput"
                    placeholder={"اسم العائلة"}
                    underlineColorAndroid ={"transparent"}
                    placeholderTextColor={colors.slate}
                    returnKeyType={"next"}
                    autoCapitalize={"words"}
                    autoCorrect={false}
                    maxLength={50}
                    blurOnSubmit={false}
                    selectionColor={colors.slate}
                    style={[mainStyles.flex, styles.textInput]}
                    onChangeText={((event) => this.handleChangeText(event, 'lastName'))}
                    value={this.state.lastName ? this.state.lastName : ""}
                    onSubmitEditing={(event) => this.dismissKeyboard()}
                  />
                  {(this.state.lastNameError == "" || this.state.lastNameError == null) ? null : this.renderTextError(this.state.lastNameError)}
                </View>
                <View style={[styles.doubleTextInputView, {paddingLeft: 16}]}
                      onLayout={event => {
                        const layout = event.nativeEvent.layout;
                        this.nameY = layout.y
                      }}
                >
                  <TextInput
                    ref="firstNameInput"
                    placeholder={"الاسم"}
                    underlineColorAndroid ={"transparent"}
                    placeholderTextColor={colors.slate}
                    returnKeyType={"next"}
                    autoCapitalize={"words"}
                    autoCorrect={false}
                    maxLength={50}
                    blurOnSubmit={false}
                    selectionColor={colors.slate}
                    style={[mainStyles.flex, styles.textInput]}
                    onChangeText={((event) => this.handleChangeText(event, 'firstName'))}
                    value={this.state.firstName ? this.state.firstName : ""}
                    onSubmitEditing={(event) => this.refs.lastNameInput.focus()}
                  />
                  {(this.state.nameError == "" || this.state.nameError == null) ? null : this.renderTextError(this.state.nameError)}
                </View>
              </View>
              <View style={[styles.mainInputField, {justifyContent:null, alignItems: null}]}>
                <View style={[mainStyles.rowFlex, mainStyles.flex, {justifyContent:'space-between', alignItems: 'center'}]}>
                  <Image style={styles.downArrow} source={require('../../Images/downArrow.png')}/>
                  <View style={[styles.singleTextInputView, {marginLeft: 16}]}
                        onLayout={event => {
                          const layout = event.nativeEvent.layout;
                          this.occuptionY = layout.y
                        }}
                  >
                    <TextInput
                      ref="jobInput"
                      placeholder={"المهنة"}
                      underlineColorAndroid ={"transparent"}
                      placeholderTextColor={colors.slate}
                      blurOnSubmit={false}
                      selectionColor={colors.slate}
                      style={[mainStyles.flex, styles.textInput]}
                      editable={false}
                      value={this.state.occupation ? this.state.occupation.name : ""}
                    />
                  </View>
                </View>
                {(this.state.occuptionError == "" || this.state.occuptionError == null) ? null : this.renderTextError(this.state.occuptionError)}
                <TouchableOpacity onPress={() => this.props.getOccupations(this.onGetOccupationsList)} style={{position: 'absolute', top: 0, left: 0, right: 0, bottom: 0}}/>
              </View>
              {areaSizeField}
              {specializationField}
              <View style={[mainStyles.rowFlex, styles.mainInputField]}>
                <View style={styles.singleTextInputView}
                      onLayout={event => {
                        const layout = event.nativeEvent.layout;
                        this.mobileY = layout.y
                      }}
                >
                  <TextInput
                    ref="mobileInput"
                    placeholder={"رقم الجوال"}
                    underlineColorAndroid ={"transparent"}
                    placeholderTextColor={colors.slate}
                    returnKeyType={"next"}
                    maxLength={10}
                    blurOnSubmit={false}
                    keyboardType={"phone-pad"}
                    selectionColor={colors.slate}
                    style={[mainStyles.flex, styles.textInput]}
                    onChangeText={((event) => this.handleChangeText(event, 'mobile'))}
                    value={this.state.mobile ? this.state.mobile : ""}
                    onSubmitEditing={(event) => this.dismissKeyboard()}
                    editable={false}
                  />
                  {(this.state.mobileError == "" || this.state.mobileError == null) ? null : this.renderTextError(this.state.mobileError)}
                </View>
              </View>
              <View style={[mainStyles.rowFlex, styles.mainInputField]}>
                <View style={[styles.doubleTextInputView, {paddingRight: 16, borderStyle: "solid", borderRightWidth: 1, borderRightColor: colors.silver}]}>
                  <View style={[mainStyles.flex, mainStyles.rowFlex, {alignItems:'center'}]}
                        onLayout={event => {
                          const layout = event.nativeEvent.layout;
                          this.cityY = layout.y
                        }}
                  >
                    <Image style={styles.downArrow} source={require('../../Images/downArrow.png')}/>
                    <TextInput
                      ref="cityInput"
                      placeholder={"المنطقة"}
                      underlineColorAndroid ={"transparent"}
                      placeholderTextColor={colors.slate}
                      blurOnSubmit={false}
                      selectionColor={colors.slate}
                      style={[mainStyles.flex, styles.textInput]}
                      editable={false}
                      value={this.state.city ? this.state.city.name : ""}
                    />
                  </View>
                  {(this.state.cityError == "" || this.state.cityError == null) ? null : this.renderTextError(this.state.cityError)}
                  <TouchableOpacity onPress={() => this.cityClicked()} style={{position: 'absolute', top: 0, left: 0, right: 0, bottom: 0}}/>
                </View>
                <View style={[styles.doubleTextInputView, {paddingLeft: 16}]}>
                  <View style={[mainStyles.flex, mainStyles.rowFlex, {alignItems:'center'}]}
                        onLayout={event => {
                          const layout = event.nativeEvent.layout;
                          this.provinceY = layout.y
                        }}
                  >
                    <Image style={styles.downArrow} source={require('../../Images/downArrow.png')}/>
                    <TextInput
                      ref="provinceInput"
                      placeholder={"محافظة"}
                      underlineColorAndroid ={"transparent"}
                      placeholderTextColor={colors.slate}
                      blurOnSubmit={false}
                      selectionColor={colors.slate}
                      style={[mainStyles.flex, styles.textInput]}
                      editable={false}
                      value={this.state.province ? this.state.province.name : ""}
                    />
                  </View>
                  {(this.state.provinceError == "" || this.state.provinceError == null) ? null : this.renderTextError(this.state.provinceError)}
                  <TouchableOpacity onPress={() => this.props.getProvince(this.onGetProvinceList)} style={{position: 'absolute', top: 0, left: 0, right: 0, bottom: 0}}/>
                </View>
              </View>
              <View style={[mainStyles.rowFlex, styles.mainInputField]}>
                <View style={[styles.doubleTextInputView, {paddingRight: 16, borderStyle: "solid", borderRightWidth: 1, borderRightColor: colors.silver}]}>
                  <View style={[mainStyles.flex, mainStyles.rowFlex, {alignItems:'center'}]}
                        onLayout={event => {
                          const layout = event.nativeEvent.layout;
                          this.birthDateY = layout.y
                        }}
                  >
                    <Image style={styles.downArrow} source={require('../../Images/downArrow.png')}/>
                    <TextInput
                      ref="birthDateInput"
                      placeholder={"تاريخ الميلاد"}
                      underlineColorAndroid ={"transparent"}
                      placeholderTextColor={colors.slate}
                      blurOnSubmit={false}
                      selectionColor={colors.slate}
                      style={[mainStyles.flex, styles.textInput]}
                      editable={false}
                      value={this.state.birthdateString}
                    />
                  </View>
                  {(this.state.birthDateError == "" || this.state.birthDateError == null) ? null : this.renderTextError(this.state.birthDateError)}
                  <TouchableOpacity onPress={this.datePickerAction} style={{position: 'absolute', top: 0, left: 0, right: 0, bottom: 0}}/>
                </View>
                <View style={[styles.doubleTextInputView, {paddingLeft: 16}]}>
                  <View style={[mainStyles.flex, mainStyles.rowFlex, {alignItems:'center'}]}
                        onLayout={event => {
                          const layout = event.nativeEvent.layout;
                          this.genderY = layout.y
                        }}
                  >
                    <Image style={styles.downArrow} source={require('../../Images/downArrow.png')}/>
                    <TextInput
                      ref="genderInput"
                      placeholder={"الجنس"}
                      underlineColorAndroid ={"transparent"}
                      placeholderTextColor={colors.slate}
                      blurOnSubmit={false}
                      selectionColor={colors.slate}
                      style={[mainStyles.flex, styles.textInput]}
                      editable={false}
                      value={this.state.gender ? this.state.gender.name : ""}
                    />
                  </View>
                  {(this.state.genderError == "" || this.state.genderError == null) ? null : this.renderTextError(this.state.genderError)}
                  <TouchableOpacity onPress={() => this.props.getGender(this.onGetGenderList)} style={{position: 'absolute', top: 0, left: 0, right: 0, bottom: 0}}/>
                </View>
              </View>
            </View>
            <View style={{marginLeft: 36, marginRight: 36, marginTop: 25, marginBottom: 25}}>
              <OverlayButton buttonColors={[colors.fernGreen, colors.fernGreenTwo]} buttonBackgroundColor={colors.fernGreen} buttonTextColor={colors.white} clickButton={this.editProfileAction} style={mainStyles.flex} text={"تعديل المعلومات الشخصية"} customStyle={styles.loginButton} textButtonStyle={styles.loginText} />
            </View>
          </Animatable.View>
        </KeyboardAwareScrollView>
        <DateTimePicker
          isVisible={this.state.isDateTimePickerVisible}
          onConfirm={this.handleDatePicked}
          onCancel={this.hideDateTimePicker}
          maximumDate={Moment().locale('en').toDate()}
          minimumDate={Moment().locale('en').subtract(100, 'y').toDate()}
          locale={'ar'}
          date={this.state.birthdate}
        />
        <Modal
            transparent={true}
            onBackButtonPress={() => this.closeSelectionPopupAction()}
            onBackdropPress={() => this.closeSelectionPopupAction()}
            style={{justifyContent: 'center', alignItems: 'center', backgroundColor: 'transparent'}}
            isVisible={this.state.selectionModal}
            animationIn={"bounceInUp"}
            animationOut={"bounceOutDown"}
            animationInTiming={1000}
            animationOutTiming={1000}
            onModalHide={() => this.onSelectionModalHide()}
          >
          <SelectionPopup popupType={this.popupType} selectedItem={this.getSelectedItemSelectionPopup()} list={this.getListForSelectionPopup()} cancel={this.closeSelectionPopupAction} confirm={(item) => this.handlePopupSelection(item, this.popupType)} />
        </Modal>
        <Modal
            transparent={true}
            style={{justifyContent: 'center', backgroundColor: 'transparent'}}
            isVisible={this.state.errorModal}
            animationIn={"bounceInUp"}
            animationOut={"bounceOutDown"}
            animationInTiming={1000}
            animationOutTiming={1000}
            onModalHide={() => this.onErrorModalHide()}
          >
          <ConfirmationPopup
            mainTitle={this.state.requestError ? "عذراً" : "تهانينا"}
            description={this.state.errorMessage}
            oneButton={this.state.requestError ? !this.state.requestError.shouldRetry : true}
            confirmText={this.state.requestError ? this.state.requestError.shouldRetry ? "إعادة" : "إغلاق" : "إغلاق"}
            rejectText={"إغلاق"}
            confirm={this.confirmVerificationPopup}
            cancel={this.closeVerificationPopup} />
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.white
  },
  headerText: {
    fontFamily: fonts.Medium,
    fontSize: 20,
    letterSpacing: 0,
    textAlign: "center",
    color: colors.charcoalGreyTwo
  },
  closeButton: {
    width:70,
    height:45,
    justifyContent: 'center',
  },
  closeIcon: {
    marginRight: 19,
    width: 14,
    height: 14,
  },
  profilePicMainView: {
    marginTop: 18,
    alignItems:'center',
    justifyContent: 'center',
    width: 90,
    height: 90,
    alignSelf: 'center'
  },
  profilePicView: {
    width: 90,
    height: 90,
    justifyContent: 'center',
    alignItems: 'center',
  },
  profilePicEmpty: {
    width: 32,
    height: 32,
    marginBottom: 8,
  },
  profilePic: {
    width: 68,
    height: 69,
    borderRadius: 34.5,
    marginBottom: 8.5,
  },
  cameraPic: {
    width: 23,
    height: 23,
    position: 'absolute',
    top: 0,
    right: 7,
    zIndex: 1000,
  },
  inputView: {
    marginTop: 13,
    marginLeft: 36,
    marginRight: 36,
    borderRadius: 4,
    borderStyle: "solid",
    borderWidth: 1,
    borderColor: colors.silverTwo
  },
  textInput: {
    paddingTop: 0,
    paddingBottom: 0,
    fontFamily: fonts.Light,
    fontSize: 15,
    fontWeight: "300",
    letterSpacing: 0,
    textAlign: "right",
    color: colors.slate
  },
  downArrow: {
    width: 12,
    height: 6
  },
  mainInputField: {
    borderStyle: "solid",
    borderBottomWidth: 1,
    borderBottomColor: colors.silver,
    height: 67.5,
    justifyContent:'space-between',
    alignItems: 'center',
    paddingLeft: 16,
    paddingRight: 16,
  },
  mainInputFieldNoBorder: {
    height: 67.5,
    justifyContent:'space-between',
    alignItems: 'center',
    paddingLeft: 16,
    paddingRight: 16,
  },
  doubleTextInputView: {
    height: '100%',
    width:'50%',
  },
  singleTextInputView: {
    height: '100%',
    flex: 1,
  },
  loginButton: {
    height: 65,
    borderRadius: 4,
    shadowColor: "rgba(0, 0, 0, 0.21)",
    shadowOffset: {
      width: 7,
      height: -8
    },
    shadowRadius: 26,
    shadowOpacity: 1,
    elevation: 5,
  },
  loginText: {
    fontFamily: fonts.Medium,
    fontSize: 18,
    letterSpacing: 0,
    textAlign: "center",
  },
  errorText: {
    fontFamily: fonts.Light,
    fontSize: 12,
    letterSpacing: 0,
    textAlign: "right",
    color: colors.darkishRed,
    marginBottom: 6,
  }
});

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps(state) {
  return {
    occupations: state.Reducer.occupations,
    province: state.Reducer.province,
    cities: state.Reducer.cities,
    gender: state.Reducer.gender,
    user: state.Reducer.user,
    areaSize: state.Reducer.areaSize,
    specialization: state.Reducer.specialization,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(EditProfile);
