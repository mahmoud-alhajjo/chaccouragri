/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TextInput,
  Keyboard,
  Image,
  Text,
  TouchableOpacity,
} from 'react-native';

import mainStyles from '../../css/MainStyle';
import colors from '../../css/Colors';
import * as fonts from '../../css/Fonts';
import OverlayButton from '../Helpers/OverlayButton'
import Validator from '../Helpers/Validator'
import ConfirmationPopup from '../Popup/ConfirmationPopup'

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import * as Animatable from 'react-native-animatable';
import Modal from "react-native-modal";
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux'
import { ActionCreators } from '../../Actions'

export class EditPassword extends Component {

  oldPasswordY = 0
  newPasswordY = 0
  confirmNewPasswordY = 0
  confirmVerificationButton = false

  constructor(props) {
    super(props);
    this.state = {
      oldPassword: null,
      newPassword: null,
      confirmNewPassword: null,
      oldPasswordError: "",
      newPasswordError: "",
      confirmNewPasswordError: "",

      errorModal: false,
      errorMessage: "",
      requestError: null,
    };
  }

  componentDidMount () {
    this.keyboardDidHideSub = Keyboard.addListener('keyboardDidHide', this.keyboardDidHide);
  }

  componentWillUnmount() {
    this.keyboardDidHideSub.remove();
  }

  keyboardDidHide = (event) => {
    this.blurTextFields()
  };

  dismissKeyboard = () => {
    Keyboard.dismiss
    this.blurTextFields()
  }

  blurTextFields = () => {
    if(this.refs.oldPasswordInput) {
      this.refs.oldPasswordInput.blur()
    }
    this.refs.newPasswordInput.blur()
    this.refs.repeatNewPasswordInput.blur()
  }

  changePasswordAction = () => {
    if(this.props.navigation.state.params !== undefined) {
      if(this.props.navigation.state.params.fromVerification) {
        let code = this.props.navigation.state.params.code

        let newPasswordError = Validator.getPasswordError(this.state.newPassword)
        let confirmNewPasswordError = Validator.getRepeatPasswordError(this.state.newPassword, this.state.confirmNewPassword)

        this.setState({
          newPasswordError: newPasswordError,
          confirmNewPasswordError: confirmNewPasswordError,
        }, this.scrollToFirstErrorField)
        if((newPasswordError == null) && (confirmNewPasswordError == null)){
          this.dismissKeyboard()
          this.props.forgetPasswordAction(this.state.newPassword, code, this.props.navigation, this.onForgetPasswordActionFailure)
        }
      }
    } else {
      let oldPasswordError = Validator.getPasswordErrorLogin(this.state.oldPassword)
      let newPasswordError = Validator.getNewPasswordError(this.state.oldPassword, this.state.newPassword)
      let confirmNewPasswordError = Validator.getRepeatPasswordError(this.state.newPassword, this.state.confirmNewPassword)

      this.setState({
        oldPasswordError: oldPasswordError,
        newPasswordError: newPasswordError,
        confirmNewPasswordError: confirmNewPasswordError,
      }, this.scrollToFirstErrorField)

      if ((oldPasswordError == null) && (newPasswordError == null) && (confirmNewPasswordError == null)){
        this.props.editPassword(this.state.oldPassword, this.state.newPassword, this.onChangePasswordSucess, this.onChangePasswordFailure)
        this.dismissKeyboard()
      }
    }
  }

  onForgetPasswordActionFailure = (error) => {
    this.setState({errorMessage:error.errorMessage, requestError: error, errorModal: true})
  }

  onChangePasswordSucess = () => {
    let message = "تم تعديل كلمة المرور الخاصة بك بنجاح"
    this.setState({errorMessage: message, requestError: null, errorModal: true})
  }

  onChangePasswordFailure = (error) => {
    this.setState({errorMessage:error.errorMessage, requestError: error, errorModal: true})
  }

  scrollToFirstErrorField = () => {
    if(this.state.oldPasswordError != null) {
      if(this.refs.oldPasswordInput) {
        this.scroll.props.scrollToPosition(0, this.oldPasswordY - 10)
        this.refs.oldPasswordInput.focus()
      }
    } else if(this.state.newPasswordError != null) {
      this.scroll.props.scrollToPosition(0, this.newPasswordY - 10)
      this.refs.newPasswordInput.focus()
    } else if(this.state.confirmNewPasswordError != null) {
      this.scroll.props.scrollToPosition(0, this.confirmNewPasswordY - 10)
      this.refs.repeatNewPasswordInput.focus()
    }
  }

  backAction = () => {
    this.props.navigation.goBack()
  }

  handleChangeText = (event, field) => {
    if(field == 'oldPassword') {
      this.setState({oldPassword: event})
    } else if(field == 'newPassword') {
      this.setState({newPassword: event})
    } else if(field == 'confirmNewPassword') {
      this.setState({confirmNewPassword: event})
    }
  }

  confirmVerificationPopup = () => {
    this.confirmVerificationButton = true
    this.setState({errorModal: false})
  }

  closeVerificationPopup = () => {
    this.confirmVerificationButton = false
    this.setState({errorModal: false})
  }

  onErrorModalHide = () => {
    if(this.confirmVerificationButton) {
      if(this.state.requestError) {
        if(this.state.requestError.shouldRetry) {
          this.changePasswordAction()
        } else if(this.state.requestError.shouldLogout) {
          this.props.backToOnBoardingFromVerification(this.props.navigation)
        }
      } else {
        this.props.navigation.goBack()
      }
    }
  }


  renderTextError = (error) => {
    return(
      <Text style={styles.errorText}>
        {error}
        </Text>
      )
  }

  render() {
    var backButton = <TouchableOpacity style={[styles.closeButton, {alignItems: 'flex-end'}]} onPress={this.backAction}>
                        <Image resizeMode={'contain'} style={styles.closeIcon} source={require('../../Images/close.png')} />
                      </TouchableOpacity>
    var oldPasswordInput = <View style={styles.textInputInnerView}
                                onLayout={event => {
                                  const layout = event.nativeEvent.layout;
                                  this.oldPasswordY = layout.y
                                }}
                            >
                              <TextInput
                                ref="oldPasswordInput"
                                placeholder={"كلمة المرور القديمة"}
                                underlineColorAndroid ={"transparent"}
                                placeholderTextColor={colors.slate}
                                returnKeyType={"next"}
                                autoCapitalize={"none"}
                                autoCorrect={false}
                                maxLength={50}
                                blurOnSubmit={false}
                                secureTextEntry={true}
                                selectionColor={colors.slate}
                                style={[mainStyles.flex, styles.textInput]}
                                onChangeText={((event) => this.handleChangeText(event, 'oldPassword'))}
                                value={this.state.oldPassword ? this.state.oldPassword : ""}
                                onSubmitEditing={(event) => this.refs.newPasswordInput.focus()}
                              />
                              {(this.state.oldPasswordError == "" || this.state.oldPasswordError == null) ? null : this.renderTextError(this.state.oldPasswordError)}
                          </View>


    if(this.props.navigation.state.params !== undefined) {
      if(this.props.navigation.state.params.fromVerification) {
        oldPasswordInput = null
        backButton = <View style={{width: 70}} />
      }
    }

    return (
      <View style={[mainStyles.flex, styles.container]}>
        <View style={[mainStyles.rowFlex, {marginTop: 25, alignItems: 'center',justifyContent: 'space-between'}]}>
          <View style={{width: 70}} />
          <Text style={styles.headerText}>
            تعديل كلمة المرور
          </Text>
          {backButton}
        </View>
        <KeyboardAwareScrollView innerRef={ref => {this.scroll = ref}} style={{backgroundColor: colors.white}} contentContainerStyle={[styles.container, {backgroundColor: colors.white}]} bounces={false} enableResetScrollToCoords={true} scrollEnabled={true} enableOnAndroid={true}>
          <Animatable.View style={mainStyles.flex} animation="fadeInDownBig">
            <View style={styles.textInputView}>
                {oldPasswordInput}
                {oldPasswordInput ? <View style={{width: '100%', height: 1, backgroundColor: colors.silver}} /> : null}
                <View style={styles.textInputInnerView}
                      onLayout={event => {
                        const layout = event.nativeEvent.layout;
                        this.newPasswordY = layout.y
                      }}
                >
                  <TextInput
                    ref="newPasswordInput"
                    placeholder={"كلمة المرور الجديدة"}
                    underlineColorAndroid ={"transparent"}
                    placeholderTextColor={colors.slate}
                    returnKeyType={"next"}
                    autoCapitalize={"none"}
                    autoCorrect={false}
                    maxLength={50}
                    blurOnSubmit={false}
                    secureTextEntry={true}
                    selectionColor={colors.slate}
                    style={[mainStyles.flex, styles.textInput]}
                    onChangeText={((event) => this.handleChangeText(event, 'newPassword'))}
                    value={this.state.newPassword ? this.state.newPassword : ""}
                    onSubmitEditing={(event) => this.refs.repeatNewPasswordInput.focus()}
                  />
                  {(this.state.newPasswordError == "" || this.state.newPasswordError == null) ? null : this.renderTextError(this.state.newPasswordError)}
                </View>
                <View style={{width: '100%', height: 1, backgroundColor: colors.silver}} />
                <View style={styles.textInputInnerView}
                      onLayout={event => {
                        const layout = event.nativeEvent.layout;
                        this.confirmNewPasswordY = layout.y
                      }}
                >
                  <TextInput
                    ref="repeatNewPasswordInput"
                    placeholder={"تأكيد كلمة المرور الجديدة"}
                    underlineColorAndroid ={"transparent"}
                    placeholderTextColor={colors.slate}
                    returnKeyType={"done"}
                    autoCapitalize={"none"}
                    autoCorrect={false}
                    maxLength={50}
                    blurOnSubmit={false}
                    secureTextEntry={true}
                    selectionColor={colors.slate}
                    style={[mainStyles.flex, styles.textInput]}
                    onChangeText={((event) => this.handleChangeText(event, 'confirmNewPassword'))}
                    value={this.state.confirmNewPassword ? this.state.confirmNewPassword : ""}
                    onSubmitEditing={(event) => this.changePasswordAction()}
                  />
                  {(this.state.confirmNewPasswordError == "" || this.state.confirmNewPasswordError == null) ? null : this.renderTextError(this.state.confirmNewPasswordError)}
                </View>
            </View>
            <View style={{marginLeft: 36, marginRight: 36, marginTop: 21}}>
              <OverlayButton buttonColors={[colors.fernGreen, colors.fernGreenTwo]} buttonBackgroundColor={colors.fernGreen} buttonTextColor={colors.white} clickButton={this.changePasswordAction} style={mainStyles.flex} text={"تغير كلمة المرور"} customStyle={styles.loginButton} textButtonStyle={styles.loginText} />
            </View>
          </Animatable.View>
        </KeyboardAwareScrollView>
        <Modal
            transparent={true}
            style={{justifyContent: 'center', backgroundColor: 'transparent'}}
            isVisible={this.state.errorModal}
            animationIn={"bounceInUp"}
            animationOut={"bounceOutDown"}
            animationInTiming={1000}
            animationOutTiming={1000}
            onModalHide={() => this.onErrorModalHide()}
          >
          <ConfirmationPopup
            mainTitle={this.state.requestError ? "عذراً" : "تهانينا"}
            description={this.state.errorMessage}
            oneButton={this.state.requestError ? !this.state.requestError.shouldRetry : true}
            confirmText={this.state.requestError ? this.state.requestError.shouldRetry ? "إعادة" : "إغلاق" : "إغلاق"}
            rejectText={"إغلاق"}
            confirm={this.confirmVerificationPopup}
            cancel={this.closeVerificationPopup} />
        </Modal>
      </View>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps(state) {
  return {
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(EditPassword);

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.white
  },
  headerText: {
    fontFamily: fonts.Medium,
    fontSize: 20,
    letterSpacing: 0,
    textAlign: "center",
    color: colors.charcoalGreyTwo
  },
  closeButton: {
    width:70,
    height:45,
    justifyContent: 'center',
  },
  closeIcon: {
    marginRight: 19,
    width: 14,
    height: 14,
  },
  textInputView: {
    marginTop: 30,
    marginLeft: 36,
    marginRight: 36,
    borderRadius: 4,
    borderStyle: "solid",
    borderWidth: 1,
    borderColor: colors.silverTwo,
  },
  textInputInnerView: {
    height: 67.5,
    paddingLeft: 16,
    paddingRight: 16,
  },
  textInput: {
    paddingTop: 0,
    paddingBottom: 0,
    fontFamily: fonts.Light,
    fontSize: 15,
    letterSpacing: 0,
    textAlign: "right",
    color: colors.slate
  },
  loginButton: {
    height: 65,
    borderRadius: 4,
    shadowColor: "rgba(0, 0, 0, 0.21)",
    shadowOffset: {
      width: 7,
      height: -8
    },
    shadowRadius: 26,
    shadowOpacity: 1,
    elevation: 5,
  },
  loginText: {
    fontFamily: fonts.Medium,
    fontSize: 18,
    letterSpacing: 0,
    textAlign: "center",
  },
  errorText: {
    fontFamily: fonts.Light,
    fontSize: 12,
    letterSpacing: 0,
    textAlign: "right",
    color: colors.darkishRed,
    marginBottom: 6,
  },
});
