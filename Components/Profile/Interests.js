/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Image,
  Text,
  TouchableOpacity,
  FlatList,
} from 'react-native';

import mainStyles from '../../css/MainStyle';
import CustomStatusBar from '../Main Structure/CustomStatusBar'
import colors from '../../css/Colors';
import * as fonts from '../../css/Fonts';
import OverlayButton from '../Helpers/OverlayButton'
import InterestItem from './InterestItem'
import ConfirmationPopup from '../Popup/ConfirmationPopup'
import * as AppConstants from '../../Config/AppConstants';

import {connect} from 'react-redux';
import { bindActionCreators } from 'redux'
import { ActionCreators } from '../../Actions'
import Modal from "react-native-modal";


export class Interests extends Component {

  endReached = false
  confirmVerificationButton = false

  constructor() {
    super();

    this.state = {
      buttonName: "التالي",
      isMainInterest: true,
      refresh: false,

      errorModal: false,
      errorMessage: "",
      requestError: null,

    }
  }

  componentDidUpdate(prevProps) {
    if(this.props.mainInterests){
          let fromProfile = false
          if(prevProps.navigation.state.params !== undefined) {
            fromProfile = prevProps.navigation.state.params.fromProfile
          }
          if(fromProfile){
            if(this.props.mainInterests){
              let selectedInterests = this.props.mainInterests.filter((item) => {
                return item.selected == true
              })
              let buttonName = "التالي"
              if(selectedInterests.length == 1) {
                if(!selectedInterests[0].hasChilds) {
                  buttonName = "حفظ"
                }
              } else {
                let noChilds = []
                for (var i = 0; i < selectedInterests.length; i++) {
                  if(!selectedInterests[i].hasChilds){
                    noChilds.push(selectedInterests[i])
                  }
                }
                if(noChilds.length == selectedInterests.length){
                  buttonName = "حفظ"
                }
              }
              return {
                buttonName: buttonName
              }
            }
          }
        }
  }

  componentDidMount() {
    this.props.getMainInterests()
  }

  skipAction = () => {
    this.props.navigateToHomeFromSplash(this.props.navigation)
  }

  saveAction = () => {
    if(this.state.isMainInterest) {
      let selectedInterests = this.props.mainInterests.filter((item) => {
        return item.selected == true
      })
      let selectedInterestsIds = selectedInterests.map(({id}) => (id));
      if(selectedInterests.length == 1) {
        if(selectedInterests[0].hasChilds){
          this.props.getInterestsByType(selectedInterestsIds, this.onGetSubInterests)
        } else {
          this.props.editProfile(null, null, null, null, null, null, null, null, selectedInterestsIds, this.props.user ? this.props.user.area_size : null, this.props.user ? this.props.user.specialization : null, this.onEditProfileSucess, this.onEditProfileFailure)
        }
      } else {
        let noChilds = []
        for (var i = 0; i < selectedInterests.length; i++) {
          if(!selectedInterests[i].hasChilds){
            noChilds.push(selectedInterests[i])
          }
        }
        if(noChilds.length == selectedInterests.length){
          this.props.editProfile(null, null, null, null, null, null, null, null, selectedInterestsIds, this.props.user ? this.props.user.area_size : null, this.props.user ? this.props.user.specialization : null, this.onEditProfileSucess, this.onEditProfileFailure)
        } else {
          this.props.getInterestsByType(selectedInterestsIds, this.onGetSubInterests)
        }
      }
    } else {
      let selectedInterestsIds = this.props.subInterests.filter((item) => {
        return item.selected == true
      }).map(({id}) => (id));
      let mainSelectedInterests = this.props.mainInterests.filter((item) => {
        return item.selected == true
      })
      for (var i = 0; i < mainSelectedInterests.length; i++) {
        if(!mainSelectedInterests[i].hasChilds){
          selectedInterestsIds.push(mainSelectedInterests[i].id)
        }
      }
      this.props.editProfile(null, null, null, null, null, null, null, null, selectedInterestsIds, this.props.user ? this.props.user.area_size : null, this.props.user ? this.props.user.specialization : null, this.onEditProfileSucess, this.onEditProfileFailure)
    }
  }

  onEditProfileSucess = () => {
    var fromProfile = false
    if(this.props.navigation.state.params !== undefined) {
      fromProfile = this.props.navigation.state.params.fromProfile
    }

    if(fromProfile) {
      this.props.logEvent(AppConstants.UpdateInterests, null)
      let message = "تم تعديل الاهتمامات الخاصة بك بنجاح"
      this.setState({errorMessage:message, requestError: null, errorModal: true})
    } else {
      this.props.navigateToHomeFromSplash(this.props.navigation)
    }
  }

  onEditProfileFailure = (error) => {
    this.setState({errorMessage:error.errorMessage, requestError: error, errorModal: true})
  }

  onGetSubInterests = () => {
    this.setState({isMainInterest: false, buttonName: "حفظ"})
  }

  backAction = () => {
    var fromProfile = false
    if(this.props.navigation.state.params !== undefined) {
      fromProfile = this.props.navigation.state.params.fromProfile
    }
    if(fromProfile && this.state.isMainInterest) {
      this.props.navigation.goBack()
    } else if(!this.state.isMainInterest) {
      let selectedInterests = this.props.mainInterests.filter((item) => {
        return item.selected == true
      })
      let buttonName = "التالي"
      if(selectedInterests.length == 1) {
        if(!selectedInterests[0].hasChilds) {
          buttonName = "حفظ"
        }
      } else {
        let noChilds = []
        for (var i = 0; i < selectedInterests.length; i++) {
          if(!selectedInterests[i].hasChilds){
            noChilds.push(selectedInterests[i])
          }
        }
        if(noChilds.length == selectedInterests.length){
          buttonName = "حفظ"
        }
      }
      this.setState({isMainInterest: true, buttonName: buttonName})
    }
  }

  itemClicked = (item, index) => {
    if(this.state.isMainInterest) {
      let data = this.props.mainInterests
      data[index].selected = !data[index].selected
      let selectedInterests = this.props.mainInterests.filter((item) => {
        return item.selected == true
      })
      let buttonName = "التالي"
      if(selectedInterests.length == 1) {
        if(!selectedInterests[0].hasChilds) {
          buttonName = "حفظ"
        }
      } else {
        let noChilds = []
        for (var i = 0; i < selectedInterests.length; i++) {
          if(!selectedInterests[i].hasChilds){
            noChilds.push(selectedInterests[i])
          }
        }
        if(noChilds.length == selectedInterests.length){
          buttonName = "حفظ"
        }
      }
      this.setState({refresh: true, buttonName: buttonName})
    } else {
      let subData = this.props.subInterests
      subData[index].selected = !subData[index].selected
      this.setState({refresh: true})
    }
  }

  onScrollFlatList = (e) => {

    this.flatListRef.setNativeProps({
      style: {
        marginBottom: 0,
      },
    });

    let paddingToBottom = this.endReached ? 65 : 0.2;

    if(e.nativeEvent.layoutMeasurement.height + e.nativeEvent.contentOffset.y >= e.nativeEvent.contentSize.height - paddingToBottom) {
      this.endReached = true
      this.flatListRef.setNativeProps({
        style: {
          marginBottom: 65,
        },
      });
    } else {
      this.endReached = false
    }
  }

  confirmVerificationPopup = () => {
    this.confirmVerificationButton = true
    this.setState({errorModal: false})
  }

  closeVerificationPopup = () => {
    this.confirmVerificationButton = false
    this.setState({errorModal: false})
  }

  onErrorModalHide = () => {
    var fromProfile = false
    if(this.props.navigation.state.params !== undefined) {
      fromProfile = this.props.navigation.state.params.fromProfile
    }

    if(this.confirmVerificationButton) {
      if(this.state.requestError) {
        if(this.state.requestError.shouldRetry) {
          this.saveAction()
        } else if(this.state.requestError.shouldLogout) {
          if(fromProfile) {
            this.props.logoutAndDeleteAllData(null)
          } else {
            this.props.backToOnBoardingFromVerification(this.props.navigation)
          }
        }
      } else {
        if(fromProfile) {
          this.props.navigation.goBack()
        }
      }
    }
  }

  setEnableSaveButton = () => {
    if(this.props.mainInterests){
      if(this.state.isMainInterest) {
        let selectedInterests = this.props.mainInterests.filter((item) => {
          return item.selected == true
        })
        let selectedInterestsIds = selectedInterests.map(({id}) => (id));
        if(selectedInterests.length == 1) {
          if(selectedInterests[0].hasChilds){
            return true
          } else {
            return false
          }
        } else {
          let noChilds = []
          for (var i = 0; i < selectedInterests.length; i++) {
            if(!selectedInterests[i].hasChilds){
              noChilds.push(selectedInterests[i])
            }
          }
          if(noChilds.length == selectedInterests.length){
            if(selectedInterests.length >= 3){
              return true
            } else {
              return false
            }
          } else {
            return true
          }
        }
      } else {
        let selectedInterestsIds = this.props.subInterests.filter((item) => {
          return item.selected == true
        }).map(({id}) => (id));
        let mainSelectedInterests = this.props.mainInterests.filter((item) => {
          return item.selected == true
        })
        for (var i = 0; i < mainSelectedInterests.length; i++) {
          if(!mainSelectedInterests[i].hasChilds){
            selectedInterestsIds.push(mainSelectedInterests[i].id)
          }
        }
        if(selectedInterestsIds.length >= 3) {
          return true
        } else {
          return false
        }
      }
    }
    return true
  }

  render() {
    let setEnableSaveButtonState = this.setEnableSaveButton()
    var fromProfile = false
    if(this.props.navigation.state.params !== undefined) {
      fromProfile = this.props.navigation.state.params.fromProfile
    }
    var selectedMainInterestsIds = []
    var selectedSubInterestsIds = []
    let backButton = !this.state.isMainInterest ? <TouchableOpacity style={[styles.closeButton, {alignItems: 'flex-end'}]} onPress={this.backAction}>
                      <Image resizeMode={'contain'} style={styles.closeIcon} source={require('../../Images/close.png')} />
                     </TouchableOpacity> : <View style={{width: 70}} />
    if(fromProfile){
      backButton = <TouchableOpacity style={[styles.closeButton, {alignItems: 'flex-end'}]} onPress={this.backAction}>
                        <Image resizeMode={'contain'} style={styles.closeIcon} source={require('../../Images/close.png')} />
                       </TouchableOpacity>
    }
    if(this.props.mainInterests !== undefined) {
      selectedMainInterestsIds = this.props.mainInterests.filter((item) => {
                                        return item.selected == true
                                  })
    }
    if(this.props.subInterests !== undefined) {
      selectedSubInterestsIds = this.props.subInterests.filter((item) => {
                                        return item.selected == true
                                })
    }

    return (
      <View style={[mainStyles.flex, styles.container]}>
        <CustomStatusBar isDark={!fromProfile} backgroundColor={fromProfile ? colors.fernGreen : colors.white} />
        <View style={[mainStyles.rowFlex, {marginTop: fromProfile ? 20 : null, alignItems: 'center',justifyContent: 'space-between'}]}>
          <View style={{width: 70}} />
          <Text style={[mainStyles.flex, styles.headerText]}>
            الاهتمامات
          </Text>
          {backButton}
        </View>
        <View style={{marginTop: 32, marginLeft: 19, marginRight: 19, marginBottom: 22}}>
          <Text style={styles.descriptionText}>
                        نودّ معرفة المزيد عن مختلف اهتماماتكم في مدة لن تتجاوز الدقيقة لتزويدكم بالمعلومات التي تهمّكم.{"\n"}{"\n"}الرجاء اختيار ٣ اهتمامات على الأقل
          </Text>
        </View>
        <FlatList
          ref={(ref) => { this.flatListRef = ref; }}
          data={this.state.isMainInterest ? this.props.mainInterests : this.props.subInterests}
          extraData={this.state}
          renderItem={({item, index}) =>
            <InterestItem item={item} index={index} totalCount={this.state.isMainInterest ? this.props.mainInterests.length : this.props.subInterests.length} itemClick={(item, index) => this.itemClicked(item, index)} />
          }
          keyExtractor={(item, index) => index.toString()}
          showsVerticalScrollIndicator={false}
          numColumns={2}
          onScroll={(e) => this.onScrollFlatList(e)}
          style={{transform: [{ scaleX: -1 }]}}
        />
        <View style={[mainStyles.rowFlex, styles.footer]}>
          <View style={{width: '100%', height: '100%', alignItems:'center', justifyContent:'center', paddingTop:15, paddingBottom:15, paddingLeft:30, paddingRight: 30}}>
            <OverlayButton
              disabled={!setEnableSaveButtonState}
              buttonColors={[colors.fernGreen, colors.fernGreenTwo]}
              buttonBackgroundColorOpacity={setEnableSaveButtonState ? 1 : 0.3}
              buttonBackgroundColor={colors.fernGreen}
              buttonTextColorOpacity={setEnableSaveButtonState ? 1 : 0.3}
              buttonTextColor={colors.white}
              clickButton={this.saveAction}
              style={mainStyles.flex}
              text={this.state.buttonName}
              customStyle={styles.loginButton}
              textButtonStyle={styles.loginText}
            />
          </View>
        </View>
        <Modal
            transparent={true}
            style={{justifyContent: 'center', backgroundColor: 'transparent'}}
            isVisible={this.state.errorModal}
            animationIn={"bounceInUp"}
            animationOut={"bounceOutDown"}
            animationInTiming={1000}
            animationOutTiming={1000}
            onModalHide={() => this.onErrorModalHide()}
          >
          <ConfirmationPopup
            mainTitle={this.state.requestError ? "عذراً" : "تهانينا"}
            description={this.state.errorMessage}
            oneButton={this.state.requestError ? !this.state.requestError.shouldRetry : true}
            confirmText={this.state.requestError ? this.state.requestError.shouldRetry ? "إعادة" : "إغلاق" : "إغلاق"}
            rejectText={"إغلاق"}
            confirm={this.confirmVerificationPopup}
            cancel={this.closeVerificationPopup} />
        </Modal>
      </View>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps(state) {
  return {
    mainInterests: state.Reducer.mainInterests,
    subInterests: state.Reducer.subInterests,
    user: state.Reducer.user,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Interests);

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.white
  },
  headerText: {
    fontFamily: fonts.Medium,
    fontSize: 20,
    letterSpacing: 0,
    textAlign: "center",
    color: colors.charcoalGreyTwo
  },
  descriptionText: {
    fontFamily: fonts.Medium,
    fontSize: 11,
    lineHeight: 17,
    letterSpacing: 0,
    textAlign: "right",
    color: colors.slate
  },
  footer: {
    position: 'absolute',
    bottom: 0,
    left: 6,
    right: 6,
    height: 65,
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: "rgba(255, 255, 255, 0.85)",
    shadowColor: "rgba(0, 0, 0, 0.21)",
    shadowOffset: {
      width: 7,
      height: -8
    },
    shadowRadius: 26,
    shadowOpacity: 1
  },
  loginButton: {
    height: 35,
    width: '100%',
    borderRadius: 4,
    shadowColor: "rgba(0, 0, 0, 0.21)",
    shadowOffset: {
      width: 7,
      height: -8
    },
    shadowRadius: 26,
    shadowOpacity: 1,
    elevation: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  loginText: {
    fontFamily: fonts.Medium,
    fontSize: 18,
    letterSpacing: 0,
    textAlign: "center",
  },
  closeButton: {
    width:70,
    height:45,
    justifyContent: 'center',
  },
  closeIcon: {
    marginRight: 19,
    width: 14,
    height: 14,
  }
});
