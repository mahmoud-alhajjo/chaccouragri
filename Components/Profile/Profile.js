/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Image,
  Text,
  TouchableOpacity,
  ScrollView,
  ImageBackground,
  Switch,
  PixelRatio
} from 'react-native';

import mainStyles from '../../css/MainStyle';
import colors from '../../css/Colors';
import * as fonts from '../../css/Fonts';
import ConfirmationPopup from '../Popup/ConfirmationPopup'
import * as AppConstants from '../../Config/AppConstants';

import Dimensions from 'Dimensions';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux'
import { ActionCreators } from '../../Actions'
import Modal from "react-native-modal";

let oldDevice = false
if(PixelRatio.get() <= 1.5){
  oldDevice = true
}

export class Profile extends Component {

  logoutFlag = false
  deleteFlag = false

  confirmVerificationButton = false

  constructor(props) {
    super(props);
    this.state = {
      logoutModal: false,
      deleteModal: false,

      errorModal: false,
      errorMessage: "",
      requestError: null,

      notificationSwitch: props.user.notificationEnabled ? (props.user.notificationEnabled != null ? props.user.notificationEnabled : true) : false,
    }
  }

  exitAction = () => {
    this.props.toggleProfileView(false)
  }

  editPorfileAction = () => {
    this.props.navigation.navigate('EditProfile')
  }

  editInterestsAction = () => {
    this.props.navigation.navigate('Interests', {fromProfile: true})
  }

  editPasswordAction = () => {
    this.props.navigation.navigate('EditPassword')
  }

  toggleNotifications = (value) => {
    
    if(value){      
      this.props.registerPushNotification(null)
    } else {
      this.props.unRegisterPushNotification()
    }
    this.setState({notificationSwitch: value})
  }

  privacyAction = () => {
    this.props.navigation.navigate('Privacy')
  }

  logoutAction = () => {
    this.logoutFlag = true
    this.closeLogoutPopup()
  }

  showLogoutPopup = () => {
    this.setState({logoutModal: true})
  }

  closeLogoutPopup = () => {
    this.setState({logoutModal: false})
  }

  deleteAccountAction = () => {
    this.deleteFlag = true
    this.closeDeleteAccountPopup()
  }

  showDeleteAccountPopup = () => {
    this.setState({deleteModal: true})
  }

  closeDeleteAccountPopup = () => {
    this.setState({deleteModal: false})
  }

  onModalHide = (ref) => {
    if(ref == "logout") {
      if(this.logoutFlag) {
        this.logoutFlag = false
        this.exitAction()
        this.props.logoutAndDeleteAllData(null)
      }
    } else if(ref == "delete") {
      if(this.deleteFlag) {
        this.deleteFlag = false
        this.props.deleteAccount(this.onDeleteAccountSucess, this.onDeleteAccountFailure)
      }
    }
  }

  onDeleteAccountSucess = () => {
    this.props.logEvent(AppConstants.DeleteAccount, null)
    this.exitAction()
    this.props.logoutAndDeleteAllData(null)
  }

  onDeleteAccountFailure = (error) => {
    this.setState({errorMessage:error.errorMessage, requestError: error, errorModal: true})
  }

  confirmVerificationPopup = () => {
    this.confirmVerificationButton = true
    this.setState({errorModal: false})
  }

  closeVerificationPopup = () => {
    this.confirmVerificationButton = false
    this.setState({errorModal: false})
  }

  onErrorModalHide = () => {
    if(this.confirmVerificationButton) {
      if(this.state.requestError) {
        if(this.state.requestError.shouldRetry) {
          this.props.deleteAccount(this.onDeleteAccountSucess, this.onDeleteAccountFailure)
        } else if(this.state.requestError.shouldLogout) {
          this.props.logoutAndDeleteAllData(null)
        }
      }
    }
  }

  render() {
    var fullName = ""
    var provinceName = ""
    var profilePic = null
    var shouldEditPassword = true
    if(this.props.user){
      fullName = this.props.user.first_name + " " + this.props.user.last_name
      provinceName = this.props.user.province_name
      if(this.props.user.photo){
        profilePic = {uri: this.props.user.photo}
      }
      if(this.props.user.facebook_id) {
        if(this.props.user.facebook_id != ""){
          shouldEditPassword = false
        }
      }
    }
    return (
      <View style={[mainStyles.flex, styles.container]}>
        <ScrollView style={styles.container} contentContainerStyle={styles.container}>
          <ImageBackground source={require('../../Images/profile-background.jpg')} style={styles.headerView}>
            <View style={[mainStyles.rowFlex, {marginTop: 20, alignItems: 'center',justifyContent: 'space-between'}]}>
              <View style={{width: 70}} />
              <Text style={styles.headerText}>
                الملف الشخصي
              </Text>
              <TouchableOpacity style={[styles.closeButton, {alignItems: 'flex-end'}]} onPress={this.exitAction}>
                <Image resizeMode={'contain'} style={styles.closeIcon} source={require('../../Images/close.png')} />
              </TouchableOpacity>
            </View>
            <View style={[mainStyles.flex, {justifyContent:'center', alignItems:'center'}]}>
              <TouchableOpacity onPress={this.editPorfileAction}>
                <ImageBackground source={require('../../Images/imgPlaceholderBg.png')} style={styles.profilePicView}>
                  <Image style={profilePic ? styles.profilePic : styles.profilePicEmpty} source={profilePic ? profilePic : require('../../Images/imgPlaceholder.png')}/>
                </ImageBackground>
                <Text style={styles.nameText}>
                  {fullName}
                </Text>
                <Text style={styles.provinceText}>
                  {provinceName}
                </Text>
              </TouchableOpacity>
            </View>
            <Image style={{width: '100%', height: 82}} source={require('../../Images/profile-background-bottom.png')}/>
          </ImageBackground>
          <View style={{marginTop: 22}}>
            <TouchableOpacity onPress={this.editPorfileAction} style={[mainStyles.rowFlex, styles.itemView]}>
              <Image resizeMode={'contain'} style={styles.arrowIcon} source={require('../../Images/left-arrow.png')} />
              <Text style={styles.itemText}>
                تعديل المعلومات الشخصية
              </Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={this.editInterestsAction} style={[mainStyles.rowFlex, styles.itemView]}>
              <Image resizeMode={'contain'} style={styles.arrowIcon} source={require('../../Images/left-arrow.png')} />
              <Text style={styles.itemText}>
                تعديل الاهتمامات
              </Text>
            </TouchableOpacity>
            {
              shouldEditPassword ? <TouchableOpacity onPress={this.editPasswordAction} style={[mainStyles.rowFlex, styles.itemView]}>
                                      <Image resizeMode={'contain'} style={styles.arrowIcon} source={require('../../Images/left-arrow.png')} />
                                      <Text style={styles.itemText}>
                                          تعديل كلمة المرور
                                      </Text>
                                    </TouchableOpacity> : null
            }
          </View>
          <View style={{marginTop: 29}}>
            <View style={[mainStyles.rowFlex, styles.itemView]}>
              <Switch style={styles.termsSwitch} value={this.state.notificationSwitch} onValueChange={(value) => this.toggleNotifications(value)} />
              <Text style={styles.itemText}>
                إشعارات
              </Text>
            </View>
          </View>
          <View style={{marginTop: 29}}>
            <TouchableOpacity onPress={this.privacyAction} style={[mainStyles.rowFlex, styles.itemView]}>
              <Image resizeMode={'contain'} style={styles.arrowIcon} source={require('../../Images/left-arrow.png')} />
              <Text style={styles.itemText}>
                سياسة الخصوصية
              </Text>
            </TouchableOpacity>
          </View>
          <View style={{marginTop: 32, marginBottom: 32}}>
            <TouchableOpacity onPress={this.showLogoutPopup} style={[mainStyles.rowFlex, styles.itemView, {justifyContent: 'flex-end'}]}>
              <Text style={styles.itemText}>
                تسجيل خروج
              </Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={this.showDeleteAccountPopup} style={[mainStyles.rowFlex, styles.itemView, {justifyContent: 'flex-end'}]}>
              <Text style={[styles.itemText, {color: colors.scarlet}]}>
                حذف الحساب
              </Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
        <Modal
            transparent={true}
            style={{justifyContent: 'center', backgroundColor: 'transparent'}}
            isVisible={this.state.logoutModal}
            animationIn={"bounceInUp"}
            animationOut={"bounceOutDown"}
            animationInTiming={1000}
            animationOutTiming={1000}
            onModalHide={() => this.onModalHide("logout")}
          >
          <ConfirmationPopup mainTitle={"هل أنت متأكد"} description={"من أنك تريد تسجيل الخروج؟"} warning cancel={this.closeLogoutPopup} confirm={this.logoutAction} />
        </Modal>
        <Modal
            transparent={true}
            style={{justifyContent: 'center', backgroundColor: 'transparent'}}
            isVisible={this.state.deleteModal}
            animationIn={"bounceInUp"}
            animationOut={"bounceOutDown"}
            animationInTiming={1000}
            animationOutTiming={1000}
            onModalHide={() => this.onModalHide("delete")}
          >
          <ConfirmationPopup mainTitle={"هل أنت متأكد"} description={"من أنك تريد حذف الحساب؟"} warning cancel={this.closeDeleteAccountPopup} confirm={this.deleteAccountAction} />
        </Modal>
        <Modal
            transparent={true}
            style={{justifyContent: 'center', backgroundColor: 'transparent'}}
            isVisible={this.state.errorModal}
            animationIn={"bounceInUp"}
            animationOut={"bounceOutDown"}
            animationInTiming={1000}
            animationOutTiming={1000}
            onModalHide={() => this.onErrorModalHide()}
          >
          <ConfirmationPopup
            mainTitle={this.state.requestError ? "عذراً" : "تهانينا"}
            description={this.state.errorMessage}
            oneButton={this.state.requestError ? !this.state.requestError.shouldRetry : true}
            confirmText={this.state.requestError ? this.state.requestError.shouldRetry ? "إعادة" : "إغلاق" : "إغلاق"}
            rejectText={"إغلاق"}
            confirm={this.confirmVerificationPopup}
            cancel={this.closeVerificationPopup} />
        </Modal>
      </View>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps(state) {
  return {
    user: state.Reducer.user,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Profile);

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.whiteTwo
  },
  headerView: {
    width: '100%',
    height: oldDevice ? Dimensions.get('window').height * 0.6 : Dimensions.get('window').height * 0.5,
    justifyContent: 'space-between',
    backgroundColor: colors.slate
  },
  headerText: {
    fontFamily: fonts.Medium,
    fontSize: 20,
    letterSpacing: 0,
    textAlign: "center",
    color: colors.white
  },
  closeButton: {
    width:70,
    height:45,
    justifyContent: 'center',
  },
  closeIcon: {
    marginRight: 19,
    width: 14,
    height: 14,
    tintColor: colors.white,
  },
  profilePic: {
    width: 110,
    height: 110,
    borderRadius: 55,
  },
  profilePicEmpty: {
    width: 45,
    height: 45,
    marginBottom: 8,
  },
  profilePicView: {
    width: 110,
    height: 110,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center'
  },
  nameText: {
    marginTop: 0,
    fontFamily: fonts.Medium,
    fontSize: 24,
    letterSpacing: -0.15,
    textAlign: "center",
    color: colors.whiteTwo
  },
  provinceText: {
    fontFamily: fonts.Light,
    fontSize: 18,
    letterSpacing: -0.11,
    textAlign: "center",
    color: colors.whiteTwo
  },
  itemView: {
    paddingRight: 29,
    paddingLeft: 19,
    paddingTop: 22,
    paddingBottom: 22,
    alignItems: 'center',
    justifyContent: 'space-between',
    borderStyle: "solid",
    borderBottomWidth: 1,
    borderBottomColor: colors.whiteThree,
    backgroundColor: colors.white
  },
  arrowIcon: {
    width: 22,
    height: 22,
  },
  itemText: {
    marginLeft: 10,
    fontFamily: fonts.Light,
    fontSize: 20,
    letterSpacing: -0.13,
    textAlign: "right",
    color: colors.slate
  },
  termsSwitch: {
    width: 51,
    height: 31
  },
});
