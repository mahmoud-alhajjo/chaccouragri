/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  FlatList,
  RefreshControl,
} from 'react-native';

import mainStyles from '../../css/MainStyle';
import colors from '../../css/Colors';
import Header from '../Main Structure/Header'
import LatestNewsItem from './LatestNewsItem'
import ConfirmationPopup from '../Popup/ConfirmationPopup'

import {connect} from 'react-redux';
import { bindActionCreators } from 'redux'
import { ActionCreators } from '../../Actions'
import Modal from "react-native-modal";
import Spinner from 'react-native-spinkit'

export class LatestNews extends Component {

  confirmVerificationButton = false
  getList = true

  constructor(props) {
    super(props);

    this.state = {
      isRefreshing: false,
      getNextData: false,

      errorModal: false,
      errorMessage: "",
      requestError: null,
    }
  }

 

  componentDidUpdate(prevProps)
  {  
    if(this.props.fbPosts !== prevProps.fbPosts) {
      this.setState({isRefreshing: false, getNextData: false})
    }
  }

  componentDidMount() {
    this.getList = true
    this.props.getFacebookPostsCms(null, this.onGetFacebookPostFailure)
  }

  onGetFacebookPostFailure = (error) => {
    this.setState({errorMessage:error.errorMessage, requestError: error, errorModal: true, isRefreshing: false, getNextData: false})
  }

  confirmVerificationPopup = () => {
    this.confirmVerificationButton = true
    this.setState({errorModal: false})
  }

  closeVerificationPopup = () => {
    this.confirmVerificationButton = false
    this.setState({errorModal: false})
  }

  onErrorModalHide = () => {
    if(this.confirmVerificationButton) {
      if(this.getList) {
        this.props.getFacebookPostsCms(null, this.onGetFacebookPostFailure)
      } else {
        this.setState({getNextData: true})
        this.props.getFacebookPostsCms(this.props.fbPosts, this.onGetFacebookPostFailure)
      }
    }
  }

  refreshList = () => {
    this.getList = true
    this.setState({isRefreshing: true})
    this.props.getFacebookPostsCms(null, this.onGetFacebookPostFailure, true)
  }

  loadNextPage = (event) => {
    const scrollHeight = Math.floor( event.nativeEvent.contentOffset.y + event.nativeEvent.layoutMeasurement.height );
    const height = Math.floor( event.nativeEvent.contentSize.height );
    if (scrollHeight >= height) {
      if((this.props.fbPosts.offset) && (!this.state.getNextData)) {
        this.setState({
          getNextData: true
        }, () => {
          this.getList = false
          this.props.getFacebookPostsCms(this.props.fbPosts, this.onGetFacebookPostFailure)
        });
      }
    }
  }

  render() {
    return (
      <View style={[mainStyles.flex, styles.container]}>
        <Header navigation={this.props.navigation} />
        {
          this.props.fbPosts ? <FlatList
                                  ref={(ref) => { this.flatListRef = ref; }}
                                  data={this.props.fbPosts.posts}
                                  extraData={this.props.fbPosts}
                                  renderItem={({item, index}) =>
                                    <LatestNewsItem item={item} index={index} totalCount={this.props.fbPosts.posts.length} />
                                  }
                                  keyExtractor={(item, index) => index.toString()}
                                  showsVerticalScrollIndicator={false}
                                  removeClippedSubviews={true}
                                  onMomentumScrollEnd={this.loadNextPage}
                                  refreshControl={
                                                  ((this.props.fbPosts != null) || (this.state.requestError != null)) ?
                                                  <RefreshControl
                                                      tintColor={colors.fernGreen}
                                                      colors={[colors.fernGreen, colors.fernGreenTwo]}
                                                      refreshing={this.state.isRefreshing}
                                                      onRefresh={this.refreshList}
                                                  /> : null
                                                }
                                /> : null
        }
        <View style={{justifyContent: 'center', alignItems:'center', display: this.state.getNextData ? 'flex' : 'none'}}>
          <Spinner isVisible={this.state.getNextData} size={40} color={colors.fernGreen} type={"ThreeBounce"} />
        </View>
        <Modal
            transparent={true}
            style={{justifyContent: 'center', backgroundColor: 'transparent'}}
            isVisible={this.state.errorModal}
            animationIn={"bounceInUp"}
            animationOut={"bounceOutDown"}
            animationInTiming={1000}
            animationOutTiming={1000}
            onModalHide={() => this.onErrorModalHide()}
          >
          <ConfirmationPopup
            mainTitle={"عذراً"}
            description={this.state.errorMessage}
            confirmText={"إعادة"}
            rejectText={"إغلاق"}
            confirm={this.confirmVerificationPopup}
            cancel={this.closeVerificationPopup} />
        </Modal>
      </View>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps(state) {
  return {
    fbPosts: state.Reducer.fbPosts,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(LatestNews);

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.white
  }
});
