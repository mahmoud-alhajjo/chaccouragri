/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  TouchableOpacity
} from 'react-native';

import mainStyles from '../../css/MainStyle';
import colors from '../../css/Colors';
import * as fonts from '../../css/Fonts';
import ResponsiveImage from '../Helpers/ResponsiveImage'

import Dimensions from 'Dimensions';
import * as Animatable from 'react-native-animatable';
import Moment from 'moment';
import 'moment/locale/ar'
import Communications from 'react-native-communications';

export default class LatestNewsItem extends Component {

  constructor(props) {
    super(props);
    this.state = {
    };
  }

  itemClick = () => {
    if(this.props.item.link) {
      Communications.web(this.props.item.link)
    }
  }

  renderSmallCell = () => {
    let postImageItem = this.props.item.thumbnail ? {uri: this.props.item.thumbnail} : null

    return(
      <Animatable.View animation="fadeInLeftBig" delay={this.props.index * 100} style={{alignItems: 'flex-end'}}>
        <TouchableOpacity style={[mainStyles.rowFlex, styles.container, {marginTop: this.props.index == 0 ? 12 : 4, marginBottom: this.props.index == (this.props.totalCount - 1) ? 12 : 4}]} onPress={this.itemClick}>
          <View style={[mainStyles.flex, {paddingTop:6, paddingBottom:6}]}>
            <Text style={styles.descriptionText}>
              {this.props.item.description}
            </Text>
            <Text style={styles.dateText}>
              {Moment(this.props.item.date).locale('ar').format('YYYY, MMMM DD')}
            </Text>
          </View>
          {
            postImageItem ? <ResponsiveImage style={styles.itemImage} source={postImageItem} /> : null
          }
        </TouchableOpacity>
      </Animatable.View>
    )
  }

  renderBigCell = () => {
    let postImageItem = this.props.item.thumbnail ? {uri: this.props.item.thumbnail} : null

    return(
      <Animatable.View animation="fadeInLeftBig" delay={this.props.index * 100} style={{alignItems: 'flex-end'}}>
        <TouchableOpacity style={[styles.container, {alignItems: null, paddingLeft: 8, marginTop: this.props.index == 0 ? 12 : 4, marginBottom: this.props.index == (this.props.totalCount - 1) ? 12 : 4}]} onPress={this.itemClick}>
          {
            postImageItem ?
            <View style={[mainStyles.rowFlex, styles.bigItemImage]}>
              <ResponsiveImage style={{width: '100%', height: '100%'}} source={postImageItem} />
            </View>
            : null
          }
          <View>
            <Text style={styles.descriptionText}>
              {this.props.item.description}
            </Text>
            <Text style={styles.dateText}>
              {Moment(this.props.item.date).locale('ar').format('YYYY, MMMM DD')}
            </Text>
          </View>
        </TouchableOpacity>
      </Animatable.View>
    )
  }

  render() {
    return (
      this.props.item.cellType == "small" ? this.renderSmallCell() : this.renderBigCell()
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginLeft: 16,
    marginRight: 16,
    paddingTop: 8,
    paddingBottom: 8,
    paddingRight: 8,
    paddingLeft: 13,
    borderRadius: 2,
    backgroundColor: colors.white,
    shadowColor: "rgba(0, 0, 0, 0.05)",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowRadius: 2,
    shadowOpacity: 1,
    borderStyle: "solid",
    borderWidth: 1,
    borderColor: "rgba(71, 78, 86, 0.05)",
    elevation: 2,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  itemImage: {
    marginLeft: 17,
    borderRadius: 2,
    width: Dimensions.get('window').width * 0.256,
    height: Dimensions.get('window').height * 0.127
  },
  bigItemImage: {
    marginBottom: 22,
    borderRadius: 2,
    width: "100%",
    height: Dimensions.get('window').height * 0.215,
    overflow: 'hidden'
  },
  descriptionText: {
    marginBottom: 5,
    fontFamily: fonts.Medium,
    fontSize: 12,
    lineHeight: 16,
    letterSpacing: 0,
    textAlign: "right",
    lineHeight: 22,
    color: colors.greyishBrown
  },
  dateText: {
    opacity: 0.3,
    fontFamily: fonts.Medium,
    fontSize: 10,
    letterSpacing: 0,
    textAlign: "left",
    color: colors.black
  }
});
