/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  Keyboard,
  Image,
  TouchableOpacity,
} from 'react-native';

import mainStyles from '../../css/MainStyle';
import colors from '../../css/Colors';
import Header from '../Main Structure/Header'
import * as fonts from '../../css/Fonts';
import OverlayButton from '../Helpers/OverlayButton'
import SelectionPopup from '../Popup/SelectionPopup'
import * as constants from '../../Config/AppConstants';
import Validator from '../Helpers/Validator'
import ConfirmationPopup from '../Popup/ConfirmationPopup'
import * as AppConstants from '../../Config/AppConstants';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Communications from 'react-native-communications';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux'
import { ActionCreators } from '../../Actions'
import Modal from "react-native-modal";

export class ContactUs extends Component {

  confirmVerificationButton = false
  popupType = null

  nameY = 0
  lastNameY = 0
  emailY = 0
  mobileY = 0
  subjectY = 0
  messageY = 0

  constructor(props) {
    super(props);
    this.state = {
      selectionModal: false,

      firstName: props.user.first_name,
      lastName: props.user.last_name,
      email: null,
      mobile: props.user.phone_number,
      subject: null,
      message: null,

      nameError:"",
      lastNameError:"",
      emailError:"",
      mobileError:"",
      subjectError:"",
      messageError:"",

      errorModal: false,
      errorMessage: "",
      requestError: null,
    }
  }

  componentDidMount () {
    this.keyboardDidHideSub = Keyboard.addListener('keyboardDidHide', this.keyboardDidHide);
    this.props.getUserFromLocal()
    this.props.getSalesman()
  }

  componentWillUnmount() {
    this.keyboardDidHideSub.remove();
  }

  keyboardDidHide = (event) => {
    this.blurTextFields()
  };

  dismissKeyboard = () => {
    Keyboard.dismiss
    this.blurTextFields()
  }

  blurTextFields = () => {
    this.refs.lastNameInput.blur()
    this.refs.firstNameInput.blur()
    this.refs.mobileInput.blur()
    this.refs.emailInput.blur()
    this.refs.messageInput.blur()
  }

  sendAction = () => {
    let nameError = Validator.getNameError(this.state.firstName)
    let lastNameError = Validator.getLastNameError(this.state.lastName)
    let emailError = Validator.getEmailError(this.state.email)
    let mobileError = Validator.getMobileError(this.state.mobile)
    let subjectError = null
    if(this.state.subject == null) {
      subjectError = "الرجاء اختيار موضوع"
    }
    let messageError = null
    if(this.state.message == null) {
      messageError = "الرجاء كتابة رسالة إلى الشركة"
    }

    this.setState({
      nameError:nameError,
      lastNameError:lastNameError,
      emailError:emailError,
      mobileError:mobileError,
      subjectError:subjectError,
      messageError: messageError
    }, this.scrollToFirstErrorField)

    if ((nameError == null) && (lastNameError == null) && (emailError == null) && (mobileError == null) && (subjectError == null) && (messageError == null)) {
      this.props.sendContactUs(this.state.firstName, this.state.lastName, this.state.email, this.state.mobile, this.state.subject.id, this.state.message,this.onSendContactUsSucess, this.onSendContactUsFailure)
      this.dismissKeyboard()
    }
  }

  onSendContactUsSucess = () => {
    let message = "لقد تم ارسال طلبك بنجاح."
    this.setState({errorMessage:message, requestError: null, errorModal: true})
  }

  onSendContactUsFailure = (error) => {
    this.setState({errorMessage:error.errorMessage, requestError: error, errorModal: true})
  }

  scrollToFirstErrorField = () => {
    if(this.state.nameError != null) {
      this.scroll.props.scrollToPosition(0, this.nameY - 10)
      this.refs.firstNameInput.focus()
    } else if(this.state.lastNameError != null) {
      this.scroll.props.scrollToPosition(0, this.lastNameY - 10)
      this.refs.lastNameInput.focus()
    } else if(this.state.emailError != null) {
      this.scroll.props.scrollToPosition(0, this.emailY - 10)
      this.refs.emailInput.focus()
    } else if(this.state.mobileError != null) {
      this.scroll.props.scrollToPosition(0, this.mobileY - 10)
      this.refs.mobileInput.focus()
    } else if(this.state.subjectError != null) {
      this.scroll.props.scrollToPosition(0, this.subjectY - 10)
    } else if(this.state.messageError != null) {
      this.refs.messageInput.focus()
      this.scroll.props.scrollToPosition(0, this.messageY - 10)
    }
  }

  onGetSubjectsList = () => {
    this.popupType = constants.ContactUsSubjects
    this.setState({selectionModal: true})
  }

  closeSelectionPopupAction = () => {
    this.setState({selectionModal: false})
  }

  handlePopupSelection = (selectedItem, popupType) => {
    if(popupType == constants.ContactUsSubjects) {
      this.setState({subject:selectedItem, subjectError:"", selectionModal: false})
    }
  }

  getListForSelectionPopup = () => {
    let popupType = this.popupType
    if(popupType == constants.ContactUsSubjects){
      return this.props.subjects
    }
    return null
  }

  getSelectedItemSelectionPopup = () => {
    let popupType = this.popupType
    if(popupType == constants.ContactUsSubjects){
      return this.state.subject
    }
    return null
  }

  onSelectionModalHide = () => {
    this.popupType = null
  }

  emailAction = () => {
    var subject = null
    var body = null

    if(this.state.subject) {
      subject = this.state.subject.name
    }
    if((this.state.message != null) && (this.state.message != "")) {
      body = this.state.message
    }
    Communications.email(['contact@chaccour-agri.com'],null,null,subject,body)
  }

  callAction = () => {
    Communications.phonecall('00963115424444', true)
  }

  callSalesmanAction = (item) => {
    this.props.salesmanCallLog(item.id)
    Communications.phonecall(item.phone, true)
  }

  handleSocialMedia = (item) => {
    var socialLink = null
    switch (item) {
      case "fb":
        this.props.logEvent(AppConstants.SocialMedia, {type: 'Facebook'})
        socialLink = "https://www.facebook.com/chaccour.agriculturalcompany"
        break;
      case "insta":
        this.props.logEvent(AppConstants.SocialMedia, {type: 'Instagram'})
        socialLink = "https://www.instagram.com/chaccouragri/"
        break;
      case "web":
        this.props.logEvent(AppConstants.SocialMedia, {type: 'Main Website'})
        socialLink = "http://chaccour-agri.com/"
        break;
      default:
        break;
    }
    if(socialLink){
      Communications.web(socialLink)
    }
  }

  renderSelsmanView = () => {
    var viewToRender = null
    if(this.props.salesman) {
      viewToRender = this.props.salesman.map((item) => {
                return (
                  <TouchableOpacity onPress={() => this.callSalesmanAction(item)} key={item.id} style={[mainStyles.rowFlex, {alignItems: 'center', marginBottom: 15}]}>
                    <Image resizeMode={'contain'} style={styles.callIcon} source={require('../../Images/phone-call.png')} />
                    <Text style={[styles.locationInfoViewText, {fontSize: 12, marginBottom: 2}]}>
                      {item.title}
                    </Text>
                  </TouchableOpacity>
                )
              });
    }
    return viewToRender
  }

  handleChangeText = (event, field) => {
    if(field == 'firstName') {
      this.setState({firstName: event})
    } else if(field == 'lastName') {
      this.setState({lastName: event})
    }  else if(field == 'email') {
      this.setState({email: event})
    } else if(field == 'mobile') {
      this.setState({mobile: event})
    } else if(field == 'message') {
      this.setState({message: event})
    }
  }

  renderTextError = (error) => {
    return(
      <Text style={styles.errorText}>
        {error}
      </Text>
    )
  }

  confirmVerificationPopup = () => {
    this.confirmVerificationButton = true
    this.setState({errorModal: false})
  }

  closeVerificationPopup = () => {
    this.confirmVerificationButton = false
    this.setState({errorModal: false})
  }

  onErrorModalHide = () => {
    if(this.confirmVerificationButton) {
      if(this.state.requestError) {
        if(this.state.requestError.shouldRetry) {
          this.props.sendContactUs(this.state.firstName, this.state.lastName, this.state.email, this.state.mobile, this.state.subject.id, this.state.message,this.onSendContactUsSucess, this.onSendContactUsFailure)
        } else if(this.state.requestError.shouldLogout) {
          this.props.logoutAndDeleteAllData(this.props.navigation)
        }
      } else {
        this.props.navigation.goBack()
      }
    }
  }

  render() {
    return (
      <View style={[mainStyles.flex, styles.container]}>
        <Header navigation={this.props.navigation} />
        <KeyboardAwareScrollView innerRef={ref => {this.scroll = ref}} style={{backgroundColor: colors.white}} contentContainerStyle={{backgroundColor: colors.white}} bounces={false} enableResetScrollToCoords={true} scrollEnabled={true} enableOnAndroid={true}>
          <View style={styles.infoView}>
            <Text style={[styles.locationInfoViewText, {fontSize: 13.5, marginBottom: 7}]}>
              شارع القصاع - الصوفانية ٢
            </Text>
            <Text style={[styles.locationInfoViewText, {fontSize: 13.5, marginBottom: 7}]}>
              ص. ب. 2175 - دمشق - سوريا
            </Text>
            <Text style={[styles.contactInfoViewText, {marginTop: 3, marginBottom: 7, fontSize: 13.5}]} onPress={this.emailAction}>
              contact@chaccour-agri.com
            </Text>
            <Text style={[styles.contactInfoViewText, {fontSize: 13.5}]} onPress={this.callAction}>
              00963 11 5424444
            </Text>
          </View>
          <View style={[mainStyles.rowFlex, styles.socialMediaView]}>
            <TouchableOpacity onPress={() => this.handleSocialMedia("fb")}>
              <Image resizeMode={'contain'} style={styles.socailIcon} source={require('../../Images/icFacebook.png')}></Image>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.handleSocialMedia("insta")}>
              <Image resizeMode={'contain'} style={styles.socailIcon} source={require('../../Images/insragram.png')}></Image>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.handleSocialMedia("web")}>
              <Image resizeMode={'contain'} style={[styles.socailIcon, {marginRight: null, tintColor: colors.fernGreen}]} source={require('../../Images/world.png')}></Image>
            </TouchableOpacity>
          </View>
          {
            this.props.salesman ? this.props.salesman.length > 0 ? <View style={styles.salesmanView}>
                        <Text style={[styles.salesmanText, {fontSize: 20, marginBottom: 10, lineHeight: 22}]}>
                          اتصل بمهندس الشركة في منطقتك
                        </Text>
                        <Text style={[styles.salesmanText, {fontSize: 16, marginBottom: 10}]}>
                          {this.props.user.province_name}
                        </Text>
                        {this.renderSelsmanView()}
                      </View> : null : null
          }
          <View style={{marginBottom: 21, paddingLeft:5, paddingRight: 5}}>
            <Text style={styles.callUsText}>
              تواصل معنا
            </Text>
          </View>
          <View style={styles.inputView}>
            <View style={[mainStyles.rowFlex, styles.mainInputField]}>
              <View style={[styles.doubleTextInputView, {paddingRight: 16, borderStyle: "solid", borderRightWidth: 1, borderRightColor: colors.silver}]}
                    onLayout={event => {
                      const layout = event.nativeEvent.layout;
                      this.lastNameY = layout.y
                    }}
              >
                <TextInput
                  ref="lastNameInput"
                  placeholder={"اسم العائلة"}
                  underlineColorAndroid ={"transparent"}
                  placeholderTextColor={colors.slate}
                  returnKeyType={"next"}
                  autoCapitalize={"words"}
                  autoCorrect={false}
                  maxLength={50}
                  blurOnSubmit={false}
                  selectionColor={colors.slate}
                  style={[mainStyles.flex, styles.textInput]}
                  onChangeText={((event) => this.handleChangeText(event, 'lastName'))}
                  value={this.state.lastName ? this.state.lastName : ""}
                  onSubmitEditing={(event) => this.refs.emailInput.focus()}
                />
                {(this.state.lastNameError == "" || this.state.lastNameError == null) ? null : this.renderTextError(this.state.lastNameError)}
              </View>
              <View style={[styles.doubleTextInputView, {paddingLeft: 16}]}
                    onLayout={event => {
                      const layout = event.nativeEvent.layout;
                      this.nameY = layout.y
                    }}
              >
                <TextInput
                  ref="firstNameInput"
                  placeholder={"الاسم"}
                  underlineColorAndroid ={"transparent"}
                  placeholderTextColor={colors.slate}
                  returnKeyType={"next"}
                  autoCapitalize={"words"}
                  autoCorrect={false}
                  maxLength={50}
                  blurOnSubmit={false}
                  selectionColor={colors.slate}
                  style={[mainStyles.flex, styles.textInput]}
                  onChangeText={((event) => this.handleChangeText(event, 'firstName'))}
                  value={this.state.firstName ? this.state.firstName : ""}
                  onSubmitEditing={(event) => this.refs.lastNameInput.focus()}
                />
                {(this.state.nameError == "" || this.state.nameError == null) ? null : this.renderTextError(this.state.nameError)}
              </View>
            </View>
            <View style={[mainStyles.rowFlex, styles.mainInputField]}>
              <View style={styles.singleTextInputView}
                    onLayout={event => {
                      const layout = event.nativeEvent.layout;
                      this.emailY = layout.y
                    }}
              >
                <TextInput
                  ref="emailInput"
                  placeholder={"البريد الالكتروني"}
                  underlineColorAndroid ={"transparent"}
                  placeholderTextColor={colors.slate}
                  returnKeyType={"next"}
                  autoCapitalize={"none"}
                  maxLength={60}
                  blurOnSubmit={false}
                  keyboardType={"email-address"}
                  selectionColor={colors.slate}
                  style={[mainStyles.flex, styles.textInput]}
                  onChangeText={((event) => this.handleChangeText(event, 'email'))}
                  value={this.state.email ? this.state.email : ""}
                  onSubmitEditing={(event) => this.refs.mobileInput.focus()}
                />
                {(this.state.emailError == "" || this.state.emailError == null) ? null : this.renderTextError(this.state.emailError)}
              </View>
            </View>
            <View style={[mainStyles.rowFlex, styles.mainInputField]}>
              <View style={styles.singleTextInputView}
                    onLayout={event => {
                      const layout = event.nativeEvent.layout;
                      this.mobileY = layout.y
                    }}
              >
                <TextInput
                  ref="mobileInput"
                  placeholder={"رقم الجوال"}
                  underlineColorAndroid ={"transparent"}
                  placeholderTextColor={colors.slate}
                  returnKeyType={"next"}
                  maxLength={10}
                  blurOnSubmit={false}
                  keyboardType={"phone-pad"}
                  selectionColor={colors.slate}
                  style={[mainStyles.flex, styles.textInput]}
                  onChangeText={((event) => this.handleChangeText(event, 'mobile'))}
                  value={this.state.mobile ? this.state.mobile : ""}
                  onSubmitEditing={(event) => this.dismissKeyboard()}
                />
                {(this.state.mobileError == "" || this.state.mobileError == null) ? null : this.renderTextError(this.state.mobileError)}
              </View>
            </View>
            <View style={[styles.mainInputField, {alignItems: null}]}>
              <View style={[mainStyles.flex, mainStyles.rowFlex, {alignItems:'center'}]}
                    onLayout={event => {
                      const layout = event.nativeEvent.layout;
                      this.subjectY = layout.y
                    }}
              >
                <Image style={styles.downArrow} source={require('../../Images/downArrow.png')}/>
                <View style={[styles.singleTextInputView, {marginLeft: 16}]}>
                  <TextInput
                    ref="subjectInput"
                    placeholder={"الموضوع"}
                    underlineColorAndroid ={"transparent"}
                    placeholderTextColor={colors.slate}
                    blurOnSubmit={false}
                    selectionColor={colors.slate}
                    style={[mainStyles.flex, styles.textInput]}
                    editable={false}
                    value={this.state.subject ? this.state.subject.name : ""}
                    multiline={true}
                  />
                </View>
              </View>
              {(this.state.subjectError == "" || this.state.subjectError == null) ? null : this.renderTextError(this.state.subjectError)}
              <TouchableOpacity onPress={() => this.props.getContactUsSubjects(this.onGetSubjectsList)} style={{position: 'absolute', top: 0, left: 0, right: 0, bottom: 0}}/>
            </View>
            <View style={styles.mainInputFieldNoBorder}>
              <View style={styles.singleTextInputView}
                    onLayout={event => {
                      const layout = event.nativeEvent.layout;
                      this.messageY = layout.y
                    }}
              >
                <TextInput
                  ref="messageInput"
                  placeholder={"رسالة إلى الشركة"}
                  textAlignVertical={'top'}
                  underlineColorAndroid ={"transparent"}
                  placeholderTextColor={colors.slate}
                  multiline={true}
                  numberOfLines={4}
                  autoCapitalize={"none"}
                  autoCorrect={false}
                  blurOnSubmit={false}
                  selectionColor={colors.slate}
                  style={[mainStyles.flex, styles.textInput]}
                  onChangeText={((event) => this.handleChangeText(event, 'message'))}
                  value={this.state.message ? this.state.message : ""}
                />
                {(this.state.messageError == "" || this.state.messageError == null) ? null : this.renderTextError(this.state.messageError)}
              </View>
            </View>
          </View>
          <View style={{marginLeft: 36, marginRight: 36, marginTop: 25, marginBottom: 40}}>
            <OverlayButton buttonColors={[colors.fernGreen, colors.fernGreenTwo]} buttonBackgroundColor={colors.fernGreen} buttonTextColor={colors.white} clickButton={this.sendAction} style={mainStyles.flex} text={"ارسال"} customStyle={styles.sendButton} textButtonStyle={styles.textButton} />
          </View>
        </KeyboardAwareScrollView>
        <Modal
            transparent={true}
            onBackButtonPress={() => this.closeSelectionPopupAction()}
            onBackdropPress={() => this.closeSelectionPopupAction()}
            style={{justifyContent: 'center', alignItems: 'center', backgroundColor: 'transparent'}}
            isVisible={this.state.selectionModal}
            animationIn={"bounceInUp"}
            animationOut={"bounceOutDown"}
            animationInTiming={1000}
            animationOutTiming={1000}
            onModalHide={() => this.onSelectionModalHide()}
          >
          <SelectionPopup popupType={this.popupType} selectedItem={this.getSelectedItemSelectionPopup()} list={this.getListForSelectionPopup()} cancel={this.closeSelectionPopupAction} confirm={(item) => this.handlePopupSelection(item, this.popupType)} />
        </Modal>
        <Modal
            transparent={true}
            style={{justifyContent: 'center', backgroundColor: 'transparent'}}
            isVisible={this.state.errorModal}
            animationIn={"bounceInUp"}
            animationOut={"bounceOutDown"}
            animationInTiming={1000}
            animationOutTiming={1000}
            onModalHide={() => this.onErrorModalHide()}
          >
          <ConfirmationPopup
            mainTitle={this.state.requestError ? "عذراً" : "تهانينا"}
            description={this.state.errorMessage}
            oneButton={this.state.requestError ? !this.state.requestError.shouldRetry : true}
            confirmText={this.state.requestError ? this.state.requestError.shouldRetry ? "إعادة" : "إغلاق" : "إغلاق"}
            rejectText={"إغلاق"}
            confirm={this.confirmVerificationPopup}
            cancel={this.closeVerificationPopup} />
        </Modal>
      </View>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps(state) {
  return {
    subjects: state.Reducer.subjects,
    salesman: state.Reducer.salesman,
    user: state.Reducer.user
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ContactUs);


const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.white
  },
  inputView: {
    marginLeft: 36,
    marginRight: 36,
    borderRadius: 4,
    borderStyle: "solid",
    borderWidth: 1,
    borderColor: colors.silverTwo
  },
  textInput: {
    paddingTop: 0,
    paddingBottom: 0,
    fontFamily: fonts.Light,
    fontSize: 15,
    fontWeight: "300",
    letterSpacing: 0,
    textAlign: "right",
    color: colors.slate
  },
  downArrow: {
    width: 12,
    height: 6
  },
  mainInputField: {
    borderStyle: "solid",
    borderBottomWidth: 1,
    borderBottomColor: colors.silver,
    minHeight: 67.5,
    justifyContent:'space-between',
    alignItems: 'center',
    paddingLeft: 16,
    paddingRight: 16,
  },
  mainInputFieldNoBorder: {
    height: 151,
    paddingTop: 18,
    paddingBottom: 18,
    paddingLeft: 16,
    paddingRight: 16,
  },
  doubleTextInputView: {
    height: '100%',
    width:'50%',
  },
  singleTextInputView: {
    height: '100%',
    flex: 1,
  },
  textButton: {
    fontFamily: fonts.Medium,
    fontSize: 18,
    letterSpacing: 0,
    textAlign: "center",
  },
  sendButton: {
    height: 65,
    borderRadius: 4,
    shadowColor: "rgba(0, 0, 0, 0.21)",
    shadowOffset: {
      width: 7,
      height: -8
    },
    shadowRadius: 26,
    shadowOpacity: 1,
    elevation: 3,
  },
  infoView: {
    marginTop: 25,
    marginLeft: 20,
    marginRight: 20,
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 5,
    paddingRight: 5,
  },
  locationInfoViewText: {
    fontFamily: fonts.Medium,
    fontSize: 11,
    lineHeight: 17,
    letterSpacing: 0,
    textAlign: "center",
    color: colors.slate
  },
  contactInfoViewText: {
    fontFamily: "LucidaGrande",
    fontSize: 11,
    fontWeight: "bold",
    fontStyle: "normal",
    lineHeight: 17,
    letterSpacing: 0,
    textAlign: "center",
    color: colors.fernGreen
  },
  socialMediaView: {
    marginTop: 13,
    marginBottom: 30,
    alignItems: 'center',
    justifyContent: 'center'
  },
  socailIcon: {
    marginRight: 13.5,
    width: 29.7,
    height: 29.7,
  },
  salesmanView: {
    marginLeft: 20,
    marginRight: 20,
    marginBottom: 10,
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 5,
    paddingRight: 5,
  },
  salesmanText: {
    fontFamily: fonts.Medium,
    letterSpacing: 0,
    textAlign: "center",
    color: colors.charcoalGreyTwo
  },
  callUsText: {
    fontFamily: fonts.Medium,
    fontSize: 20,
    letterSpacing: 0,
    textAlign: "center",
    color: colors.charcoalGreyTwo
  },
  errorText: {
    fontFamily: fonts.Light,
    fontSize: 12,
    letterSpacing: 0,
    textAlign: "right",
    color: colors.darkishRed,
    marginBottom: 6,
  },
  callIcon: {
    marginRight: 10,
    width: 20,
    height: 20,
    alignSelf: 'center',
    tintColor: colors.fernGreen,
  }
});
