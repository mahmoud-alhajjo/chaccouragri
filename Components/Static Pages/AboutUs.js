/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  TouchableOpacity,
  ImageBackground,
  ScrollView,
  PixelRatio
} from 'react-native';

import mainStyles from '../../css/MainStyle';
import colors from '../../css/Colors';
import * as fonts from '../../css/Fonts';

import Swiper from 'react-native-swiper';
import Dimensions from 'Dimensions';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux'
import { ActionCreators } from '../../Actions'

export class AboutUs extends Component {

  data = [
    {title:"تاريخ الشركة", description:"تعود أصول شركة شكور الرزاعية إلى عام ١٩١٩، وهي السنة التي أسست فيها شركة عائلية متخصصة لتجارة السلع الصناعية والزراعية المتنوعة في دمشق باسم شركة شكور إخوان من قبل الأخوين خليل و إيميل شكور.\n\nتعهدت شركة شكور إخوان بعمل تصدير السلع المنتجة محلياً مثل بذور االنباتات الطبيعية والعطرية وقمر الدين لأوروبا واستيراد المواد الكيميائية الزراعية العامة. ومن بين هذه المنتجات المبيدات الفطرية المستخدمة لمعالجة البذور هيكزاكلوروبنزين ، مبيد آفات التربة بنزينهيكزاكلورايد، زهر الكبريت التعفيري وخلطة بوردو المستخدمان بمثابة مبيدات فطرية واسعة الطيف.", image:require('../../Images/about1.jpg')},
    {title:"تاريخ الشركة", description:"ومنذ ذلك الحين اشتهرت شركة شكور إخوان كشركة زراعية رائدة في سورية.\nبمنتصف عقد السبعينات، وبما أن التركيبة السكانية السورية توسعت بالإضافة إلى تطبيق سياسات الاكتفاء الذاتي من قبل الدولة، فقد شهد القطاع الزراعي السوري زيادة في الاستثمارات وتطبيق التقنيات الحديثة. وقد ازداد طلب السوق الزراعي السوري على الأسمدة ومبيدات الآفات الزراعية التخصصية الأكثر فعالية بشكل ملحوظ.\n\nوبناء على ذلك فقد بدأت شركة شكور إخوان في عام ١٩٧٤ استيراد وتوزيع مبيدات الآفات الزراعية والأسمدة ذات الجودة العالية بشكل أوسع. إن نجاح هذا المشروع أدى إلى تأسيس شركة شكور الزراعية في عام ١٩٧٨ من قبل الجيل الثاني من العائلة المهندس الزراعي نقولا شكور.", image:require('../../Images/about2.jpg')},
    {title:"تعريف عن شركتنا", description:"منذ تأسيسها، بقيت شركة شكور الزراعية شركة عائلية مستقلة والتي يتم إدارتها من قبل مؤسسها المهندس نقولا شكور وقد وسعت نشاطاتها من خلال أفراد الجيل الثالث للعائلة. إننا نمثل شركات كبرى، ونقدم تشكيلة واسعة من مبيدات الآفات، والأسمدة الزراعية، وبذور الخضروات الهجينة. وقد تمكنا من تغطية السوق الزراعي في سورية عن طريق شبكة توزيع واسعة وفريق تقني قوي مما يجعلنا شركة زراعية سورية رائدة.", image:require('../../Images/about3.jpg')},
    {title:"رؤيتنا", description:"نعتقد بأن تفهم التحديات المتنوعة التي يمر فيها القطاع الزراعي في سورية هو بمثابة حاجة عاجلة وملحة لا تحتمل التأجيل. التغيير المناخي ونقص المياه وملوحة التربة والآفات والأمراض التي تنتقل عن طريق التربة ومقاومة الآفات للمبيدات الزراعية المتزايدة وآثار المبيدات المتبقية في المحاصيل التي تم حصادها وأمان وجودة الغذاء جميعها تشكل قضايا جادة وحقيقية يجب أخذها بعين الاعتبار.\n\nوكانت شركة شكور الزراعية وما زالت تسعى لتقديم منتجات ذات جودة وفعالية عالية مع متابعة تقنية احترافية للصيدليات الزراعية والمزارعين، وكوننا شركة ذات معرفة ووعي بيئي واجتماعي، فإننا نسعى باستمرار لعرض المنتجات الآمنة للبيئة والمستهلك من أجل بناء قاعدة صلبة لزراعة سورية مستدامة في المستقبل.", image:require('../../Images/about4.jpg')}
  ].reverse()

  constructor() {
    super();
    this.state = {
    };
  }

  renderPagination = (index, total) => {
    if(total === 1) {
      return null
    }
    var dots = []
    for (var i = 0; i < total; i++) {
      dots.push(<View key={i} style={{width: 10, height: 10, backgroundColor: colors.white, borderRadius: 5, opacity: index == i ? 1 : 0.5, marginRight: (i == (total - 1)) ? null : 20 }} />)
    }
    return(
      <View style={styles.dotsView}>
        <View style={mainStyles.rowFlex}>
          {dots}
        </View>
      </View>
    )
  }

  backAction = () => {
    this.props.toggleAboutUsView(false)
  }

  render() {

    let oldDevice = false
    if(PixelRatio.get() <= 1.5){
      oldDevice = true
    }

    let swiperItems = this.data.map((item, index) => {
            return oldDevice ? <ImageBackground key={index.toString()} source={item.image} style={[mainStyles.flex, {padding: 39, paddingTop: 90, paddingBottom: 70, justifyContent: 'center'}]}>
                           <ScrollView showsVerticalScrollIndicator={false}>
                            <Text style={styles.titleText}>
                              {item.title}
                            </Text>
                            <Text style={styles.descriptionText}>
                              {item.description}
                            </Text>
                          </ScrollView>
                        </ImageBackground> : <ImageBackground key={index.toString()} source={item.image} style={[mainStyles.flex, {padding: 39, justifyContent: 'center'}]}>
                                              <Text style={styles.titleText}>
                                                {item.title}
                                              </Text>
                                              <Text style={styles.descriptionText}>
                                                {item.description}
                                              </Text>
                                            </ImageBackground>
          });

    return (
      <View style={[mainStyles.flex, styles.container]}>
        <View style={styles.headerView}>
          <TouchableOpacity style={[styles.closeButton, {alignItems: 'flex-end'}]} onPress={this.backAction}>
            <Image resizeMode={'contain'} style={styles.closeIcon} source={require('../../Images/close.png')} />
          </TouchableOpacity>
        </View>
        <View style={mainStyles.flex}>
          <Swiper index={this.data.length - 1} autoplay={false} showsButtons={false} showsPagination={true} loop={false} scrollEnabled={true} renderPagination={(index, total, context) => this.renderPagination(index, total)}>
            {swiperItems}
          </Swiper>
        </View>
      </View>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps(state) {
  return {
    isAboutUsOpened: state.Reducer.isAboutUsOpened
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(AboutUs);

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'transparent'
  },
  headerView: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    zIndex: 1000,
    alignItems: 'flex-end',
  },
  closeButton: {
    width:70,
    height:70,
    justifyContent: 'flex-start',
  },
  closeIcon: {
    marginTop: 44,
    marginRight: 19,
    width: 14,
    height: 14,
    tintColor: colors.white
  },
  dotsView: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    height: 10,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 34,
    zIndex: 1000,
  },
  titleText: {
    marginBottom: 24,
    fontFamily: fonts.Bold,
    fontSize: 30,
    letterSpacing: 0,
    textAlign: "right",
    color: colors.white
  },
  descriptionText: {
    fontFamily: fonts.Medium,
    fontSize: 14,
    letterSpacing: 0,
    textAlign: "right",
    lineHeight: 22,
    color: colors.white
  },
});
